import { $Class } from "./yung-JS-extend";

/**
 * Calendar tool, like JAVA calendar
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Calendar extends $Class {
    
	classProp: {
        name : "com.yung.util.Calendar",
        unimplemented: []
    }
    
    /**
     * Date object
     * @member {jsDate}
     * @instance
     * @memberof com_yung_util_Calendar
     */
    d : Date
    
    /**
	 * constructor
     * @memberof com_yung_util_Calendar
     * @param  {number} arg1 - year
     * @param  {number} arg2 - month
     * @param  {number} arg3 - day of month
     * @param  {number} arg4 - hour(24hr)
     * @param  {number} arg5 - minute
     * @param  {number} arg6 - second
     * @param  {number} arg7 - millisecond
     */
    constructor (arg1: any, arg2?: number, arg3?: number, arg4?: number, arg5?: number, arg6?: number, arg7?: number)
    
    /** 
     * get year
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} year number
     */
    getYear (): number
    
    /** 
     * get month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} month number
     */
    getMonth (): number
    
    /** 
     * get day of month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} day of month number
     */
    getDate (): number
    
    /** 
     * get hour(24hr)
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} hour(24hr) number
     */
    getHours (): number
    
    /** 
     * get minute
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} minute number
     */
    getMinutes (): number
    
    /** 
     * get second
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} second number
     */
    getSeconds (): number
    
    /** 
     * get millisecond
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} millisecond number
     */
    getMilliseconds (): number
    
    /** 
     * get day of week
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} day of week number
     */
    getDay (): number
    
    /** 
     * set year
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} year - year number
     */
    setYear (year: number): void
    
    /** 
     * set month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} month - month number
     */
    setMonth (month: number): void
    
    /** 
     * set day of month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} day - day of month number
     */
    setDate (day: number): void
    
    /** 
     * set hours
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} hours - hours number
     */
    setHours (hours: number): void
    
    /** 
     * set minute
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} min - minute number
     */
    setMinutes (min: number): void
    
    /** 
     * set second
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} sec - second number
     */
    setSeconds (sec: number): void
    
    /** 
     * set millisecond
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} milli - millisecond number
     */
    setMilliseconds (milli: number): void

    /** 
     * add year to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} year - year number
     */
    addYear (year: number): void
    
    /** 
     * add month to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} month - month number
     */
    addMonth (month: number): void
    
    /** 
     * add day to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} day - day number
     */
    addDay (day: number): void
    
    /** 
     * add hour to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} hours - hours number
     */
    addHours (hours: number): void
    
    /** 
     * add minute to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} min - minute number
     */
    addMinutes (min: number): void
    
    /** 
     * add second to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} sec - second number
     */
    addSeconds (sec: number): void
    
    /** 
     * add millisecond to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} milli - millisecond number
     */
    addMilliseconds (milli: number): void
    
    /** 
     * get unix time
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} unit time number
     */
    getTime (): number
    
    /** 
     * set unix time
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} time - unit time number
     */
    setTime (time: number): void
    
    /** 
     * convert to jsDate
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {Date} jsDate
     */
    toDate (): Date
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     */
    toString (): string
}

/**
 * Date format tool, like JAVA SimpleDateFormat
 * @property {object}  classProp - class property
 * @property {string}  classProp.year - year pattern
 * @property {string}  classProp.month - month pattern
 * @property {string}  classProp.date - day pattern
 * @property {string}  classProp.hour - hour pattern
 * @property {string}  classProp.min - minute pattern
 * @property {string}  classProp.sec - second pattern
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_SimpleDateFormat extends $Class {
    
	classProp : { 
        name : "com.yung.util.SimpleDateFormat",
        unimplemented: [],
    	year : "yyyy", 
    	month : "MM", 
    	date : "dd", 
    	hour : "HH", 
    	min : "mm", 
    	sec : "ss",
    	milli : "SSS"
    }
    
    /**
     * date pattern
     * @member {string}
     * @instance
     * @memberof com_yung_util_SimpleDateFormat
     */
    dateFormat : string
    
    /**
	 * constructor
	 * 
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} arg1 - date format pattern
     */
    constructor(arg1: string)
    
    /**
	 * use date pattern to parse string to get com_yung_util_Calendar instance
 	 * 
	 * @instance
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} dateString - date string
     * @return {com_yung_util_Calendar} calendar
     */
    parse (dateString: string): com_yung_util_Calendar
    
    /**
	 * padding zero to string
	 * 
	 * @private
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} str - target string
     * @param  {number} total - total length of result string
     * @return {string} result string
     */
    paddingZero (str: string, total: number): string
    
    /**
	 * use date pattern to format calendar to date string
	 * 
	 * @instance
	 * @memberof com_yung_util_SimpleDateFormat
     * @param  {com_yung_util_Calendar | jsDate} date - calendar
     * @return {string} date string
     */
    format (date: any): string
    
    /**
	 * use date pattern to check date string if valid
	 * 
	 * @instance
	 * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} dateString - date string
     * @return {boolean} valid or not
     */
    validate (dateString: string): boolean
}