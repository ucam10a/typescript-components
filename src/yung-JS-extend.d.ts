import * as jQuery from "jquery";

/**
 * global variable object
 */
export declare var yung_global_var : object
export declare var _base: object
export declare var $Y: object

export declare function _packageCheck(packages: any) : any 

export declare function _cacheClassProperty (clsName: string, key: string, value: any) : void

export declare function _getSuperClass (clsName: string) : string 

export declare function _getClassHierarchy(clsName: string) : Array<string> 

export declare function _replaceAll (targetStr: string, strFind: string, strReplace: string) : string 

/**
 * check current browser properties
 * 
 * @return {string} browser properties
 */
export declare function com_yung_util_getbrowser () : string

export declare function md5(input: string): string

/**
 * Basic Class, like JAVA Object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare abstract class $Class {
    
    // for reflection
	abstract classProp : { 
	    name : string,
	    unimplemented : Array<any>
    }
    
    constructor()

    /** 
     * hash this instance to md5 string
     * 
     * @instance
     * @memberof $Class
     * @return {string} hash string
     */
    hashCode (): string 
    
    /** 
     * print to string
     * 
     * @instance
     * @memberof $Class
     * @return {string} result string
     */
    toString (): string 
    
    /** 
     * inherit property of from super class to current class
     * 
     * @private
     * @instance
     * @memberof $Class
     */
    inheritProp (): void 
    
    /** 
     * replace string function
     * 
     * @instance
     * @memberof $Class
     * @param  {string} targetStr - target string
     * @param  {string} strFind - string to find
     * @param  {string} strReplace - string to replace
     * @return {string} result string
     */
    replaceAll (targetStr: string, strFind: string, strReplace: string): string 

    static getInstance (arg1: any, arg2?: any, arg3?: any, arg4?: any, arg5?: any, arg6?: any, arg7?: any, arg8?: any, arg9?: any, arg10?: any): any 

    static validate (className: string, id: string): void 

    static getClassType (className: string): any 

    static instanceOf (className: string, obj: any): boolean 

    static getClassName (type: any): string 

    static checkFunction (type: any, funct: any): boolean 

}

export declare function _validType (eleType: any, element: any): boolean 

export declare abstract class Comparable extends $Class {
    abstract compareTo (b: Comparable): number;
}

/**
 * Md5 hash utility
 *
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class MD5Util {
	
	/**
	 * calculate string to md5 hash string
	 * 
	 * @memberof MD5Util
	 * @param {string} str - input string
	 * @return {string} hash string
	 */	
    static calc (str: string): string 
}

/**
 * check current user locale
 * 
 * @return {string} user locale
 */
export declare function com_yung_util_getLang (): string 

export declare function _isValidMd5(input: any): boolean

export declare function fireInstanceMethod (hashId: string, method: string, arg1?: any, arg2?: any, arg3?: any, arg4?: any, arg5?: any, arg6?: any, arg7?: any, arg8?: any, arg9?: any, arg10?: any): void 

export declare function binarySearchArray (arr: Array<any>, x: any, start: number, end: number, reverse?: boolean, exact?: boolean): number

export declare function expandSearchArray (arr: Array<any>, x: any, start: number): number 

/**
 * Collection interface. Define implement methods.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export interface com_yung_util_CollectionInterface<T> {

    // @ts-ignore
    //classProp : { 
	//	name : "com.yung.util.CollectionInterface",
	//	unimplemented : ['toArray', 'size', 'getFirstElement', 'add', 'remove', 'contains']
    //}
    
    toArray(): Array<T>;

    size(): number;

    getFirstElement(): T;

    add(element: T): boolean;

    remove(element: any): boolean;

    contains(element: any): boolean;

    clear(): void;
}

/**
 * Map interface. Define implement methods.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export interface com_yung_util_MapInterface<K, V> {

    //classProp : { 
	//	name : "com.yung.util.MapInterface",
	//	unimplemented : ['entryArray', 'size', 'getKeyArray', 'get', 'put', 'remove', 'containsKey']
    //}
    
    entryArray () : Array<com_yung_util_EntryInterface<K, V>>;

    size () : number;

    getKeyArray () : Array<K>

    get (obj: any) : V

    put (key: K, value: V) : void

    remove (K: K): V | boolean

    containsKey (key: K) : boolean

    firstKey () : K

    isMap (map: any) : boolean
}

/**
 * Map entry
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export interface com_yung_util_EntryInterface<K, V> {
    
	//classProp : { 
    //    name : "com.yung.util.Entry"
    //    unimplemented : ['getKey', 'getValue', 'setValue']
	//}
    
	/**
     * key
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_Entry
     */
	key : K,
    
	/**
     * value
     * @member {string | number | object}
     * @instance
     * @memberof com_yung_util_Entry
     */
	value : V,
    
    /** 
     * get key
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number} key
     */
    getKey (): K
    
    /** 
     * get value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number | bollean | object} value
     */
    getValue (): V 
    
    /** 
     * set value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @param  {string | number | bollean | object} value - value
     */
    setValue (value: V): void
}