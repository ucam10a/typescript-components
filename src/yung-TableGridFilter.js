/**
 * Table Grid Utility Filter 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TableGridFilter = $Class.extend({
    classProp : { name : "com.yung.util.TableGridFilter"},
    tableGrid : null,
    id : null,
    filterDiv : null,
    
    /**
     * original source data
     * @member {object}
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    srcData : null,
    
    /**
     * filtered data
     * @member {object}
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    filterData : null,
    
    /**
     * option template max text length, default 15
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    maxOptionTxtLength : 15,
    
    /**
     * filter float div width, default 200
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    filterDivWidth : 200,
    
    /**
     * filter float div height, default 335
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    filterDivHeight : 335,
    
    template : null,
    optTemplate : null,
    colIdx : null,
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     * @param {string} tableGridDivId - table grid div id
     */
    init : function (tableGridDivId) {
        var tableGrid = $Class.getInstance("com.yung.util.TableGridUtil", tableGridDivId);
        if (tableGrid == null) {
            throw "tableGridDivId: " + tableGridDivId + " is not validate, please check!";
        }
        $Class.validate(this.classProp.name, tableGridDivId);
        this.tableGrid = tableGrid;
        var gridDivId = tableGrid.gridDivId;
        this.id = gridDivId;
        this.filterDiv = com.yung.util.FloatDiv.instance('filter', false, 'border-style: solid; border-width: 1px;');
        var _filterDiv = this.filterDiv;
        jQuery("#" + gridDivId).bind("click", function() {
            _filterDiv.closeFloatDiv();
        });
        this.filterDiv.showCloseButton(true);
        this.srcData = {};
        this.filterData = {};
        var templateData = {};
        templateData["id"] = this.id;
        this.template = new com.yung.util.BasicTemplate(templateData);
        this.template.add('<table style="width: 96%; height: 100%">');
        this.template.add('    <tr>');
        this.template.add('        <td style="cursor: pointer;"><span onclick="com_yung_util_TableGridFilter.instance(\'{{id}}\').sort({{colIdx}}, \'asc\');">Sort Ascending &uarr; </span></td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td style="cursor: pointer;"><span onclick="com_yung_util_TableGridFilter.instance(\'{{id}}\').sort({{colIdx}}, \'desc\');">Sort Descending &darr; </span></td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td><input id="tablegrid-filter-{{colIdx}}" type="text" value="" onkeyup="com_yung_util_TableGridFilter.instance(\'{{id}}\').filterOptions(this);" /></td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td>');
        this.template.add('            <select id="tablegrid-filter-condition-{{colIdx}}" onchange="com_yung_util_TableGridFilter.instance(\'{{id}}\').filterOptions();" >');
        this.template.add('                <option value="include">include</option>');
        this.template.add('                <option value="exclude">exclude</option>');
        this.template.add('                <option value="equal">equal</option>');
        this.template.add('                <option value="not equal">not equal</option>');
        this.template.add('                <option value="greater">greater</option>');
        this.template.add('                <option value="less">less</option>');
        this.template.add('            </select>');
        this.template.add('        </td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td> <a href="javascript:void(null);" onclick="com_yung_util_TableGridFilter.instance(\'{{id}}\').checkAll();" >Check All</a> <a href="javascript:void(null);" onclick="com_yung_util_TableGridFilter.instance(\'{{id}}\').uncheckAll();" >UnCheck All</a> </td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td style="border-style: solid; border-width: 1px; border-radius: 2px; border-color: grey;" >');
        this.template.add('            <div style="width: 99%; height: 130px; overflow-x: hidden;" >{{optTemplate}}</div>');
        this.template.add('        </td>');
        this.template.add('    </tr>');
        this.template.add('    <tr>');
        this.template.add('        <td align="center" ><input type="button" value="OK" onclick="com_yung_util_TableGridFilter.instance(\'{{id}}\').executeFilter()" /> &nbsp; &nbsp; <input type="button" value="Cancel" onclick="com_yung_util_TableGridFilter.instance(\'{{id}}\').hideFilter()" /></td>');
        this.template.add('    </tr>');
        this.template.add('</table>');
        this.optTemplate = new com.yung.util.BasicTemplate({});
        this.optTemplate.add('<div style="display: block;"> <input name="tablegrid-filter-checkbox" data="{{rowIdxs}}" type="checkbox" value="{{value}}"> {{shortVal}} </div>');
        this.colIdx = null;
        return this;
    },
    
    /** 
     * setup and show filter option in table 
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    setupFilter : function () {
        this.renderHeader();
        this.srcData["rowOrderList"] = this.tableGrid.rowOrderList.clone();
        this.srcData["cacheRowMap"] = this.tableGrid.cacheRowMap.clone();
        this.srcData["dirtyRowMap"] = this.tableGrid.dirtyRowMap.clone();
        this.srcData["commentMap"] = this.tableGrid.commentMap.clone();
    },
    renderHeader : function () {
        this.tableGrid.headerTemplate = new com.yung.util.BasicTemplate();
        var headerHtml = this.tableGrid.getHeaderHtml();
        jQuery("#" + this.tableGrid.gridDivId + "-headerDiv").html('<table class="' + this.tableGrid.gridCls + '" style="' + this.tableGrid.gridStyle + '" align="' + this.tableGrid.tableAlign + '">' + headerHtml + '</table>');
        this.tableGrid.render();
    },
    
    /** 
     * cancel and hide filter option in table 
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    cancelFilter : function () {
        this.tableGrid.filter = false;
        this.renderHeader();
        if (this.filterData["rowOrderList"] != null && this.filterData["rowOrderList"].size() >= 0) {
            // cancel filter and rollback
            this.updateFilterCache();
            var cacheRowMap = this.srcData["cacheRowMap"].clone();
            var rowOrderList = this.srcData["rowOrderList"].clone();
            var dirtyRowMap = this.srcData["dirtyRowMap"].clone();
            var commentMap = this.srcData["commentMap"].clone();
            var rows = [];
            for (var i = 0; i < rowOrderList.size(); i++) {
                var rowIdx = rowOrderList.get(i);
                var row = cacheRowMap.get(rowIdx);
                if (row != null) {
                    rows.push(row);
                }
            }
            this.tableGrid.loadData(rows);
            this.tableGrid.dirtyRowMap = dirtyRowMap;
            this.tableGrid.commentMap = commentMap;
            // set dirty row
            this.setDirtyRows();
            // set comment map
            this.setComments();
        }
        this.tableGrid.render();
        this.hideFilter();
        this.srcData = {};
        this.filterData = {};
    },
    
    /** 
     * display filter option window
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     * @param {number} colIdx - column index
     * @param {HTMLElement} img - filter option image element
     */
    showFilter : function (colIdx, img) {
        this.colIdx = colIdx;
        var td = jQuery(img).parent();
        var pos = new com.yung.util.Position(td).getBotLeftPosition();
        pos.top = pos.top + 6;
        pos.left = pos.left + 1;
        var tableHtml = this.getFilterHtml(colIdx);
        this.filterDiv.openFloatDiv(tableHtml, pos, this.filterDivWidth, this.filterDivHeight);
    },
    
    /** 
     * hide filter option window
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    hideFilter : function () {
        this.filterDiv.showFloatDiv(false);
    },
    
    /** 
     * get grid data by column id
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     * @param {number} colIdx - column index
     * @return {Array} data array of column
     */
    getGridDataAtCol : function (colIdx) {
        var ret = [];
        var columnArray = this.tableGrid.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        for (var i = 0; i < this.tableGrid.rowOrderList.size(); i++) {
            var rowIdx = this.tableGrid.rowOrderList.get(i);
            var value = this.tableGrid.getCellValue(rowIdx, columnId);
            ret.push(value);
        }
        return ret;
    },
    getFilterHtml : function (colIdx) {
        var colArray = this.getGridDataAtCol(colIdx);
        var valueMap = new com.yung.util.Map('string', com.yung.util.BasicSet);
        for (var i = 0; i < colArray.length; i++) {
            if (colArray[i] != null && colArray[i] != '') {
                var key = this.getEscapeHtmlText(colArray[i]);
                if (key != '') {
                    var rowIdSet = valueMap.get(key);
                    if (rowIdSet == null) {
                        rowIdSet = new com.yung.util.BasicSet('number');
                    }
                    rowIdSet.add(i);
                    valueMap.put(key, rowIdSet);
                }
            }
        }
        var optTemplateHtml = '';
        var tempArray = valueMap.getKeyArray();
        for (var i = 0; i < tempArray.length; i++) {
            var val = tempArray[i];
            var shortVal = val;
            if (val.length > this.maxOptionTxtLength) {
                shortVal = val.substring(0, this.maxOptionTxtLength) + "...";
            }
            this.optTemplate.data['value'] = val;
            this.optTemplate.data['shortVal'] = shortVal;
            var rowIdArray = valueMap.get(val + '').toArray();
            this.optTemplate.data['rowIdxs'] = JSON.stringify(rowIdArray);
            optTemplateHtml = optTemplateHtml + this.optTemplate.toHtml();
        }
        this.template.data["colIdx"] = colIdx + "";
        this.template.data["optTemplate"] = optTemplateHtml;
        return this.template.toHtml();
    },
    getEscapeHtmlText : function (value) {
        if (value != null) {
            value = value + "";
            if (value.indexOf(">") >= 0 || value.indexOf("<") >= 0) {
                value = jQuery(value).text();
            }
        }
        return value;
    },
    
    /** 
     * update filter cache data when data change
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    updateFilterCache : function () {
        this.srcData["dirtyRowMap"] = this.tableGrid.dirtyRowMap.clone();
        this.srcData["commentMap"] = this.tableGrid.commentMap.clone();
        var keyArray = this.tableGrid.dirtyRowMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var rowIdx = keyArray[i];
            var row = this.tableGrid.cacheRowMap.get(rowIdx);
            if (row != null) {
                this.srcData["cacheRowMap"].put(rowIdx, row);
            }
        }
    },
    
    /** 
     * sort current grid data
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     * @param {number} colIdx - column index
     * @param {string} order - for example, asc, desc
     */
    sort : function (colIdx, order) {
        var currentData = {};
        currentData["rowOrderList"] = this.tableGrid.rowOrderList.clone();
        currentData["cacheRowMap"] = this.tableGrid.cacheRowMap.clone();
        currentData["dirtyRowMap"] = this.tableGrid.dirtyRowMap.clone();
        currentData["commentMap"] = this.tableGrid.commentMap.clone();
        var columnArray = this.tableGrid.columnUtil.getColumnArray();
        var colArray = this.getGridDataAtCol(colIdx);
        var sortValueMap = null;
        var valueType = "string";
        if (columnArray[colIdx].dataType == "number") {
            valueType = "number";
        }
        if (order == 'asc') {
            sortValueMap = new com.yung.util.QuickSortValueMap('string', valueType);
        } else if (order == 'desc') {
            sortValueMap = new com.yung.util.QuickSortValueMap('string', valueType, true);
        } else {
            throw "unknown order: " + order;
        }
        for (var i = 0; i < colArray.length; i++) {
            var val = colArray[i];
            if (val == null) {
                val = '';
            }
			if (valueType == 'number') {
				if (jQuery.isNumeric(val) == true) {
                    val = val * 1.0;
				} else {
					throw "val:'" + val + "' is not number!";;
				}
            } else {
				val = val + "";
			}
			var rowIdx = this.tableGrid.rowOrderList.get(i);
            sortValueMap.put(rowIdx, val);
        }
        var newKeyArray = sortValueMap.getKeyArray();
        this.tableGrid.rowOrderList.clear();
        var rows = [];
        for (var i = 0; i < newKeyArray.length; i++) {
            var rowIdx = newKeyArray[i];
            this.tableGrid.rowOrderList.add(rowIdx);
            var row = this.tableGrid.cacheRowMap.get(rowIdx);
            if (row != null) {
                rows.push(row);
            }
        }
        this.tableGrid.loadData(rows);
        this.tableGrid.dirtyRowMap = currentData["dirtyRowMap"];
        this.tableGrid.commentMap = currentData["commentMap"];
        // set dirty row
        this.setDirtyRows();
        // set comment map
        this.setComments();
        if (this.filterData["rowOrderList"] != null && this.filterData["rowOrderList"].size() >= 0) {
            this.tableGrid.render();
            this.hideFilter();
            this.updateFilterCache();
        } else {
            this.cancelFilter();
		}
    },
    setDirtyRows : function () {
        var keyArray = this.tableGrid.dirtyRowMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var rowIdx = keyArray[i];
            var state = this.tableGrid.dirtyRowMap.get(rowIdx);
            if (state == 'deleted') {
                this.tableGrid.setDeleted(rowIdx);
            } else {
                this.tableGrid.setUpdated(rowIdx, state);
            }
        }
    },
    setComments : function () {
        var keyArray = this.tableGrid.commentMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var rowIdx = keyArray[i];
            var commentRowMap = this.tableGrid.commentMap.get(rowIdx);
            var colKeyArray = commentRowMap.getKeyArray();
            for (var j = 0; j < colKeyArray.length; j++) {
                var columnId = colKeyArray[j];
                var comment = commentRowMap.get(columnId);
                this.tableGrid.setComment(rowIdx, columnId, comment);    
            }
        }
    },
    checkAll : function () {
        this.uncheckAll();
        jQuery("input[name='tablegrid-filter-checkbox']").each(function( index ) {
            var parentDiv = jQuery(this).parent();
            var display = parentDiv.css("display");
            if (display == 'block') {
                jQuery(this).prop("checked", true);
            }
        });
    },
    uncheckAll : function () {
        jQuery("input[name='tablegrid-filter-checkbox']").each(function( index ) {
            jQuery(this).prop("checked", false);
        });
    },
    filterOptions : function (input) {
        if (input == null) {
            input = jQuery("#tablegrid-filter-" + this.colIdx).get(0);
        }
        this.uncheckAll();
        var criteria = jQuery(input).val();
        var condition = jQuery("#tablegrid-filter-condition-" + this.colIdx).val();
        var self = this;
        jQuery("input[name='tablegrid-filter-checkbox']").each(function( index ) {
            var rawVal = $(this).val();
            var val = self.getEscapeHtmlText(rawVal);
            var parentDiv = jQuery(this).parent();
            if (criteria === '') {
                parentDiv.css("display", "block");
            } else {
                var show = "block";
                if (condition == "include") {
                    var include = val.indexOf(criteria) >= 0;
                    if (include == false) {
                        show = "none";
                    }
                } else if (condition == "exclude") {
                    var include = val.indexOf(criteria) >= 0;
                    if (include == true) {
                        show = "none";
                    }
                } else if (condition == "equal") {
                    if (criteria != val) {
                        show = "none";
                    }
                } else if (condition == "not equal") {
                    if (criteria === val) {
                        show = "none";
                    }
                } else if (condition == "greater") {
                    if (jQuery.isNumeric(val) && jQuery.isNumeric(criteria)) {
                        val = val * 1.0;
                        criteria = criteria * 1.0;
                    }
                    if (val <= criteria) {
                        show = "none";
                    }
                } else if (condition == "less") {
                    if (jQuery.isNumeric(val) && jQuery.isNumeric(criteria)) {
                        val = val * 1.0;
                        criteria = criteria * 1.0;
                    }
                    if (val >= criteria) {
                        show = "none";
                    }
                }
                parentDiv.css("display", show);
            }
        });
    },
    
    /** 
     * execute filter
     * 
     * @instance
     * @memberof com_yung_util_TableGridFilter
     */
    executeFilter : function () {
        var checkedList = this.getFilterCheckedRowIds();
        this.filterData["rowOrderList"] = checkedList;
        this.tableGrid.rowOrderList.clear();
        var rows = [];
        for (var i = 0; i < checkedList.size(); i++) {
            var rowIdx = checkedList.get(i);
            this.tableGrid.rowOrderList.add(rowIdx);
            var row = this.tableGrid.cacheRowMap.get(rowIdx);
            if (row != null) {
                rows.push(row);
            }
        }
        this.tableGrid.loadData(rows);
        this.tableGrid.dirtyRowMap = this.srcData["dirtyRowMap"].clone();
        this.tableGrid.commentMap = this.srcData["commentMap"].clone();
        var keyArray = null;
        // set dirty row
        keyArray = this.tableGrid.dirtyRowMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var rowIdx = keyArray[i];
            var state = this.tableGrid.dirtyRowMap.get(rowIdx);
            if (state == 'deleted') {
                this.tableGrid.setDeleted(rowIdx);
            } else {
                this.tableGrid.setUpdated(rowIdx, state);
            }
        }
        // set comment map
        keyArray = this.tableGrid.commentMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var rowIdx = keyArray[i];
            var commentRowMap = this.tableGrid.commentMap.get(rowIdx);
            var colKeyArray = commentRowMap.getKeyArray();
            for (var j = 0; j < colKeyArray.length; j++) {
                var columnId = colKeyArray[j];
                var comment = commentRowMap.get(columnId);
                this.tableGrid.setComment(rowIdx, columnId, comment);    
            }
        }
        this.tableGrid.render();
        var contentScrollDiv = jQuery('#' + this.tableGrid.gridDivId + '-contentScrollDiv');
        var innerWidth = contentScrollDiv.innerWidth();
        var hasHScroll = contentScrollDiv.get(0).scrollWidth < innerWidth;
        if (hasHScroll == false){
            jQuery("#" + this.tableGrid.gridDivId + "-headerScrollDiv").css("left", "0px");
        }
        this.hideFilter();
        // setup filterPng
        jQuery("#" + this.tableGrid.gridDivId + "-filter-" + this.colIdx).attr("src", this.tableGrid.classProp.filterPng);
    },
    getFilterCheckedRowIds : function () {
        var checkedList = new com.yung.util.BasicList('string');
        var grid = this.tableGrid;
        jQuery("input[name='tablegrid-filter-checkbox']").each(function( index ) {
            var checked = jQuery(this).prop("checked");
            if (checked == true) {
                var data = jQuery(this).attr("data");
                var rowArray = JSON.parse(data);
                for (var i = 0; i < rowArray.length; i++) {
                    var rowId = rowArray[i];
                    var rowIdx = grid.rowOrderList.get(rowId);
                    checkedList.add(rowIdx);
                }
            }
        });
        return checkedList;
    },
    keepLockColumn : function () {
        // TODO
    }
});

com_yung_util_TableGridFilter.instance = function (gridDivId) {
    return $Class.getInstance("com.yung.util.TableGridFilter", gridDivId);
}