/**
 * Combo box, like autocomplete
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Combo = $Class.extend({

    classProp : { 
        name : "com.yung.util.Combo" 
    },
    
    hashId : '',
    
    /**
     * minimum length for filter
     * @member {number}
     * @instance
     * @memberof com_yung_util_Combo
     */
    minLength : 2,
    
    /**
     * maximum items to display
     * @member {number}
     * @instance
     * @memberof com_yung_util_Combo
     */
    maxItems : 8,
    
    /**
     * element id
     * @member {string}
     * @instance
     * @memberof com_yung_util_Combo
     */
    eleId : null,
    
    /**
     * float div to place word list
     * @member {com_yung_util_FloatDiv}
     * @instance
     * @memberof com_yung_util_Combo
     */
    floatDiv : null,
    displayHeight : 250,
    
    /**
     * select value array
     * @member {com_yung_util_Set}
     * @instance
     * @memberof com_yung_util_Combo
     */
    selectValues : null,
    
    
    selectIndex : null,
    
    
    chosedValue : "",
    
    /**
     * Callback for customized filter for selectValues
     *
     * @callback ComboCustomFilter
     * @param {string} reqCode - input code
     * @param {com_yung_util_Combo} combo - com_yung_util_Combo instance
     * @return {Array} customized select value array
     */
    /**
     * customized filter for selectValues
     * @member {ComboCustomFilter}
     * @instance
     * @memberof com_yung_util_Combo
     */
    filter : null,
    
    /**
     * Callback after combo choose value
     *
     * @callback AfterComboChoose
     * @param {string} eleId - element id
     * @param {string} inputVal - select value
     */
    /**
     * callback after combo choose value
     * @member {AfterComboChoose}
     * @instance
     * @memberof com_yung_util_Combo
     */
    afterChoose : null,
    
    
    /**
     * constructor
     * @memberof com_yung_util_Combo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    init : function (eleId, selectValues) {
        $Class.validate(this.classProp.name, eleId);
        if (eleId == null || eleId == '') {
            throw "Please specify eleId";
        }
        this.eleId = eleId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + eleId);
        this.selectValues = new com_yung_util_Set('string');
        this.setSelectValues(selectValues);
        jQuery("#" + eleId).attr("autocomplete", "off");
        this.createCombo();
        return this;
    },
    
    /** 
     * set select values
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {Array | com_yung_util_CollectionInterface} selectValues - select values
     */
    setSelectValues : function (selectValues) {
        if (selectValues == null) {
            return;
        }
        if (_validType(Array, selectValues) == true) {
            this.selectValues.clear();
            this.selectValues.addByArray(selectValues);
        } else if (_validType(com_yung_util_CollectionInterface, selectValues) == true) {
            this.selectValues.clear();
            this.selectValues.addAll(selectValues);
        } else {
            throw "selectValues can only be Array or com.yung.util.CollectionInterface type";
        }
    },
    
    /** 
     * add select values
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} selectValue - select value
     */
    addSelectValues : function (selectValue) {
        if (selectValue == null) {
            return;
        }
        if (_validType('string', selectValue) == false) {
            throw "selectValue can only be string";
        }
        this.selectValues.add(selectValue);
    },
    
    /** 
     * set minimum length for filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {number} minLength - minimum length for filter
     */
    setMinLength : function (minLength) {
        if (minLength == null) {
            return;
        }
        if (_validType('number', minLength) == false) {
            throw "minLength can only be number";
        }
        this.minLength = minLength;
    },
    
    /** 
     * set maximum items to display
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {number} maxItems - maximum items to display
     */
    setMaxItems : function (maxItems) {
        if (maxItems == null) {
            return;
        }
        if (_validType('number', maxItems) == false) {
            throw "maxItems can only be number";
        }
        this.maxItems = maxItems;
    },
    
    /** 
     * set customized filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {ComboCustomFilter} filter - customized filter for selectValues
     */
    setFilter : function (filter) {
        if (typeof filter != 'function') {
            throw "filter is not function";
        }
        this.filter = filter;
    },
    
    /** 
     * set after choose callback
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {AfterComboChoose} afterChoose - after choose callback
     */
    setAfterChoose : function (afterChoose) {
        if (typeof afterChoose != 'function') {
            throw "afterChoose is not function";
        }
        this.afterChoose = afterChoose;
    },
    
    /** 
     * set input value
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} inputVal - string put into input
     */
    setInputValue : function (inputVal) {
        if (inputVal == null) {
            inputVal = this.chosedValue;
        }
        jQuery("#" + this.eleId).val(inputVal);
        if (typeof this.afterChoose == 'function') {
            this.afterChoose(this.eleId, inputVal);
        }
        this.floatDiv.closeFloatDiv();
        this.selectIndex = null;
        this.chosedValue = '';
    },
    
    /** 
     * standard filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} reqCode - request code from input
     * @return {Array} filtered value array
     */
    standardFilter : function (reqCode) {
        var displayValues = [];
        var array = this.selectValues.toArray();
        for (var i = 0; i < array.length; i++) {
            var val = array[i];
            if (val.indexOf(reqCode) >= 0) {
                displayValues.push(val);
            }
        }
        return displayValues;
    },
    
    /** 
     * create combo
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Combo
     */
    createCombo : function () {
        this.floatDiv = $Class.getInstance("com.yung.util.FloatDiv", "combo-" + this.eleId, true, "overflow-x: hidden; overflow-y: auto;");
        this.bindFunction();
    },
    bindFunction : function () {
        var bindId = this.eleId;
        var minLen = this.minLength;
        var self = this;
        jQuery("#" + bindId).keyup(function(event){
            var length = jQuery("#yung-Combo-table-" + bindId + " td").length;
            var scrollExist = false;
            var floatDivElement = self.floatDiv.getFloatDivElement();
            if (jQuery("#yung-Combo-table-" + bindId).height() > self.displayHeight) {
                scrollExist = true;
            }
            // press up
            if (event.keyCode == 38) {
                event.preventDefault();
                if (self.selectIndex == 0) {
                    // already on top
                    return;
                }
                if (self.selectIndex == null) {
                    return;
                } else {
                    var inputbox = self;
                    var setIdx = null;
                    var setVal = '';
                    jQuery("#yung-Combo-table-" + bindId + " td").each(function( index ) {
                        if (inputbox.selectIndex == (index + 1)) {
                            setIdx = index;
                            setVal = jQuery( this ).text();
                            jQuery( this ).css("background", "#A4BED4");
                            if (scrollExist) {
                                var position = new com.yung.util.Position(jQuery(this));
                                var botPos = position.getBotLeftPosition(null, null, floatDivElement);
                                var topPos = position.getTopLeftPosition(null, null, floatDivElement);
                                var currentScrollTop = floatDivElement.scrollTop();
                                var tdHeight = jQuery(this).height();
                                if (botPos.top > (currentScrollTop + self.displayHeight)){
                                    // outside bottom
                                    var offsetY = 3;
                                    if (com_yung_util_getbrowser() == 'msie') {
                                        offsetY = offsetY + 3;
                                    }
                                    floatDivElement.scrollTop(botPos.top - self.displayHeight + offsetY);
                                }
                                if (currentScrollTop > topPos.top) {
                                    // outside top
                                    floatDivElement.scrollTop(topPos.top - 3);
                                }
                            }
                            return;
                        }
                        jQuery( this ).css("background", "");
                    });
                    if (self.selectIndex != setIdx) {
                        self.selectIndex = setIdx;
                        self.chosedValue = setVal;
                    }
                }
                return;
            }
            // press down
            if (event.keyCode == 40) {
                event.preventDefault();
                if (self.selectIndex == (length - 1)) {
                    // already on bottom
                    return;
                }
                var inputbox = self;
                var setIdx = null;
                var setVal = '';
                jQuery("#yung-Combo-table-" + bindId + " td").each(function( index ) {
                    if (inputbox.selectIndex == null) {
                        if (index == 0) {
                            setIdx = index;
                            setVal = jQuery( this ).text();
                            jQuery( this ).css("background", "#A4BED4");
                            return;
                        }
                    }
                    if (inputbox.selectIndex == (index - 1)) {
                        setIdx = index;
                        setVal = jQuery( this ).text();
                        jQuery( this ).css("background", "#A4BED4");
                        if (scrollExist) {
                            var position = new com.yung.util.Position(jQuery(this));
                            var botPos = position.getBotLeftPosition(null, null, floatDivElement);
                            var topPos = position.getTopLeftPosition(null, null, floatDivElement);
                            var currentScrollTop = floatDivElement.scrollTop();
                            var tdHeight = jQuery(this).height();
                            if (botPos.top > (currentScrollTop + self.displayHeight)){
                                // outside bottom
                                var offsetY = 3;
                                if (com_yung_util_getbrowser() == 'msie') {
                                    offsetY = offsetY + 3;
                                }
                                floatDivElement.scrollTop(botPos.top - self.displayHeight + offsetY);
                            }
                            if (currentScrollTop > topPos.top) {
                                // outside top
                                floatDivElement.scrollTop(topPos.top - 3);
                            }
                        }
                        return;
                    }
                    jQuery( this ).css("background", "");
                });
                if (self.selectIndex != setIdx) {
                    self.selectIndex = setIdx;
                    self.chosedValue = setVal;
                }
                return;
            }
            // press enter
            if (event.keyCode == 13) {
                event.preventDefault();
                if (self.selectIndex == null) {
                    return;
                } else {
                    self.setInputValue(self.chosedValue);
                }
                return;
            }
            self.selectIndex = null;
            self.chosedValue = '';
            var reqCode = jQuery("#" + bindId).val();
            if (reqCode.length < minLen) {
                return;
            }
            if (typeof self.filter == 'function') {
                var array = self.filter(reqCode, self);
                if (array != null) {
                    if (_validType(Array, array) == true) {
                        self.showDropList(array, bindId);
                    }
                }
            } else {
                // standard filter
                var displayValues = self.standardFilter(reqCode);
                self.showDropList(displayValues, bindId);
            }
        });
    },
    
    /** 
     * show drop list
     * 
     * @memberof com_yung_util_Combo
     * @param {Array} displayValues - list values to display
     * @param {string} bindId - binding element id
     */
    showDropList : function (displayValues, bindId) {
        if (bindId == null) {
            bindId = this.eleId;
        }
        if (displayValues == null || displayValues.length == 0) {
            this.floatDiv.closeFloatDiv();
            return;
        }
        var _displayValues = [];
        for (var i = 0; i < displayValues.length; i++) {
            _displayValues.push(displayValues[i]);
            if (_displayValues.length > this.maxItems) {
                break;
            }
        }
        displayValues = _displayValues;
        // display floatDiv
        var display = this.floatDiv.isDisplay();
        var html = "<table id='yung-Combo-table-" + bindId + "' style='width: 100%;'>";
        var basicTmp = new com.yung.util.BasicTemplate();
        var template = new com.yung.util.ArrayTemplate();
        basicTmp.add("<tr>");
        basicTmp.add("    <td class='yung-combo' style='' align='left' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"setInputValue\", \"{{val}}\");'>{{val}}</td>");
        basicTmp.add("</tr>");
        template.setBasicTmp(basicTmp);
        var data = [];
        for (var i = 0; i < displayValues.length; i++) {
            var obj = {};
            obj["val"] = displayValues[i];
            data.push(obj);
        }
        var html = html + template.toHtml(data) + "</table>";
        if (display == false) {
            var browser = com_yung_util_getbrowser();
            var ele = jQuery("#" + bindId);
            var position = new com_yung_util_Position(ele);
            var offsetY = 6;
            var typeName = jQuery("#" + this.eleId).prop('nodeName');
            typeName = typeName.toUpperCase();
            if ("TEXTAREA" == typeName) {
            	offsetY = offsetY - 3;
            }
            if (browser == 'msie') {
                offsetY = 2;
            }
            var pos = position.getBotLeftPosition(0, offsetY);
            var width = jQuery("#" + bindId).width();
            this.floatDiv.openFloatDiv(html, pos, width, this.displayHeight);
        } else {
            this.floatDiv.setFloatContent(html);
        }
    }
    
});

/** 
 * get com_yung_util_Combo global instance,
 * 
 * @param  {string} eleId - element id
 * @param  {Array} selectValues - select value array
 * @return  {com_yung_util_Combo} com_yung_util_Combo instance
 */
com_yung_util_Combo.instance = function(eleId, selectValues) {
    return $Class.getInstance("com.yung.util.Combo", eleId, selectValues);
}

/**
 * select Combo box, like autocomplete, but show drop list when focus
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Combo
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SelectCombo = com_yung_util_Combo.extend({

    classProp : { 
        name : "com.yung.util.SelectCombo" 
    },
    
    /**
     * minimum length for filter
     * @member {number}
     * @instance
     * @memberof com_yung_util_SelectCombo
     */
    minLength : 1,
    
    /**
     * constructor
     * @memberof com_yung_util_SelectCombo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    init : function (eleId, selectValues) {
        return this._super(eleId, selectValues);
    },
    bindFunction : function () {
        var bindId = this.eleId;
        var self = this;
        // show when onFoucus
        jQuery("#" + bindId).focus(function() {
            var reqCode = jQuery("#" + bindId).val();
            if (reqCode == '') {
                self.showDropList(self.selectValues.toArray(), bindId);
            } else {
                var displayValues = [];
                var array = self.selectValues.toArray();
                for (var i = 0; i < array.length; i++) {
                    var val = array[i];
                    if (val.indexOf(reqCode) >= 0) {
                        displayValues.push(val);
                    }
                }
                self.showDropList(displayValues, bindId);
            }
        });
        this._super();
    }
});

/** 
 * get com_yung_util_SelectCombo global instance,
 * 
 * @param  {string} eleId - element id
 * @param  {Array} selectValues - select value array
 * @return  {com_yung_util_SelectCombo} com_yung_util_SelectCombo instance
 */
com_yung_util_SelectCombo.instance = function(eleId, selectValues) {
    return $Class.getInstance("com.yung.util.SelectCombo", eleId, selectValues);
}

/**
 * select Combo box, like autocomplete. 
 * allow to append search word.
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Combo
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AppendCombo = com_yung_util_Combo.extend({
    
    classProp : { 
        name : "com.yung.util.AppendCombo" 
    },
    
    /**
     * constructor
     * @memberof com_yung_util_AppendCombo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    init : function (eleId, selectValues) {
        return this._super(eleId, selectValues);
    },
    
    /** 
     * standard filter
     * 
     * @instance
     * @memberof com_yung_util_AppendCombo
     * @param {string} reqCode - request code from input
     * @return {Array} filtered value array
     */
    standardFilter : function (reqCode) {
        var searchCode = reqCode;
        var prefixCode = '';
        var idx = reqCode.lastIndexOf(' ');
        if (idx > 0) {
            searchCode = searchCode.substring(idx + 1, searchCode.length);
            prefixCode = reqCode.substring(0, idx + 1);
        }
        if (searchCode.length < this.minLength) {
            return [];
        }
        var displayValues = [];
        var array = this.selectValues.toArray();
        for (var i = 0; i < array.length; i++) {
            var val = array[i];
            if (val.indexOf(searchCode) >= 0) {
                displayValues.push(prefixCode + val);
            }
        }
        return displayValues;
    }
    
});

/** 
 * get com_yung_util_AppendCombo global instance,
 * 
 * @param  {string} eleId - element id
 * @param  {Array} selectValues - select value array
 * @return  {com_yung_util_AppendCombo} com_yung_util_AppendCombo instance
 */
com_yung_util_AppendCombo.instance = function(eleId, selectValues) {
    return $Class.getInstance("com.yung.util.AppendCombo", eleId, selectValues);
}

jQuery( document ).ready(function() {
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-Combo.js requires yung-Collection.js!");
    }
    if (typeof com_yung_util_FloatDiv == 'undefined') {
        alert("yung-Combo.js requires yung-FloatDiv.js!");
    }
    if (typeof com_yung_util_Position == 'undefined') {
        alert("yung-Combo.js requires yung-Position.js!");
    }
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-Combo.js requires yung-BasicTemplate.js!");
    }
});