import { $Class, com_yung_util_CollectionInterface, com_yung_util_MapInterface, Comparable } from "./yung-JS-extend";

/**
 * Abstract Class for collection
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare abstract class com_yung_util_Collection<T> extends $Class implements com_yung_util_CollectionInterface<T> {
    
	classProp : { 
		name : "com.yung.util.Collection",
		unimplemented : ['add', 'remove']
	}
    
	/**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_Collection
     */
	type : any
    
	/**
     * element array
     * @member {string | number | boolean | object}
     * @instance
     * @memberof com_yung_util_Collection
     */
    array : Array<T>
    obj: object
    tsoArray: Array<number>
    tsoIdx: number
    valueHash: boolean
	
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_Collection
     */
    clear (): void 
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {number} collection size
     */
    size (): number 

    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType (element: any): boolean 
    
    getFirstElement (): T 
    
    binarySearchArray (arr: Array<any>, x: any, start: number, end: number, reverse?: boolean, exact?: boolean): number 

    abstract add(element: T): boolean;
    abstract remove(element: any): boolean;
    /**
	 * add elements by array
	 * 
	 * @instance
     * @memberof com_yung_util_Collection
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray (elementArray: Array<T>): boolean 
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains (element: any): boolean 
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {boolean} empty or not
     */
    isEmpty (): boolean 
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {Array} element array
     */
    toArray (): Array<T> 
    
    /** 
     * return a copy of element array
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {Array} element array
     */
    cloneArray (): Array<T> 
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {string} message string
     */
    toString (): string 
    
    /** 
     * sort collection elements
     * 
     * @instance
     * @param  {boolean} reverse - is reverse sort
     * @memberof com_yung_util_Collection
     */
    sort (reverse?: boolean): void 
    
    /** 
     * check object is collection or not
     * 
     * @memberof com_yung_util_Collection
     * @param  {object} obj - object to check
     * @return {boolean} is collection
     */
    isCollection (obj: any): boolean 
    
}

/**
 * Basic set, only allow type string ,number and boolean
 * 
 * @property {object} classProp - class property
 * @property {string} classProp.name - class name
 * @augments com_yung_util_Collection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_BasicSet<T> extends com_yung_util_Collection<T> {
    
    // @ts-ignore
	classProp : { 
        name : "com.yung.util.BasicSet"
        unimplemented: []
	}
	
	/**
     * element array
     * @member {string | numbe | boolean}
     * @instance
     * @memberof com_yung_util_Collection
     */
	array : Array<T>
	obj : object
	tsoArray : Array<number>
	tsoIdx : number
	
	/**
	 * constructor
     * @memberof com_yung_util_BasicSet
     * @param  {string} type - element type
     */
    constructor (type: any)
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /** 
     * check collection if contains all elements in another collection 
     * 
     * @instance
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to check
     * @return {boolean} exist or not
     */
    containsAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
    
    /**
     * Removes the all elements of collection to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to remove
     * @instance
     * @return {boolean} success or not
     */
    removeAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
}

/**
 * raw object set, not good in performance
 *
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_RawObjectSet<T> extends com_yung_util_BasicSet<T> {
    
    // @ts-ignore
    classProp : { 
		name : "com.yung.util.RawObjectSet"
	}
	
    /**
     * element array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_RawObjectSet
     */
    array : Array<T>
    
    /**
	 * constructor
     * @memberof com_yung_util_RawObjectSet
     */
    constructor ()
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_RawObjectSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains (element: any): boolean 
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
    
}

/**
 * Raw object set, only allow type object
 * object element will use JSON.stringify to check exist 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_ValueHashSet<T extends $Class> extends com_yung_util_BasicSet<T> {
    
    // @ts-ignore
	classProp : { 
	    name : "com.yung.util.ValueHashSet"
    }
    
    /**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_ValueHashSet
     */
    array : Array<T>
    obj : object
    tsoArray : Array<number>
    tsoIdx : number
    valueHash : true
    
    /**
	 * constructor
     * @memberof com_yung_util_ValueHashSet
     */
    constructor ()
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_ValueHashSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains (element: any): boolean 
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
    
}

/**
 * sorted set, only allow type string and number
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_SortedSet<T> extends com_yung_util_BasicSet<T> {
    
    // @ts-ignore
    classProp : { 
    	name : "com.yung.util.SortedSet"
    }
    
    /**
     * reverse falg
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedSet
     */
    reverse : boolean
    
    /**
     * element array
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_SortedSet
     */
    array : Array<T>
    obj : any
    
    /**
	 * constructor
     * @memberof com_yung_util_SortedSet
     * @param  {string} type - element type
     * @param  {boolean} reverse - reverse flag
     */
    constructor (type: any, reverse?: boolean)
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_SortedSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_SortedSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
}

/**
 * object hash set, only allow element which implements hashCode function
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_ObjectHashSet<T extends $Class> extends com_yung_util_BasicSet<T> {
    
    // @ts-ignore
	classProp : { 
		name : "com.yung.util.ObjectHashSet"
	}
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     */
	typeName : string
    
	/**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     */
	array : Array<T>
	obj : object
	tsoArray : Array<number>
	tsoIdx : number
	
	/**
	 * constructor
     * @memberof com_yung_util_ObjectHashSet
     * @param  {function} type - element function
     */
	constructor (type: any)
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains (element: any): boolean 
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
    
}

/**
 * tree object hash set, only allow element which implements hashCode and compareTo function
 * element will be sorted
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_ObjectHashSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_SortedHashSet<T extends Comparable> extends com_yung_util_ObjectHashSet<T> {
    
    // @ts-ignore
	classProp : { 
		name : "com.yung.util.SortedHashSet"
	}
    
	/**
     * reverse falg
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	reverse : boolean
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	typeName : string
    
	/**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	array : Array<T>
	obj : object
	
	/**
	 * constructor
     * @memberof com_yung_util_SortedHashSet
     * @param  {function} type - element function
     * @param  {boolean} reverse - reverse flag
     */
	constructor (type: any, reverse?: boolean)
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_SortedHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_SortedHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
}

/**
 * Basic list, only allow type string and number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Collection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare abstract class com_yung_util_AbstractQueue<T> extends com_yung_util_Collection<T> {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.AbstractQueue",
        unimplemented : ['add', 'remove']
    }
    
    abstract add (element: T, index?: number): boolean
    abstract remove(element: T): boolean
    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @memberof com_yung_util_AbstractQueue
     * @instance
     * @return {T} retrieve element
     */
    peek (): T 
    
    /**
     * Retrieves and removes the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @memberof com_yung_util_AbstractQueue
     * @instance
     * @return {T} retrieve element
     */
    poll (): T 
    
}

/**
 * Basic list, only allow type string and number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_AbstractQueue
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_BasicList<T> extends com_yung_util_AbstractQueue<T> {
    
    // @ts-ignore
	classProp : { 
		name : "com.yung.util.BasicList"
    }
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicList
     * @param  {string} type - element type
     */
    constructor (type: any)
    
    /**
     * Adds the specified element to this list by index 
     * index is optional
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element to add
     * @param  {number} index - index to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T, index?: number): boolean 
    
    /**
     * Adds the specified element to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {T} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    addElement (element: T): boolean 
    
    /**
     * Adds the specified element to this list by index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {T} element - element to add
     * @param  {number} index - index to add
     * @instance
     * @return {boolean} success or not
     */
    addByIndex (element: T, index: number): boolean 
    
    /**
     * Adds the elements of collection to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @param  {number} index - index to add, if null then add to list tail
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>, index: number): boolean 
    
    /**
     * Adds the elements of collection to this list tail 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAllElements (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /**
     * Adds the elements of collection to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @param  {number} index - index to add, if null then add to list tail
     * @instance
     * @return {boolean} success or not
     */
    addAllByIndex (collection: com_yung_util_CollectionInterface<T>, index: number): boolean 
    
    /**
     * get element by index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index of element
     * @instance
     * @return {T} element
     */
    get (index: number): T 
    
    /**
     * Returns the index of the first occurrence of the specified element in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {T} element - element
     * @instance
     * @return {number} index
     */
    indexOf (element: T): number 
    
    /**
     * Returns the index of the last occurrence of the specified element in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element
     * @instance
     * @return {number} index
     */
    lastIndexOf (element: T): number 
    
    /**
     * Removes the element at the specified position in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index
     * @instance
     * @return {T} removed element
     */
    removeByIndex (index: number): T 
    
    /**
     * Removes the first occurrence of the specified element from this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {T} element - element
     * @instance
     * @return {boolean} success or not
     */
    removeElement (element: T): boolean 
    remove (element: T): boolean 
    
    /**
     * Removes from this list all of its elements that are contained in the specified collection
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to remove
     * @instance
     * @return {boolean} success or not
     */
    removeAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /**
     * set element by specified index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index to set
     * @param  {T} element - element to set
     * @instance
     * @return {T} element to set
     */
    set (index: number, element: T): T 
    
    /**
     * Returns a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} fromIndex - from index
     * @param  {number} toIndex - to index
     * @instance
     * @return {com_yung_util_BasicList} result list
     */
    subList (fromIndex: number, toIndex: number): com_yung_util_BasicList<T> 
}

/**
 * raw object list, only allow type object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicList
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_RawObjectList<T> extends com_yung_util_BasicList<T> {
    
    // @ts-ignore
	classProp : { 
		name : "com.yung.util.RawObjectList"
    }
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicList
     */
    constructor ()
}

/**
 * function object list, only allow type function object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicList
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_ObjectList<T extends $Class> extends com_yung_util_BasicList<T> {
    
    // @ts-ignore
	classProp : { 
		name : "com.yung.util.ObjectList"
	}
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_ObjectList
     */
	typeName : string
    
	/**
	 * constructor
     * @memberof com_yung_util_BasicList
     * @param  {function} type - element function
     */
	constructor (type: any)
}

/**
 * Link node
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_LinkNode<T> extends $Class {
    
    classProp : { 
        name : "com.yung.util.LinkNode"
        unimplemented: []
    }
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    type : any
    
    /**
     * before link node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    before : com_yung_util_LinkNode<T>
    
    /**
     * after link node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    after : com_yung_util_LinkNode<T>
    
    /**
     * element
     * @member {T}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    value : T
    
    /**
     * constructor
     * @memberof com_yung_util_LinkNode
     * @param  {all} type - element type
     */
    constructor (type: any)
    
    /**
     * set before link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} beforeNode - before link node
     */
    setBefore (beforeNode: com_yung_util_LinkNode<T>) : void
    
    /**
     * set after link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} afterNode - after link node
     */
    setAfter (afterNode: com_yung_util_LinkNode<T>): void 
    
    /**
     * next link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @return {com_yung_util_LinkNode} next link node
     */
    next (): com_yung_util_LinkNode<T> 
    
    /**
     * set link node value
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} value - link node value
     */
    setValue (value: T): void 
    
    /**
     * get link node value
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @return {all} link node value
     */
    getValue (): T 
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType (element: any): boolean 
    
}

/**
 * Abstract Class for link collection
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare abstract class com_yung_util_LinkCollection<T> implements com_yung_util_CollectionInterface<T> {
    
    classProp : { 
        name : "com.yung.util.LinkCollection",
        unimplemented : ['add', 'remove']
    }
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    type : any
    
    /**
     * object to store link node
     * @member {object}
     * @private
     * @memberof com_yung_util_LinkCollection
     */
    obj : object
    
    /**
     * collection length
     * @member {number}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    length : 0
    
    /**
     * first linked node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    first : com_yung_util_LinkNode<T>
    
    /**
     * last linked node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    last : com_yung_util_LinkNode<T>
    
    valueHash : boolean

    /**
     * constructor
     * @memberof com_yung_util_LinkCollection
     * @param  {string} type - element type
     */
    constructor (type: any)
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    clear (): void
    
    abstract add (element: T): boolean;
    abstract remove (element: any): boolean;  
    /**
     * add elements by array
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray (elementArray: Array<T>): boolean 
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains (element: T) : boolean 
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {boolean} empty or not
     */
    isEmpty (): boolean 
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {number} collection size
     */
    size (): number 
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {Array} element array
     */
    toArray (): Array<T> 
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {string} message string
     */
    toString (): string 
    
    /** 
     * check object is link collection or not
     * 
     * @memberof com_yung_util_LinkCollection
     * @param  {object} obj - object to check
     * @return {boolean} is link collection
     */
    isCollection (obj: any): boolean 
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_LinkCollection
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    getFirstElement (): T 
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType (element: any): boolean 
}

/**
 * basic linked set, only allow type string and number.
 * better for large scale, ex more than 100000
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_LinkCollection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_LinkedSet<T> extends com_yung_util_LinkCollection<T> {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.LinkedSet",
        unimplemented : []
    }
    
    /**
     * constructor
     * @memberof com_yung_util_LinkCollection
     * @param  {string} type - element type
     */
    constructor (type: any)
    
    /**
     * add element
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {all} element - element to add
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * remove element
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {all} element - element to remove
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
    
}

/**
 * JS utility Set by restrict type, best performance
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class system_util_Set<T> implements com_yung_util_CollectionInterface<T> {
    
    classProp : { 
        name : "system.util.Set"
    }
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof system_util_Set
     */
    type : T
    
    /**
     * JS set to store value
     * @member {Set}
     * @private
     * @memberof system_util_Set
     */
    obj : Set<T>
    
    /**
     * constructor
     * @memberof system_util_Set
     * @param  {any} type - element type
     */
    constructor (type: any)
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof system_util_Set
     */
    clear (): void 
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray (elementArray: Array<T>): boolean 
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains (element: T): boolean 
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof system_util_Set
     * @return {boolean} empty or not
     */
    isEmpty (): boolean 
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof system_util_Set
     * @return {number} collection size
     */
    size (): number 
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof system_util_Set
     * @return {Array} element array
     */
    toArray (): Array<T> 
    
    /** 
     * to string
     * 
     * @instance
     * @memberof system_util_Set
     * @return {string} message string
     */
    toString (): string 
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof system_util_Set
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType (element: any): boolean 
    
    /**
     * add element
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {all} element - element to add
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof system_util_Set
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /**
     * remove element
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {all} element - element to remove
     * @return {all} removed element
     */
    remove (element: any): boolean 
    
    isCollection (obj: any): boolean 
    
    getFirstElement (): T 
    
}

/**
 * Tree set collection, use TreeMap as base
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_TreeSet<T> implements com_yung_util_CollectionInterface<T> {
    
    classProp : { 
        name : "com.yung.util.TreeSet"
    }

    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeSet
     */
    type : any
    reverse : boolean
    map : com_yung_util_MapInterface<T, T>
    validType (element: any): boolean 
    
    /**
     * constructor
     * @memberof com_yung_util_TreeSet
     * @param  {string} type - string or number
     * @param  {boolean} reverse - reverse flag
     */
    constructor (type: any, reverse?: boolean)
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add (element: T): boolean 
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll (collection: com_yung_util_CollectionInterface<T>): boolean 
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove (element: any): boolean 
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     */
    clear (): void 
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray (elementArray: Array<T>): boolean 
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains (element: any): boolean 
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {boolean} empty or not
     */
    isEmpty (): boolean 
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {number} collection size
     */
    size (): number 
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {Array} element array
     */
    toArray (): Array<T> 
    
    getFirstElement (): T 
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {string} message string
     */
    toString (): string 
    
    isCollection(collection: any): boolean 

}