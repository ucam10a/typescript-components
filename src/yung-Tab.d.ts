import { $Class } from "./yung-JS-extend";
import { com_yung_util_BasicList } from "./yung-Collection";

/**
 * Create tab tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Tab extends $Class {
    
    classProp : { 
        name : "com.yung.util.Tab",
        unimplemented: []
    }
    
    hashId : string
    
    /**
     * div id to put tab
     * @member {string}
     * @instance
     * @memberof com_yung_util_Tab
     */
    divId : string
    
    /**
     * tab title array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_Tab
     */
    titles : Array<string>
    
    /**
     * tab color
     * @member {string}
     * @instance
     * @memberof com_yung_util_Tab
     */
    color : string
    
    /**
     * tab width
     * @member {number}
     * @instance
     * @memberof com_yung_util_Tab
     */
    width : number
    
    /**
     * tab span
     * @member {number}
     * @instance
     * @memberof com_yung_util_Tab
     */
    span : number
    
    /**
     * tab unique id to identify
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_Tab
     */
    tabId : string
    
    srcContent : com_yung_util_BasicList<string>
    
    /**
     * Callback when click tab
     *
     * @callback clickTabCallback
     * @param {number} tabIndex - tab index
     */
    /**
     * tab click callback
     * @member {clickTabCallback}
     * @instance
     * @memberof com_yung_util_Tab
     */
    callback : Function
    
    /**
     * Callback when create tab
     *
     * @callback createTabCallback
     */
    /**
     * constructor
     * @memberof com_yung_util_Tab
     * @param  {string} divId - div id to put tab
     * @param  {Array} titles - tab title array
     * @param  {string} tabId - tab unique id to identify, if empty, system will create one
     * @param  {string} color - tab color, if empty, it will be black
     * @param  {number} width - tab width, if empty, it will be 120
     * @param  {number} span - tab span, if empty, it will be 30
     * @param  {createTabCallback} afterRender - data object to render HTML
     * @param  {number} afterRenderTime - wait millisecond to run afterRender
     */
    private constructor (divId: string, titles: Array<string>, tabId: string, color: string, width: number, span: number, afterRender: Function, afterRenderTime: number)
    
    /** 
     * create tab element
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     */
    createTab (): void
    
    /** 
     * adjust tab width
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {com_yung_util_Tab} tab - tab instance
     */
    adjustRightWidth (tab: com_yung_util_Tab): void
    
    /** 
     * get render tab data array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @return  {Array} data array
     */
    getDataArray (): Array<object>
    
    /** 
     * show specified tab
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     */
    showTab (idx: number): void
    
    /** 
     * get tab source default HTML code
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @return  {string} source default HTML code
     */
    getSourceTabContent (): void
    
    /** 
     * set tab content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     * @param  {string} html - tab content
     */
    setTabContent (idx: number, html: string): void
    
    /** 
     * set tab title
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {number} idx - tab index
     * @param  {string} title - tab title
     */
    setTabTitle (idx: number, title: string): void
    
    /** 
     * set tab click callback
     * 
     * @instance
     * @memberof com_yung_util_Tab
     * @param  {clickTabCallback} callbackFunct - tab click callback
     */
    setCallback (callbackFunct: Function): void

    static instance (divId: string, titles: Array<string>, tabId: string, color: string, width: number, span: number, afterRender: Function, afterRenderTime: Function) : com_yung_util_Tab

}