import { $Class } from "./yung-JS-extend";

/**
 * An easy tool create popup iframe
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Popup extends $Class {
	
	classProp : { 
        name : "com.yung.util.Popup",
        unimplemented: []
    }
    
    color : string
    
    /**
     * Index to identified iframe window
     * @member {number}
     * @instance
     * @memberof com_yung_util_Popup
     */
    currentPopupIndex : number
    currentWidth : number
    currentHeight : number
    enableFixScorll : boolean
    
    /**
     * a flag to decide whether can close iframe window
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_Popup
     */
    canCloseWindow : boolean
    
    /**
	 * constructor, private used only
	 * Note: do not use this constructor to create instance.
	 * Use com.yung.util.Popup.instance()
	 * 
	 * @private
     * @memberof com_yung_util_Popup
     */
    private constructor ()
    
    /** 
     * set popup color
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} color - popup window color
     */
    setColor (color: string): void
    
    /** 
     * create popup iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     */
    createPopoupFrame (index: number): void
    
    /** 
     * set canClose flag
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {boolean} canClose - a flag to decide whether can close iframe window
     */
    setCanCloseWindow (canClose: boolean): void
    
    /** 
     * show popup iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @param  {boolean} show - show iframe or not
     */
    showIframe (index: number, show: boolean): void
    
    /**
     * Callback when close popup
     *
     * @callback closePopupCallback
     */
    /** 
     * close[hide] popup iframe
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @param  {closePopupCallback} callback - callback when close popup
     */
    closePopup (index?: number, callback?: Function): void
    
    /** 
     * open popup iframe, position and index are optional.
     * if position is empty, use center position of window.
     * if index is empty, use currentPopupIndex plus one[next popup]
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} title - popup window title
     * @param  {number} width - popup window width
     * @param  {number} height - popup window height
     * @param  {string} url - popup iframe url
     * @param  {object} pos - position for popup iframe [optional]
     * @param  {number} index - index of popup iframe [optional]
     */
    openPopup (title: string, width: number, height: number, url: string, pos?: object, index?: number): void
    
    /** 
     * open popup iframe, position and index are optional.
     * if position is empty, use center position of window.
     * if index is empty, use currentPopupIndex plus one[next popup]
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} title - popup window title
     * @param  {number} width - popup window width
     * @param  {number} height - popup window height
     * @param  {string} url - popup iframe url
     * @param  {object} pos - position for popup iframe [optional]
     * @param  {number} index - index of popup iframe [optional]
     */
    openPopupWithPos (title: string, width: number, height: number, url: string, pos?: object, index?: number): void
    
    /** 
     * get iframe window by index, if index is empty, use currentPopupIndex
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {number} index - index to identified iframe
     * @return  {window} iframe window
     */
    getIframeWin (index?: number): Window
    
    /** 
     * get parent iframe window
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @return  {window} iframe window
     */
    getParentWin (): Window
    
    /** 
     * search parent iframe window iteratively until find partial url string. 
     * if not found return null
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {string} partialUrl - partial url
     * @return  {window} iframe window
     */
    getParentWinByUrl (partialUrl: string): Window
    
    /** 
     * To Fix popup position when scroll window
     * 
     * @instance
     * @memberof com_yung_util_Popup
     * @param  {boolean} enableFixScorll - fix or not
     */
    setEnableFixScroll (enableFixScorll: boolean): void
    
    /** 
     * get com_yung_util_Popup global instance,
     * com_yung_util_Popup instance is global unique.
     * 
     * @param  {com_yung_util_Popup} popup - com_yung_util_Popup instance, for private use only
     * @return  {com_yung_util_Popup} com_yung_util_Popup instance
     */
    static instance (popup: com_yung_util_Popup): com_yung_util_Popup 
}