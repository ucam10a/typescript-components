/**
 * Smart Render Utility
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SmartRender = $Class.extend({
    
    classProp : {
        name : "com.yung.util.SmartRender"
    },
    
    /**
     * ratio upper bound to trigger append data
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    ratioUpper : 0.6, // recommend 0.6
    
    /**
     * ratio lower bound to trigger prepend data
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    ratioLower : 0.3, // recommend 0.3
    
    /**
     * Pre-loaded data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    preloadSize : 60, // recommend 60
    
    /**
     * buffer data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bufferSize : 20, // recommend 20
    
    /**
     * row number in window div
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winDataSize : 0,
    
    /**
     * window div height
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winHeight : 600,
    
    /**
     * window div width
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winWidth : 800,
    
    /**
     * display bar size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    barSize : 30,
    
    /**
     * bar boundary
     * @member {object}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bound : {},
    
    /**
     * total data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    dataSize : 0,
    
    /**
     * fix scroll top position, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    fixScrollTop : 0,
    
    /**
     * way direction, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    way : 0,
    
    /**
     * fix count, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    fixCnt : 0,
    
    /**
     * suspend flag
     * 
     * @private
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    suspend : false,
    
    /**
     * switch flag to turn off smart render
     * 
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    off : false,
    
    /**
     * top row index
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    topIdx : 0,
    
    /**
     * bottom row index
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bottomIdx : 0,
    
    /**
     * table content height
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentHeight : 0,
    
    /**
     * table content width
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentWidth : 0,
    
    /**
     * scroll record to detect scroll direction
     * 
     * @private
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    scrollPosList : null,
    
    /**
     * display window id
     * 
     * @member {string}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winId : null,
    winDiv : null,
    contentScrollDivId : null,
    
    /**
     * display content id
     * 
     * @member {string}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentDivId : null,
    tableId : null,
    tableCls : '',
    rowElementTypeName : 'div',
    
    /**
     * Callback for render row html
     *
     * @callback RowRenderCallback
     * @param {number} rowId - row id
     * @return {string} row HTML code
     */
    /**
     * Callback for render row html
     * @member {RowRenderCallback}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    rowRenderCallback : null,
    
    /**
     * constructor
     * @memberof com_yung_util_SmartRender
     * @param  {string} id - window id
     * @param  {string} contentId - content div id
     * @param  {number} dataSize - total data size
     * @param  {RowRenderCallback} rowRenderCallback - Callback for render row html
     * @param  {string} tableId - content table id [optional]
     * @param  {string} rowElementTypeName - row element type name [optional, default : 'tr']
     */
    init : function(id, contentId, dataSize, rowRenderCallback, tableId, rowElementTypeName) {
        if (id == null || id == '') {
            throw "id is empty";
        }
        if (_validType('string', id) == false) {
            throw "id is not string";
        }
        this.winId = id;
        this.contentScrollDivId = id;
        this.contentDivId = contentId;
        if (_validType('number', dataSize) == false) {
            throw "dataSize is not number";
        }
        this.dataSize = dataSize;
        if (typeof rowRenderCallback != 'function') {
            throw "rowRenderCallback is not function";
        }
        this.rowRenderCallback = rowRenderCallback;
        if (tableId != null && tableId != '') {
            this.tableId = tableId;
        }
        if (rowElementTypeName != null && rowElementTypeName != '') {
            this.rowElementTypeName = rowElementTypeName;
        }
        this.scrollPosList = new com.yung.util.List("number");
        this.winDiv = jQuery('#' + id);
        if (com_yung_util_getbrowser() == 'msie') {
        	this.winHeight = this.winDiv[0].offsetHeight;
        } else {
        	this.winHeight = this.winDiv.height();
        }
        this.winWidth = this.winDiv.width();
        this.creatDisplayBar();
        this.bindScroll();
        this.enableBarDrag();
        this.bound["maxX"] = 0;
        this.bound["minX"] = 0;
        this.bound["maxY"] = this.winHeight - this.barSize;
        this.bound["minY"] = 0;
        return this;
    },
    
    /** 
     * create display bar to cover original bar
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    creatDisplayBar : function () {
        var template = new com.yung.util.BasicTemplate();
        template.add('<div id="SmartRender-{{winId}}-BarContainer" style="position: absolute; left: 0px; top: 0px; width: {{barWidth}}px; height: {{winHeight}}px; z-index: 2000; background: rgb(241, 241, 241);" onclick="com_yung_util_SmartRender.scroll(this, \'' + this.contentScrollDivId + '\', event);" >');
        template.add('    <div id="SmartRender-{{winId}}-BarY" style="position: relative; left: 0px; top: 0px; width: {{barWidth}}px; height: {{barSize}}px; background: rgb(193, 193, 193);"></div>');
        template.add('</div>');
        var data = {};
        data["winId"] = this.winId;
        data["winHeight"] = this.winHeight;
        data["barSize"] = this.barSize;
        data["barWidth"] = com_yung_util_Position.getScrollbarWidth();
        var html = template.toHtml(data);
        jQuery("body").append(html);
        this.render();
    },
    
    /** 
     * render display bar
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    render : function () {
        if (this.off == true) {
            jQuery('#SmartRender-' + this.winId + '-BarContainer').css("display", "none");
            return;
        }
        var boundPos = new com.yung.util.Position(jQuery("#" + this.contentScrollDivId));
        var tlPos = boundPos.getTopLeftPosition();
        var brPos = boundPos.getBotRightPosition();
        jQuery('#SmartRender-' + this.winId + '-BarContainer').css("top", tlPos.top + "px");
        var diff = 1;
        if (com_yung_util_getbrowser() == 'msie') {
        	diff = diff - 2;
        }
        jQuery('#SmartRender-' + this.winId + '-BarContainer').css("left", (brPos.left - com_yung_util_Position.getScrollbarWidth() + diff) + "px");
        if (com_yung_util_getbrowser() == 'msie') {
        	this.winHeight = jQuery("#" + this.contentScrollDivId)[0].offsetHeight;
        } else {
        	this.winHeight = jQuery("#" + this.contentScrollDivId).height();
        }
        this.winWidth = jQuery("#" + this.contentScrollDivId).width();
        jQuery('#SmartRender-' + this.winId + '-BarContainer').css("display", "");
    },
    
    /** 
     * bind scroll event
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bindScroll : function () {
        var self = this;
        jQuery("#" + this.contentScrollDivId).on('scroll', function (evt) {
            if (self.off == true) return;
            evt.preventDefault();
            evt.stopPropagation();
            var sTop = jQuery(this).scrollTop();
            self.insertScroll(sTop);
            var direction = self.scrollDirection();
            var rowId = null;
            var diff = null;
            if (direction > 0 || direction < 0) {
                var posInfo = self.getWindowFirstRowId(sTop);
                rowId = posInfo["rowId"];
                diff = posInfo["diff"];
                var adjTop = rowId / (self.dataSize - self.winDataSize) * (self.winHeight - self.barSize);
                jQuery('#SmartRender-' + self.winId + '-BarY').css("top", adjTop + "px");
            }
            if (self.suspend == true) {
                return;
            }
            var ratio = sTop / self.contentHeight;
            // fix old IE scroll issue
            if (com_yung_util_getbrowser() == 'msie') {
                if (self.fixScrollTop > 0) {
                	if (self.fixCnt > 1) {
                        self.fixCnt = 0;
                        self.fixScrollTop = 0;
                        self.way = 0;
                    } else {
                        if (self.way > 0 && ratio < self.ratioUpper) {
                            self.fixCnt++;
                        } else if (self.way < 0 && ratio > self.ratioLower) {
                            self.fixCnt++;
                        } else {
                            self.fixCnt = 0;
                            jQuery(this).scrollTop(self.fixScrollTop);
                        }
                        return;
                    }
                }
            }
            if (direction > 0 && ratio > self.ratioUpper && self.bottomIdx < self.dataSize) {
                var newTop = self.loadNext(rowId);
                jQuery(this).scrollTop(newTop);
                self.fixScrollTop = newTop;
                self.way = 1;
            }
            if (direction < 0 && ratio < self.ratioLower && self.topIdx > 0) {
                var newTop = self.loadPrev(rowId);
                jQuery(this).scrollTop(newTop);
                self.fixScrollTop = newTop;
                self.way = -1;
            }
        });
    },
    
    /** 
     * enable display bar draggable
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    enableBarDrag : function () {
        var self = this;
        com_yung_util_Movable.enable('SmartRender-' + this.winId + '-BarY', true, false, this.bound, function (startX, startY, endX, endY) {
            if (self.off == true) return;
            var posTop = jQuery("#SmartRender-" + self.winId + "-BarY").css("top");
            posTop = _replaceAll(posTop, "px", "");
            posTop = posTop * 1.0;
            var rowId = parseInt(self.dataSize * (posTop - self.bound["minY"]) / (self.bound["maxY"] - self.bound["minY"]));
            if (rowId > self.dataSize) {
                rowId = self.dataSize;
            }
            var span = self.getSpan(rowId);
            self.topIdx = span[0]["start"];
            self.bottomIdx = span[span.length - 1]["end"];
            var html = self.toContentHtml(span);
            jQuery("#" + self.contentDivId).html(html);
            if (com_yung_util_getbrowser() == 'msie') {
            	self.contentHeight = jQuery('#' + self.contentDivId)[0].offsetHeight;
            } else {
            	self.contentHeight = jQuery('#' + self.contentDivId).height();
            }
            if (rowId == self.dataSize) {
                rowId = rowId - 1;
            }
            var position = new com.yung.util.Position(jQuery("#data-" + rowId));
            var pos = position.getTopLeftPosition(null, null, jQuery('#' + self.winId));
            jQuery('#' + self.winId).scrollTop(pos.top);
            if (com_yung_util_getbrowser() == 'msie') {
                self.fixCnt = 0;
                self.fixScrollTop = 0;
                self.way = 0;
            }
        });
    },
    getSpan : function (rowId) {
        var maxIdx = this.dataSize;
        var ret = [];
        var length = parseInt(this.preloadSize / this.bufferSize);
        var midStart = parseInt(rowId / this.bufferSize) * this.bufferSize;
        var midEnd = (parseInt(rowId / this.bufferSize) + 1) * this.bufferSize;
        if (midEnd >= maxIdx) {
            midEnd = maxIdx;
        }
        if (length % 2 == 0) {
            var expand = length / 2;
            for (var i = (expand - 1); i > 0; i--) {
                var obj = {};
                var start = midStart - (i * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart - ((i - 1) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] >= maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
            var temp = {};
            temp["start"] = midStart;
            temp["end"] = midEnd;
            ret.push(temp);
            for (var i = 0; i < expand; i++) {
                var obj = {};
                var start = midStart + ((i + 1) * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart + ((i + 2) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] > maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
        } else {
            var expand = parseInt(length / 2);
            for (var i = expand; i > 0; i--) {
                var obj = {};
                var start = midStart - (i * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart - ((i - 1) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] > maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
            var temp = {};
            temp["start"] = midStart;
            temp["end"] = midEnd;
            ret.push(temp);
            for (var i = 0; i < expand; i++) {
                var obj = {};
                var start = midStart + ((i + 1) * this.bufferSize);
                if (start > maxIdx) break;
                if (start < 0) break;
                var end = midStart + ((i + 2) * this.bufferSize);
                obj["start"] = start;
                obj["end"] = end;
                if (obj["end"] > maxIdx) {
                    obj["end"] = maxIdx;
                }
                ret.push(obj);
            }
        }
        return this.makeup(length, maxIdx, ret);
    },
    makeup : function (length, maxIdx, array) {
        if (array.length < length) {
            var plusSize = length - array.length;
            if (array[0]["start"] == 0) {
                var nextIdx = array[array.length - 1]["end"];
                for (var i = 0; i < plusSize; i++) {
                    var obj = {};
                    var start = nextIdx + i * this.bufferSize;
                    if (start > maxIdx) break;
                    if (start < 0) break;
                    var end = start + this.bufferSize;
                    obj["start"] = start;
                    obj["end"] = end;
                    if (obj["end"] > maxIdx) {
                        obj["end"] = maxIdx;
                    }
                    array.push(obj);
                }
            } else if (array[array.length - 1]["end"] == maxIdx) {
                var nextIdx = array[0]["start"];
                for (var i = 0; i < plusSize; i++) {
                    var obj = {};
                    var end = nextIdx - (i * this.bufferSize);
                    var start = end - this.bufferSize;
                    if (start > maxIdx) break;
                    if (start < 0) break;
                    obj["start"] = start;
                    obj["end"] = end;
                    if (obj["end"] > maxIdx) {
                        obj["end"] = maxIdx;
                    }
                    array.splice(0, 0, obj);
                }
            }
        }
        return array;
    },
    
    /** 
     * insert scroll position
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} sTop - scroll top position
     */
    insertScroll : function (sTop) {
        if (this.scrollPosList.size() < 3) {
            this.scrollPosList.add(sTop);
            return;
        } else {
            this.scrollPosList.removeByIndex(0);
            this.scrollPosList.add(sTop);
        }
    },
    
    /** 
     * return scroll direction
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} scroll direction, up : -1, down : 1, unknown : 0
     */
    scrollDirection : function () {
        if (this.scrollPosList.size() == 3) {
            if (this.scrollPosList.get(2) > this.scrollPosList.get(1) && this.scrollPosList.get(1) > this.scrollPosList.get(0)) {
                return 1;
            } else if (this.scrollPosList.get(2) < this.scrollPosList.get(1) && this.scrollPosList.get(1) < this.scrollPosList.get(0)) {
                return -1;
            }
        }
        return 0;
    },
    
    /** 
     * return window div first row id
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} row id
     */
    getWindowFirstRowId : function (sTop) {
        var ret = {};
        var height = 0;
        var found = false;
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        jQuery("#" + selector).children().each(function( index ) {
            if (found == true) return;
            var rowHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	rowHeight = this.offsetHeight;
            } else {
            	rowHeight = jQuery(this).height();
            }
            if (sTop < rowHeight) {
                ret["rowId"] = 0;
                ret["diff"] = sTop;
                found = true;
                return;
            }
            if ((height + rowHeight) > sTop) {
                ret["rowId"] = jQuery(this).attr("data-rowno");
                ret["diff"] = height + rowHeight - sTop;
                found = true;
                return;
            }
            height = height + rowHeight;
        });
        if (ret["rowId"] > (this.dataSize - this.winDataSize)) {
            ret["rowId"] = (this.dataSize - this.winDataSize);
        }
        return ret;
    },
    
    /** 
     * load next buffer
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} new scroll top position
     */
    loadNext : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.bottomIdx;
        this.bottomIdx = this.bottomIdx + this.bufferSize;
        if (this.bottomIdx > this.dataSize) {
            this.bottomIdx = this.dataSize;
        }
        var end = this.bottomIdx;
        var html = this.getHtml(start, end);
        var contentId = this.contentDivId;
        if (this.tableId != null) {
            contentId = this.tableId;
        }
        jQuery('#' + contentId).append(html);
        this.removeTop();
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.topIdx = this.topIdx + this.bufferSize;
        this.scrollPosList.clear();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        this.suspend = false;
        return pos.top;
    },
    removeTop : function () {
        var cnt = 0;
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        var self = this;
        jQuery("#" + selector).children().each(function( index ) {
            if (cnt >= self.bufferSize) return;
            jQuery(this).remove();
            cnt++;
        });
    },
    
    /** 
     * load previous buffer
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} new scroll top position
     */
    loadPrev : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.topIdx - this.bufferSize;
        if (start < 0) {
            start = 0;
        }
        var end = this.topIdx;
        this.topIdx = this.topIdx - this.bufferSize;
        if (this.topIdx < 0) {
            this.topIdx = 0;
        }
        var html = this.getHtml(start, end);
        var contentId = this.contentDivId;
        if (this.tableId != null) {
            contentId = this.tableId;
        }
        jQuery('#' + contentId).prepend(html);
        this.removeBottom();
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        if (this.bottomIdx % this.bufferSize == 0) {
            this.bottomIdx = this.bottomIdx - this.bufferSize;
        } else {
            this.bottomIdx = parseInt(this.bottomIdx / this.bufferSize) * this.bufferSize;
        }
        this.scrollPosList.clear();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        this.suspend = false;
        return pos.top;
    },
    removeBottom : function () {
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        var length = jQuery("#" + selector).children().length;
        var maxIdx = length - this.bufferSize;
        if (length % this.bufferSize != 0) {
            maxIdx = parseInt(length / this.bufferSize) * this.bufferSize;
        }
        jQuery("#" + selector).children().each(function( index ) {
            if (index >= maxIdx) {
                jQuery(this).remove();
            }
        });
    },
    
    /** 
     * get HTML code from start and end row id
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} start - start row id
     * @param {number} end - end row id
     * @return {string} html code
     */
    getHtml : function (start, end) {
        var span = this.split(start, end);
        return this.toRowHtml(span);
    },
    
    /** 
     * get HTML code from span array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {Array} span - span array
     * @return {string} html code
     */
    toRowHtml : function (span) {
        var html = "";
        for (var i = 0; i < span.length; i++) {
            var begin = span[i]["start"];
            var finish = span[i]["end"];
            for (var j = begin; j < finish; j++) {
                html = html + this.getRowHtml(j);
            }
        }
        return html;
    },
    
    /** 
     * split start row id and end row id to array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} start - start row id
     * @param {number} end - end row id
     * @return {Array} span array
     */
    split : function (start, end) {
        var length = (end - start) / this.bufferSize;
        var mod = (end - start) % this.bufferSize;
        if (mod != 0) {
            length = length + 1;
        }
        var ret = [];
        for (var i = 0; i < length; i++) {
            var obj = {};
            obj["start"] = i * this.bufferSize + start;
            obj["end"] = (i + 1) * this.bufferSize + start;
            if (obj["end"] > end) {
                obj["end"] = end;
            }
            ret.push(obj);
        }
        return ret;
    },
    
    /** 
     * convert span array to HTML code
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {Array} span - span array
     * @return {string} HTML code
     */
    toContentHtml : function (span) {
        var html = this.toRowHtml(span);
        if (this.tableId != null) {
            html = "<table id='" + this.tableId + "' class='" + this.tableCls + "'>" + html + "</table>";
        }
        return html;
    },
    
    /** 
     * preload content html
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    preload : function () {
        if (this.off == true) return;
        this.bottomIdx = this.topIdx + this.preloadSize;
        if (this.bottomIdx >= this.dataSize) {
            this.bottomIdx = this.dataSize;
        }
        var span = this.split(this.topIdx, this.bottomIdx);
        var html = this.toContentHtml(span);
        jQuery("#" + this.contentDivId).html(html);
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        var found = false;
        var height = 0;
        var selector = "";
        if (this.tableId != null) {
            selector = selector + this.tableId + " tbody";
        } else {
            selector = selector + this.contentDivId;
        }
        var self = this;
        jQuery("#" + selector).children().each(function( index ) {
            if (found == true) return;
            var rowHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	rowHeight = this.offsetHeight;
            } else {
            	rowHeight = jQuery(this).height();
            }
            if ((height + rowHeight) > self.winHeight) {
                if (self.winDataSize < (index + 1)) {
                    self.winDataSize = index + 1;
                }
                found = true;
                return;
            }
            height = height + rowHeight;
        });
        this.adjustBuffer();
    },
    
    /** 
     * adjust buffer and preload size to keep better performance
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    adjustBuffer : function () {
        if ((this.bufferSize / this.winDataSize) < 1.5) {
            // adjust buffer
            while ((this.bufferSize / this.winDataSize) < 1.5) {
                this.bufferSize = this.bufferSize + 10;
            }
            this.preloadSize = this.bufferSize * 3;
            this.preload();
        }
    },
    
    /** 
     * get row HTML code by row id
     * Note: override method or use callback
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} rowId - row id
     * @return {string} HTML code
     */
    getRowHtml : function (rowId) {
        return this.rowRenderCallback(rowId);
    }
    
});

com_yung_util_SmartRender.scroll = function (container, contentScrollDivId, event) {
    event.preventDefault();
    event.stopPropagation();
    var barY = jQuery(container).children();
    var barSize = barY.height();
    var position = new com.yung.util.Position(barY);
    var baseTop = position.getBaseTop();
    var clickY = baseTop + event.clientY;
    var pos = position.getTopLeftPosition();
    if (clickY < pos.top) {
        var sTop = jQuery("#" + contentScrollDivId).scrollTop();
        jQuery("#" + contentScrollDivId).scrollTop(sTop - barSize);
    } else if (clickY > (pos.top + barSize)) {
        var sTop = jQuery("#" + contentScrollDivId).scrollTop();
        jQuery("#" + contentScrollDivId).scrollTop(sTop + barSize);
    }
}

/**
 * Table Utility Schema Data Object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TableGridSchema = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.TableGridSchema",
        typeArray : ['ro','edtxt','ed','edn','co','select','ch', 'date'],
        dataTypeArray : ['string','number','date']
    },
    
    /**
     * td align
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    align : "center",
    
    /**
     * td vertical align
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    vAlign : "",
    
    /**
     * allow null value
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    allowNull : true,
    
    /**
     * column color code
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnColor : "",
    
    /**
     * column hidden
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnHidden : false,
    
    /**
     * column id
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnId : "",
    
    /**
     * column validator, a drop list to select
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnValidator : [],
    
    /**
     * column combo map, a drop key/value to select
     * @member {object}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    comboMap : {},
    
    /**
     * date format, ex. MM/dd/yyyy
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    dateFormat : "MM/dd/yyyy",
    
    /**
     * column header style
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    headStyle : "",
    
    /**
     * column header text
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    header : "",
    
    /**
     * column index
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    index : 0,
    
    /**
     * column type
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    type : "ro",
    
    /**
     * column width
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    width : "80",
    
    /**
     * column data type
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    dataType : "string",
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} columnId - column id
     * @param {string} width - column width
     * @param {string} header - column header text
     */
    init: function(columnId, width, header){
        if (_validType('string', columnId) == false) {
            throw "columnId is not string";
        }
        this.columnId = columnId;
        if (width != null) {
            this.setWidth(width);
        }
        if (header != null) {
            this.setHeader(header);
        }
        return this;
    },
    
    /** 
     * set property value
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} attribute - column id
     * @param {all} value - value
     */
    setProperty : function (attribute, value) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "set" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            this[method](value);
        }
    },
    
    /** 
     * get property value
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} attribute - column id
     * @return {all} value
     */
    getProperty : function (attribute) {
        if (_validType('string', attribute) == false) {
            throw "attribute is not string";
        }
        var ch = attribute.charAt(0) + "";
        var method = "get" + ch.toUpperCase() + attribute.substring(1);
        if (typeof this[method] == 'function') {
            return this[method]();
        }
        return null;
    },
    
    setAlign : function (align) {
        if (_validType('string', align) == false) {
            throw "align is not string";
        }
        if (align != 'left' && align != 'center' && align != 'right') {
            throw "unknown vAlign: " + vAlign;
        }
        this.align = align;
    },

    getAlign : function () {
        return this.align;
    },

    setVAlign : function (vAlign) {
        if (_validType('string', vAlign) == false) {
            throw "vAlign is not string";
        }
        if (vAlign != 'top' && vAlign != 'middle' && vAlign != 'bottom') {
            throw "unknown vAlign: " + vAlign;
        }
        this.vAlign = vAlign;
    },

    getVAlign : function () {
        return this.vAlign;
    },

    setAllowNull : function (allowNull) {
        if (allowNull == true) {
            this.allowNull = true;
        } else {
            this.allowNull = false;
        }
    },

    getAllowNull : function () {
        return this.allowNull;
    },

    setColumnColor : function (columnColor) {
        if (_validType('string', columnColor) == false) {
            throw "columnColor is not string";
        }
        this.columnColor = columnColor;
    },

    getColumnColor : function () {
        return this.columnColor;
    },

    setColumnHidden : function (columnHidden) {
        if (columnHidden == true) {
            this.columnHidden = true;
        } else {
            this.columnHidden = false;
        }
    },

    getColumnHidden : function () {
        return this.columnHidden;
    },

    setColumnId : function (columnId) {
        if (_validType('string', columnId) == false) {
            throw "columnId is not string";
        }
        this.columnId = columnId;
    },

    getColumnId : function () {
        return this.columnId;
    },

    setColumnValidator : function (columnValidator) {
        if (_validType(Array, columnValidator) == false) {
            throw "columnValidator is not Array";
        }
        this.columnValidator = columnValidator;
    },

    getColumnValidator : function () {
        return this.columnValidator;
    },

    setComboMap : function (comboMap) {
        if (_validType('object', comboMap) == false) {
            throw "comboMap is not object";
        }
        this.comboMap = comboMap;
    },

    getComboMap : function () {
        return this.comboMap;
    },

    setDateFormat : function (dateFormat) {
        if (_validType('string', dateFormat) == false) {
            throw "dateFormat is not string";
        }
        this.dateFormat = dateFormat;
    },

    getDateFormat : function () {
        return this.dateFormat;
    },

    setHeadStyle : function (headStyle) {
        if (_validType('string', headStyle) == false) {
            throw "headStyle is not string";
        }
        this.headStyle = headStyle;
    },

    getHeadStyle : function () {
        return this.headStyle;
    },

    setHeader : function (header) {
        if (_validType('string', header) == false) {
            throw "header is not string";
        }
        this.header = header;
    },

    getHeader : function () {
        return this.header;
    },

    setIndex : function (index) {
        if (_validType('number', index) == false) {
            throw "index is not number";
        }
        index = parseInt(index);
        this.index = index;
    },

    getIndex : function () {
        return this.index;
    },

    setType : function (type) {
        if (jQuery.inArray(type, this.classProp.typeArray) < 0) {
            throw "unknown type: " + type;
        }
        this.type = type;
    },

    getType : function () {
        return this.type;
    },
    
    setDataType : function (dataType) {
        if (jQuery.inArray(dataType, this.classProp.dataTypeArray) < 0) {
            throw "unknown dataType: " + dataType;
        }
        this.dataType = dataType;
    },

    getDataType : function () {
        return this.dataType;
    },

    setWidth : function (width) {
        if (jQuery.isNumeric(width) == false) {
            throw "width is not number";
        }
        this.width = width;
    },

    getWidth : function () {
        return this.width;
    }
    
});
com_yung_util_TableGridSchema.convertToLZString = function (SchemaArray) {
    if (_validType(Array, SchemaArray) == false) {
        throw "SchemaArray is not Array";
    }
    if (SchemaArray.length > 0) {
        if (_validType(com_yung_util_TableGridSchema, SchemaArray[0]) == false) {
            throw "SchemaArray element is not com.yung.util.TableGridSchema";
        }
    }
    // reset index and check columnId
    var id = [];
    for (var i = 0; i < SchemaArray.length; i++) {
        if (jQuery.inArray(SchemaArray[i].columnId, id) < 0) {
            id.push(SchemaArray[i].columnId);
        } else {
            throw "columnId: " + SchemaArray[i].columnId + " duplicate";
        }
        SchemaArray[i].setIndex(i);
    }
    var columnData = {};
    columnData["columns"] = SchemaArray;
    var json = JSON.stringify(columnData);
    return com.yung.util.LZString.encode(json);
}

/**
 * Column Definition of Table Utility 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ColumnUtil = $Class.extend({
    
    classProp : { name : "com.yung.util.ColumnUtil" },
    
    /**
     * column schema array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_ColumnUtil
     */
    columnArray: null,
    columnMap : null,
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} lzColumnInfo - column information encoded to LZString 
     */
    init: function(lzColumnInfo){
        var json = com.yung.util.LZString.decode(lzColumnInfo);
        var columnInfo = JSON.parse(json);
        this.columnMap = new Object();
        this.columnArray = columnInfo.columns;
        for (var i = 0; i < this.columnArray.length; i++) {
            this.columnMap['' + this.columnArray[i].columnId] = this.columnArray[i];
        }
        return this;
    },
    
    /** 
     * get column array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @return {Array} column array
     */
    getColumnArray : function() {
        return this.columnArray;
    },
    
    /** 
     * get column schema
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @return {object} column schema object
     */
    getColumn : function (columnId){
        return this.columnMap[columnId];
    },
    
    /** 
     * get column index
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @return {number} column index
     */
    getColumnIndex : function (columnId){
        var column = this.columnMap[columnId];
        if (column != null) {
            return column.index;
        } else {
            return null;
        }
    },
    
    /** 
     * get column attribute value
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @param {string} attribute - column attribute
     * @return {all} column attribute value
     */
    getColumnAttribute : function (columnId, attribute){
        var column = this.getColumn(columnId);
        if (column == null) {
            return null;
        }
        return column[attribute];
    },
    
    /** 
     * get column attribute value string array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} attribute - column attribute
     * @return {string} array of column attribute value separate by ','
     */
    getColumnAttrArrayString : function (attribute){
        var result = '';
        for (var i = 0; i < this.columnArray.length; i++) {
            var column = this.columnArray[i];
            result = result + column[attribute];
            if (i != (this.columnArray.length - 1)) {
                result = result + ',';
            }
        }
        return result;
    },
    
    /** 
     * get column attribute array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} attribute - column attribute
     * @return {Array} array of column attribute value
     */
    getColumnAttrArray : function (attribute){
        var result = [];
        for (var i = 0; i < this.columnArray.length; i++) {
            var column = this.columnArray[i];
            result.push(column[attribute]);
        }
        return result;
    }
});

// specific smart render for table utility
var com_yung_util_GridSmartRender = com_yung_util_SmartRender.extend({
    
    classProp : {
        name : "com.yung.util.GridSmartRender"
    },
    
    grid : null,
    rowElementTypeName : 'tr',
    off : true,
    
    init : function(grid) {
        this.grid = grid;
        this.winId = grid.gridDivId;
        this.contentScrollDivId = grid.gridDivId + "-contentScrollDiv";
        this.contentDivId = grid.gridDivId + "-contentDiv";
        this.tableId = grid.gridDivId + "-contentTable";
        this.scrollPosList = new com.yung.util.List("number");
        this.winDiv = jQuery('#' + this.winId);
        if (com_yung_util_getbrowser() == 'msie') {
        	this.winHeight = jQuery('#' + this.contentScrollDivId)[0].offsetHeight;	
        } else {
        	this.winHeight = jQuery('#' + this.contentScrollDivId).height();
        }
        this.winWidth = jQuery('#' + this.contentScrollDivId).width();
        this.creatDisplayBar();
        this.bindScroll();
        this.enableBarDrag();
        this.bound["maxX"] = 0;
        this.bound["minX"] = 0;
        this.bound["maxY"] = this.winHeight - this.barSize;
        this.bound["minY"] = 0;
        if (com_yung_util_getbrowser() == 'msie') {
        	this.preloadSize = 40;
        	this.bufferSize = 10;
        }
        return this;
    },
    
    enableBarDrag : function () {
        var self = this;
        com_yung_util_Movable.enable('SmartRender-' + this.winId + '-BarY', true, false, this.bound, function (startX, startY, endX, endY) {
            if (self.off == true) return;
            var posTop = jQuery("#SmartRender-" + self.winId + "-BarY").css("top");
            posTop = _replaceAll(posTop, "px", "");
            posTop = posTop * 1.0;
            var rowId = parseInt(self.dataSize * (posTop - self.bound["minY"]) / (self.bound["maxY"] - self.bound["minY"]));
            if (rowId > self.dataSize) {
                rowId = self.dataSize;
            }
            var span = self.getSpan(rowId);
            self.topIdx = span[0]["start"];
            self.bottomIdx = span[span.length - 1]["end"];
            var html = self.toContentHtml(span);
            jQuery("#" + self.contentDivId).html(html);
            // grid render
            self.grid.render();
            if (rowId == self.dataSize) {
                rowId = rowId - 1;
            }
            var position = new com.yung.util.Position(jQuery("#data-" + rowId));
            var pos = position.getTopLeftPosition(null, null, jQuery('#' + self.contentScrollDivId));
            var headerHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	headerHeight = jQuery('#' + self.tableId + " tr:first")[0].offsetHeight;
            } else {
            	headerHeight = jQuery('#' + self.tableId + " tr:first").height();
            }
            jQuery('#' + self.contentScrollDivId).scrollTop(pos.top - headerHeight);
            if (com_yung_util_getbrowser() == 'msie') {
            	self.contentHeight = jQuery('#' + self.contentDivId)[0].offsetHeight;
            } else {
            	self.contentHeight = jQuery('#' + self.contentDivId).height();
            }
            if (com_yung_util_getbrowser() == 'msie') {
                self.fixCnt = 0;
                self.fixScrollTop = 0;
                self.way = 0;
            }
        });
    },
    
    getRowHtml : function (rowId) {
        var rowIdx = this.grid.rowOrderList.get(rowId);
        var rowData = this.grid.cacheRowMap.get(rowIdx);
        var columnArray = this.grid.columnUtil.getColumnArray();
        var html = "";
        html = html + "<tr id='data-" + rowId + "' data-rowNo='" + rowId + "' class='" + this.grid.rowCls + "' >";
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            var content = rowData[column.columnId];
            if (content == null) {
                content = '';
            }
            var comment = this.grid.getComment(rowIdx, column.columnId);
            if (comment != null && comment != '') {
                content = "<img id='" + this.grid.gridDivId + "-comment-" + rowIdx + "-" + c + "' src='" + this.grid.classProp.commentBase64 + "' width='12' style='position: absolute; top: 0px; left: 0px;' title='" + comment + "' />" + content;
            }
            var clickFunt = '';
            if ('edtxt' == column.type || 'ed' == column.type || 'edn' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
            } else if ('co' == column.type || 'select' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
            } else if ('ch' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
                if (content == '1' || content == 1) {
                    content = '<input type="checkbox" checked />';
                } else {
                    content = '<input type="checkbox" />'
                }
            } else if ('date' == column.type) {
                clickFunt = "fireInstanceMethod('" + this.grid.hashId + "', 'edit', '" + this.grid.gridDivId + "-" + rowIdx + "-" + c + "');";
            } 
            html = html + '<td id="' + this.grid.gridDivId + '-' + rowIdx + '-' + c + '" data-columnId="' + column.columnId + '" class="' + this.grid.cellCls + '" width="' + column.width + '" align="' + column.align + '" style="position: relative; background-clip: padding-box;" onclick="' + clickFunt + '" >' + content + '</td>';
        }
        html = html + "</tr>";
        return html;
    },
    
    preload : function () {
        if (this.off == true) return;
        this.bottomIdx = this.topIdx + this.preloadSize;
        if (this.bottomIdx >= this.dataSize) {
            this.bottomIdx = this.dataSize;
        }
        var span = this.split(this.topIdx, this.bottomIdx);
        var html = this.toContentHtml(span);
        jQuery("#" + this.contentDivId).html(html);
        // grid render
        this.grid.render();
        var found = false;
        var height = 0;
        var selector = this.tableId;
        selector = selector + " " + this.rowElementTypeName;
        var self = this;
        jQuery("#" + selector).each(function( index ) {
            if (found == true) return;
            var rowHeight = 0;
            if (com_yung_util_getbrowser() == 'msie') {
            	rowHeight = jQuery(this)[0].offsetHeight;
            } else {
            	rowHeight = jQuery(this).height();
            }
            if ((height + rowHeight) > self.winHeight) {
                if (self.winDataSize < (index + 1)) {
                    self.winDataSize = index + 1;
                }
                found = true;
                return;
            }
            height = height + rowHeight;
        });
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.render();
        this.adjustBuffer();
    },
    
    loadNext : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.bottomIdx;
        this.bottomIdx = this.bottomIdx + this.bufferSize;
        if (this.bottomIdx > this.dataSize) {
            this.bottomIdx = this.dataSize;
        }
        var end = this.bottomIdx;
        var html = this.getHtml(start, end);
        jQuery('#' + this.tableId).append(html);
        this.removeTop();
        this.topIdx = this.topIdx + this.bufferSize;
        this.scrollPosList.clear();
        // grid render
        this.grid.render();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.suspend = false;
        return pos.top;
    },
    
    loadPrev : function (rowId) {
        if (this.suspend == true) {
            return;
        }
        this.suspend = true;
        var start = this.topIdx - this.bufferSize;
        if (start < 0) {
            start = 0;
        }
        var end = this.topIdx;
        this.topIdx = this.topIdx - this.bufferSize;
        if (this.topIdx < 0) {
            this.topIdx = 0;
        }
        var html = this.getHtml(start, end);
        // remove header first
        jQuery('#' + this.tableId + " tr:first").remove();
        // add rows
        jQuery('#' + this.tableId).prepend(html);
        // prepend header 
        var headerHtml = this.grid.getHeaderHtml();
        jQuery('#' + this.tableId).prepend(headerHtml);
        this.removeBottom();
        if (this.bottomIdx % this.bufferSize == 0) {
            this.bottomIdx = this.bottomIdx - this.bufferSize;
        } else {
            this.bottomIdx = parseInt(this.bottomIdx / this.bufferSize) * this.bufferSize;
        }
        this.scrollPosList.clear();
        // grid render
        this.grid.render();
        var position = new com.yung.util.Position(jQuery("#data-" + rowId));
        var pos = position.getBotLeftPosition(null, null, jQuery('#' + this.contentScrollDivId));
        if (com_yung_util_getbrowser() == 'msie') {
        	this.contentHeight = jQuery('#' + this.contentDivId)[0].offsetHeight;
        } else {
        	this.contentHeight = jQuery('#' + this.contentDivId).height();
        }
        this.suspend = false;
        return pos.top;
    },
    
    toContentHtml : function (span) {
        var html = this.toRowHtml(span);
        var headerHtml = this.grid.getHeaderHtml();
        html = "<table id='" + this.tableId + "' class='" + this.grid.gridCls + "'>" + headerHtml + html + "</table>";
        return html;
    },
    
    removeTop : function () {
        var cnt = 0;
        var selector = this.tableId + " ." + this.grid.rowCls;
        var self = this;
        jQuery("#" + selector).each(function( index ) {
            if (cnt >= self.bufferSize) return;
            jQuery(this).remove();
            cnt++;
        });
    },
    
    removeBottom : function () {
        var selector = this.tableId + " ." + this.grid.rowCls;
        var length = jQuery("#" + selector).length;
        var maxIdx = length - this.bufferSize;
        if (length % this.bufferSize != 0) {
            maxIdx = parseInt(length / this.bufferSize) * this.bufferSize;
        }
        jQuery("#" + selector).each(function( index ) {
            if (index >= maxIdx) {
                jQuery(this).remove();
            }
        });
    }
    
});

/**
 * Table Utility 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TableGridUtil = $Class.extend({
    classProp : { 
        name : "com.yung.util.TableGridUtil",
        commentBase64 : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTZEaa/1AAABVElEQVRoQ+2RW2rDQAxFB+KfFEPJn7eTTXT/O0mn+Iq6J03ixzwkyIHDkHgk3UHpltItssngh0h+XS75EPwYRcX/hRci+GcTBi95V7Hv4UXPXs/nfDyAl72quI9hgUc/T6d8vIBF3lTM17DQk6s2YbDYi4q3Hjbw4EdW8dbDJr1VrO2wUU93bcJgs14qzn7YsIeHNmGwaWsV4zhs3NIimzDYvJUaXw4OaGHRTRgcUluNLQ8H1bTKJgwOq6XG1YMDa1h1EwaHllZj6sPBJW2yCYPDS6n27WCAEjbdhMEQR1Xb9jDIEbtswmCYvapdPxhoj103YTDUVtWmPwy2xbncCctgW1S5HxhwjXOZM5YB16gyfzDoM+frTlkGfaau+4WB/3MYhnw4h6GprvmHwZeG2ITB8KY+x4EP+HEcx3wEg4/Q3/FYPmKapvgP0c+4hN/EG5ek9A0zMUHnKiaJVAAAAABJRU5ErkJggg==",
        filterPng : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABZ0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMDvo9WkAAABiSURBVChTYwCCx0D8Hwe+DcQo4AUQoyt6BMRYAbJinIpgAKaQICBKIRMQE6WQC4g/QWmCwBCImZydnS8yMzODTWdnZ/8fHh5+HCyLDfDw8FxmYmL6LyoquhgqhBdch1AMDAA/siKyg6ZPUQAAAABJRU5ErkJggg==",
        selectPng : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwQAADsEBuJFr7QAAABZ0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMDvo9WkAAABUSURBVChTYwCC/0RiCEEAkKGwoaHhPzYMkgMCiMK8vDwQAysuLS2FsSGEn58fXBKGg4ODwXIgNpwAAagEuiIQQFUIAiBnQJ2CDDAV4gBwhQQww38ALWdP1YmBkgUAAAAASUVORK5CYII="
    },
    gridDivId : null,
    hashId : null,
    columnUtil : null,
    errorCells : null,
    dirtyRowMap : null,
    gridStyle : null,
    headerCls : null,
    rowCls : null,
    cellCls : null,
    tableTemplate : null,
    headerTemplate : null,
    siderHeaderTemplate : null,
    cacheRowMap : null,
    rowOrderList : null,
    commentMap : null,
    paging : false,
    currentPageIndex : null,
    tableAlign : null,
    smartRender : null,
    combo : null,
    datePicker : null,
    filter : false,
    
    /**
     * Callback after grid edit value
     *
     * @callback AfterGridEdit
     * @param {jQueryEle} ele - jQuery table cell element
     * @param {string} rowIdx - row id string
     * @param {number} colIdx - column index
     * @param {com_yung_util_TableGridUtil} grid - this table grid
     * @param {string} newContent - new content for after edit
     */
    /**
     * Callback after grid edit value
     * @member {AfterGridEdit}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    afterEdit : null,
    
    /**
     * Callback before grid edit value. return false will block edit
     *
     * @callback BeforeGridEdit
     * @param {jQueryEle} ele - jQuery table cell element
     * @param {string} rowIdx - row id string
     * @param {number} colIdx - column index
     * @param {com_yung_util_TableGridUtil} grid - this table grid
     * @param {string} oldContent - old content for before edit
     */
    /**
     * Callback before grid edit value
     * @member {BeforeGridEdit}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    beforeEdit : null,
    
    /**
     * paging size, if null for not paging
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    pageSize : null,
    
    /**
     * border width size
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    borderWidth : 0,
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} gridDivId - grid div id
     * @param {com_yung_util_ColumnUtil} columnUtil - column information schema utility
     * @param {string} gridCls - grid CSS name, default 'yung-tableborder'
     * @param {string} gridStyle - grid style, default 'table-layout:fixed; word-break:break-all;'
     * @param {string} headerCls - grid header CSS name, default 'yung-tableHeader'
     * @param {string} cellCls - grid cell CSS name, default 'yung-tableCell'
     * @param {string} tableAlign - grid align, default left
     * @param {string} rowCls - grid row CSS name, default 'yung-tableRow'
     */
    init: function(gridDivId, columnUtil, gridCls, gridStyle, headerCls, cellCls, tableAlign, rowCls){
        $Class.validate(this.classProp.name, gridDivId);
        if (columnUtil == null || gridDivId == null || gridDivId == '') {
            alert("Please define columnUtil and gridDivId!, see yung-TableGridUtil.js.");
        }
        this.gridDivId = gridDivId;
        jQuery("#" + this.gridDivId).css("position", "relative");
        this.hashId = MD5Util.calc(this.classProp.name + "-" + gridDivId);
        this.beforeEdit = null;
        this.afterEdit = null;
        this.columnUtil = columnUtil;
        this.errorCells = new com.yung.util.ObjectHashSet(com.yung.util.TableErrorCell);
        this.dirtyRowMap = new com.yung.util.Map('string', 'string');
        this.cacheRowMap = new com.yung.util.Map('string', 'object');
        this.rowOrderList = new com.yung.util.BasicList('string');
        this.commentMap = new com.yung.util.Map('string', com.yung.util.MapInterface);
        var columnArray = columnUtil.getColumnArray();
        if (gridCls == null || gridCls == '') {
            this.gridCls = "yung-tableborder";
        } else {
            this.gridCls = gridCls;
        }
        if (gridStyle == null || gridStyle == '') {
            this.gridStyle = "table-layout:fixed; word-break:break-all;";
        } else {
            this.gridStyle = gridStyle;
        }
        if (headerCls == null || headerCls == '') {
            this.headerCls = "yung-tableHeader";
        } else {
            this.headerCls = headerCls;
        }
        if (rowCls == null || rowCls == '') {
            this.rowCls = "yung-tableRow";
        } else {
            this.rowCls = rowCls;
        }
        if (cellCls == null || cellCls == '') {
            this.cellCls = "yung-tableCell";
        } else {
            this.cellCls = cellCls;
        }
        if (tableAlign == null || tableAlign == '') {
            this.tableAlign = "left";
        } else {
            this.tableAlign = tableAlign;
        }
        this.tableTemplate = new com.yung.util.BasicTemplate();
        this.headerTemplate = new com.yung.util.BasicTemplate();
        this.siderHeaderTemplate = new com.yung.util.BasicTemplate();
        this.createGrid();
        this.smartRender = new com.yung.util.GridSmartRender(this);
        var self = this;
        setTimeout(function () {
            self.render();
        }, 500);
        return this;
    },
    
    /** 
     * set after grid edit callback
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {AfterGridEdit} funct - after grid edit callback
     */
    setAfterEdit : function (funct) {
        if (typeof funct == 'function') {
            this.afterEdit = funct;
        }
    },
    
    /** 
     * set before grid edit callback
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {BeforeGridEdit} funct - before grid edit callback
     */
    setBeforeEdit : function (funct) {
        if (typeof funct == 'function') {
            this.beforeEdit = funct;
        }
    },
    
    /** 
     * set paging size
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} pageSize - paging size
     */
    setPageSize : function (pageSize) {
        this.pageSize = pageSize * 1.0;
        this.paging = true;
        this.refresh();
    },
    createGrid : function() {
    	if (this.paging == true) {
    		var pageHtml = this.getPageHtml();
            jQuery("#" + this.gridDivId).html('<div id="' + this.gridDivId + '-pageDiv">' + pageHtml + '</div><div id="' + this.gridDivId + '-tableDiv"></div>');
            this.loadPage(0);
        } else {
        	this.setupTableTemplate();
            jQuery("#" + this.gridDivId).html(this.tableTemplate.toHtml());	
            var headerHtml = this.getHeaderHtml();
            jQuery("#" + this.gridDivId + "-headerDiv").html('<table class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + '</table>');
            
            
            
            // TODO
            var siderHeaderHtml = this.getSiderHeaderHtml();
            var siderRowHtml = this.renderSiderRows([]);
        	jQuery("#" + this.gridDivId + "-siderDiv").html('<table class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + siderHeaderHtml + siderRowHtml + '</table>');
            
            
            
            
            
            var rowHtml = this.renderRows([]);
        	jQuery("#" + this.gridDivId + "-contentDiv").html('<table id="' + this.gridDivId + '-contentTable" class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + rowHtml + '</table>');
        	var grid = this;
        	jQuery('#' + this.gridDivId + '-contentScrollDiv').on('scroll', function () {
        	    var contentLeft = jQuery(this).scrollLeft();
        	    if (com_yung_util_TableGridUtil.fireScrollManually == true) {
        	        com_yung_util_TableGridUtil.fireScrollManually = false;
        	        return;
        	    }
        	    var txtAreaEdit = jQuery("#" + grid.gridDivId + "-editTxt");
        		var selectDiv = jQuery("#" + grid.gridDivId + "-selectDiv");
        		var txtAreaDatePciker = jQuery("#" + grid.gridDivId + "-datePicker");
        		txtAreaEdit.css("display", "none");
        		selectDiv.css("display", "none");
        		txtAreaDatePciker.css("display", "none");
        		grid.datePicker.floatDiv.showFloatDiv(false);
        		jQuery('#' + grid.gridDivId + '-headerScrollDiv').scrollLeft(contentLeft);
        	});
        }
    	this.createTextEditor();
    	this.createSelectEditor();
    	this.createDatePicker();
    },
    
    /** 
     * hide grid page bar
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    hidePageHtml : function () {
    	jQuery("#" + this.gridDivId + '-pageDiv').css("display", "none");
    },
    
    /** 
     * show grid page bar
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    showPageHtml : function () {
    	jQuery("#" + this.gridDivId + '-pageDiv').css("display", "");
    },
    getHeaderHtml : function () {
    	var html = this.headerTemplate.toHtml();
        if (html == '') {
            var columnArray = this.columnUtil.getColumnArray();
            this.headerTemplate.add('<tr>');
            for (var i = 0; i < columnArray.length; i++) {
                var column = columnArray[i];
                var filterHtml = "";
                if (this.filter == true) {
                    filterHtml = ' <img id="' + this.gridDivId + '-filter-' + i + '" src="' + this.classProp.selectPng + '" width="8" title="filter" style="cursor: pointer;" onclick="com_yung_util_TableGridFilter.instance(\'' + this.gridDivId + '\').showFilter(' + i + ', this)" />';
                }
                this.headerTemplate.add('<td class="' + this.headerCls + '" data-columnId="' + column.columnId + '" width="' + column.width + '" align="center" >' + column.header + filterHtml + '</td>');
            }
            this.headerTemplate.add('</tr>');
        }
        return this.headerTemplate.toHtml();
    },
    
    
    
    // TODO
    getSiderHeaderHtml : function () {
    	var html = this.siderHeaderTemplate.toHtml();
        if (html == '') {
            var columnArray = this.columnUtil.getColumnArray();
            this.siderHeaderTemplate.add('<tr>');
            var column = columnArray[1];
            var filterHtml = "";
            if (this.filter == true) {
                filterHtml = ' <img id="' + this.gridDivId + '-filter-' + i + '" src="' + this.classProp.selectPng + '" width="8" title="filter" style="cursor: pointer;" onclick="com_yung_util_TableGridFilter.instance(\'' + this.gridDivId + '\').showFilter(' + i + ', this)" />';
            }
            this.siderHeaderTemplate.add('<td class="' + this.headerCls + '" data-columnId="' + column.columnId + '" width="' + column.width + '" align="center" >' + column.header + filterHtml + '</td>');
            this.siderHeaderTemplate.add('</tr>');
        }
        return this.siderHeaderTemplate.toHtml();
    },
    
    
    
    
    getPageHtml : function (titleData) {
        if (this.paging == true) {
            var pageBarTemplate = new com.yung.util.BasicTemplate();
            pageBarTemplate.add("<span> Page </span> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-pageIndex' type='text' value='{{pageIndex}}' size='3' /> / <span id='{{gridDivId}}-pageMax' >{{pageNum}}</span> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-changePage' type='button' value='{{change}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\");' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toStartPage' type='button' value='{{first}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", 0)' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toNextPage' type='button' value='{{prev}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", -1)' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toPrevPage' type='button' value='{{next}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", -2)' /> &nbsp; ");
            pageBarTemplate.add("<input id='{{gridDivId}}-toEndPage' type='button' value='{{last}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"changePage\", -3)' /> &nbsp; ");
            pageBarTemplate.add("<br><br>");
            var data = {};
            data['gridDivId'] = this.gridDivId;
            data['pageIndex'] = '1';
            data['pageNum'] = '1';
            if (titleData != null) {
                data['change'] = titleData["change"];
                data['first'] = titleData["first"];
                data['prev'] = titleData["prev"];
                data['next'] = titleData["next"];
                data['last'] = titleData["last"];
            }
            if (data["change"] == null) {
                data['change'] = "change";
            }
            if (data["first"] == null) {
                data['first'] = "first";
            }
            if (data["prev"] == null) {
                data['prev'] = "prev";
            }
            if (data["next"] == null) {
                data['next'] = "next";
            }
            if (data["last"] == null) {
                data['last'] = "last";
            }
            this.currentPageIndex = 0;
            return pageBarTemplate.toHtml(data);
        } else {
            return "";
        }
    },
    
    /** 
     * get current page number
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {number} current page number
     */
    getPageNum : function () {
        var pageNum = this.rowOrderList.size() / this.pageSize;
        pageNum = parseInt(pageNum);
        if ((pageNum * this.pageSize) == this.rowOrderList.size()) {
            return pageNum;
        }
        return pageNum + 1;
    },
    
    /** 
     * get current total record number
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {number} current total record number
     */
    getRowCnt : function () {
        return this.rowOrderList.size();
    },
    
    /** 
     * load JSON for url
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} dUrl - Ajax Url
     * @param {function} unblock - function after call
     */
    loadJSON : function(dUrl, unblock) {
        var idx = dUrl.indexOf("?");
        if (idx > 0) {
            var dummyForm = jQuery("#dummyForm");
            if (dummyForm.length == 0) {
                jQuery("body").append("<div id='dummyFormDiv' ></div>");
            }
            var formDiv = jQuery("#dummyFormDiv");
            var map = com.yung.util.StringUtils.parseURL(dUrl);
            dUrl = dUrl.substring(0, idx);
            var html = "<form id='dummyForm' method='post' >";
            for (var k in map) {
                html = html + "<input type='hidden' name='" + k + "' value='" + map[k] + "' />";
            }
            html = html + "</form>";
            formDiv.html(html);
        }
        var ajaxhttp = new com.yung.util.AjaxHttp(dUrl, 'dummyForm', function callback(response, self, caller)
        {
            if (response.action == 'error_main') {
                alert("load data error: " + response.errormsg);
                if (unblock != null) unblock();
                return;
            }
            caller.clean();
            caller.loadData(response.rows);
            caller.render();
            if (typeof unblock == 'function') {
                unblock();
            }
        });
        if (unblock != null) ajaxhttp.setLoading(false);
        ajaxhttp.send(this);
    },
    setupTableTemplate : function () {
    	if (this.tableTemplate.template.size() == 0) {
    	    var width = 0;
    	    var widthStr = jQuery("#" + this.gridDivId).css("width");
    	    widthStr = widthStr.toLowerCase();
    	    if (widthStr.indexOf('%') > 0) {
    	        width = 1000;
    	    } else if (widthStr.indexOf('px') > 0) {
    	        width = this.replaceAll(width + "", "px", "") * 1.0;
    	    } else {
    	        width = widthStr * 1.0;
    	    }
            var data = {};
    		var tableWidth = 0;
    		var columnArray = this.columnUtil.getColumnArray();
    		for (var c = 0; c < columnArray.length; c++) {
                var column = columnArray[c];
                if (column.columnHidden == true) {
                	continue;
                }
                tableWidth = tableWidth + (column.width * 1.0) + this.borderWidth;
    		}
    		data["tableWidth"] = tableWidth;
    		if (width > (tableWidth + com_yung_util_Position.getScrollbarWidth())) {
    			width = tableWidth;
    		}
    		data["width"] = width;
    		data["contentWidth"] = width + com_yung_util_Position.getScrollbarWidth();
    		var height = jQuery("#" + this.gridDivId).css("height");
    		height = this.replaceAll(height + "", "px", "") * 1.0;
    		if (height == null || height == 0) {
    			height = 600;
    		}
    		data["height"] = height;
    		this.tableTemplate.setData(data);
    		this.tableTemplate.add('<div id="' + this.gridDivId + '-headerScrollDiv" style="position: absolute; top: 0px; width: {{width}}px; overflow: hidden; z-index: 1010;" >');
        	this.tableTemplate.add('    <div id="' + this.gridDivId + '-headerDiv" style="width: {{tableWidth}}px;"></div>');
        	this.tableTemplate.add('</div>');
        	
            
            
            // TODO
            this.tableTemplate.add('<div id="' + this.gridDivId + '-siderScrollDiv" style="position: absolute; top: 0px; height: {{height}}px; overflow: hidden; z-index: 1020;" >');
        	this.tableTemplate.add('    <div id="' + this.gridDivId + '-siderDiv" style=""></div>');
        	this.tableTemplate.add('</div>');
        	
            
            
            
            
            this.tableTemplate.add('<div id="' + this.gridDivId + '-contentScrollDiv" style="position: absolute; top: 0px; width: {{contentWidth}}px; height: {{height}}px; overflow: auto; z-index: 1000;" >');
        	this.tableTemplate.add('    <div id="' + this.gridDivId + '-contentDiv" style="width: {{tableWidth}}px;"></div>');
        	this.tableTemplate.add('</div>');
    		return true;
        }
    	return false;
    },
    
    /** 
     * load data
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {Array} rows - data object array
     * @param {number} pageIndex - page index to render
     */
    loadData : function (rows, pageIndex) {
        this.clean();
        this.setupData(rows);
        var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        jQuery("#" + this.gridDivId).css("width", (windowWidth * 0.95) + "px");
        jQuery("#" + this.gridDivId).css("height", (windowHeight * 0.85) + "px");
        jQuery("#" + this.gridDivId + "-contentScrollDiv").css("height", (windowHeight * 0.85) + "px");
        if (this.paging == true) {
            this.smartRender.off = true;
            this.smartRender.render();
        	if (pageIndex == null) {
        		pageIndex = 0;
        	}
        	this.loadPage(pageIndex);
        } else if (rows.length > 300) {
            this.smartRender.off = false;
            this.smartRender.dataSize = rows.length;
            this.smartRender.preload();
        } else {
            this.smartRender.off = true;
            this.smartRender.render();
            
            
            // TODO
            var siderHeaderHtml = this.getSiderHeaderHtml();
            var siderRowHtml = this.renderSiderRows(rows);
        	jQuery("#" + this.gridDivId + "-siderDiv").html('<table id="' + this.gridDivId + '-siderTable" class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + siderHeaderHtml + siderRowHtml+ '</table>');
            
            
            
        	var headerHtml = this.getHeaderHtml();
        	var rowHtml = this.renderRows(rows);
        	jQuery("#" + this.gridDivId + "-contentDiv").html('<table id="' + this.gridDivId + '-contentTable" class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + rowHtml + '</table>');
            this.render();	
        }
    },
    
    /** 
     * load page
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} pageIndex - page index to render
     */
    loadPage : function (pageIndex) {
        var headerHtml = this.getHeaderHtml();
        var rowHtml = this.renderPageRow(pageIndex);
        jQuery("#" + this.gridDivId + "-tableDiv").html('<table class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + rowHtml + '</table>');
        this.render();
    },
    
    /** 
     * refresh and re-render grid, use when turn paging on/off, smart-render on/off
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    refresh : function () {
    	// refresh create grid structure
    	if (this.paging == true) {
    		var headerScroll = jQuery("#" + this.gridDivId + "-headerScrollDiv")[0];
    		if (headerScroll != null) {
    			this.createGrid();
    		}
    	} else {
    		var headerScroll = jQuery("#" + this.gridDivId + "-headerScrollDiv")[0];
    		if (headerScroll == null) {
    			this.createGrid();
    		}
    	}
    	if (this.paging == true) {
    		var pageIndex = this.currentPageIndex;
            if (pageIndex != 0) {
            	this.loadPage(pageIndex);
            }
    	} else {
    		var rows = [];
            for (var i = 0; i < this.rowOrderList.size(); i++) {
                var rowIdx = this.rowOrderList.get(i);
                var row = this.cacheRowMap.get(rowIdx);
                if (row != null) {
                    rows.push(row);
                }
            }
            this.loadData(rows);
        }
    	this.render();
    },
    setupData : function (rows) {
    	this.clean();
    	var columnArray = this.columnUtil.getColumnArray();
        if (rows == null || rows.length == 0) {
            return;
        }
        var columnIdx = -1;
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            if (column.columnId == 'id') {
                columnIdx = c;
                break;
            }
        }
        if (columnIdx == -1) {
            alert('Please define id column!');
            return;
        }
        this.cacheRowMap.clear();
        this.commentMap.clear();
        this.rowOrderList.clear();
        for (var r = 0; r < rows.length; r++) {
            var rowIdx = rows[r]['id'];
            this.cacheRowMap.put(rowIdx, rows[r]);
            this.rowOrderList.add(rowIdx);
        }
    },
    renderPageRow : function (pageIndex) {
        var rows = [];
        var startIdx = pageIndex * this.pageSize;
        var endIdx = (pageIndex + 1) * this.pageSize;
        for (var i = startIdx; i < endIdx; i++) {
            var rowIdx = this.rowOrderList.get(i);
            var row = this.cacheRowMap.get(rowIdx);
            if (row != null) {
                rows.push(row);
            }
        }
        return this.renderRows(rows);
    },
    renderRows : function (rows) {
        if (rows == null || rows.length == 0) {
            return "";
        } else {
            var columnArray = this.columnUtil.getColumnArray();
            var html = "";
            for (var r = 0; r < rows.length ; r++) {
                var rowIdx = rows[r]['id'];
                html = html + "<tr class='" + this.rowCls + "' >";
                for (var c = 0; c < columnArray.length; c++) {
                    var column = columnArray[c];
                    var content = rows[r][column.columnId];
                    if (content == null) {
                        content = '';
                    }
                    var comment = this.getComment(rowIdx, column.columnId);
                    if (comment != null && comment != '') {
                        content = "<img id='" + this.gridDivId + "-comment-" + rowIdx + "-" + c + "' src='" + this.classProp.commentBase64 + "' width='12' style='position: absolute; top: 0px; left: 0px;' title='" + comment + "' />" + content;
                    }
                    var clickFunt = '';
                    if ('edtxt' == column.type || 'ed' == column.type || 'edn' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                    } else if ('co' == column.type || 'select' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                    } else if ('ch' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                        if (content == '1' || content == 1) {
                            content = '<input type="checkbox" checked />';
                        } else {
                            content = '<input type="checkbox" />'
                        }
                    } else if ('date' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + c + "');";
                    } 
                    html = html + '<td id="' + this.gridDivId + '-' + rowIdx + '-' + c + '" data-columnId="' + column.columnId + '" class="' + this.cellCls + '" width="' + column.width + '" align="' + column.align + '" style="position: relative; background-clip: padding-box;" onclick="' + clickFunt + '" >' + content + '</td>';
                }
                html = html + "</tr>";
            }
            return html;
        }
    },
    
    
    
    // TODO
    renderSiderRows : function (rows) {
        if (rows == null || rows.length == 0) {
            return "";
        } else {
            var columnArray = this.columnUtil.getColumnArray();
            var html = "";
            for (var r = 0; r < rows.length ; r++) {
                var rowIdx = rows[r]['id'];
                html = html + "<tr id='sider-" + r + "' data-rowno='" + r + "' class='" + this.rowCls + "' >";
                var column = columnArray[1];
                var content = rows[r][column.columnId];
                if (content == null) {
                    content = '';
                }
                var comment = this.getComment(rowIdx, column.columnId);
                if (comment != null && comment != '') {
                    content = "<img id='" + this.gridDivId + "-comment-" + rowIdx + "-" + 1 + "' src='" + this.classProp.commentBase64 + "' width='12' style='position: absolute; top: 0px; left: 0px;' title='" + comment + "' />" + content;
                }
                var clickFunt = '';
                if ('edtxt' == column.type || 'ed' == column.type || 'edn' == column.type) {
                    clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + 1 + "');";
                } else if ('co' == column.type || 'select' == column.type) {
                    clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + 1 + "');";
                } else if ('ch' == column.type) {
                    clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + 1 + "');";
                    if (content == '1' || content == 1) {
                        content = '<input type="checkbox" checked />';
                    } else {
                        content = '<input type="checkbox" />'
                    }
                } else if ('date' == column.type) {
                    clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + rowIdx + "-" + 1 + "');";
                } 
                html = html + '<td id="' + this.gridDivId + '-' + rowIdx + '-' + 1 + '" data-columnId="' + column.columnId + '" class="' + this.cellCls + '" width="' + column.width + '" align="' + column.align + '" style="position: relative; background-clip: padding-box;" onclick="' + clickFunt + '" >' + content + '</td>';
                html = html + "</tr>";
            }
            return html;
        }
    },
    
    
    
    /** 
     * re-render grid, only adjust width, show/hide column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    render : function () {
        var columnArray = this.columnUtil.getColumnArray();
        for (var i = 0; i < columnArray.length; i++) {
            var column = columnArray[i];
            var col = "table td[data-columnId='" + column.columnId + "']";
            if (column.columnHidden == true) {
                jQuery(col).hide();
            } else {
                jQuery(col).show();
            }
        }
        var keyArray = this.dirtyRowMap.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var rowIdx = keyArray[i];
            var state = this.dirtyRowMap.get(rowIdx);
            if (state == 'updated' || state == 'inserted') {
                this.setUpdated(rowIdx, state);
            } else if (state == 'deleted') {
                this.deleteRow(rowIdx);
            }
        }
        if (this.paging == true) {
            var pageNum = this.getPageNum();
            jQuery("#" + this.gridDivId + "-pageMax").html(pageNum);
        } else {
            // align header to top
        	//var contentScrollDiv = jQuery("#" + this.gridDivId + "-contentScrollDiv");
        	//var headerScrollDiv = jQuery("#" + this.gridDivId + "-headerScrollDiv");
        	//var headerHeight = headerScrollDiv.height();
        	//contentScrollDiv.css("top", "-" + headerHeight + "px");
        	this.adjustHeight();
        	this.adjustWidth();
        }
        this.smartRender.render();
    },
    adjustHeight : function () {
        var browser = com_yung_util_getbrowser();
		var gridDivHeight = jQuery("#" + this.gridDivId).height();
        var contentScrollDiv = jQuery("#" + this.gridDivId + "-contentScrollDiv");
		var contentScrollDivHeight = contentScrollDiv.height();
		var display = jQuery('#SmartRender-' + this.smartRender.winId + '-BarContainer').css("display");
		if (display != "none" && contentScrollDivHeight < gridDivHeight){
			contentScrollDiv.height(gridDivHeight);
			contentScrollDivHeight = gridDivHeight;
		}
		var contentTable = jQuery("#" + this.gridDivId + "-contentTable");
        var contentTableHeight = contentTable.height();
        if (contentTableHeight > 1) {
            if (contentScrollDivHeight > contentTableHeight) {
                var contentTableWidth = contentTable.width();
                var contentScrollDivWidth = contentScrollDiv.width();
                if (contentTableWidth > contentScrollDivWidth) {
                    // horizontal scroll bar exist
                    contentTableHeight = contentTableHeight + 6;
                    if (browser == 'firefox') {
                        contentTableHeight = contentTableHeight - 4;
                    }
                    contentScrollDiv.css("height", contentTableHeight + com_yung_util_Position.getScrollbarHeight());
                } else {
                    contentTableHeight = contentTableHeight + 6;
                    contentScrollDiv.css("height", contentTableHeight);
                }
            }
        }
    },
    adjustWidth : function () {
        var browser = com_yung_util_getbrowser();
        var width = 0;
        var contentWidth = 0;
        var width = jQuery("#" + this.gridDivId).width();
        var tableWidth = 0;
        var columnArray = this.columnUtil.getColumnArray();
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            if (column.columnHidden == true) {
                continue;
            }
            tableWidth = tableWidth + (column.width * 1.0) + this.borderWidth;
        }
        if (width >= (tableWidth + com_yung_util_Position.getScrollbarWidth())) {
            width = tableWidth + com_yung_util_Position.getScrollbarWidth();
            contentWidth = tableWidth + com_yung_util_Position.getScrollbarWidth();
            if (browser == 'msie') {
                contentWidth = contentWidth + 5;
            }
        } else {
            contentWidth = width;
            var tableHeight = jQuery("#" + this.gridDivId + "-contentTable").height();
            var scrollHeight = jQuery("#" + this.gridDivId + "-contentScrollDiv").height();
            if (scrollHeight - (tableHeight + com_yung_util_Position.getScrollbarHeight()) < 3) {
                // vertical scroll bar not exist
                width = width - com_yung_util_Position.getScrollbarWidth();
            }
        }
        jQuery("#" + this.gridDivId + "-contentScrollDiv").width(contentWidth);
        jQuery("#" + this.gridDivId + "-contentDiv").width(tableWidth);
        jQuery("#" + this.gridDivId + "-headerScrollDiv").width(width);
        jQuery("#" + this.gridDivId + "-headerDiv").width(tableWidth);
        // fix for table align
        var contentScrollTablePos = new com.yung.util.Position(jQuery("#" + this.gridDivId + "-contentScrollDiv table"));
        var contentPos = contentScrollTablePos.getTopLeftPosition();
        var headerScrollTablePos = new com.yung.util.Position(jQuery("#" + this.gridDivId + "-headerScrollDiv table"));
        var headerPos = headerScrollTablePos.getTopLeftPosition();
        if ((contentPos.left - headerPos.left) > 2 || (contentPos.left - headerPos.left) < -2) {
			jQuery("#" + this.gridDivId + "-headerScrollDiv").css("left", (contentPos.left - headerPos.left) + "px");
        }
    },
    
    /** 
     * hide column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} arg - column id
     */
    hideColumn : function (arg) {
        var column = null;
        if (typeof arg == 'string') {
            column = this.columnUtil.columnMap[arg];
        } else if (typeof arg == 'number') {
            column = this.columnUtil.columnArray[arg];
        }
        var col = "table td[data-columnId='" + column.columnId + "']";
        column.columnHidden = true;
        jQuery(col).hide();
        this.adjustWidth();
    },
    
    /** 
     * show column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} arg - column id
     */
    showColumn : function (arg) {
        var column = null;
        if (typeof arg == 'string') {
            column = this.columnUtil.columnMap[arg];
        } else if (typeof arg == 'number') {
            column = this.columnUtil.columnArray[arg];
        }
        var col = "table td[data-columnId='" + column.columnId + "']";
        column.columnHidden = false;
        jQuery(col).show();
        this.adjustWidth();
    },
    
    /** 
     * set update status to row
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} state - row state
     */
    setUpdated : function(rowId, state) {
        var originState = this.dirtyRowMap.get(rowId);
        if (originState == null) {
            if (state == null || state == '' || state == 'updated') {
                state = "updated";
            } else if (state == 'inserted') {
                state = 'inserted';
            } else {
                throw "unknown state: " + state;
            }
            this.dirtyRowMap.put(rowId, state);
        }
        var columnArray = this.columnUtil.getColumnArray();
        for (var i = 0; i < columnArray.length; i++) {
            var column = columnArray[i];
            this.setCellBold(rowId, column.columnId);
        }
    },
    
    /** 
     * get all row id by order
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {Array} row id array
     */
    getAllRowIds : function() {
        return this.rowOrderList.toArray();
    },
    
    /** 
     * set cell value
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} value - cell value
     */
    setCellValue : function (rowId, columnId, value) {
        var column = this.columnUtil.getColumn(columnId);
        var colIdx = column.index;
        if (column == null) {
            if (window['console'] != null) {
                console.log("Err: columnId: " + columnId + " is not defined!");    
            }
            return;
        }
        var comment = this.getComment(rowId, columnId);
        var rowData = this.cacheRowMap.get(rowId);
        if (column.type != 'ch') {
            rowData[columnId] = value;
            this.insertCommentPng(rowId, colIdx, value, comment);
        } else {
            var rowData = this.cacheRowMap.get(rowId);
            if (value == '1' || value == 1 || value == 0 || value == '0') {
                rowData[columnId] = value + "";
                if (value == '1' || value == 1) {
                    var tdHtml = '<input type="checkbox" checked />';
                    this.insertCommentPng(rowId, colIdx, tdHtml, comment);
                } else {
                    var tdHtml = '<input type="checkbox" />';
                    this.insertCommentPng(rowId, colIdx, tdHtml, comment);
                }
            } else {
                // html
                this.insertCommentPng(rowId, colIdx, value, comment);
                var idx = (value + "").indexOf('checked');
                if (idx < 0) {
                    rowData[columnId] = '0';
                } else {
                    rowData[columnId] = '1';
                }
            }
        }
        this.cacheRowMap.put(rowId, rowData);
    },
    
    /** 
     * mark cell as delete, CSS text-decoration: line-through
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    setCellDelete : function (rowId, columnId) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        if (colIdx == null) {
            console.log("Err: columnId: " + columnId + " is not defined!");
            return;
        }
        jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).css('text-decoration', 'line-through');
    },
    
    /** 
     * mark cell as update, CSS font-weight: bold
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    setCellBold : function (rowId, columnId) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        if (colIdx == null) {
            console.log("Err: columnId: " + columnId + " is not defined!");
            return;
        }
        jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).css('font-weight', 'bold');
    },
    
    /** 
     * set cell background color
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} color - color code
     */
    setCellColor : function (rowId, columnId, color) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        if (colIdx == null) {
            console.log("Err: columnId: " + columnId + " is not defined!");
            return;
        }
        jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).css('background', color);
    },
    
    /** 
     * get cell value
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell value
     */
    getCellValue : function (rowId, columnId) {
        var rowData = this.cacheRowMap.get(rowId);
        if (rowData != null) {
            var ret = rowData[columnId];
            if (ret == '&nbsp;') {
                return null;
            }
            return ret;
        } else {
            return null;
        }
    },
    
    /** 
     * get cell html, different with getCellValue, 
     * ex. ch type cell value is '0' or '1' not html 
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell html
     */
    getCellHtml : function (rowId, columnId) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        return jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx).html();
    },
    
    /** 
     * get cell checked or not, only for type ch only 
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {boolean} checked or not
     */
    getCellCheck : function (rowId, columnId) {
        var value = this.getCellValue(rowId, columnId);
        if (value != null) {
            if (value == '1' || value == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },
    getNewRowId : function guidGenerator() {
        var d = new Date();
        var timeStampInMs = d.getTime();
        return "ZzZzZ" + parseInt(timeStampInMs);
    },
    
    /** 
     * add a new empty row by index
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} index - row index to add
     * @param {string} idColumn - id column name
     * @return {boolean} call render or not
     */
    addEmptyRow : function (index, idColumn, rendor) {
        if (idColumn == null) {
             idColumn = 'id';
        }
        var newRowId = this.getNewRowId();
        var nextRowId = this.getNewRowId();
        while (nextRowId == newRowId) {
            nextRowId = this.getNewRowId();
        }
        this.rowOrderList.addByIndex(newRowId, index);
        this.dirtyRowMap.put(newRowId, "inserted");
        var columnArray = this.columnUtil.getColumnArray();
        var rowData = {};
        for (var c = 0; c < columnArray.length; c++) {
            var column = columnArray[c];
            if (idColumn != column.columnId) {
                rowData[column.columnId] = '&nbsp;';
            } else {
                rowData[idColumn] = newRowId;
            }
        }
        this.cacheRowMap.put(newRowId, rowData);
        if (rendor == true) {
            if (this.paging == false) {
                var tableRef = jQuery("#" + this.gridDivId + " table tbody")[0];
                // Insert a row in the table at the last row
                var newRow   = tableRef.insertRow(index + 1);
                var newRowHtml = ' ';
                for (var c = 0; c < columnArray.length; c++) {
                    var column = columnArray[c];
                    var content = '&nbsp;';
                    if ('id' == column.columnId) {
                        content = newRowId;
                    }
                    var clickFunt = '';
                    if ('edtxt' == column.type || 'ed' == column.type || 'edn' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + newRowId + "-" + c + "');";
                    } else if ('co' == column.type || 'select' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + newRowId + "-" + c + "');";
                    } else if ('ch' == column.type) {
                        clickFunt = "fireInstanceMethod('" + this.hashId + "', 'edit', '" + this.gridDivId + "-" + newRowId + "-" + c + "');";
                    }
                    newRowHtml = newRowHtml + '<td id="' + this.gridDivId + '-' + newRowId + '-' + c + '" data-columnId="' + column.columnId + '" class="' + this.cellCls + '" width="' + column.width + '" align="' + column.align + '" style="position: relative; background-clip: padding-box;" onclick="' + clickFunt + '" >' + content + '</td>';
                }
                jQuery(newRow).html(newRowHtml);
                this.render();
            } else {
                this.changePage(this.gridDivId, this.currentPageIndex);
            }
        }
        return newRowId;
    },
    
    /** 
     * get all rows which state is not empty
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {boolean} validate - run validate or not
     * @return {Array} rows which state is not empty
     */
    getChangedRows : function() {
        var result = [];
        var dirtyRowIds = this.dirtyRowMap.getKeyArray();
        for (var i = 0; i < dirtyRowIds.length; i++) {
            var rowIdx = dirtyRowIds[i];
            var row = this.getRowData(rowIdx);
            result.push(row);
        }
        return result;
    },
    
    /** 
     * delete grid row, if row state is inserted then remove row,
     * if state is not inserted, then mark as delete
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {boolean} forceDelete - remove row immediately 
     */
    deleteRow : function (rowId, forceDelete) {
        if (forceDelete == true) {
            this.dirtyRowMap.remove(rowId);
            this.cacheRowMap.remove(rowId);
            this.commentMap.remove(rowId);
            this.rowOrderList.removeElement(rowId);
            this.refresh();
            return;
        }
        var originState = this.dirtyRowMap.get(rowId);
        if (originState == 'inserted') {
            this.dirtyRowMap.remove(rowId);
            this.cacheRowMap.remove(rowId);
            this.commentMap.remove(rowId);
            this.rowOrderList.removeElement(rowId);
            this.refresh();
        } else {
            this.dirtyRowMap.put(rowId, 'deleted');
            this.setDeleted(rowId);
        }
    },
    setDeleted : function (rowId) {
        var columnArray = this.columnUtil.getColumnArray();
        for (var i = 0; i < columnArray.length; i++) {
            var column = columnArray[i];
            this.setCellDelete(rowId, column.columnId);
        }
    },
    
    /** 
     * get all rows which state is not empty, and validate row
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {boolean} validate - run validate or not
     * @return {Array} rows which state is not empty
     */
    getAllUpdateData : function (validate) {
        var result = this.getChangedRows();
        if (validate == true) {
            var ret = this.validate(result);
            if (ret == false) {
                return;
            }
        }
        return result;
    },
    
    /** 
     * clean all grid state and data
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clean : function () {
        this.clearErrorCells();
        this.dirtyRowMap.clear();
        this.cacheRowMap.clear();
        this.commentMap.clear();
        this.rowOrderList.clear();
    },
    
    /** 
     * clean all grid state and data, then re-render
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clearAll : function () {
        var headerHtml = this.getHeaderHtml();
        jQuery("#" + this.gridDivId).html('<div id="' + this.gridDivId + '-pageDiv"></div><div id="' + this.gridDivId + '-tableDiv"><table class="' + this.gridCls + '" style="' + this.gridStyle + '" align="' + this.tableAlign + '">' + headerHtml + '</table></div>');
        this.clean();
        this.render();
    },
    
    /** 
     * clean all grid error cell, rollback yellow color
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clearErrorCells : function () {
        var errArray = this.errorCells.toArray();
        for (var i = 0; i < errArray.length; i++) {
            var errCell = errArray[i];
            var rowIdx = errCell.rowIdx;
            var columnId = errCell.columnId;
            this.setCellColor(rowIdx, columnId, "");
        }
        this.errorCells.clear();
    },
    
    /** 
     * add error cell information, set cell color yellow
     * and mark cell error
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} rowId - row id
     * @param {string} columnId - id column name
     */
    addErrorCells : function(rowId, columnId) {
        var errCell = new com.yung.util.TableErrorCell(rowId, columnId);
        this.errorCells.add(errCell);
        this.setCellColor(rowId, columnId, 'yellow');
    },
    
    /** 
     * validate current grid rows, and put error information into error cells.
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {Array} dataArray - row data array, optional
     * @return {boolean} with error or not
     */
    validate : function (dataArray) {
        this.clearErrorCells();
        if (dataArray == null) {
            dataArray = this.getChangedRows();
        }
        var errormsg = "";
        var colArray = this.columnUtil.getColumnArray();
        if (dataArray != null && dataArray.length > 0) {
            for (var i = 0; i < dataArray.length; i++) {
                var obj = dataArray[i];
                for (var j = 0; j < colArray.length; j++) {
                    var column = colArray[j];
                    if (column.numberFormat != null && column.numberFormat != '') {
                        var value = obj[column.columnId];
                        if (value != null) {
                            value = value + "";
                            if (value != '' && !jQuery.isNumeric(value)) {
                                var rowIdx = obj['_rowIdx'];
                                var rowNo = this.getRowNumber(rowIdx);
                                if (rowNo == null) {
                                    errormsg = errormsg + "rowId[" + rowIdx + "] column[" + column.header + "]: '" + value + "' is not number! \n";
                                } else {
                                    errormsg = errormsg + "row[" + rowNo + "] column[" + column.header + "]: '" + value + "' is not number! \n";
                                }
                                this.addErrorCells(rowIdx, column.columnId);
                            }
                        }
                    }
                    if (column.length != null && column.length > 0) {
                        var value = obj[column.columnId];
                        var rowIdx = obj['_rowIdx'];
                        if (value != null && value != '') {
                            value = value + "";
                            if (value.length > column.length) {
                                errormsg = errormsg + "rowId[" + rowIdx + "] column[" + column.header + "]: '" + value + "' length is over " + column.length + "! \n";
                                this.addErrorCells(rowIdx, column.columnId);
                            }
                        }
                    }
                    if (column.allowNull != null && column.allowNull === false) {
                        var value = obj[column.columnId];
                        var rowIdx = obj['_rowIdx'];
                        if (value == null || value == '') {
                            errormsg = errormsg + "rowId[" + rowIdx + "] column[" + column.header + "]: '" + value + "' value is empty! \n";
                            this.addErrorCells(rowIdx, column.columnId);
                        }
                    }
                }
            }
        }
        if (errormsg != '') {
            alert(errormsg);
            return false;
        }
        return true;
    },
    
    /** 
     * get row data as data object
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @return {object} data object
     */
    getRowData : function (rowIdx) {
        var obj = {};
        obj["_rowIdx"] = rowIdx;
        var state = this.dirtyRowMap.get(rowIdx);
        if (state == null) {
            state = '';
        }
        obj["_state"] = state;
        var colIds = this.columnUtil.getColumnAttrArray('columnId');
        for (var c = 0; c < colIds.length; c++) {
            var val = this.getCellValue(rowIdx, colIds[c]);
            obj[colIds[c]] = val;
        }
        return obj;
    },
    getRowNumber : function (rowIdx) {
        return this.getCellValue(rowIdx, 'rowNo');
    },
    fireAfterEdit : function (ele, rowIdx, colIdx, newContent) {
        if (this.afterEdit != null) {
            this.afterEdit(ele, rowIdx, colIdx, this, newContent);
        }
    },
    fireBeforeEdit : function (ele, rowIdx, colIdx, oldcontent) {
        if (this.beforeEdit != null) {
            return this.beforeEdit(ele, rowIdx, colIdx, this, oldcontent);
        }
    },
    
    /** 
     * get current grid cell value, Note: ch type will return '1' or '0'
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell value
     */
    getCurrentCellValue : function (rowId, columnId) {
        var cacheRowData = this.cacheRowMap.get(rowId);
        var content = cacheRowData[columnId];
        return content;
    },
    
    /** 
     * change page index and render html
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} index - page index
     */
    changePage : function (index) {
        var pageIndex = jQuery("#" + this.gridDivId + "-pageIndex").val();
        var isNum = com.yung.util.StringUtils.isNumeric(pageIndex);
        if (isNum == false) {
            alert(pageIndex + " is not number!");
            return;
        }
        pageIndex = pageIndex * 1.0 - 1;
        pageIndex = parseInt(pageIndex);
        var maxPage = this.getPageNum() - 1;
        if (index == 0) {
            pageIndex = 0;
        } else if (index == -1) {
            pageIndex = this.currentPageIndex - 1;
            if (pageIndex < 0) {
                return;
            }
        } else if (index == -2) {
            pageIndex = this.currentPageIndex + 1;
            if (pageIndex > maxPage) {
                return;
            }
        } else if (index == -3) {
            pageIndex = maxPage;
        } else {
            if (pageIndex < 0) {
                alert(pageIndex + " is not validate!");
                return;
            }
            if (pageIndex > maxPage) {
                alert(pageIndex + " is not validate!");
                return;
            }
        }
        this.currentPageIndex = pageIndex;
        this.loadPage(pageIndex);
        jQuery("#" + this.gridDivId + "-pageIndex").val(pageIndex + 1);
    },
    edit : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var columnArray = this.columnUtil.getColumnArray();
        var type = columnArray[colIdx].type;
        var oldContent = jQuery("#" + cellId).text();
        if ('ch' == type) {
            var content = this.getCurrentCellValue(rowIdx, columnId);
            if (content != "1" && content != 1) {
                content = 0;
            }
            oldContent = content + "";
        }
        var ele = jQuery("#" + cellId);
        var ret = this.fireBeforeEdit(ele, rowIdx, colIdx, oldContent);
        if (ret === false) {
            return;
        }
        if (this.paging == false) {
            // adjust scroll bar 
            var jEle = jQuery("#" + cellId);
            var tdWidth = jEle.width();
            var tdHright = jEle.height();
            var headerEle = jQuery('#' + this.gridDivId + '-headerScrollDiv');
            var contentEle = jQuery('#' + this.gridDivId + '-contentScrollDiv');
            var contentScrollTopVal = contentEle.scrollTop();
            var contentScrollLeftVal = contentEle.scrollLeft();
            var position = new com.yung.util.Position(ele);
            var headerPosition = new com.yung.util.Position(headerEle);
            var contentPosition = new com.yung.util.Position(contentEle);
            var leftTopPos = position.getTopLeftPosition();
            var botLeftPos = position.getBotLeftPosition();
            var botRightPos = position.getBotRightPosition();
            var headerHeight = headerEle.height();
            var headerTopLeftPos = headerPosition.getTopLeftPosition();
            var contentBotLeftPos = contentPosition.getBotLeftPosition();
            var contentBotRightPos = contentPosition.getBotRightPosition();
            if (leftTopPos.top < (headerTopLeftPos.top + headerHeight)) {
                var topOffset = (headerTopLeftPos.top + headerHeight) - leftTopPos.top;
                contentEle.scrollTop(contentScrollTopVal - topOffset);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
            if (botRightPos.top > contentBotRightPos.top - com_yung_util_Position.getScrollbarHeight()) {
                var topOffset = (contentBotRightPos.top - com_yung_util_Position.getScrollbarHeight()) - botRightPos.top;
                topOffset = topOffset - 5;
                contentEle.scrollTop(contentScrollTopVal - topOffset);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
            if (leftTopPos.left < contentBotLeftPos.left) {
                var leftOffset = contentBotLeftPos.left - leftTopPos.left;
                contentEle.scrollLeft(contentScrollLeftVal - leftOffset);
                contentScrollLeftVal = contentEle.scrollLeft();
                headerEle.scrollLeft(contentScrollLeftVal);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
            if (botRightPos.left > contentBotRightPos.left - com_yung_util_Position.getScrollbarWidth()) {
                var leftOffset = (contentBotRightPos.left - com_yung_util_Position.getScrollbarWidth()) - botRightPos.left;
                contentEle.scrollLeft(contentScrollLeftVal - leftOffset + 6);
                contentScrollLeftVal = contentEle.scrollLeft();
                headerEle.scrollLeft(contentScrollLeftVal);
                com_yung_util_TableGridUtil.fireScrollManually = true;
            }
        }
        if ('edtxt' == type || 'ed' == type || 'edn' == type) {
            this.editTxt(cellId);
        } else if ('co' == type || 'select' == type) {
            this.select(cellId);
        } else if ('ch' == type) {
            this.clickCheck(cellId);
        } else if ('date' == type) {
            this.pickDate(cellId);
        }
    },
    createTextEditor : function () {
        var txtAreaEdit = jQuery("#" + this.gridDivId + "-editTxt");
        if (txtAreaEdit[0] == null) {
            var html = '<textarea id="' + this.gridDivId + '-editTxt' + '" data-cellId="" data-lzOldContent="" style="position: absolute; display: none; border: 2px; border-color: black; z-index: 5010;" onblur="fireInstanceMethod(\'' + this.hashId + '\', \'editDone\', this);" ></textarea>';
            jQuery("body").append(html);
        }
        this.combo = com.yung.util.SelectCombo.instance(this.gridDivId + '-editTxt');
        var self = this;
        this.combo.setAfterChoose(function (eleId, inputVal) {
            // fire editTxtDone
            var editor = jQuery("#" + eleId);
            var dataCellId = editor.attr("data-cellId");
            var tokens = dataCellId.split("-");
            var rowIdx = tokens[1];
            var colIdx = tokens[2] * 1.0;
            var columnArray = self.columnUtil.getColumnArray();
            var columnId = columnArray[colIdx].columnId;
            var content = inputVal;
            var oldContent = self.getCurrentCellValue(rowIdx, columnId);
            if (content != oldContent) {
                self.setUpdated(rowIdx);
            }
            self.setCellValue(rowIdx, columnId, content);
            editor.css("display", "none");
        });
    },
    editTxt : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var txtAreaEdit = jQuery("#" + this.gridDivId + "-editTxt");
        txtAreaEdit.attr("data-cellId", cellId);
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        var lzOldContent = com_yung_util_LZString.encode(oldContent);
        txtAreaEdit.attr("data-lzOldContent", lzOldContent);
        var width = jQuery("#" + cellId).width() + yung_global_var.widthOffset[browser];
        var height = jQuery("#" + cellId).height() + yung_global_var.heightOffset[browser];
        var position = new com.yung.util.Position(jQuery("#" + cellId));
        var pos = position.getTopLeftPosition();
        pos.left = pos.left + yung_global_var.leftOffset[browser];
        pos.top = pos.top + yung_global_var.topOffset[browser];
        txtAreaEdit.css("width", width + "px");
        txtAreaEdit.css("height", height + "px");
        txtAreaEdit.css("left", pos.left + "px");
        txtAreaEdit.css("top", pos.top + "px");
        txtAreaEdit.val(oldContent);
        txtAreaEdit.css("display", "block");
        var displayValues = columnArray[colIdx].columnValidator;
        this.combo.setSelectValues(displayValues);
        txtAreaEdit.focus();
    },
    createSelectEditor : function () {
        var selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        if (selectDiv[0] == null) {
            var html = '<div id="' + this.gridDivId + '-selectDiv' + '" data-cellId="" data-lzOldContent="" style="position: absolute; display: none; z-index: 5000; background: white; box-shadow: 5px 5px 5px #d7d7d7; border: 1px solid #486c99;" ></div>';
            jQuery("body").append(html);
            var self = this;
            jQuery("body").bind("click", function() {
                var div = jQuery("#" + self.gridDivId + "-selectDiv");
                var display = div.css("display");
                if (display == 'block') {
                    div.css("display", "none");
                }
            });
        }
    },
    select : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        selectDiv.attr("data-cellId", cellId);
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        var lzOldContent = com_yung_util_LZString.encode(oldContent);
        selectDiv.attr("data-lzOldContent", lzOldContent);
        var html = "<table style='width: 100%;'>";
        var width = jQuery("#" + cellId).width();
        var size = 0;
        var comboMap = this.columnUtil.columnArray[colIdx].comboMap;
        for (var key in comboMap) {
            var content = comboMap[key];
            var lzKey = com_yung_util_LZString.encode(key);
            html = html + "<tr>";
            html = html + "<td class='yung-combo' align='left' data-cellId='" + cellId + "' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"editDone\", this, \"" + lzKey + "\");'>" + content + "</td>";
            html = html + "</tr>";
            size++;
            if (comboMap[key] != null) {
                var w = comboMap[key].length * 7.5;
                w = parseInt(w);
                if (w > width) {
                    if (w > 250) {
                        width = 250;
                    } else {
                        width = w;
                    }
                }
            }
        }
        html = html + "</table>";
        selectDiv.html(html);
        width = width + yung_global_var.widthOffset[browser];
        if (browser == 'msie') {
            width = width + 4;
        }
        var position = new com.yung.util.Position(jQuery("#" + cellId));
        var pos = position.getBotLeftPosition();
        pos.left = pos.left + yung_global_var.leftOffset[browser];
        pos.top = pos.top + yung_global_var.topOffset[browser];
        selectDiv.css("width", width + "px");
        selectDiv.css("left", pos.left + "px");
        selectDiv.css("top", pos.top + "px");
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            jQuery(".yung-combo").each(function() {
                jQuery(this).hover(
                    function() {
                        jQuery(this).css("background-color", "#A4BED4");
                    }, function() {
                        jQuery(this).css("background-color", "");
                    }
                );
            });
        }
        setTimeout(function () {
            selectDiv.css("display", "block");
        }, 100);
    },
    pickDate : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var txtAreaDatePciker = jQuery("#" + this.gridDivId + "-datePicker");
        var nowEdit = this.datePicker.floatDiv.isDisplay();
        if (nowEdit == true) {
            var self = this;
            setTimeout(function () {
                self.prepareTxtDatePicker(cellId);
                txtAreaDatePciker.focus();
            }, this.datePicker.delayFire * 1.1);
        } else {
            this.prepareTxtDatePicker(cellId);
            txtAreaDatePciker.focus();
        }
    },
    prepareTxtDatePicker : function (cellId) {
        var browser = com_yung_util_getbrowser();
        var txtAreaDatePciker = jQuery("#" + this.gridDivId + "-datePicker");
        txtAreaDatePciker.attr("data-cellId", cellId);
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        var lzOldContent = com_yung_util_LZString.encode(oldContent);
        txtAreaDatePciker.attr("data-lzOldContent", lzOldContent);
        var width = jQuery("#" + cellId).width() + yung_global_var.widthOffset[browser];
        var height = jQuery("#" + cellId).height() + yung_global_var.heightOffset[browser];
        var position = new com.yung.util.Position(jQuery("#" + cellId));
        var pos = position.getTopLeftPosition();
        pos.left = pos.left + yung_global_var.leftOffset[browser];
        pos.top = pos.top + yung_global_var.topOffset[browser];
        txtAreaDatePciker.css("width", width + "px");
        txtAreaDatePciker.css("height", height + "px");
        txtAreaDatePciker.css("left", pos.left + "px");
        txtAreaDatePciker.css("top", pos.top + "px");
        txtAreaDatePciker.val(oldContent);
        txtAreaDatePciker.css("display", "block");
    },
    createDatePicker : function () {
        var txtAreaDatePciker = jQuery("#" + this.gridDivId + "-datePicker");
        if (txtAreaDatePciker[0] == null) {
            var html = '<textarea id="' + this.gridDivId + '-datePicker' + '" data-cellId="" data-lzOldContent="" style="position: absolute; display: none; border: 2px; border-color: black; z-index: 5080;" ></textarea>';
            jQuery("body").append(html);
        }
        this.datePicker = com.yung.util.GridDatePicker.instance(this.gridDivId + '-datePicker', 'MM/dd/yyyy', this);
        if (com_yung_util_getbrowser() == 'msie') {
            this.datePicker.adjustX = -1;
            this.datePicker.adjustY = 4;
        }
        var self = this;
        this.datePicker.setAfterChoose(function (eleId, inputVal) {
            // fire editTxtDone
            var editor = jQuery("#" + eleId);
            var dataCellId = editor.attr("data-cellId");
            var tokens = dataCellId.split("-");
            var rowIdx = tokens[1];
            var colIdx = tokens[2] * 1.0;
            var columnArray = self.columnUtil.getColumnArray();
            var columnId = columnArray[colIdx].columnId;
            var content = inputVal;
            var oldContent = self.getCurrentCellValue(rowIdx, columnId);
            if (content != oldContent) {
                self.setUpdated(rowIdx);
            }
            self.setCellValue(rowIdx, columnId, content);
            editor.css("display", "none");
        });
    },
    clickCheck : function (cellId) {
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var content = "";
        var checkAttr = "";
        var chk = jQuery("#" + cellId).find("input");
        var checked = chk.prop("checked");
        if (checked) {
            checkAttr = "checked";
            content = "1";
        } else {
            content = "0";
        }
        var html = '<input type="checkbox" ' + checkAttr + ' >';
        var oldContent = this.getCurrentCellValue(rowIdx, columnId);
        if (oldContent == 1 || oldContent == '1') {
            oldContent = "1";
        } else {
            oldContent = "0";
        }
        this.setCellValue(rowIdx, columnId, html);
        if (oldContent != content) {
            this.setUpdated(rowIdx);    
        }
    },
    editDone : function (editor, lzContent) {
        var cellId = jQuery(editor).attr("data-cellId");
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var type = columnArray[colIdx].type;
        var newContent = '';
        if ('edtxt' == type || 'ed' == type || 'edn' == type) {
            newContent = this.editTxtDone(cellId);
        } else if ('co' == type || 'select' == type ) {
            var selectContent = com_yung_util_LZString.decode(lzContent);
            newContent = this.selectDone(cellId, selectContent);
        } else if ('date' == type) {
            newContent = this.editTxtDone(cellId);
        }
        this.fireAfterEdit(jQuery("#" + cellId), rowIdx, colIdx, newContent);
    },
    editTxtDone : function (cellId) {
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        var editor = jQuery("#" + this.gridDivId + "-editTxt");
        var content = editor.val();
        this.setCellValue(rowIdx, columnArray[colIdx].columnId, content);
        var lzOldContent = editor.attr("data-lzOldContent");
        var oldContent = com_yung_util_LZString.decode(lzOldContent);
        if (content != oldContent) {
            this.setUpdated(rowIdx);
        }
        editor.css("display", "none");
        return content;
    },
    selectDone : function (cellId, selectContent) {
        var tokens = cellId.split("-");
        var rowIdx = tokens[1];
        var colIdx = tokens[2] * 1.0;
        var columnArray = this.columnUtil.getColumnArray();
        var columnId = columnArray[colIdx].columnId;
        this.setCellValue(rowIdx, columnId, selectContent);
        var selectDiv = jQuery("#" + this.gridDivId + "-selectDiv");
        var lzOldContent = selectDiv.attr("data-lzOldContent");
        var oldContent = com_yung_util_LZString.decode(lzOldContent);
        if (selectContent != oldContent) {
            this.setUpdated(rowIdx);
        }
        selectDiv.css("display", "none");
        return selectContent;
    },
    
    /** 
     * set comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} comment - comment text
     */
    setComment : function (rowId, columnId, comment) {
        var commentRowMap = this.commentMap.get(rowId);
        if (commentRowMap == null) {
            commentRowMap = new com.yung.util.Map('string', 'string');
            commentRowMap.put(columnId, comment);
            this.commentMap.put(rowId, commentRowMap);
        } else {
            commentRowMap.put(columnId, comment);
        }
        this.renderComment(rowId, columnId, comment);
    },
    renderComment : function (rowId, columnId, comment) {
        var colIdx = this.columnUtil.getColumnIndex(columnId);
        var tdHtml = this.getCellHtml(rowId, columnId);
        this.insertCommentPng(rowId, colIdx, tdHtml, comment);
    },
    insertCommentPng : function (rowId, colIdx, tdHtml, comment) {
        var jTdEle = jQuery("#" + this.gridDivId + '-' + rowId + '-' + colIdx);
        if (jTdEle[0] != null) {
            if (comment == null || comment == '') {
                jTdEle.html(tdHtml);
            } else {
                var left = 0;
                if (com_yung_util_getbrowser() == 'edge') {
                    left = -1;
                }
                var jCommentEle = jQuery("#" + this.gridDivId + "-comment-" + rowId + "-" + colIdx);
                if (jCommentEle[0] != null) {
                    jCommentEle.remove();
                }
                tdHtml = "<img id='" + this.gridDivId + "-comment-" + rowId + "-" + colIdx + "' src='" + this.classProp.commentBase64 + "' width='12' style='position: absolute; top: 0px; left: " + left + "px;' title='" + comment + "' />" + tdHtml;
                jTdEle.html(tdHtml);
            }
        }
    },
    
    /** 
     * get comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} comment text
     */
    getComment : function (rowId, columnId) {
        var commentRowMap = this.commentMap.get(rowId);
        if (commentRowMap != null) {
            return commentRowMap.get(columnId);
        }
        return null;
    },
    
    /** 
     * remove comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    removeComment : function (rowId, columnId) {
        var commentRowMap = this.commentMap.get(rowId);
        if (commentRowMap != null) {
            if (commentRowMap.containsKey(columnId) == true) {
                commentRowMap.remove(columnId);
                var colIdx = this.columnUtil.getColumnIndex(columnId);
                this.deleteCommentPng(rowId, colIdx);
            }
        }
    },
    deleteCommentPng : function (rowId, colIdx) {
        var jCommentEle = jQuery("#" + this.gridDivId + "-comment-" + rowId + "-" + colIdx);
        if (jCommentEle[0] != null) {
            jCommentEle.remove();
        }
    },
    switchFilter : function (flag) {
        if (flag == null) {
            if (this.filter) {
                this.filter = false;
            } else {
                this.filter = true;
            }
        } else {
            if (flag == true) {
                this.filter = true;
            } else {
                this.filter = false;
            }
        }
        if (this.filter == true) {
			com_yung_util_TableGridFilter.instance(this.gridDivId).setupFilter();
		} else {
			com_yung_util_TableGridFilter.instance(this.gridDivId).cancelFilter();
		}
    }
});
com_yung_util_TableGridUtil.fireScrollManually = false;

com_yung_util_TableGridUtil.instance = function (gridDivId, columnUtil, gridCls, gridStyle, headerCls, cellCls, tableAlign, rowCls) {
    return $Class.getInstance("com.yung.util.TableGridUtil", gridDivId, columnUtil, gridCls, gridStyle, headerCls, cellCls, tableAlign, rowCls);
}

var com_yung_util_TableErrorCell = $Class.extend({
    classProp : { name : "com.yung.util.TableErrorCell" },
    rowIdx : null,
    columnId : null,
    init: function(rowIdx, columnId) {
        this.rowIdx = rowIdx;
        this.columnId = columnId;
        return this;
    }    
});

jQuery( document ).ready(function() {
    if (typeof com_yung_util_StringUtils == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-StringUtils.js!");
    }
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-Collection.js!");
    }
    if (typeof com_yung_util_Map == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-Map.js!");
    }
    if (typeof  com_yung_util_Position == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-Position.js!");
    }
    if (typeof com_yung_util_LZString == 'undefined') {
        alert("yung-TableGridUtil.js requires lz-string.js!");
    }
    if (typeof com_yung_util_Combo == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-Combo.js!");
    }
    if (typeof com_yung_util_DatePicker == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-DatePicker.js!");
    }
    if (typeof com_yung_util_Calendar == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-Calendar.js!");
    }
    if (typeof com_yung_util_FloatDiv == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-FloatDiv.js!");
    }
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-TableGridUtil.js requires yung-BasicTemplate.js!");
    }
});