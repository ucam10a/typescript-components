/**
 * An easy tool to implement movable
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Movable {
    
	classProp : { 
		name : "com.yung.util.Movable" 
	}
    
	/**
     * Callback when drag
     *
     * @callback MovableDragCallback
     * @param  {number} startX - element start left position
     * @param  {number} startY - element start top position
     * @param  {number} endX - element end left position
     * @param  {number} endY - element end top position
     */
	/**
	 * enable draggable to element
	 * 
     * @memberof com_yung_util_Movable
     * @param  {string} eleId - element id to enable movable
     * @param  {boolean} disableX - element disable movable in X-axis
     * @param  {boolean} disableY - element disable movable in Y-axis
     * @param  {object} bound - element move boundary ex. {maxX: 100, minX: 0, maxY: 100, minY: 0}
     * @param  {MovableDragCallback} callback - callback when drag
     */
	static enable (eleId: string, disableX?: boolean, disableY?: boolean, bound?: object, callback?: Function): void 
    
    /**
     * Callback for mouse event listener
     *
     * @callback draggableEventCallback
     * @param {event} e - mouse event
     */
    /** 
     * add event listener to specified element
     * 
     * @private
     * @memberof com_yung_util_Movable
     * @param  {HTMLElement} element - element to enable draggable
     * @param  {string} type - event type, ex. mousedown, mouseon
     * @param  {draggableEventCallback} callback - listener callback
     * @param  {boolean} capture - capture flag
     */
    static addListener (element: HTMLElement | Document, type: string, callback: Function, capture?: boolean): void 
    
    /** 
     * enable draggble to specified element
     * 
     * @private
     * @memberof com_yung_util_Movable
     * @param  {HTMLElement} element - element to enable draggable
     * @param  {boolean} disableX - element disable movable in X-axis
     * @param  {boolean} disableY - element disable movable in Y-axis
     * @param  {object} bound - element move boundary ex. {maxX: 100, minX: 0, maxY: 100, minY: 0}
     */
    static draggable (element: HTMLElement, disableX: boolean, disableY: boolean, bound: object, callback: Function): void 
}

export declare function _dragElement(elmnt: HTMLElement, disableX: boolean, disableY: boolean, bound: object, callback: Function): void 