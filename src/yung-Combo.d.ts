import { $Class } from "./yung-JS-extend";
import { com_yung_util_FloatDiv } from "./yung-FloatDiv";
import { com_yung_util_BasicSet } from "./yung-Collection";

/**
 * Combo box, like autocomplete
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Combo extends $Class {

    classProp : { 
        name : "com.yung.util.Combo",
        unimplemented: []
    }
    
    hashId : string
    
    /**
     * minimum length for filter
     * @member {number}
     * @instance
     * @memberof com_yung_util_Combo
     */
    minLength : number
    
    /**
     * maximum items to display
     * @member {number}
     * @instance
     * @memberof com_yung_util_Combo
     */
    maxItems : number
    
    /**
     * element id
     * @member {string}
     * @instance
     * @memberof com_yung_util_Combo
     */
    eleId : string
    
    /**
     * float div to place word list
     * @member {com_yung_util_FloatDiv}
     * @instance
     * @memberof com_yung_util_Combo
     */
    floatDiv : com_yung_util_FloatDiv
    displayHeight : number
    
    /**
     * select value array
     * @member {com_yung_util_Set}
     * @instance
     * @memberof com_yung_util_Combo
     */
    selectValues : com_yung_util_BasicSet<string>
    selectIndex : number
    chosedValue : string
    
    /**
     * Callback for customized filter for selectValues
     *
     * @callback ComboCustomFilter
     * @param {string} reqCode - input code
     * @param {com_yung_util_Combo} combo - com_yung_util_Combo instance
     * @return {Array} customized select value array
     */
    /**
     * customized filter for selectValues
     * @member {ComboCustomFilter}
     * @instance
     * @memberof com_yung_util_Combo
     */
    filter : Function
    
    /**
     * Callback after combo choose value
     *
     * @callback AfterComboChoose
     * @param {string} eleId - element id
     * @param {string} inputVal - select value
     */
    /**
     * callback after combo choose value
     * @member {AfterComboChoose}
     * @instance
     * @memberof com_yung_util_Combo
     */
    afterChoose : Function
    
    
    /**
     * constructor
     * @memberof com_yung_util_Combo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    protected constructor (eleId: string, selectValues: Array<string>)
    
    /** 
     * set select values
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {Array | com_yung_util_BasicSet<string>} selectValues - select values
     */
    setSelectValues (selectValues: Array<string> | com_yung_util_BasicSet<string>): void
    
    /** 
     * add select values
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} selectValue - select value
     */
    addSelectValues (selectValue: string): void
    
    /** 
     * set minimum length for filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {number} minLength - minimum length for filter
     */
    setMinLength (minLength: number): void
    
    /** 
     * set maximum items to display
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {number} maxItems - maximum items to display
     */
    setMaxItems (maxItems: number): void
    
    /** 
     * set customized filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {ComboCustomFilter} filter - customized filter for selectValues
     */
    setFilter (filter: Function): void
    
    /** 
     * set after choose callback
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {AfterComboChoose} afterChoose - after choose callback
     */
    setAfterChoose (afterChoose: Function): void
    
    /** 
     * set input value
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} inputVal - string put into input
     */
    setInputValue (inputVal: string): void
    
    /** 
     * standard filter
     * 
     * @instance
     * @memberof com_yung_util_Combo
     * @param {string} reqCode - request code from input
     * @return {Array} filtered value array
     */
    standardFilter (reqCode: string): Array<string>
    
    /** 
     * create combo
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Combo
     */
    createCombo (): void
    private bindFunction ()
    
    /** 
     * show drop list
     * 
     * @memberof com_yung_util_Combo
     * @param {Array} displayValues - list values to display
     * @param {string} bindId - binding element id
     */
    showDropList (displayValues: Array<string>, bindId: string): void
    
    /** 
     * get com_yung_util_Combo global instance,
     * 
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     * @return  {com_yung_util_Combo} com_yung_util_Combo instance
     */
    static instance (eleId: string, selectValues: Array<string>) : com_yung_util_Combo

}

/**
 * select Combo box, like autocomplete, but show drop list when focus
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Combo
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_SelectCombo extends com_yung_util_Combo {

    // @ts-ignore
    classProp : { 
        name : "com.yung.util.SelectCombo",
        unimplemented: [] 
    }
    
    /**
     * minimum length for filter
     * @member {number}
     * @instance
     * @memberof com_yung_util_SelectCombo
     */
    minLength : number
    
    /**
     * constructor
     * @memberof com_yung_util_SelectCombo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    protected constructor (eleId: string, selectValues: Array<string>)
    private bindFunction (): void

    /** 
     * get com_yung_util_SelectCombo global instance,
     * 
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     * @return  {com_yung_util_SelectCombo} com_yung_util_SelectCombo instance
     */
    static instance(eleId: string, selectValues?: Array<string>): com_yung_util_SelectCombo

}

/**
 * select Combo box, like autocomplete. 
 * allow to append search word.
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Combo
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_AppendCombo extends com_yung_util_Combo {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.AppendCombo",
        unimplemented: []  
    }
    
    /**
     * constructor
     * @memberof com_yung_util_AppendCombo
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     */
    protected constructor (eleId: string, selectValues: Array<string>)
    
    /** 
     * standard filter
     * 
     * @instance
     * @memberof com_yung_util_AppendCombo
     * @param {string} reqCode - request code from input
     * @return {Array} filtered value array
     */
    standardFilter (reqCode: string): Array<string>
    
    /** 
     * get com_yung_util_AppendCombo global instance,
     * 
     * @param  {string} eleId - element id
     * @param  {Array} selectValues - select value array
     * @return  {com_yung_util_AppendCombo} com_yung_util_AppendCombo instance
     */
    static instance (eleId: string, selectValues: Array<string>): com_yung_util_AppendCombo

}