import { $Class } from "./yung-JS-extend";
import { com_yung_util_BasicList, com_yung_util_ObjectHashSet } from "./yung-Collection";
import { com_yung_util_BasicTemplate } from "./yung-BasicTemplate";
import { com_yung_util_BasicMap, system_util_Map } from "./yung-Map";
import { com_yung_util_SelectCombo } from "./yung-Combo";
import { com_yung_util_GridDatePicker } from "./yung-DatePicker";

/**
 * Smart Render Utility
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_SmartRender extends $Class {
    
    classProp : {
        name : "com.yung.util.SmartRender",
        unimplemented: []
    }
    
    /**
     * ratio upper bound to trigger append data
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    ratioUpper : 0.6 // recommend 0.6
    
    /**
     * ratio lower bound to trigger prepend data
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    ratioLower : 0.3  // recommend 0.3
    
    /**
     * Pre-loaded data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    preloadSize : number // recommend 60
    
    /**
     * buffer data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bufferSize : number // recommend 20
    
    /**
     * row number in window div
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winDataSize : number
    
    /**
     * window div height
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winHeight : number
    
    /**
     * window div width
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winWidth : number
    
    /**
     * display bar size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    barSize : number
    
    /**
     * bar boundary
     * @member {object}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bound : any
    
    /**
     * total data size
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    dataSize : number
    
    /**
     * fix scroll top position, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    fixScrollTop : number
    
    /**
     * way direction, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    way : number
    
    /**
     * fix count, for old IE use only
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    fixCnt : number
    
    /**
     * suspend flag
     * 
     * @private
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    suspend : boolean
    
    /**
     * switch flag to turn off smart render
     * 
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    off : boolean
    
    /**
     * top row index
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    topIdx : number
    
    /**
     * bottom row index
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    bottomIdx : number
    
    /**
     * table content height
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentHeight : number
    
    /**
     * table content width
     * 
     * @private
     * @member {number}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentWidth : number
    
    /**
     * scroll record to detect scroll direction
     * 
     * @private
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    scrollPosList : com_yung_util_BasicList<number>
    
    /**
     * display window id
     * 
     * @member {string}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    winId : string
    winDiv : any
    contentScrollDivId : string
    
    /**
     * display content id
     * 
     * @member {string}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    contentDivId : string
    tableId : string
    tableCls : string
    rowElementTypeName : string
    
    /**
     * Callback for render row html
     *
     * @callback RowRenderCallback
     * @param {number} rowId - row id
     * @return {string} row HTML code
     */
    /**
     * Callback for render row html
     * @member {RowRenderCallback}
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    rowRenderCallback : Function
    
    /**
     * constructor
     * @memberof com_yung_util_SmartRender
     * @param  {string} id - window id
     * @param  {string} contentId - content div id
     * @param  {number} dataSize - total data size
     * @param  {RowRenderCallback} rowRenderCallback - Callback for render row html
     * @param  {string} tableId - content table id [optional]
     * @param  {string} rowElementTypeName - row element type name [optional, default : 'tr']
     */
    constructor (id: string, contentId: string, dataSize: number, rowRenderCallback: Function, tableId: string, rowElementTypeName: string)
    
    /** 
     * create display bar to cover original bar
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    creatDisplayBar (): void 
    
    /** 
     * render display bar
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    render (): void 
    
    /** 
     * bind scroll event
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    protected bindScroll (): void 
    
    /** 
     * enable display bar draggable
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    private enableBarDrag (): void 
    protected getSpan (rowId: number): Array<any>
    private makeup (length: number, maxIdx: number, array: Array<any>): Array<any>
    
    /** 
     * insert scroll position
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} sTop - scroll top position
     */
    insertScroll (sTop: number): void
    
    /** 
     * return scroll direction
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} scroll direction, up : -1, down : 1, unknown : 0
     */
    scrollDirection (): number
    
    /** 
     * return window div first row id
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} row id
     */
    getWindowFirstRowId (sTop: number): object
    
    /** 
     * load next buffer
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} new scroll top position
     */
    loadNext (rowId: number): number
    private removeTop (): void
    
    /** 
     * load previous buffer
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @return {number} new scroll top position
     */
    loadPrev (rowId: number): number
    private removeBottom (): void
    
    /** 
     * get HTML code from start and end row id
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} start - start row id
     * @param {number} end - end row id
     * @return {string} html code
     */
    getHtml (start: number, end: number): string
    
    /** 
     * get HTML code from span array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {Array} span - span array
     * @return {string} html code
     */
    toRowHtml (span: Array<number>): string
    
    /** 
     * split start row id and end row id to array
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} start - start row id
     * @param {number} end - end row id
     * @return {Array} span array
     */
    split (start: number, end: number): Array<number>
    
    /** 
     * convert span array to HTML code
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {Array} span - span array
     * @return {string} HTML code
     */
    toContentHtml (span: Array<number>): string
    
    /** 
     * preload content html
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    preload (): void
    
    /** 
     * adjust buffer and preload size to keep better performance
     * 
     * @private
     * @instance
     * @memberof com_yung_util_SmartRender
     */
    adjustBuffer (): void
    
    /** 
     * get row HTML code by row id
     * Note: override method or use callback
     * 
     * @instance
     * @memberof com_yung_util_SmartRender
     * @param {number} rowId - row id
     * @return {string} HTML code
     */
    getRowHtml (rowId: number): string
    
    static scroll(container: any, contentScrollDivId: string, event: Event): void

}

/**
 * Table Utility Schema Data Object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_TableGridSchema extends $Class {
    
    classProp : { 
        name : "com.yung.util.TableGridSchema",
        unimplemented: [],
        typeArray : ['edtxt','ed','edn','co','select','ch', 'date']
    }
    
    /**
     * td align
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    align : string
    
    /**
     * td vertical align
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    vAlign : string
    
    /**
     * allow null value
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    allowNull : boolean
    
    /**
     * column color code
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnColor : string
    
    /**
     * column hidden
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnHidden : boolean
    
    /**
     * column id
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnId : string
    
    /**
     * column validator, a drop list to select
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    columnValidator : Array<string>
    
    /**
     * column combo map, a drop key/value to select
     * @member {object}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    comboMap : object
    
    /**
     * date format, ex. MM/dd/yyyy
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    dateFormat : string
    numberFormat: string
    
    /**
     * column header style
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    headStyle : string
    
    /**
     * column header text
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    header : string
    
    /**
     * column index
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    index : number
    
    /**
     * column type
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    type : string
    
    /**
     * column width
     * @member {string}
     * @instance
     * @memberof com_yung_util_TableGridSchema
     */
    width : string

    length: number
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} columnId - column id
     * @param {string} width - column width
     * @param {string} header - column header text
     */
    constructor (columnId: string, width: string, header: string)
    
    /** 
     * set property value
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} attribute - column id
     * @param {all} value - value
     */
    setProperty (attribute: string, value: any): void
    
    /** 
     * get property value
     * 
     * @instance
     * @memberof com_yung_util_TableGridSchema
     * @param {string} attribute - column id
     * @return {all} value
     */
    getProperty (attribute: string): any
    
    setAlign (align: string): void

    getAlign (): string

    setVAlign (vAlign: string): void

    getVAlign (): string

    setAllowNull (allowNull: boolean): void

    getAllowNull (): boolean

    setColumnColor (columnColor: string): void

    getColumnColor (): string

    setColumnHidden (columnHidden: boolean): void

    getColumnHidden (): boolean

    setColumnId (columnId: string): void

    getColumnId (): string

    setColumnValidator (columnValidator: Array<string>): void

    getColumnValidator (): Array<string>

    setComboMap (comboMap: object): void

    getComboMap (): object

    setDateFormat (dateFormat: string): void

    getDateFormat (): string

    setHeadStyle (headStyle: string): void

    getHeadStyle (): string

    setHeader (header: string): void

    getHeader (): string

    setIndex (index: number): void

    getIndex (): number

    setType (type: string): void

    getType (): string

    setWidth (width: string): void

    getWidth (): string

    static convertToLZString (SchemaArray: Array<com_yung_util_TableGridSchema>): string
    
}

/**
 * Column Definition of Table Utility 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_ColumnUtil extends $Class {
    
    classProp : { 
        name : "com.yung.util.ColumnUtil",
        unimplemented : []
    }
    
    /**
     * column schema array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_ColumnUtil
     */
    columnArray: Array<com_yung_util_TableGridSchema>
    columnMap : object
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} lzColumnInfo - column information encoded to LZString 
     */
    constructor (lzColumnInfo: string)
    
    /** 
     * get column array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @return {Array} column array
     */
    getColumnArray (): Array<com_yung_util_TableGridSchema>
    
    /** 
     * get column schema
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @return {object} column schema object
     */
    getColumn (columnId: string): com_yung_util_TableGridSchema
    
    /** 
     * get column index
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @return {number} column index
     */
    getColumnIndex (columnId: string): number
    
    /** 
     * get column attribute value
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} columnId - column id
     * @param {string} attribute - column attribute
     * @return {all} column attribute value
     */
    getColumnAttribute (columnId: string, attribute: string): com_yung_util_TableGridSchema
    
    /** 
     * get column attribute value string array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} attribute - column attribute
     * @return {string} array of column attribute value separate by ','
     */
    getColumnAttrArrayString (attribute: string): string 
    
    /** 
     * get column attribute array
     * 
     * @instance
     * @memberof com_yung_util_ColumnUtil
     * @param {string} attribute - column attribute
     * @return {Array} array of column attribute value
     */
    getColumnAttrArray (attribute: string): Array<any>

}

// specific smart render for table utility
export declare class com_yung_util_GridSmartRender extends com_yung_util_SmartRender {
    
    // @ts-ignore
    classProp : {
        name : "com.yung.util.GridSmartRender",
        unimplemented: []
    }
    
    grid : com_yung_util_TableGridUtil
    rowElementTypeName : string
    off : boolean
    
    constructor (grid: com_yung_util_TableGridUtil)
    
    enableBarDrag (): void
    
    getRowHtml (rowId: number): string
    
    preload (): void 
    
    loadNext (rowId: number) : number
    
    loadPrev (rowId: number): number
    
    toContentHtml (span: any): string
    
    removeTop (): void
    
    removeBottom (): void
    
}

export declare class com_yung_util_TableErrorCell extends $Class {
    
    classProp : { 
        name : "com.yung.util.TableErrorCell",
        unimplemented: []
    }
    
    rowIdx : string

    columnId : string
    
    constructor (rowIdx: string, columnId: string)
    
}

/**
 * Table Utility 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_TableGridUtil extends $Class {
    
    classProp : { 
        name : "com.yung.util.TableGridUtil",
        commentBase64 : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTZEaa/1AAABVElEQVRoQ+2RW2rDQAxFB+KfFEPJn7eTTXT/O0mn+Iq6J03ixzwkyIHDkHgk3UHpltItssngh0h+XS75EPwYRcX/hRci+GcTBi95V7Hv4UXPXs/nfDyAl72quI9hgUc/T6d8vIBF3lTM17DQk6s2YbDYi4q3Hjbw4EdW8dbDJr1VrO2wUU93bcJgs14qzn7YsIeHNmGwaWsV4zhs3NIimzDYvJUaXw4OaGHRTRgcUluNLQ8H1bTKJgwOq6XG1YMDa1h1EwaHllZj6sPBJW2yCYPDS6n27WCAEjbdhMEQR1Xb9jDIEbtswmCYvapdPxhoj103YTDUVtWmPwy2xbncCctgW1S5HxhwjXOZM5YB16gyfzDoM+frTlkGfaau+4WB/3MYhnw4h6GprvmHwZeG2ITB8KY+x4EP+HEcx3wEg4/Q3/FYPmKapvgP0c+4hN/EG5ek9A0zMUHnKiaJVAAAAABJRU5ErkJggg==",
        unimplemented: []    
    }
    
    gridDivId : string
    hashId : string
    gridCls : string
    columnUtil : com_yung_util_ColumnUtil
    errorCells : com_yung_util_ObjectHashSet<com_yung_util_TableErrorCell>
    dirtyRowMap : com_yung_util_BasicMap<string, string>
    gridStyle : string
    headerCls : string
    rowCls : string
    cellCls : string
    tableTemplate : com_yung_util_BasicTemplate
    headerTemplate : com_yung_util_BasicTemplate
    cacheRowMap : com_yung_util_BasicMap<string, object>
    rowOrderList : com_yung_util_BasicList<string>
    commentMap : system_util_Map<string, com_yung_util_BasicMap<string, string>>
    paging : boolean
    currentPageIndex : number
    tableAlign : string
    smartRender : com_yung_util_GridSmartRender
    combo : com_yung_util_SelectCombo
    datePicker : com_yung_util_GridDatePicker
    fireScrollManually : boolean
    
    /**
     * Callback after grid edit value
     *
     * @callback AfterGridEdit
     * @param {jQueryEle} ele - jQuery table cell element
     * @param {string} rowIdx - row id string
     * @param {number} colIdx - column index
     * @param {com_yung_util_TableGridUtil} grid - this table grid
     * @param {string} newContent - new content for after edit
     */
    /**
     * Callback after grid edit value
     * @member {AfterGridEdit}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    afterEdit : Function
    
    /**
     * Callback before grid edit value. return false will block edit
     *
     * @callback BeforeGridEdit
     * @param {jQueryEle} ele - jQuery table cell element
     * @param {string} rowIdx - row id string
     * @param {number} colIdx - column index
     * @param {com_yung_util_TableGridUtil} grid - this table grid
     * @param {string} oldContent - old content for before edit
     */
    /**
     * Callback before grid edit value
     * @member {BeforeGridEdit}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    beforeEdit : Function
    
    /**
     * paging size, if null for not paging
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    pageSize : number
    
    /**
     * border width size
     * @member {number}
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    borderWidth : number
    static fireScrollManually: boolean
    
    /** 
     * constructor
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} gridDivId - grid div id
     * @param {com_yung_util_ColumnUtil} columnUtil - column information schema utility
     * @param {string} gridCls - grid CSS name, default 'yung-tableborder'
     * @param {string} gridStyle - grid style, default 'table-layout:fixed; word-break:break-all;'
     * @param {string} headerCls - grid header CSS name, default 'yung-tableHeader'
     * @param {string} cellCls - grid cell CSS name, default 'yung-tableCell'
     * @param {string} tableAlign - grid align, default left
     * @param {string} rowCls - grid row CSS name, default 'yung-tableRow'
     */
    constructor (gridDivId: string, columnUtil: com_yung_util_ColumnUtil, gridCls?: string, gridStyle?: string, headerCls?: string, cellCls?: string, tableAlign?: string, rowCls?: string)
    
    /** 
     * set after grid edit callback
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {AfterGridEdit} funct - after grid edit callback
     */
    setAfterEdit (funct: Function): void
    
    /** 
     * set before grid edit callback
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {BeforeGridEdit} funct - before grid edit callback
     */
    setBeforeEdit (funct: Function): void
    
    /** 
     * set paging size
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} pageSize - paging size
     */
    setPageSize (pageSize: number): void
    private createGrid (): void
    
    /** 
     * hide grid page bar
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    hidePageHtml (): void
    
    /** 
     * show grid page bar
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    showPageHtml (): void
    getHeaderHtml (): string
    private getPageHtml (titleData?: object): string
    
    /** 
     * get current page number
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {number} current page number
     */
    getPageNum (): number
    
    /** 
     * get current total record number
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {number} current total record number
     */
    getRowCnt (): number
    
    /** 
     * load JSON for url
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} dUrl - Ajax Url
     * @param {function} unblock - function after call
     */
    loadJSON (dUrl: string, unblock?: Function): void
    private setupTableTemplate (): boolean
    
    /** 
     * load data
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {Array} rows - data object array
     * @param {number} pageIndex - page index to render
     */
    loadData (rows: Array<object>, pageIndex?: number): void
    
    /** 
     * load page
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} pageIndex - page index to render
     */
    loadPage (pageIndex: number): void
    
    /** 
     * refresh and re-render grid, use when turn paging on/off, smart-render on/off
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    refresh (): void
    private setupData (rows: Array<object>): void
    renderPageRow (pageIndex: number): string
    renderRows (rows: Array<object>): string
    
    /** 
     * re-render grid, only adjust width, show/hide column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    render (): void
    private adjustHeight (): void
    private adjustWidth (): void
    
    /** 
     * hide column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} arg - column id
     */
    hideColumn (columnId: string | number): void
    
    /** 
     * show column
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} arg - column id
     */
    showColumn (columnId: string | number): void
    
    /** 
     * set update status to row
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowIdx - row id
     * @param {string} state - row state
     */
    setUpdated (rowIdx: string, state?: string): void
    
    /** 
     * get all row id by order
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @return {Array} row id array
     */
    getAllRowIds (): Array<string>

    /** 
     * set cell value
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} value - cell value
     */
    setCellValue (rowIdx: string, columnId: string, value: string): void
    
    /** 
     * mark cell as delete, CSS text-decoration: line-through
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    setCellDelete (rowIdx: string, columnId: string): void
    
    /** 
     * mark cell as update, CSS font-weight: bold
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    setCellBold (rowIdx: string, columnId: string): void
    
    /** 
     * set cell background color
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowIdx - row id
     * @param {string} columnId - column id
     * @param {string} color - color code
     */
    setCellColor (rowIdx: string, columnId: string, color: string): void
    
    /** 
     * get cell value
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell value
     */
    getCellValue (rowIdx: string, columnId: string): string
    
    /** 
     * get cell html, different with getCellValue, 
     * ex. ch type cell value is '0' or '1' not html 
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell html
     */
    getCellHtml (rowId: string, columnId: string): string
    
    /** 
     * get cell checked or not, only for type ch only 
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {boolean} checked or not
     */
    getCellCheck (rowId: string, columnId: string): boolean
    private getNewRowId (): string
    
    /** 
     * add a new empty row by index
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} index - row index to add
     * @param {string} idColumn - id column name
     * @return {boolean} call render or not
     */
    addEmptyRow (index: number, idColumn: string, rendor?: boolean): string
    
    /** 
     * get all rows which state is not empty
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {boolean} validate - run validate or not
     * @return {Array} rows which state is not empty
     */
    getChangedRows (): Array<string>
    
    /** 
     * delete grid row, if row state is inserted then remove row,
     * if state is not inserted, then mark as delete
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {boolean} forceDelete - remove row immediately 
     */
    deleteRow (rowIdx: string, forceDelete?: boolean): void
    
    /** 
     * get all rows which state is not empty, and validate row
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {boolean} validate - run validate or not
     * @return {Array} rows which state is not empty
     */
    getAllUpdateData (validate?: boolean): Array<string>
    
    /** 
     * clean all grid state and data
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clean (): void
    
    /** 
     * clean all grid state and data, then re-render
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clearAll (): void
    
    /** 
     * clean all grid error cell, rollback yellow color
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     */
    clearErrorCells (): void
    
    /** 
     * add error cell information, set cell color yellow
     * and mark cell error
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} rowId - row id
     * @param {string} columnId - id column name
     */
    addErrorCells (rowIdx: string, columnId: string): void
    
    /** 
     * validate current grid rows, and put error information into error cells.
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {Array} dataArray - row data array, optional
     * @return {boolean} with error or not
     */
    validate (dataArray?: Array<string>): boolean
    
    /** 
     * get row data as data object
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @return {object} data object
     */
    getRowData (rowIdx: string): object
    getRowNumber (rowIdx: string): number
    fireAfterEdit (ele: JQuery, rowIdx: string, colIdx: number, newContent: string): any
    fireBeforeEdit (ele: JQuery, rowIdx: string, colIdx: number, oldcontent: string): any
    
    /** 
     * get current grid cell value, Note: ch type will return '1' or '0'
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} cell value
     */
    getCurrentCellValue (rowIdx: string, columnId: string): string
    
    /** 
     * change page index and render html
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {number} index - page index
     */
    changePage (index: number): void
    private edit (cellId: string): void
    private createTextEditor (): void
    private editTxt (cellId: string): void
    private createSelectEditor (): void
    private select (cellId: string): void
    private pickDate (cellId: string): void
    private prepareTxtDatePicker (cellId: string): void
    private createDatePicker (): void
    private clickCheck (cellId: string): void
    private editDone (editor: JQuery, lzContent: string): void
    private editTxtDone (cellId: string): string
    private selectDone (cellId: string, selectContent: string): string
    
    /** 
     * set comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @param {string} comment - comment text
     */
    setComment (rowIdx: string, columnId: string, comment: string): void
    private renderComment (rowIdx: string, columnId: string, comment: string): void
    private insertCommentPng (rowIdx: string, colIdx: number, tdHtml: string, comment: string): void
    
    /** 
     * get comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     * @return {string} comment text
     */
    getComment (rowIdx: string, columnId: string): string
    
    /** 
     * remove comment in grid
     * 
     * @instance
     * @memberof com_yung_util_TableGridUtil
     * @param {string} rowId - row id
     * @param {string} columnId - column id
     */
    removeComment (rowId: string, columnId: string): void
    private deleteCommentPng (rowIdx: string, colIdx: number): void

    static instance (gridDivId: string, columnUtil: com_yung_util_ColumnUtil, gridCls?: string, gridStyle?: string, headerCls?: string, cellCls?: string, tableAlign?: string, rowCls?: string) : com_yung_util_TableGridUtil

}