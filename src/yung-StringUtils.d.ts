/**
 * String utility, require {@link com_yung_util_Validator}
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_StringUtils {
    
	classProp : { 
		name : "com.yung.util.StringUtils"
	}
	
	/** 
     * check string is empty or null
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @return {boolean} true or false
     */
    static isBlank (str: string): boolean
    
    /** 
     * Returns string, with leading and trailing whitespace omitted.
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - target string
     * @return {string} result string
     */
    static trim (str: string): string
    
    /** 
     * check string if it start with pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    static startsWith (str: string, pattern: string): boolean
    
    /** 
     * check string if it end with pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    static endsWith (str: string, pattern: string): boolean
    
    /** 
     * check string if it contains pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    static contains (str: string, pattern: string): boolean
    
    /** 
     * check string and replace string pattern to specific string
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} targetStr - target string
     * @param {string} strFind - string pattern
     * @param {strReplace} strFind - string to replace pattern
     * @return {string} result string
     */
    static replaceAll (targetStr: string, strFind: string, strReplace: string): string
    
    /** 
     * check string and remove string pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} targetStr - target string
     * @param {string} pattern - string pattern
     * @return {string} result string
     */
    static remove (str: string, pattern: string): string
    
    /** 
     * if string is null then return empty string
     * otherwise return itself
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - check string
     * @return {string} result string
     */
    static defaultString (str: string): string
    
    /** 
     * convert string to number
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to convert
     * @return {number} result number
     */
    static toNumber (str: string): number
    
    /** 
     * check string if it can convert to number
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @return {boolean} true or false
     */
    static isNumeric (str: string): boolean
    
    /** 
     * parse input url string, then convert to object map
     * , parameter key and parameter value, value will decodeURIComponent.
     * Note. if url is null then use window location
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} url - url string
     * @return {object} object map of parse result
     */
    static parseURL (url: string): object
    
    /** 
     * parse input url string, then convert to object map
     * , parameter key and parameter value, value will not decodeURIComponent.
     * Note. if url is null then use window location
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} url - url string
     * @return {object} object map of parse result
     */
    static parseURINotDecode (url: string): object
    
    /** 
     * parse object map, then revert to string url, like '&a=1&b=2'
     * 
     * @memberof com_yung_util_StringUtils
     * @param {object} map - object map
     * @return {string} url string
     */
    static mapToURL (map: object): string
    
    /** 
     * calculate string length, for those special string character(non ascii)
     * will be consider as 3 length(default), or specific length
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} input - target sting
     * @param {number} nonASCIILength - the length of special string character(non ascii)
     * @return {number} total length
     */
    static calcLength (input: string, nonASCIILength: number): number
    
    /** 
     * substring, for those special string character(non ascii)
     * will be consider as 3 length(default), or specific length
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} input - target sting
     * @param {number} length - final total length
     * @param {number} nonASCIILength - the length of special string character(non ascii)
     * @return {string} result string
     */
    static cutLength (input: string, length: number, nonASCIILength: number): string
}