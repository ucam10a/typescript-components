import { $Class } from "./yung-JS-extend";

/**
 * To defined tool bar item schema
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_ToolbarItem extends $Class {
    
    classProp : { 
        name : "com.yung.util.ToolbarItem",
        unimplemented: []
    }
    
    /**
     * item image path
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemImg : string
    
    /**
     * item label text
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemLabel : string
    
    /**
     * item tooltip text
     * @member {string}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemTooltip : string
    
    /**
     * item function to run
     * @member {function}
     * @instance
     * @memberof com_yung_util_ToolbarItem
     */
    itemFunct : Function
    
    
    /**
     * constructor
     * @memberof com_yung_util_ToolbarItem
     * @param {string} itemImg - item image path
     * @param {string} itemLabel - item label text
     * @param {string} itemTooltip - item tooltip text
     * @param {createTabCallback} itemFunct - item function to run
     */
    constructor (itemImg: string, itemLabel: string, itemTooltip: string, itemFunct: Function)
    
    /** 
     * set property value
     * 
     * @instance
     * @memberof com_yung_util_ToolbarItem
     * @param {string} attribute - property key
     * @param {allType} value - property value
     */
    setProperty (attribute: string, value: any): void
    
    /** 
     * get property value
     * 
     * @instance
     * @memberof com_yung_util_ToolbarItem
     * @param {string} attribute - property key
     * @return {allType} property value
     */
    getProperty (attribute: string): any
    
    setItemImg (itemImg: string): void
    
    getItemImg (): string
    
    setItemLabel (itemLabel: string): void
    
    getItemLabel (): string
    
    setItemTooltip (itemTooltip: string): void
    
    getItemTooltip (): string
    
    setItemFunct (itemFunct: Function): void 
    
    getItemFunct (): Function

}

/**
 * Create toolbar tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Toolbar extends $Class {
    
    classProp : { 
        name : "com.yung.util.Toolbar",
        unimplemented: []
    }
    
    /**
     * toolbar container
     * @member {HTMLElement}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    container : HTMLElement
    
    /**
     * toolbar button css
     * @member {CSSStyle}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    btnCSS : HTMLElement
    
    /**
     * toolbar background color
     * @member {string}
     * @instance
     * @memberof com_yung_util_Toolbar
     */
    color : string
    
    /**
     * constructor
     * @memberof com_yung_util_Toolbar
     * @param {string} containerDivId - toolbar container id
     * @param {string} btnCSS - button css style name
     * @param {string} color - toolbar background color
     */
    private constructor (containerDivId: string, btnCSS: string, color: string)
    
    /** 
     * create and add separator element
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @return {HTMLElement} separator element
     */
    addSeparator (): HTMLSpanElement
    
    /** 
     * create button by item schema and set button function
     * 
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param  {Array} items - item schema array
     */
    addItems (items: Array<HTMLElement>): void
    
    /** 
     * initialize button function
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param {Array} items - item array
     */
    initFunction (items: Array<HTMLElement>): void

    private addClickHandler (id: string, funct: Function): void
    
    
    /** 
     * initialize button style, image or label
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Toolbar
     * @param  {string} id - button id
     * @param  {string} itemImg - button image path
     * @param  {string} label - button label text
     * @param  {string} tooltip - button tooltip
     * @return {HTMLElement} button element
     */
    addItem (id: string, itemImg: string, label: string, tooltip: string): HTMLAnchorElement

    addButton (id: string, itemImg: string, label: string, tooltip: string): HTMLAnchorElement

    createButton (itemImg: string, tooltip: string): HTMLAnchorElement

    initElement (id: string, elt: HTMLAnchorElement, label: string, tooltip: string): void

    createCSS (itemImg: string, tooltip: string): void

    createLabel (label: string): HTMLAnchorElement
    
    static instance (containerDivId: string, btnCSS: string, color: string) : com_yung_util_Toolbar

}