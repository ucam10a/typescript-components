// Copyright (c) 2013 Pieroxy <pieroxy@pieroxy.net>
// This work is free. You can redistribute it and/or modify it
// under the terms of the WTFPL, Version 2
// For more information see LICENSE.txt or http://www.wtfpl.net/
//
// For more information, the home page:
// http://pieroxy.net/blog/pages/lz-string/testing.html
//
// LZ-based compression algorithm, version 1.4.4
// modified by: Yung Long Li

//yung_global_var['com_yung_util_LZString.f'] = String.fromCharCode;
//yung_global_var['com_yung_util_LZString.keyStrUriSafe'] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*-$";
//yung_global_var['com_yung_util_LZString.baseReverseDic'] = {};
/**
 * LZString utility
 *
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_LZString {

	classProp : {
		name : "com.yung.util.LZString"
	}

	private static getBaseValue (alphabet: string, character: string)

	// compress into a string that is already URI encoded
	private static compressToEncodedURIComponent(input: string): string

	/**
     * convert source string to LZString code
     *
     * @memberof com_yung_util_LZString
     * @param {String} input - source string
     * @return {String} LZString code
     */
	static encode(input: string): string

	// decompress from an output of compressToEncodedURIComponent
	private static decompressFromEncodedURIComponent (input: string): string

	/**
     * convert LZString code to source string
     *
     * @memberof com_yung_util_LZString
     * @param {String} input - LZString code
     * @return {String} source string
     */
	static decode (input: string): string

	static compress (uncompressed: string): string

	private static _compress (uncompressed: string, bitsPerChar: number, getCharFromInt: Function)

	static decompress(compressed: string): string

	private static _decompress (length: number, resetValue: number, getNextValue: Function): string
}