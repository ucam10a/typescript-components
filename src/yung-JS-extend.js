var jsdom = require("jsdom");
const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;
var $ = jQuery = require('jquery')(window);

/**
 * global variable object
 */
var yung_global_var = {
    _global_ajaxTimeout : null,
    YungClassProperty : {}
};

/**
 * Base component, like jQuery '$'
 * 
 */
window["$Y"] = function () {};

/**
 * register class to package
 * 
 * @param {$Class} type - class type
 */
window["$Y"]["reg"] = function (type) {
	if (type != null) {
		var className = type.classProp.name;
		window["$Y"]["register"](className, type);
	}
}

/**
 * register class to package
 * 
 * @param {string} className - class name
 * @param {$Class} type - class type
 */
window["$Y"]["register"] = function (className, type) {
    var clsArray = className.split(".");
    if (clsArray.length == 1) {
        return;
    }
    var idx = className.lastIndexOf(".");
    var packageName = className.substring(0, idx);
    var pack = _packageCheck(packageName);
    var clsName = className.substring(idx + 1, className.length);
    if (type != null) {
        if (pack[clsName] == null) {
            if (typeof type == "function") {
                pack[clsName] = type;
                return type;
            } else if (typeof type == "object") {
                pack[clsName] = type;
                return type;
            } else {
                throw "argument type is not function";
            }
        } else {
            throw className + " is already registered";
        }
    }
}

/**
 * import class to base component
 * 
 * @param {string} className - class name
 */
window["$Y"]["import"] = function (className) {
    if (typeof className != "string") {
        return;
    }
    var clsArray = className.split(".");
    if (clsArray.length == 1) {
        return;
    }
    var idx = className.lastIndexOf(".");
    var packageName = className.substring(0, idx);
    var pack = _packageCheck(packageName);
    var clsName = className.substring(idx + 1, className.length);
    if (clsName == '*') {
        for (var key in pack) {
            if (typeof pack[key] == 'function') {
                $Y[key] = pack[key];
            } else if (typeof pack[key] == 'object') {
                // for static import
                $Y[key] = pack[key];
            }
        }
    } else {
        if (typeof pack[clsName] == 'function') {
            $Y[clsName] = pack[clsName];
        } else if (typeof pack[clsName] == 'object') {
            // for static import
            $Y[clsName] = pack[clsName];
        } else if (pack[clsName] == null) {
            $Y.register(className);
            $Y[clsName] = pack[clsName];
        }
    }
}

/**
 * equal to import function
 * 
 */
window["$Y"]["def"] = window["$Y"]["import"];

/**
 * convenient to detact scroll end
 * 
 */
jQuery.fn.scrollEnd = function(callback, timeout) {          
    jQuery(this).scroll(function(){
    var $this = jQuery(this);
    if ($this.data('scrollTimeout')) {
      clearTimeout($this.data('scrollTimeout'));
    }
    $this.data('scrollTimeout', setTimeout(callback,timeout));
  });
};
/*
jQuery(window).scrollEnd(function(){
    alert('stopped scrolling');
}, 1000);
 */

var _base;
function _packageCheck(packages) {
    if (typeof packages == "string") {
        packages = packages.split(".");
    }
    _base = window;
    if (packages != null && packages.length > 0) {
        for (var i = 0; i < packages.length; i++) {
            if (_base[packages[i]] == null) {
                _base[packages[i]] = {};
            }
            _base = _base[packages[i]];
        }
    }
    return _base;
}

function _cacheClassProperty (clsName, key, value) {
    if (yung_global_var["YungClassProperty"] == null) {
        yung_global_var["YungClassProperty"] = {};
    }
    if (yung_global_var["YungClassProperty"][clsName] == null) {
        yung_global_var["YungClassProperty"][clsName] = {};
    }
    if (value != null) {
        if (jQuery.isArray(value)) {
            if (value.length != 0) {
                throw clsName + " initial property["+ key +"] Array should be empty!";
            }
            yung_global_var["YungClassProperty"][clsName][key] = new Array();
        } else if (typeof value == 'object') {
            if (jQuery.isPlainObject(value) == false) {
                throw clsName + " initial property["+ key +"] Object should be empty!";
            }
            yung_global_var["YungClassProperty"][clsName][key] = new Object();
        } else if (typeof value != 'string' && typeof value != 'number' && typeof value != 'boolean') {
            throw clsName + " initial property[" + key + "] type should be string, number, boolean, empty array or empty object";
        } else {
            yung_global_var["YungClassProperty"][clsName][key] = value;
        }
    } else {
        yung_global_var["YungClassProperty"][clsName][key] = null;
    }
}

function _getSuperClass (clsName) {
    return yung_global_var["YungClassProperty"][clsName]['superClass'];
}

function _getClassHierarchy(clsName) {
    var currentCls = clsName;
    var temp = [];
    temp.push(currentCls);
    while (true) {
        var currentCls = _getSuperClass(currentCls);
        if (currentCls != null) {
            temp.push(currentCls);
        } else {
            break;
        }
    }
    var ret = [];
    for (var i = temp.length - 1; i >= 0; i--){
        ret.push(temp[i]);
    }
    return ret;
}

function _replaceAll (targetStr, strFind, strReplace) {
    var index = 0;
    while (targetStr.indexOf(strFind, index) != -1) {
        targetStr = targetStr.replace(strFind, strReplace);
        index = targetStr.indexOf(strFind, index);
    }
    return targetStr;
}

/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
 
  // The base AbstractClass implementation (does nothing)
  this.AbstractClass = function(){};
 
  // Create a new AbstractClass that inherits from this class
  AbstractClass.extend = function(prop) {
    var _super = this.prototype;
    
    var unimplemented = [];
    if (typeof _super.classProp != 'undefined') {
        if (_super.classProp.name != '$Class') {
            var originalName = prop.classProp.name;
            var originalSuperName = _super.classProp.name;
            _cacheClassProperty(originalName, 'superClass', originalSuperName);
        }
        if (_super["classProp"]["unimplemented"] != null && _super["classProp"]["unimplemented"].length > 0) {
            if (jQuery.isArray(_super["classProp"]["unimplemented"]) == false) {
                throw "classProp.unimplemented is not Array";
            }
            for (var i = 0; i < _super["classProp"]["unimplemented"].length; i++) {
                unimplemented.push(_super["classProp"]["unimplemented"][i]);
            }
        }
    }
    
    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;
    var cls = null;
    var pack = null;
    
    if (typeof prop["classProp"] == 'undefined') {
    	throw "Please define classProp in Class";
    }
    
    // Copy the properties over onto the new prototype
    for (var name in prop) {
      if (name === 'classProp') {
          try {
              Object.freeze(prop[name]);
          } catch (err) {
              // for IE quirk mode
          }
          var className = prop[name].name;
          if (yung_global_var["YungClassProperty"][className] == null) {
              yung_global_var["YungClassProperty"][className] = {};
          }
          var packages = className.split(".");
          if (packages.length > 1) {
            cls = packages[packages.length - 1];
            packages.splice(packages.length - 1, 1);
            pack = _packageCheck(packages);
          } else {
            cls = null;
            pack = null;
          }
      } else {
          if (typeof prop[name] != "function") {
              var originalName = prop.classProp.name;
              _cacheClassProperty(originalName, name, prop[name]);
          } else {
              var index = jQuery.inArray(name, unimplemented);
              if (index >= 0) {
                  unimplemented.splice(index, 1);
              }
          }
      }
      // Check if we're overwriting an existing function
      prototype[name] = typeof prop[name] == "function" &&
        typeof _super[name] == "function" && fnTest.test(prop[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;
           
            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];
           
            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);        
            this._super = tmp;
           
            return ret;
          };
        })(name, prop[name]) :
        prop[name];
    }
    
    if (unimplemented != null && unimplemented.length > 0) {
        var todo = prop["classProp"]["unimplemented"];
        if (todo != null && todo.length > 0) {
            for (var i = 0; i < todo.length; i++) {
                var index = jQuery.inArray(todo[i], unimplemented);
                if (index >= 0) {
                    unimplemented.splice(index, 1);
                }
            }
        }
        if (unimplemented.length > 0) {
            throw "Class{" + prop["classProp"]["name"] + "} please implement method:" + JSON.stringify(unimplemented);    
        }
    }
    
    // The dummy class constructor
    function AbstractClass() {
      // block abstract or interface to create intance
      if (!initializing) {
          if (this["classProp"] != null && this["classProp"]["unimplemented"] != null && this["classProp"]["unimplemented"].length > 0) {
              throw "Class{" + this["classProp"]["name"] + "} is abstract or interface, can not create instance!";
          }   
      }
      // All construction is actually done in the init method
      if ( !initializing && this.init ) {
          if (typeof this.inheritProp == 'function') {
              this.inheritProp();
          }
          this.init.apply(this, arguments);
      }
    }
   
    // Populate our constructed prototype object
    AbstractClass.prototype = prototype;
   
    // Enforce the constructor to be what we expect
    AbstractClass.prototype.constructor = $Class;
 
    // And make this class extendable
    AbstractClass.extend = arguments.callee;
    
    if (pack != null && cls != null) {
        pack[cls] = AbstractClass;
    }
    return AbstractClass;
  };
})();

/*
 * JavaScript MD5
 * https://github.com/blueimp/JavaScript-MD5
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 *
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */

/* global define */

;(function ($) {
  'use strict'

  /*
  * Add integers, wrapping at 2^32. This uses 16-bit operations internally
  * to work around bugs in some JS interpreters.
  */
  function safeAdd (x, y) {
    var lsw = (x & 0xffff) + (y & 0xffff)
    var msw = (x >> 16) + (y >> 16) + (lsw >> 16)
    return (msw << 16) | (lsw & 0xffff)
  }

  /*
  * Bitwise rotate a 32-bit number to the left.
  */
  function bitRotateLeft (num, cnt) {
    return (num << cnt) | (num >>> (32 - cnt))
  }

  /*
  * These functions implement the four basic operations the algorithm uses.
  */
  function md5cmn (q, a, b, x, s, t) {
    return safeAdd(bitRotateLeft(safeAdd(safeAdd(a, q), safeAdd(x, t)), s), b)
  }
  function md5ff (a, b, c, d, x, s, t) {
    return md5cmn((b & c) | (~b & d), a, b, x, s, t)
  }
  function md5gg (a, b, c, d, x, s, t) {
    return md5cmn((b & d) | (c & ~d), a, b, x, s, t)
  }
  function md5hh (a, b, c, d, x, s, t) {
    return md5cmn(b ^ c ^ d, a, b, x, s, t)
  }
  function md5ii (a, b, c, d, x, s, t) {
    return md5cmn(c ^ (b | ~d), a, b, x, s, t)
  }

  /*
  * Calculate the MD5 of an array of little-endian words, and a bit length.
  */
  function binlMD5 (x, len) {
    /* append padding */
    x[len >> 5] |= 0x80 << (len % 32)
    x[((len + 64) >>> 9 << 4) + 14] = len

    var i
    var olda
    var oldb
    var oldc
    var oldd
    var a = 1732584193
    var b = -271733879
    var c = -1732584194
    var d = 271733878

    for (i = 0; i < x.length; i += 16) {
      olda = a
      oldb = b
      oldc = c
      oldd = d

      a = md5ff(a, b, c, d, x[i], 7, -680876936)
      d = md5ff(d, a, b, c, x[i + 1], 12, -389564586)
      c = md5ff(c, d, a, b, x[i + 2], 17, 606105819)
      b = md5ff(b, c, d, a, x[i + 3], 22, -1044525330)
      a = md5ff(a, b, c, d, x[i + 4], 7, -176418897)
      d = md5ff(d, a, b, c, x[i + 5], 12, 1200080426)
      c = md5ff(c, d, a, b, x[i + 6], 17, -1473231341)
      b = md5ff(b, c, d, a, x[i + 7], 22, -45705983)
      a = md5ff(a, b, c, d, x[i + 8], 7, 1770035416)
      d = md5ff(d, a, b, c, x[i + 9], 12, -1958414417)
      c = md5ff(c, d, a, b, x[i + 10], 17, -42063)
      b = md5ff(b, c, d, a, x[i + 11], 22, -1990404162)
      a = md5ff(a, b, c, d, x[i + 12], 7, 1804603682)
      d = md5ff(d, a, b, c, x[i + 13], 12, -40341101)
      c = md5ff(c, d, a, b, x[i + 14], 17, -1502002290)
      b = md5ff(b, c, d, a, x[i + 15], 22, 1236535329)

      a = md5gg(a, b, c, d, x[i + 1], 5, -165796510)
      d = md5gg(d, a, b, c, x[i + 6], 9, -1069501632)
      c = md5gg(c, d, a, b, x[i + 11], 14, 643717713)
      b = md5gg(b, c, d, a, x[i], 20, -373897302)
      a = md5gg(a, b, c, d, x[i + 5], 5, -701558691)
      d = md5gg(d, a, b, c, x[i + 10], 9, 38016083)
      c = md5gg(c, d, a, b, x[i + 15], 14, -660478335)
      b = md5gg(b, c, d, a, x[i + 4], 20, -405537848)
      a = md5gg(a, b, c, d, x[i + 9], 5, 568446438)
      d = md5gg(d, a, b, c, x[i + 14], 9, -1019803690)
      c = md5gg(c, d, a, b, x[i + 3], 14, -187363961)
      b = md5gg(b, c, d, a, x[i + 8], 20, 1163531501)
      a = md5gg(a, b, c, d, x[i + 13], 5, -1444681467)
      d = md5gg(d, a, b, c, x[i + 2], 9, -51403784)
      c = md5gg(c, d, a, b, x[i + 7], 14, 1735328473)
      b = md5gg(b, c, d, a, x[i + 12], 20, -1926607734)

      a = md5hh(a, b, c, d, x[i + 5], 4, -378558)
      d = md5hh(d, a, b, c, x[i + 8], 11, -2022574463)
      c = md5hh(c, d, a, b, x[i + 11], 16, 1839030562)
      b = md5hh(b, c, d, a, x[i + 14], 23, -35309556)
      a = md5hh(a, b, c, d, x[i + 1], 4, -1530992060)
      d = md5hh(d, a, b, c, x[i + 4], 11, 1272893353)
      c = md5hh(c, d, a, b, x[i + 7], 16, -155497632)
      b = md5hh(b, c, d, a, x[i + 10], 23, -1094730640)
      a = md5hh(a, b, c, d, x[i + 13], 4, 681279174)
      d = md5hh(d, a, b, c, x[i], 11, -358537222)
      c = md5hh(c, d, a, b, x[i + 3], 16, -722521979)
      b = md5hh(b, c, d, a, x[i + 6], 23, 76029189)
      a = md5hh(a, b, c, d, x[i + 9], 4, -640364487)
      d = md5hh(d, a, b, c, x[i + 12], 11, -421815835)
      c = md5hh(c, d, a, b, x[i + 15], 16, 530742520)
      b = md5hh(b, c, d, a, x[i + 2], 23, -995338651)

      a = md5ii(a, b, c, d, x[i], 6, -198630844)
      d = md5ii(d, a, b, c, x[i + 7], 10, 1126891415)
      c = md5ii(c, d, a, b, x[i + 14], 15, -1416354905)
      b = md5ii(b, c, d, a, x[i + 5], 21, -57434055)
      a = md5ii(a, b, c, d, x[i + 12], 6, 1700485571)
      d = md5ii(d, a, b, c, x[i + 3], 10, -1894986606)
      c = md5ii(c, d, a, b, x[i + 10], 15, -1051523)
      b = md5ii(b, c, d, a, x[i + 1], 21, -2054922799)
      a = md5ii(a, b, c, d, x[i + 8], 6, 1873313359)
      d = md5ii(d, a, b, c, x[i + 15], 10, -30611744)
      c = md5ii(c, d, a, b, x[i + 6], 15, -1560198380)
      b = md5ii(b, c, d, a, x[i + 13], 21, 1309151649)
      a = md5ii(a, b, c, d, x[i + 4], 6, -145523070)
      d = md5ii(d, a, b, c, x[i + 11], 10, -1120210379)
      c = md5ii(c, d, a, b, x[i + 2], 15, 718787259)
      b = md5ii(b, c, d, a, x[i + 9], 21, -343485551)

      a = safeAdd(a, olda)
      b = safeAdd(b, oldb)
      c = safeAdd(c, oldc)
      d = safeAdd(d, oldd)
    }
    return [a, b, c, d]
  }

  /*
  * Convert an array of little-endian words to a string
  */
  function binl2rstr (input) {
    var i
    var output = ''
    var length32 = input.length * 32
    for (i = 0; i < length32; i += 8) {
      output += String.fromCharCode((input[i >> 5] >>> (i % 32)) & 0xff)
    }
    return output
  }

  /*
  * Convert a raw string to an array of little-endian words
  * Characters >255 have their high-byte silently ignored.
  */
  function rstr2binl (input) {
    var i
    var output = []
    output[(input.length >> 2) - 1] = undefined
    for (i = 0; i < output.length; i += 1) {
      output[i] = 0
    }
    var length8 = input.length * 8
    for (i = 0; i < length8; i += 8) {
      output[i >> 5] |= (input.charCodeAt(i / 8) & 0xff) << (i % 32)
    }
    return output
  }

  /*
  * Calculate the MD5 of a raw string
  */
  function rstrMD5 (s) {
    return binl2rstr(binlMD5(rstr2binl(s), s.length * 8))
  }

  /*
  * Calculate the HMAC-MD5, of a key and some data (raw strings)
  */
  function rstrHMACMD5 (key, data) {
    var i
    var bkey = rstr2binl(key)
    var ipad = []
    var opad = []
    var hash
    ipad[15] = opad[15] = undefined
    if (bkey.length > 16) {
      bkey = binlMD5(bkey, key.length * 8)
    }
    for (i = 0; i < 16; i += 1) {
      ipad[i] = bkey[i] ^ 0x36363636
      opad[i] = bkey[i] ^ 0x5c5c5c5c
    }
    hash = binlMD5(ipad.concat(rstr2binl(data)), 512 + data.length * 8)
    return binl2rstr(binlMD5(opad.concat(hash), 512 + 128))
  }

  /*
  * Convert a raw string to a hex string
  */
  function rstr2hex (input) {
    var hexTab = '0123456789abcdef'
    var output = ''
    var x
    var i
    for (i = 0; i < input.length; i += 1) {
      x = input.charCodeAt(i)
      output += hexTab.charAt((x >>> 4) & 0x0f) + hexTab.charAt(x & 0x0f)
    }
    return output
  }

  /*
  * Encode a string as utf-8
  */
  function str2rstrUTF8 (input) {
    return unescape(encodeURIComponent(input))
  }

  /*
  * Take string arguments and return either raw or hex encoded strings
  */
  function rawMD5 (s) {
    return rstrMD5(str2rstrUTF8(s))
  }
  function hexMD5 (s) {
    return rstr2hex(rawMD5(s))
  }
  function rawHMACMD5 (k, d) {
    return rstrHMACMD5(str2rstrUTF8(k), str2rstrUTF8(d))
  }
  function hexHMACMD5 (k, d) {
    return rstr2hex(rawHMACMD5(k, d))
  }

  function md5 (string, key, raw) {
    if (!key) {
      if (!raw) {
        return hexMD5(string)
      }
      return rawMD5(string)
    }
    if (!raw) {
      return hexHMACMD5(key, string)
    }
    return rawHMACMD5(key, string)
  }

  if (typeof define === 'function' && define.amd) {
    define(function () {
      return md5
    })
  } else if (typeof module === 'object' && module.exports) {
    module.exports = md5
  } else {
    $.md5 = md5
  }
})(this)

/**
 * check current browser properties
 * 
 * @return {string} browser properties
 */
com_yung_util_getbrowser = function () {
    var ret = yung_global_var["com_yung_util_getbrowser.result"];
    if (ret != null) {
        return ret;
    }
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf('msie') != -1) {
        this.browser = "msie";
        yung_global_var["com_yung_util_getbrowser.result"] = "msie";
        return "msie";
    }
    if (agt.indexOf('firefox') != -1) {
        this.browser = "firefox";
        yung_global_var["com_yung_util_getbrowser.result"] = "firefox";
        return "firefox";
    }
    if (agt.indexOf('safari') != -1) {
        this.browser = "safari";
        yung_global_var["com_yung_util_getbrowser.result"] = "safari";
        return "safari";
    }
    if (agt.indexOf('seamonkey') != -1) {
        this.browser = "seamonkey";
        yung_global_var["com_yung_util_getbrowser.result"] = "seamonkey";
        return "seamonkey";
    }
    if (agt.indexOf('netscape') != -1) {
        this.browser = "netscape";
        yung_global_var["com_yung_util_getbrowser.result"] = "netscape";
        return "netscape";
    }
    if (agt.indexOf('opera') != -1) {
        this.browser = "opera";
        yung_global_var["com_yung_util_getbrowser.result"] = "opera";
        return "opera";
    }
    if (agt.indexOf('windows') != -1 && agt.indexOf('gecko') != -1) {
        this.browser = "edge";
        yung_global_var["com_yung_util_getbrowser.result"] = "edge";
        return "edge";
    }
    yung_global_var["com_yung_util_getbrowser.result"] = "unknown";
    return "unknown";
}

/**
 * Basic Class, like JAVA Object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var $Class = AbstractClass.extend({
	// for reflection
	classProp : { 
	    name : "$Class",
	    unimplemented : []
    },
    
    init: function(){
        throw "$Class can not be initialized!";
    },
    
    /** 
     * hash this instance to md5 string
     * 
     * @instance
     * @memberof $Class
     * @return {string} hash string
     */
    hashCode : function () {
        var json = JSON.stringify(this);
        if (this.classProp != null && this.classProp.name != null) {
            json = this.classProp.name + json;
        }
        return MD5Util.calc(json);
    },
    
    /** 
     * print to string
     * 
     * @instance
     * @memberof $Class
     * @return {string} result string
     */
    toString : function () {
        if (this.classProp != null && this.classProp.name != null) {
            return this.classProp.name + JSON.stringify(this);
        } else {
            return JSON.stringify(this);
        }
    },
    
    /** 
     * inherit property of from super class to current class
     * 
     * @private
     * @instance
     * @memberof $Class
     */
    inheritProp : function () {
        if (this.classProp != null && this.classProp.name != null) {
            var originClassName = this.classProp.name;
            var classArray = _getClassHierarchy(originClassName);
            for (var i = 0; i < classArray.length; i++) {
                var cls = classArray[i];
                var prop = yung_global_var["YungClassProperty"][cls];
                for (var key in prop) {
                    if (key != 'superClass') {
                        var value = prop[key];
                        if (value != null) {
                            if (jQuery.isArray(value)) {
                                this[key] = new Array();
                            } else if (typeof value == 'object') {
                                this[key] = new Object();
                            } else {
                                this[key] = value;
                            }
                        } else {
                            this[key] = null;
                        }
                    }
                }
            }
        }
    },
    
    /** 
     * replace string function
     * 
     * @instance
     * @memberof $Class
     * @param  {string} targetStr - target string
     * @param  {string} strFind - string to find
     * @param  {string} strReplace - string to replace
     * @return {string} result string
     */
    replaceAll : function(targetStr, strFind, strReplace) {
        return _replaceAll(targetStr, strFind, strReplace);
    }
});

function _validType (eleType, element) {
    if (typeof eleType == 'string') {
		var elementType = typeof element;
		if (eleType == 'string' && eleType == elementType) {
            return true;
        }
		if (eleType == 'number' && eleType == elementType) {
        	return true;
        }
		if (eleType == 'boolean' && eleType == elementType) {
        	return true;
        }
		if (eleType == 'function' && eleType == elementType) {
        	return true;
        }
	}
	if (typeof eleType == 'function' && element instanceof eleType) {
    	return true;
    }
	if (typeof eleType == 'function') {
        if (element["classProp"] != null && element["classProp"]["name"] != null) {
        	if (element["classProp"]["name"] == $Class.getClassName(eleType)) {
        		return true;
        	}
        }
	}
    if (typeof eleType == 'string') {
    	var elementType = typeof element;
    	if (eleType == 'object' && elementType == 'object') {
            return true;
        }
    }
    return false;
}

/**
 * Md5 hash utility
 *
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var MD5Util = {
	
	/**
	 * calculate string to md5 hash string
	 * 
	 * @memberof MD5Util
	 * @param {string} str - input string
	 * @return {string} hash string
	 */	
    calc : function (str) {
		return md5(str);
	}
};

/**
 * check current user locale
 * 
 * @return {string} user locale
 */
com_yung_util_getLang = function () {
    var ret = yung_global_var["com_yung_util_getLang.result"];
    if (ret != null) {
        return ret;
    }
    if (navigator.languages != undefined) {
        yung_global_var["com_yung_util_getLang.result"] = navigator.languages[0];
        return navigator.languages[0]; 
    } else {
        yung_global_var["com_yung_util_getLang.result"] = navigator.language;
        return navigator.language;
    }
}

function _isValidMd5(input)
{
    if (_validType('string', input) == false) {
        return false;
    }
    if (input == null || input == '') {
        return false;
    }
    if (/^[a-fA-F0-9]{32}$/.test(input)) {
        return true;
    }
    return false;
}
$Class.getInstance = function (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) {
    if (yung_global_var["instance"] == null) {
        yung_global_var["instance"] = {};
    }
    if (_validType('string', arg1) == false) {
        throw arg1 + " type is not string";
    }
    var isMd5 = _isValidMd5(arg1);
    if (isMd5) {
        return yung_global_var["instance"][arg1];
    } else {
        var className = arg1;
        var tokens = className.split(".");
        var type = null;
        var i = 0;
        while (i < tokens.length) {
            if (i == 0) {
                type = window[tokens[i]];
            } else {
                type = type[tokens[i]];
            }
            i++;
        }
        if (typeof type != 'function') {
            throw className + " is not constructor";
        }
        var id = arg2;
        var key = arg1 + "-" + arg2;
        var hashId = MD5Util.calc(key);
        if (yung_global_var["instance"][hashId] != null) {
            if (typeof yung_global_var["instance"][hashId] == 'string') {
                throw "cache dirty: " + yung_global_var["instance"][hashId];
            }
            return yung_global_var["instance"][hashId];
        }
        yung_global_var["instance"][hashId] = 'ready';
        var instance = new type(arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
        yung_global_var["instance"][hashId] = instance;
        return instance;
    }
}
$Class.validate = function (className, id) {
    if (className == null) {
        throw className + " is null";
    }
    if (id == null) {
        throw className + " method[init] first argument id is null";
    }
    if (_validType('string', className) == false) {
        throw className + " is not string";
    }
    if (_validType('string', id) == false) {
        throw className + " method[init] first argument id is not string";
    }
    var hashId = MD5Util.calc(className + "-" + id);
    if ($Class.getInstance(hashId) != 'ready') {
        throw "Please use $Class.getInstance('" + className + "', '" + id + "', arguments) to create instance!";
    }
}
$Class.getClassType = function (className) {
    var tokens = className.split(".");
    var type = null;
    var i = 0;
    while (i < tokens.length) {
        if (i == 0) {
            type = window[tokens[i]];
        } else {
            type = type[tokens[i]];
        }
        i++;
    }
    return type;
}
$Class.instanceOf = function (className, obj) {
    if (obj == null) {
        return false;
    }
    var clsType = $Class.getClassType(className);
    if (obj instanceof clsType) {
        return true;
    }
    return false;
}
$Class.getClassName = function (type) {
    if (type["classProp"] == null) {
        var superType = type.prototype;
        while (superType != null) {
            if (superType["classProp"] != null) {
                return superType["classProp"]["name"];
            }
            if (superType == type.prototype) {
                break;
            }
            superType = type.prototype;
        }
    } else {
        if (type["classProp"]["name"] != null) {
            return type["classProp"]["name"];
        }
    }
    if (type["name"] != null) {
        return type["name"];
    }
    return type + "";
}
$Class.checkFunction = function (type, funct) {
    if (typeof type[funct] != 'function') {
        var superType = type.prototype;
        while (superType != null) {
            if (typeof superType[funct] == 'function') {
                return true;
            }
            if (superType == type.prototype) {
                break;
            }
            superType = type.prototype;
        }
    }
    return false;
}


function fireInstanceMethod (hashId, method, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) {
    if (_validType('string', hashId) == false) {
        throw "hashId is not string";
    }
    var isMd5 = _isValidMd5(hashId);
    if (isMd5 == false) {
        throw "hashId is not md5";
    }
    var instance = $Class.getInstance(hashId);
    if (instance == null) {
        throw "instance not exist";
    }
    if (typeof instance[method] == 'function') {
        instance[method](arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);    
    }
}

var _searchIdx;
function binarySearchArray (arr, x, start, end, reverse, exact) {
	if (arr.length == 0) {
		return 0;
	}
	// Base Condtion
	if (start > end) {
		if (exact == true) {
			return -1;
		}
		return _searchIdx;
	}
	// Find the middle index
	var mid = Math.floor((start + end) / 2);
	_searchIdx = mid;
	// Compare mid with given key x
	if (typeof x.compareTo == 'function') {
		if (x.compareTo(arr[mid]) == 0) {
			if (exact) {
				return mid;
			} else {
				return -1;
			}
		}
	} else {
		if (arr[mid] === x){
			if (exact) {
				return mid;
			} else {
				return -1;
			}
		}
	}
	var bigger = null;
	var val = arr[mid];
	if (typeof x == 'number' || typeof x == 'string') {
		if (val > x) {
			bigger = true;
		} else {
			bigger = false;
		}
	} else if (typeof x.compareTo == 'function') {
		if (val.compareTo(x) > 0) {
			bigger = true;
		} else {
			bigger = false;
		}
	} else {
		return -1;
	}
	if (reverse == true) {
		if (bigger == true) {
			bigger = false;
		} else {
			bigger = true;
		}
	}
	if (bigger) {
		// If element at mid is greater than x,
		// search in the left half of mid
		return binarySearchArray(arr, x, start, mid - 1, reverse, exact);
	} else {
		// If element at mid is smaller than x,
		// search in the right half of mid
		return binarySearchArray(arr, x, mid + 1, end, reverse, exact);
	}
}

function expandSearchArray (arr, x, start) {
	if (arr.length == 0) {
		return -1;
	}
	if (arr[start] === x) {
		return start;
	}
	var leftIdx = start - 1;
	var rightIdx = start + 1;
	while (true) {
		if (leftIdx < 0 && rightIdx > arr.length) {
			break;
		}
		if (leftIdx >= 0 && arr[leftIdx] === x) {
			return leftIdx;
		}
		if (rightIdx < arr.length && arr[rightIdx] === x) {
			return rightIdx;
		}
		leftIdx--;
		rightIdx++;
	}
	return -1;
}
// global setting
yung_global_var.instance = {};
yung_global_var.YungClassProperty = {};

yung_global_var.widthOffset = {};
yung_global_var.widthOffset["msie"] = 1;
yung_global_var.widthOffset["firefox"] = 1;
yung_global_var.widthOffset["safari"] = 3;
yung_global_var.widthOffset["seamonkey"] = 2;
yung_global_var.widthOffset["netscape"] = 2;
yung_global_var.widthOffset["opera"] = 2;
yung_global_var.widthOffset["edge"] = 1;
yung_global_var.widthOffset["unknown"] = 2;

yung_global_var.heightOffset = {};
yung_global_var.heightOffset["msie"] = 1;
yung_global_var.heightOffset["firefox"] = 0;
yung_global_var.heightOffset["safari"] = 2;
yung_global_var.heightOffset["seamonkey"] = 0;
yung_global_var.heightOffset["netscape"] = 0;
yung_global_var.heightOffset["opera"] = 0;
yung_global_var.heightOffset["edge"] = 1;
yung_global_var.heightOffset["unknown"] = 0;

yung_global_var.leftOffset = {};
yung_global_var.leftOffset["msie"] = -2;
yung_global_var.leftOffset["firefox"] = 1;
yung_global_var.leftOffset["safari"] = 1;
yung_global_var.leftOffset["seamonkey"] = 0;
yung_global_var.leftOffset["netscape"] = 0;
yung_global_var.leftOffset["opera"] = 0;
yung_global_var.leftOffset["edge"] = 1;
yung_global_var.leftOffset["unknown"] = 0;

yung_global_var.topOffset = {};
yung_global_var.topOffset["msie"] = -1;
yung_global_var.topOffset["firefox"] = 1;
yung_global_var.topOffset["safari"] = 1;
yung_global_var.topOffset["seamonkey"] = 3;
yung_global_var.topOffset["netscape"] = 3;
yung_global_var.topOffset["opera"] = 3;
yung_global_var.topOffset["edge"] = 1;
yung_global_var.topOffset["unknown"] = 3;