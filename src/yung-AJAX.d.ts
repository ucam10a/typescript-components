import { $Class } from "./yung-JS-extend";

/**
 * AJAX tool, only support data type json and post action
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {string}  classProp.type - AJAX action type
 * @property {string}  classProp.dataType - AJAX data type
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_AjaxHttp extends $Class {
	
    classProp : { 
        name : "com.yung.util.AjaxHttp",
        unimplemented : [],
    	type : 'POST',
    	dataType : 'json',
        loadGif : "data:image/png;base64,R0lGODlhSwBLAPcAAERERP7+/v39/fz8/Pn5+f////Dw8Pf39+bm5uvr6/b29tjY2H9/f5aWloKCgvv7+6CgoOrq6vHx8Y6Ojnx8fIWFheDg4Ofn59TU1OPj47GxsfPz85mZmaSkpLe3t9vb26ysrLW1tampqaGhoc3Nzby8vLq6urOzs6amptHR0YGBgX5+fpCQkI2NjcvLy5OTk3t7e+Hh4aenp9/f38XFxa6urpycnMLCwvr6+p6enoSEhIiIiO3t7d3d3cfHx4eHh/j4+MPDw/X19ZiYmOjo6M/Pz4qKiouLi7CwsMrKyra2tuLi4tbW1pSUlNnZ2eTk5MTExNra2u7u7sHBwb6+vu/v79PT05ubm97e3vT09Li4uMnJyfLy8uzs7NDQ0NLS0oaGhoyMjMzMzK+vr6KioqioqMbGxqqqqpGRkdzc3LS0tLm5uaOjo8jIyLu7u729vdfX13l5eZeXl3h4eJqamp+fn7Kysr+/v+Xl5c7OzpKSktXV1aWlpZ2dnenp6YODg62trYCAgI+Pj4mJiaurq319fcDAwJWVlXd3d3p6enZ2dkVFRUZGRkhISEtLS05OTk1NTXR0dHNzc29vb0pKSkdHR0xMTG5ubnJycnFxcXV1dUlJSVZWVlBQUFNTU1RUVGxsbFVVVWFhYWRkZHBwcG1tbWhoaGVlZWtra1JSUl9fX2BgYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NDkxMSwgMjAxMy8xMC8yOS0xMTo0NzoxNiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmODFlMTVmMC1jY2FlLTRkNjEtODhjMy1jMGE1MGRjZmE1YWMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTYzOThDRkVBODRGMTFFMzkyMzhFOEZGRjdFQ0E0OUUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTYzOThDRkRBODRGMTFFMzkyMzhFOEZGRjdFQ0E0OUUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ZjgxZTE1ZjAtY2NhZS00ZDYxLTg4YzMtYzBhNTBkY2ZhNWFjIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmY4MWUxNWYwLWNjYWUtNGQ2MS04OGMzLWMwYTUwZGNmYTVhYyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAUEAEUAIf4jUmVzaXplZCBvbiBodHRwczovL2V6Z2lmLmNvbS9yZXNpemUALAAAAABLAEsAAAjYAIsIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qFGWcm66aRDG4RVPADqhKWmEQgWGZDYBAGCKYY6tYJuKDAN2yMJUYAHMWcgpLYBHIyuARaQQhtsXCiG43VpI5BGwSRV2ShtnIdS0cEfuiCOIoQ2tAEY5djt15gkbHBy+ePR2x9EiMD6LHk26tOnTqFOrXs26tevXsGPLnk27tu3buHPr3s27d5GAACH5BAUEAAEALBEAIQAqAAkAAAiCAAMIHEiwoMGDCAO0MFjjCoiEBD2gAQOR4JBOASCFGdgEgMcdFfs0EiiqYoArBXUEUANgIIAaEDEOxFQxVEFHAeq0FAhATsJLBVlARGnwkoadAQAAMjlQUkWZA3Eq9AigQsUmIwOoMtmg4EKBJ4aMYVqGThOmCiEFcKQCrVuIQAcGBAAh+QQFBAANACwQACEAKwAJAAAIjAAbCGxgiMnAgwgTKlTYJCGdVAAssVh4UEuTQRQRPmzQSdDAPgAGAsBIMcfAUxkF9kFI0hPCRhgopjqoKCUnhJYayAl5EICbhTUP6snIR6EiLJAQUkoh86CmlDMP5tTJswEANBnpDFyVssEQhB4FsnAEoBHWlBquyOkqUNCjBpB0IBQzZQ/buwiDCgwIACH5BAkEAA8ALBAAIQArAAkAAAiFAB8IJBSHj8CDCBMqXJjwisI5AB4AMOWD4UE3QwRZTNjnk0A9B+MkHLWxwyaBpjYKrJPwyIMxERECIGPRE8JEKhU+erBiYcqFDHIiHAOUUMyDACAIfYBzo02EOx9ISihqY52TD0guPQjyASBSEQGIuqNSzQMbSw8JfGQkoQ1TNLfKdYAwIAAh+QQJBABMACwQACEAKwAJAAAIjwCZCGwD5Y7AgwgTKlyYUI7CMJsAbGLAEGGJBmEqJrziCUAnNAcnABg58odGMhEBnNIosA/JkUeYJElJctOUiqleJmKp6iWAR0yU+Bx5giEMnyAryhgKAMYWmiM3vcH5EhHLji+BMgHj849GDilHsWQiB+nBQBE3ORhrxwaHsQLldPppEmESEzfh6kVIAWFAACH5BAkEABgALBAAIQArAAkAAAiMADEIxIBloMGDCBMqbIBwwiUAowQpNDiGg56JB6+kAtBpwkBBmwCIBHAF44uQAEoZwoghx0iRRwROeglAFcZOL1uw5EQTEoYMNEWWUAiF5g6MKIICmIMBFU1ON3Oy3PjSJ4ZBNJtgnIAS1BuWDWiyGKhnVM1DLDHYsUEnrcAmjwBAqoCwjdu7CBMZDAgAIfkECQQAEAAsEQAhACoACQAACJQAIQgcSLAgQR9FDCoceKQgmziiVMhYOPCMKAAA/jChOJBDJwCQGkLg8xFjIw0U7TTCiDERRwhyWGKsAIGCTABzKBq5CQAQx1A3KUEwdTMURaI3R1DswxPAJBg3FVEcxFMEx5IsLUFAshJjJUIUAUV9eehmC4FnVoRCVOPlGVUYYZB4CWGCJQCUAtGlSCPJ3oGlCAYEACH5BAkEAAcALBEAIQAqAAkAAAibAA8IHEiwoEA3LRTpYGGwoUA9BgM5iGPCocAzjgBoBADK4kAOqQA8mjDw0sZRFRu28bRx4xGPNloC2HHgh8w/DnPI1OgIjkVOMh8dANoSYkMdOzVycIgiqSQHMh04HJKUEhOLIVtaOuDD1EZRWhySyChTh8dDMhkeMKSoyR81HslsapmJhMcDhywBsGT2bkMtglAheuF3oCKCAQEAIfkECQQACAAsEQAgACoACwAACJgAEQgcSLAgQSYGEypcuFBQKQCQVNRgSLCJQTaHclAMBKBjx04dKHJIBaATi4E7PMJYeMWjS1AMW7o0goCPSwAhE/656TFnQk43ISHIcZNmQjA8O85RKCKpIjY32ShckRTAiIWebloSqMNjnIWHkppiOORmi4F0WgyhiOgmJRkUWTyCqIOiwiuqADSKhMSuQER+GY75EjhhQAAh+QQJBAAKACwRACEAKgAJAAAImAAVCBxIsKBAJSpGaWJhsKFADgbf7FjjcCCHTQAyArjkpqJAG54ApGIocAjGTRAdQqGkUWMgj31aAgijYArGjJsAOZwgEyeUipxkPlKgR6Ymh6V6ZlTikJDSQlRuAth0xmELpZu0VAwlVCCdk00qlpCqEYzHKzJfDNQCo4ZHBUbInnrz9lAnAI92vHV4hgEnU39+7lWwgmBAACH5BAkEABYALBEAIQAqAAkAAAiOAC0IHEiwoEBDFY7AgGCwoUAjDWE4JGhDFYCLABJNHKinE4BHLAZ2sBjKzkZTGDEO2fgiJYAWAldh9DRxiMuLozZ6TLlpSyCXQRw2uXkRgUM9RG9YsHgxVU2iqnS63KQ0BycAntRsPHXz0EYWLv8QPLFRIIurGB2UtfACEoBNYNY2XDNHEAOGci2o2TIwIAAh+QQJBAAHACwRACEAKwAJAAAIjwAPCBxIsODAOoIEaTDIUMMOTRP4GEzA0KApABgZoak4UBDGj00GQjHFyNRCjhQ+ZhzCUY3KjxAElvooiuOZlxhVcDyCE4COA1koqexRsUbPUxwT9RwlkybHED0ZcATT04HAIBdFaeF4QBHOIxyN4iRDMAPXgao+Mjp0VsdLI2c5NlnBwkbcA2wGmapAZ2BAACH5BAkEAA0ALBIAIAAqAAsAAAieABsIHEiwoMGDBsXYkDOkDcKHA93A6ANxYB9LADJuklPxYKBGGUWNgQgho8mMegRSIIWmIouTADrVQCgGEsyMjbSsMBkGYqebFBCOuGnyhSKTox5eIcoJYR2iGQsVMnnE580dCHNABWCjwQ5ULCqCgQnpDEIvjog2GtlxoCKQADiVgUiHaIW2Bc+gGtSWDlwAjcDgHXzwhhwHTewMDAgAIfkECQQACQAsEgAgACoACwAACJQAEwgcSLCgwYMGbagQRCEHwocE1TiEKJABgIsXM1E8yIHTxTkmIAbCSPKSwCuqGlAc0Yikqoc5SMrs0wfjC4ikZAKogxCTTow7RGEMBNGjzCsIG/y8WMgGRhYQC+nsgzDn0goJ+qg6RLFOS4yoHtZcqnLjQA6pLoKaAlHRT1FmDSrRYlaSTFE04uo12EFFoB9IBQYEACH5BAkEABkALBIAIAAqAAsAAAiWADMIHEiwoMGDBqG0+NFADcKHBC9AJDhhE4CLm8BMPDgi0yZOGiEKukjyogqBdOawmCiDU0kKD31YLIlxxJWZfSAaoQkgBEI0PEmiCUQSFcQjPOkgZBH04ik6M69AHMRTCcKRTSdkGKIpJ0QynUrCeHhnJk8+GwmeQQXgUciHLYL+SWuQCV0jZgFoosv34BgjoPScGRgQACH5BAkEADsALBIAIAAqAAsAAAiSAHcIHEiwoMGDB89k6oCwoUOHbE4BmKhqwkOELS7paPKww6aJIDcNEcghDouLo0ACOORQosqJn3bY+Aigj8MGL1M1JPNSJRoHIC85hNGzBUKiPSfKmDnRhsMmLzs1PJN04p8dQxTZfFhKZR2HopJ2InGR4JsXpyhweCiHpsorZeMehMAJZKe1cvMWbKJnUJ6BAQEAIfkECQQAMQAsEgAgACoACwAACJUAYwgcSLCgwYMH7dzgg7ChQ4dnCm0CACCTjIcIr6iCUeMhIVUUQ3IiI5BDnCYYI4WkBMghg5AwTcW4MhEABIcQYAIA1VBLTZ0ANAQKeckhKp2PuCCkATSkCJoUbTisozNOQyVNKYaIMURRDoyFQm4S4RBRU00YDeoJ86PMQw2cdFLSkLbuQSSTQqI6Y7evQQ1jlBAMCAAh+QQJBAAJACwSACAAKgALAAAIjgATCBxIsKDBgwjhIFzIsGGCH5wAcKLgEOGZPocqggLAkeOqgXIQvXCooxFHU3cYDurIskUCDiYB2GA4BRLLHwxVsexoKQGDjqAYeti5YuGTnSzJwORIh2GRmBz1MIyItGeCQ5GuOFwKYJQJhj+QAjBSseAIDhNIOBy1c5SHsnAN/ugEANKRNXHzFhxTMCAAIfkECQQADAAsEgAhACoACQAACIAAGQgcSLCgwYMG+awYBYMOwocDXRwqA3GgAwAYMUoaSCdRg4oM+nDC+KeijYwo9YTchLFPRVQom0C8iBLjHAYqMqKCaKbmzoemamLkxMAGSwAuIcLM+AJiTqGKBHJQVAekHU8kK9YRCiAMSINQjJD5mqhmoK9oH9JZwSlRi4EBAQAh+QQJBABGACwSACEAKQAJAAAIgQCNCBxIsKDBgwaR/JGzIgfChwNPeIA4cMgmABg3CRLIIU4TikZQnMIIg6KdixgzqrGBsg/FUikBjID4I2ZKFYFSXoLYxeYgiIpsYhx0BeUViqZi2oBYQSiAFUYOzVlKkY0qjJgoykCZchMgkAZBnAE7iOsml2DTHoQw6IiKMQIDAgAh+QQJBAAbACwSACEAKQAJAAAIgwA3CBxIsKDBgwaZ9NEzxAXChwNP2IE4kI4oABhVoRA4ZM4Eihv4jMI4B+QpjCg5TbmyCaMNiqZQAuAA8YVMlEcCoSwFkcfNChCH3MSYiE5LAFdgyhwCUdBQAA02DNH0kuIIVRgxUWSCVeanOyANgjgTNkxXAJ48hF2LMAkaB0amCAwIACH5BAkEAAsALBIAIQApAAkAAAh7ABcIHEiwoMGDBzWoIISwIUE1IRwShAGgIqY7AukkOiRxgYxTFSl0pFCxpKkFEDZVrCMRVUkAVxyeeVnShoOSkxxKobnDYQuaFU3ZUAmATsuXNhyeAApAzgIOiEZ0zLGqoqKOc2ie6ngQ0BmuCySVHNUGrNmDHS7FFBgQACH5BAkEADUALBIAIQApAAkAAAh9AGsIHEiwoMGDCEO0QciQ4Ak1DQnqeQRgk46BHOK8iFgDxSkAAGBw3AGyZKEadTaBvBIRVUkANiJSfLnJB4OSlxp2eQngR8MKPEFqyaESAJ2Ipl5yiNiJ56Y7NYYoysGRjSqQkTi24AmG48ExIrzWGKRy0w6xaBFOMeRhYEAAIfkECQQAHwAsEgAhACkACQAACHkAPwgcSLCgwYMIiSBcWPDECYYED00CgOrQwCFzXkD8wGcUAAAUNjbZ9PEjmg9XSAKoA9FUSQAQIGZ6CUDUh0AlSzFMQPMHwyw0P/qgo9JGy5dXIE58qUqgHE0xIfZR9RHTRhYqPxrdaBCECK4fjrhUlRSsWYR7CAYEACH5BAUoADcALBIAIQApAAkAAAh4AG8IHEiwoMGDCBMqJHhCzcKBHSalijNiIIc4eh6iOAUAAAyNmzoC2HTlBoeQAGwsRCUSQJ2FmFoCkHQjkMhLCiXI/LGQk0wABq6gLKnQVMscMGViEjhEUZ+HbFR1jPSQDsqRDR4eBHFG6w0UiDZFGuK1bEIhBAMCADs="
    }
    
    /**
     * action url
     * @member {string}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    actionUrl : string
    
    /**
     * form id for sending parameter
     * @member {string}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    formId : string
    
    
    /**
     * Callback for AJAX reponse
     *
     * @callback AJAXCallback
     * @param {object} response - response data object
     * @param {com_yung_util_AjaxHttp} self - this com_yung_util_AjaxHttp
     * @param {object} caller - caller which pass by send method
     */
    /**
     * AJAX callback function
     * @member {AJAXCallback}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    callback : Function
    
    /**
     * show loading image, default true
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    loading : boolean
    
    /**
     * show error message when AJAX error occurs, default true
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    showError : boolean
    
    /**
     * show AJAX loading function
     * @member {function}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    ajaxLoadingShow : Function
    
    /**
     * hide AJAX loading function
     * @member {function}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    ajaxLoadingHide : Function
    
    /**
	 * constructor
     * @memberof com_yung_util_AjaxHttp
     * @param  {string} actionUrl - action url
     * @param  {string} formId - from id
     * @param  {AJAXCallback} callback - ajax callback
     * @param  {function} ajaxLoadingShow - function to show loading image
     * @param  {function} ajaxLoadingHide - function to hide loading image
     */
    constructor (actionUrl: string, formId: string, callback: Function, ajaxLoadingShow?: Function, ajaxLoadingHide?: Function) 
    
    /** 
     * before send
     *
     * @private
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    beforeSend (): void 
    
    /** 
     * after send
     *
     * @private
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    sendComplete (): void
    
    fixImageWhenScroll (): void
    
    /** 
     * set show loading flag
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {boolean} loading - show loading flag
     */
    setLoading (loading: boolean): void
    
    /** 
     * set show error flag
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {boolean} loading - show error flag
     */
    setShowError (showErr: boolean): void
    
    /** 
     * send request
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {object} caller - caller for callback use
     */
    send (caller: any): void

    static createLoadingDiv (): void 

    static setLoadingText (text: string, self: com_yung_util_AjaxHttp): void 

    static setImage (imgSrc: string, self: com_yung_util_AjaxHttp): void 

}

/**
 * AJAX upload tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_AjaxHttp
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_AjaxUpload extends com_yung_util_AjaxHttp {
    
    // @ts-ignore
	classProp : { 
        name : "com.yung.util.AjaxUpload",
        unimplemented : []
	}
	
	/**
	 * constructor
     * @memberof com_yung_util_AjaxUpload
     * @param  {string} actionUrl - action url
     * @param  {string} formId - from id
     * @param  {AJAXCallback} callback - ajax callback
     * @param  {function} ajaxLoadingShow - function to show loading image
     * @param  {function} ajaxLoadingHide - function to hide loading image
     */
    constructor (actionUrl: string, formId: string, callback: Function, ajaxLoadingShow?: Function, ajaxLoadingHide?: Function)
    
    /** 
     * send request
     *
     * @instance
     * @memberof com_yung_util_AjaxUpload
     * @param  {object} caller - caller for callback use
     */
    send (caller: Function): void 
    
}