import { $Class } from "./yung-JS-extend";
import { com_yung_util_Calendar } from "./yung-Calendar";
import { com_yung_util_FloatDiv } from "./yung-FloatDiv";
import { com_yung_util_TableGridUtil } from "./yung-TableGridUtil";

/**
 * Date picker
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_DatePicker extends $Class {

    classProp : { 
        name : "com.yung.util.DatePicker",
        unimplemented: []
    }
    
    hashId : string
    
    /**
     * element id
     * @member {string}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    eleId : string
    
    /**
     * float div to place word list
     * @member {com_yung_util_FloatDiv}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    floatDiv : com_yung_util_FloatDiv
    displayHeight : number
    displayWidth : number
    blockFire : boolean
    delayFire : number
    adjustX : number
    adjustY : number
    format : string
    
    
    /**
     * select date
     * @member {com_yung_util_Calendar}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    selectDate : com_yung_util_Calendar
    
    /**
     * Callback after date choose value
     *
     * @callback AfterDateChoose
     * @param {string} eleId - element id
     * @param {string} inputVal - select date
     */
    /**
     * callback after date choose value
     * @member {AfterDateChoose}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    afterChoose : Function
    
    
    /**
     * constructor
     * @memberof com_yung_util_DatePicker
     * @param  {string} eleId - element id
     * @param  {string} format - date format, ex: yyyy/MM/dd, See JAVA SimpleDateFormat
     */
    protected constructor (eleId: string, format: string) 
    
    /** 
     * set after choose callback
     * 
     * @instance
     * @memberof com_yung_util_DatePicker
     * @param {AfterDateChoose} afterChoose - after choose callback
     */
    setAfterChoose (afterChoose: Function): void

    /** 
     * set input value
     * 
     * @instance
     * @memberof com_yung_util_DatePicker
     * @param {string} inputVal - date put into input
     */
    setInputValue (inputVal: string): void
    
    /** 
     * create date picker
     * 
     * @private
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    createPicker (): void
    private bindFunction (): void
    
    /** 
     * show date picker
     * 
     * @memberof com_yung_util_DatePicker
     * @param {string} bindId - binding element id
     */
    showPicker (bindId?: string): void
    private adjustSize (): void
    private createMonthTable (val?: string)
    
    getMonthDays (year: number, month: number): number
    
    getDateString (year: number, month: number, day: number): string
    
    getMonthString (year: number, month: number): string
    
    prevYear (): void
    
    prevMonth (): void
    
    nextYear (): void
    
    nextMonth (): void

    /** 
     * get com_yung_util_DatePicker global instance
     * 
     * @param  {string} eleId - element id
     * @param  {string} format - date format, ex: yyyy/MM/dd, See JAVA SimpleDateFormat
     * @return  {com_yung_util_DatePicker} com_yung_util_DatePicker instance
     */
    static instance (eleId: string, format: string): com_yung_util_DatePicker
    _super (eleId: string, format: string): any

}

// specific table grid date picker
declare class com_yung_util_GridDatePicker extends com_yung_util_DatePicker {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.GridDatePicker",
        unimplemented: []
    }
    
    grid : string
    delayFire : number
    
    protected constructor (eleId: string, format: string, grid: string)
    
    private bindFunction (): void
    
    static instance (eleId: string, format: string, grid: com_yung_util_TableGridUtil) : com_yung_util_GridDatePicker

}