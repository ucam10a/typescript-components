import { $Class } from "./yung-JS-extend";
import { com_yung_util_BasicList } from "./yung-Collection";
import { com_yung_util_BasicMap } from "./yung-Map";

/**
 * Abstract HTML template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare abstract class com_yung_util_RawTemplate extends $Class {
	
	classProp : { 
		name : "com.yung.util.RawTemplate",
		unimplemented : ['setData', 'getData', 'toHtml']
	}
    
    abstract setData(data: object | Array<object> | com_yung_util_TemplateData): void
    abstract getData(): object | Array<object>
    abstract toHtml(data?: object | Array<object>): string
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_RawTemplate
     */
	template : com_yung_util_BasicList<string>
	
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     */
	constructor ()
    
    /** 
     * add HTML to template
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - HTML code
     */
    add (html: string): void 
    
    /** 
     * search HTML code and find string pattern then return an array
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - source HTML code
     * @return  {Array} string pattern array
     */
    searchPattern (html: string): Array<string> 
    
    /** 
     * remove string pattern in HTML code
     * 
     * @instance
     * @memberof com_yung_util_RawTemplate
     * @param  {string} html - source HTML code
     * @return  {string} html without string pattern
     */
    removePattern (html: string): string 
}

/**
 * simple HTML template, only one line.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_SimpleTemplate extends com_yung_util_RawTemplate {
    
    // @ts-ignore
	classProp : { 
        name : "com.yung.util.SimpleTemplate",
        unimplemented : []
	}
    
	/**
     * data object to render HTML
     * @member {object}
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     */
	data : object
    
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     */
	template : com_yung_util_BasicList<string>
	
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {string} tmpStr - one line template string
     * @param  {string} key - data key
     * @param  {string} value - data value
     */
	constructor (tmpStr: string, key: string, value: string)
    
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @param  {object} data - data object
     */
    setData (data: object): void 
    
    /** 
     * get data object
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @return  {object} data object
     */
    getData (): object 
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_SimpleTemplate
     * @param  {object} data - data object optional
     * @return  {string} html replaced by render key/value
     */
    toHtml (data?: object): string 
}

/**
 * Basic HTML template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_BasicTemplate extends com_yung_util_RawTemplate {
    
    // @ts-ignore
	classProp : { 
        name : "com.yung.util.BasicTemplate",
        unimplemented : [] 
	}
    
	/**
     * data object to render HTML
     * @member {object}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	data : object
    
	/**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	template : com_yung_util_BasicList<string>
	
	/**
     * function map for render HTML
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_BasicTemplate
     */
	conditionMap : com_yung_util_BasicMap<string, Function>
    
	/**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {object} data - data object to render HTML
     */
	constructor (data?: object)
    
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {object} data - data object
     */
    setData (data: object): void 
    
    /** 
     * get data array object
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @return  {object} data object
     */
    getData (): object 
    
    /**
     * Callback to override data value for render HTML
     *
     * @callback overrideRenderValue
     * @param {object} data - data object
     * @param {string} key - render key
     * @param {string} originValue - original render value
     */
    /** 
     * set data object to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {string} key - render key
     * @param  {overrideRenderValue} condition - a callback to run
     */
    setCondition (key: string, condition?: Function): void 
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_BasicTemplate
     * @param  {object} data - data object optional
     * @return  {string} html replaced by render key/value
     */
    toHtml (data?: object): string 
}

/**
 * Array HTML template, to render basic template multiple times 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_ArrayTemplate extends com_yung_util_RawTemplate {
    
    // @ts-ignore
	classProp : { 
        name : "com.yung.util.ArrayTemplate",
        unimplemented : [] 
	}
	
	/**
     * basic template to render HTML
     * @member {com_yung_util_BasicTemplate}
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     */
    basicTmp : com_yung_util_BasicTemplate
    
    /**
     * data array to render HTML
     * @member {Array}
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     */
    dataArray : Array<object>
    
    /**
	 * constructor
     * @memberof com_yung_util_RawTemplate
     * @param  {Array} dataArray - data array to render HTML
     * @param  {com_yung_util_BasicTemplate} basicTmp - basic template to render HTML
     */
    constructor (dataArray?: Array<object>, basicTmp?: com_yung_util_BasicTemplate)
    
    /** 
     * set data array to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {Array} dataArray - data array
     */
    setDataArray (dataArray: Array<object>): void 
    setData (dataArray: Array<object>): void 
    
    /** 
     * get data array object
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @return  {object} data object
     */
    getDataArray (): Array<object> 
    getData (): object 
    
    /** 
     * set basic template to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {com_yung_util_BasicTemplate} basicTmp - basic template
     */
    setBasicTmp (basicTmp: com_yung_util_BasicTemplate): void 
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ArrayTemplate
     * @param  {Array} dataArray - data array optional
     * @return  {string} html replaced by render key/value
     */
    toHtml (dataArray?: Array<object>): string 
}

/**
 * Complex template data
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_TemplateData extends $Class {
    
    // @ts-ignore
	classProp : { 
        name : "com.yung.util.TemplateData",
        unimplemented : [] 
	}
    
	/**
     * basic template map
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_TemplateData
     */
    tmpMap : com_yung_util_BasicMap<string, com_yung_util_RawTemplate>
    
    /**
     * data map
     * @member {com_yung_util_Map}
     * @instance
     * @memberof com_yung_util_TemplateData
     */
    dataMap : com_yung_util_BasicMap<string, object>
    
	/**
	 * constructor
     * @memberof com_yung_util_TemplateData
     */
	constructor ()
    
    /** 
     * add template and data
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @param  {string} key - pattern key [template key] 
     * @param  {com_yung_util_RawTemplate} template - template
     * @param  {object | Array | com_yung_util_TemplateData} data - data object
     */
    addTemplate (key: string, template: com_yung_util_RawTemplate, data: object): void 
    
    /** 
     * get basic template map to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @return  {com_yung_util_Map} basic template map
     */
    getTmpMap (): com_yung_util_BasicMap<string, com_yung_util_RawTemplate> 
    
    /** 
     * get data map to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_TemplateData
     * @return {com_yung_util_Map} data map
     */
    getDataMap (): com_yung_util_BasicMap<string, object> 
}


/**
 * Complex HTML template, to render two types of template
 * basic template and array template
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_RawTemplate
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_ComplexTemplate extends com_yung_util_RawTemplate {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.ComplexTemplate",
        unimplemented : [] 
    }
    
    /**
     * complex template data
     * @member {com_yung_util_TemplateData}
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     */
    data : com_yung_util_TemplateData
    
    /**
     * template list to store HTML
     * @member {com_yung_util_BasicList}
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     */
    template : com_yung_util_BasicList<string>
    
    /**
	 * constructor
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_Map} tmpMap - basic template map
     * @param  {com_yung_util_Map} dataMap - data map
     */
    constructor ()
    
    /** 
     * add template and data
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {string} key - pattern key [template key] 
     * @param  {com_yung_util_RawTemplate} template - template
     * @param  {object | Array | com_yung_util_TemplateData} data - data object
     */
    addTemplate (key: string, template: com_yung_util_RawTemplate, data: object): void
    
    /** 
     * set complex template data to render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_Map} dataMap - data map
     */
    setData (data: com_yung_util_TemplateData) : void
    
    /** 
     * get data object
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @return  {object} data object
     */
    getData (): com_yung_util_TemplateData 
    
    /** 
     * render HTML code
     * 
     * @instance
     * @memberof com_yung_util_ComplexTemplate
     * @param  {com_yung_util_TemplateData} data - complex tempalte data [optional]
     * @return  {string} html replaced by render key/value
     */
    toHtml (data?: com_yung_util_TemplateData): string 
}