// modified by: Yung Long Li
/**
 * Base64 utility
 *
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Base64 {

    classProp : { name : "com.yung.util.Base64"}

    static _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
    
    /**
     * convert source string to base64 code
     *
     * @memberof com_yung_util_Base64
     * @param {String} input - source string
     * @return {String} base64 string
     */
    static encode(input: string): string
    
    /**
     * convert base64 code to source string
     *
     * @memberof com_yung_util_Base64
     * @param {String} input - base64 code
     * @return {String} source string
     */
    static decode(input: string): string

    private static _utf8_encode(string: string)
    
    private static _utf8_decode(utftext: string)

}