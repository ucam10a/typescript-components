import { $Class } from "./yung-JS-extend";

/**
 * Float iframe element, usually used for display complex message, like report, table.
 * Note: float iframe will not create mask to block screen. 
 * if mask is required, use com_yung_util_Popup
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_FloatIframe extends $Class {
    
    classProp : { 
        name : "com.yung.util.FloatIframe",
        unimplemented: []
    }
    
    hashId : string
    
    /**
     * float iframe div id
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    divId : string
    
    /**
     * constructor
     * @memberof com_yung_util_FloatIframe
     * @param  {string} divId - float iframe div id
     * @param  {boolean} onblurClose - a flag to close float iframe when onblur
     */
    private constructor (divId: string, onblurClose: boolean)
    
    /** 
     * close float iframe when onblur
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    onblurClose (): void
    
    /** 
     * show close button in div
     * 
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {boolean} show - show or not
     */
    showCloseButton (show: boolean): void
    
    /** 
     * create iframe content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    createFloatIFrame (): void
    
    /** 
     * show float content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     */
    showFloatContent (): void
    
    /** 
     * show float iframe
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {boolean} show - show flag
     */
    showFloatIframe (show: boolean): void
    
    /**
     * Callback when close float iframe 
     *
     * @callback floatIframeCloseCallback
     */
    /** 
     * close and hide iframe
     * 
     * @instance
     * @memberof com_yung_util_FloatIframe
     * @param  {floatIframeCloseCallback} callback - callback when close float iframe 
     */
    closeFloatIframe (callback?: Function): void
    
    /** 
     * open float iframe, width and height are optional.
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} url - url for float iframe
     * @param  {object} pos - position object, include top and left number
     * @param  {number} width - float iframe width
     * @param  {number} height - float iframe height
     */
    openFloatIframe (url: string, pos: object, width?: number, height?: number): void

    /** 
     * get com_yung_util_FloatIframe global instance,
     * 
     * @param  {string} divId - div id
     * @param  {boolean} onblurClose - a flag to close float div when onblur
     * @return  {com_yung_util_FloatIframe} com_yung_util_FloatIframe instance
     */
    static instance (divId: string, onblurClose: boolean) : com_yung_util_FloatIframe
}