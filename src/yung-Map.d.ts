import { $Class, Comparable, com_yung_util_CollectionInterface, com_yung_util_EntryInterface, com_yung_util_MapInterface } from "./yung-JS-extend";

/**
 * Map entry
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Entry<K, V> extends $Class implements com_yung_util_EntryInterface<K, V> {
    
    // @ts-ignore
	classProp : { 
        name : "com.yung.util.Entry",
        unimplemented: []
	}
    
	/**
     * key
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_Entry
     */
	key : K
    
	/**
     * value
     * @member {string | number | object}
     * @instance
     * @memberof com_yung_util_Entry
     */
	value : V
    
	/**
	 * constructor
     * @memberof com_yung_util_Entry
     * @param  {string | number} key - key
     * @param  {string | number | boolean | object} value - value
     */
	constructor (key: any, value: any)
    
    /** 
     * get key
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number} key
     */
    getKey (): K 
    
    /** 
     * get value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number | bollean | object} value
     */
    getValue (): V 
    
    /** 
     * set value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @param  {string | number | bollean | object} value - value
     */
    setValue (value: V): void 
}

/**
 * Basic Map, only allow key type string, number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_BasicMap<K, V> extends $Class implements com_yung_util_MapInterface<K, V> {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.BasicMap",
        unimplemented: []
    }
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    keyType : K
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueType : V
    
    /**
     * value type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueTypeName : string
    
    /**
     * set to store all keys 
     * @member {com_yung_util_BasicSet}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    keyCollection : com_yung_util_CollectionInterface<K>
    
    /**
     * obj to store all key value pair 
     * @member {object}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    obj : object
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     */
    constructor (keyType: any, valueType: any)
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    clear (): void 
    
    isMap (map: any): boolean 

    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {all} value
     */
    validValueType (value: any): boolean 
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {all} key
     */
    validKeyType (key: any): boolean 

    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey (key: K): boolean 
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue (value: V): boolean 
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get (key: K): V 
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put (key: K, value: V): void 
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll (map: com_yung_util_MapInterface<K, V>): void 
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove (key: K): V 
    
    /** 
     * map size
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {number} map size
     */
    size (): number 
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {boolean} is empty or not
     */
    isEmpty (): boolean 
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} map entry array
     */
    entryArray (): Array<com_yung_util_EntryInterface<K, V>> 
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} map key array
     */
    getKeyArray (): Array<K> 
    
    /** 
     * return a copy of key array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} a copy of key array
     */
    cloneKeyArray (): Array<K> 
    
    /** 
     * return a copy of key set
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_CollectionInterface<K>} a copy of key set
     */
    keySet (): com_yung_util_CollectionInterface<K> 
    
    /** 
     * return a copy of value list
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_CollectionInterface} a copy of value list
     */
    values (): com_yung_util_CollectionInterface<V> 
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {string} string
     */
    toString (): string 
    
    firstKey () : K 

    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_BasicMap} a copy of map
     */
    clone (): com_yung_util_MapInterface<K, V> 
    
}

/**
 * Sorted Map
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicMap
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_SortedMap<K, V> extends com_yung_util_BasicMap<K, V> {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.SortedMap",
        unimplemented: []
    }
	
	/**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    keyType : K
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    valueType : V
    
    /**
     * value type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueTypeName : string
    
    /**
     * set to store all keys 
     * @member {com_yung_util_SortedSet}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    // @ts-ignore
    keyCollection : com_yung_util_CollectionInterface<K>
    
    /**
     * obj to store all key value pair 
     * @member {object}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    obj : object
    
    /**
     * flag to sort keys
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
	reverse : boolean
    
	/**
	 * constructor
     * @memberof com_yung_util_SortedMap
     * @param  {K} keyType - key type
     * @param  {string | function | object} valueType - value type
     * @param  {boolean} reverse - flag to sort keys
     */
	constructor (keyType: any, valueType: any, reverse?: boolean)
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put (key: K, value: V): void 
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @return {com_yung_util_SortedMap} a copy of map
     */
    clone (): com_yung_util_MapInterface<K, V> 
    
    private binarySearchArray (arr: Array<K>, x: K, start: number, end: number, reverse?: boolean, exact?: boolean): number 
	
	/** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove (key: K): V 
}


/**
 * JS Map, only allow key type string, number. best performance
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class system_util_Map<K, V> implements com_yung_util_MapInterface<K, V> {
    
    // @ts-ignore
    classProp : { 
        name : "system.util.Map"
        unimplemented: []
    }
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof system_util_Map
     */
    keyType : K
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof system_util_Map
     */
    valueType : V
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof system_util_Map
     */
    valueTypeName : string
    
    /**
     * set to store all keys 
     * @member {system_util_Set}
     * @instance
     * @memberof system_util_Map
     */
    keyCollection : com_yung_util_CollectionInterface<K>
    
    /**
     * map to store all key value pair 
     * @member {Map}
     * @instance
     * @memberof system_util_Map
     */
    obj : Map<K, V>
    
    /**
     * constructor
     * @memberof system_util_Map
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     */
    constructor (keyType: any, valueType: any)
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof system_util_Map
     */
    clear (): void 
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof system_util_Map
     * @return {all} value
     */
    validValueType (value: any): boolean 
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof system_util_Map
     * @return {all} key
     */
    validKeyType (key: any): boolean 

    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey (key: K): boolean 
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue (value: V): boolean 
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get (key: K): V 
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put (key: K, value: V): void 

    isMap (map: any): boolean 
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll (map: com_yung_util_MapInterface<K, V>): void 
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove (key: K): V 
    
    /** 
     * map size
     * 
     * @instance
     * @memberof system_util_Map
     * @return {number} map size
     */
    size (): number 
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof system_util_Map
     * @return {boolean} is empty or not
     */
    isEmpty (): boolean 
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof system_util_Map
     * @return {Array} map entry array
     */
    entryArray (): Array<com_yung_util_EntryInterface<K, V>> 
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof system_util_Map
     * @return {Array} map key array
     */
    getKeyArray (): Array<K> 
    
    /** 
     * return a copy of key set
     * 
     * @instance
     * @memberof system_util_Map
     * @return {com_yung_util_CollectionInterface<K>} a copy of key set
     */
    keySet (): com_yung_util_CollectionInterface<K> 
    
    /** 
     * return a copy of value list
     * 
     * @instance
     * @memberof system_util_Map
     * @return {com_yung_util_CollectionInterface} a copy of value list
     */
    values (): com_yung_util_CollectionInterface<V> 
    
    firstKey (): K 

    /** 
     * to string
     * 
     * @instance
     * @memberof system_util_Map
     * @return {string} string
     */
    toString (): string 
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof system_util_Map
     * @return {com_yung_util_MapInterface<K, V>} a copy of map
     */
    clone (): com_yung_util_MapInterface<K, V> 
    
}

/**
 * Tree node.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_TreeNode<K> extends $Class {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.TreeNode",
        unimplemented: []
    }

    /**
     * is leaf node
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    isLeaf : boolean
    
    /**
     * collection to store all keys 
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    keys : Array<K>
    keyNumber : number
    leftMostKey : K
    keyType : K
    reverse: boolean
    
    /**
     * object reference
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    nodeRef : Array<any>
    prevLeaf : K
    mergeInternal : boolean
    
    /**
     * constructor
     * @memberof com_yung_util_TreeNode
     * @param  {boolean} isLeaf - whether leaf node
     * @param  {boolean} reverse - reverse order
     * @param  {all} keyType - key type
     */
    constructor (isLeaf: boolean, reverse: boolean, keyType: any)
    
    /**
     * copy node
     * @memberof com_yung_util_TreeNode
     * @param  {com_yung_util_TreeNode} node - the node to copy from
     * @param  {number} from - where in n to start copying from
     * @param  {number} num - the number of keys/refs to copy
     */
    copy (node: com_yung_util_TreeNode<K>, from: number, num: number): void 
    
    /**
     * Find the "<=" match position in this node.
     * @memberof com_yung_util_TreeNode
     * @param  {key} key - the key to be matched.
     * @return  {number} the position of match within node, where nKeys indicates no match
     */
    find (key: K): number 
    
}
//declare var com_yung_util_TreeNode["notFound"] = function (){};

/**
 * Tree map. Best performance for sorted key, but only allow key type string or number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_TreeMap<K, V> implements com_yung_util_MapInterface<K, V> {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.TreeMap",
        unimplemented: []
    }
    
    ORDER : 29  // odd number only
    MAX : number
    MID : number
    MIN_LEAF : number
    MIN_NODE : number
    
    /**
     * reverse order
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    reverse : boolean
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyType : K
    
    /**
     * key type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyTypeName : string
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    valueType : V
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    valueTypeName : string
    
    /**
     * root of tree
     * @member {com_yung_util_TreeNode}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    root : com_yung_util_TreeNode<K>
    
    /**
     * first leaf of tree
     * @member {com_yung_util_TreeNode}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    firstLeaf : com_yung_util_TreeNode<K>
    
    /**
     * key size
     * @member {number}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyCount : number
    
    /**
     * constructor
     * @memberof com_yung_util_TreeMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     * @param  {boolean} reverse - reverse order
     */
    constructor (keyType: any, valueType: any, reverse?: boolean)
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {Array} map entry array
     */
    entryArray (): Array<com_yung_util_Entry<K, V>> 
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {Array} map key array
     */
    getKeyArray (): Array<K> 
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {all} value
     */
    validValueType (value: any): boolean 
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {all} key
     */
    validKeyType (key: any): boolean 

    /** 
     * get value by key
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get (key: any): V 
    private find (key: K, node: com_yung_util_TreeNode<K>): V 
    private findNextNode (key: K, node: com_yung_util_TreeNode<K>): number 
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to get value
     * @return {boolean} removed or not
     */
    remove (key: K): V | boolean 
    private delKey (key: K, node: com_yung_util_TreeNode<K>): com_yung_util_TreeNode<K> 
    private splice (key: K, node: com_yung_util_TreeNode<K>, index: number, isLeft: boolean): boolean 
    private deleteNode (deleteKey: K, handleNode: com_yung_util_TreeNode<K>, node: com_yung_util_TreeNode<K>): com_yung_util_TreeNode<K> 
    private seekNodeIndex (parent: com_yung_util_TreeNode<K>, child: com_yung_util_TreeNode<K>): number 
    private merge (leftNode: com_yung_util_TreeNode<K>, rightNode: com_yung_util_TreeNode<K>, topNode: com_yung_util_TreeNode<K>): void 
    private mergeInternal (leftNode: com_yung_util_TreeNode<K>, rightNode: com_yung_util_TreeNode<K>, topNode: com_yung_util_TreeNode<K>): void 
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put (key: K, value: V): boolean 
    private insert (key: K, value: V, node: com_yung_util_TreeNode<K>): com_yung_util_TreeNode<K> 
    private wedge (key: K, value: V | com_yung_util_TreeNode<K>, node: com_yung_util_TreeNode<K>, index: number, isLeft: boolean) 
    private insertNode (key: K, value: com_yung_util_TreeNode<K>, node: com_yung_util_TreeNode<K>) 
    private split (node: com_yung_util_TreeNode<K>): com_yung_util_TreeNode<K> 
    private splitInternal (node: com_yung_util_TreeNode<K>): com_yung_util_TreeNode<K> 
    private makeRoot (leftChildNode: com_yung_util_TreeNode<K>, key: K, rightChildNode: com_yung_util_TreeNode<K>): com_yung_util_TreeNode<K> 
    
    /** 
     * return the first key of map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {keyType}the first key of map
     */
    firstKey (): K 
    
    /** 
     * return the last key of map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {keyType} the last key of map
     */
    lastKey (): K 
    
    /** 
     * map size
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {number} map size
     */
    size (): number 
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    clear (): void 
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey (key: K): boolean 
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue (value: V): boolean 
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll (map: com_yung_util_MapInterface<K, V>): void
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {boolean} is empty or not
     */
    isEmpty (): boolean 
    
    toString (): string 
    
    debugNode (node: com_yung_util_TreeNode<K>, level: number, bud: string): string 
    
    debugLeaf (leaf: com_yung_util_TreeNode<K>, bud: string): string 
    
    isMap (map: any): boolean 
}

/**
 * Tree hash map, for key type is function
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_TreeMap
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_TreeHashMap<K extends Comparable, V> extends com_yung_util_TreeMap<K, V> {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.TreeHashMap",
        unimplemented: []
    }

    /**
     * constructor
     * @memberof com_yung_util_TreeHashMap
     * @param  {function} keyType - key type
     * @param  {all} valueType - value type
     * @param  {boolean} reverse - reverse order
     */
    constructor (keyType: any, valueType: any, reverse?: boolean)
    
    toString (): string 
    
}