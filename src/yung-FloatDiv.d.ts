import { $Class } from "./yung-JS-extend";

/**
 * Float div element, usually used for display message[simple HTML], like tooltip or menu. 
 * Note: float div will not create mask to block screen. 
 * if mask is required, use com_yung_util_Popup
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_FloatDiv extends $Class {

    classProp : { 
        name : "com.yung.util.FloatDiv",
        unimplemented: [] 
    }
    
    hashId : string
    
    /**
     * float div id
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    divId : string
    
    /**
     * div overflow
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    overflow : string
    
    /**
     * constructor
     * @memberof com_yung_util_FloatDiv
     * @param  {string} divId - float div id
     * @param  {boolean} onblurClose - a flag to close float div when onblur
     * @param  {string} overflow - float div overflow
     */
    constructor (divId: string, onblurClose?: boolean, overflow?: string)
    
    /** 
     * close float div when onblur
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    onblurClose (): void
    
    /** 
     * show close button in div
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {boolean} show - show or not
     */
    showCloseButton (show: boolean): void
    
    /** 
     * create div content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    createFloatDiv (): void
    
    /** 
     * set html content for float div
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} html - HTML code 
     */
    setFloatContent (html: string): void
    
    /** 
     * show float content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    showFloatContent (): void
    
    /** 
     * show float div
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {boolean} show - show flag
     * @param  {com_yung_util_FloatDiv} self - com_yung_util_FloatDiv itself, usually for callback use
     */
    showFloatDiv (show: boolean, self?: com_yung_util_FloatDiv): void
    
    /**
     * Callback when close float div 
     *
     * @callback floatDivCloseCallback
     */
    /** 
     * close and hide div
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {floatDivCloseCallback} callback - callback when close float div 
     */
    closeFloatDiv (callback?: Function): void
    
    /** 
     * open float div, width and height are optional.
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} html - HTML code for float div
     * @param  {object} pos - position object, include top and left number
     * @param  {number} width - float div width
     * @param  {number} height - float div height
     * @param  {function} callback - callback
     */
    openFloatDiv (html: string, pos: object, width: number, height: number, callback?: Function): void
    
    /**
     * return if div display
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @return {boolean} display or not
     */
    isDisplay (): boolean
    
    /**
     * return float div element
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @return {HTMLElement} float div element
     */
    getFloatDivElement (): any

    /** 
     * get com_yung_util_FloatDiv global instance,
     * 
     * @param  {string} divId - div id
     * @param  {boolean} onblurClose - a flag to close float div when onblur
     * @param  {string} overflow - float div overflow
     * @return  {com_yung_util_FloatDiv} com_yung_util_FloatDiv instance
     */
    static instance(divId: string, onblurClose: boolean, overflow: string) : com_yung_util_FloatDiv

}