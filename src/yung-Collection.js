/**
 * Collection interface. Define implement methods.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_CollectionInterface = $Class.extend({

    classProp : { 
		name : "com.yung.util.CollectionInterface",
		unimplemented : ['toArray', 'size', 'getFirstElement', 'add', 'remove', 'contains']
    }
    
});

/**
 * Abstract Class for collection
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Collection = com_yung_util_CollectionInterface.extend({
    
	classProp : { 
		name : "com.yung.util.Collection",
		unimplemented : ['add', 'remove']
	},
    
	/**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_Collection
     */
	type : null,
    
	/**
     * element array
     * @member {string | number | boolean | object}
     * @instance
     * @memberof com_yung_util_Collection
     */
	array : [],
	
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_Collection
     */
    clear : function () {
        this.array = [];
        if (this.obj != null) {
            this.obj = {};
        }
        if (this.tsoArray != null) {
            this.tsoArray = [];
        }
        this.tsoIdx = 0;
    },
    
    /**
	 * add elements by array
	 * 
	 * @instance
     * @memberof com_yung_util_Collection
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        if (this.type == 'string' || this.type == 'number') {
        	if (this.obj != null) {
        		if (this.obj[element + ""] == null) {
                    return false;
                } else {
                    return true;
                }
        	} else {
        		if (jQuery.inArray(element, this.array) < 0) {
                    return false;
                } else {
                    return true;
                }
        	}
        } else if (typeof this.type == 'function') {
        	var hash = null;
        	if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            } else {
                if (typeof element["hashCode"] == 'function') {
                    hash = element["hashCode"]();
                } 
            }
        	if (this.obj != null && hash != null) {
        		if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
        	}
        } else if (this.type == 'object') {
            var hash = null;
            if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            }
        	if (this.obj != null && hash != null) {
        		if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
        	}
        }
        if (jQuery.inArray(element, this.array) < 0) {
            return false;
        } else {
            return true;
        }
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.array.length == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {number} collection size
     */
    size : function () {
        return this.array.length;
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {Array} element array
     */
    toArray : function () {
        return this.array;
    },
    
    /** 
     * return a copy of element array
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {Array} element array
     */
    cloneArray : function () {
        var arr = [];
        for (var i = 0; i < this.array.length; i++) {
            arr.push(this.array[i]);
        }
        return arr;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @return {string} message string
     */
    toString : function () {
        var ret = "";
        if (this.array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "[";
        }
        for (var i = 0; i < this.array.length; i++) {
            
            if (this.type == 'string') {
                ret = ret + "'" + this.array[i] + "', ";
            } else if (this.type == 'number') {
                ret = ret + this.array[i] + ", ";
            } else {
                if (i == 0) ret = ret + "\n";
                if (jQuery.isArray(this.array[i])) {
                    ret = ret + "    " + JSON.stringify(this.array[i]) + ",\n"; 
                } else if (typeof this.array[i].toString == "function") {
                    ret = ret + "    " + this.array[i].toString() + ",\n"; 
                } else {
                    ret = ret + "    " + JSON.stringify(this.array[i]) + ",\n"; 
                }
            }
        }
        if (ret.length > 0) {
            ret = ret.substring(0, ret.length - 2);
        }
        if (this.array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "]";
        }
        return this.classProp.name + ret;
    },
    
    /** 
     * sort collection elements
     * 
     * @instance
     * @param  {boolean} reverse - is reverse sort
     * @memberof com_yung_util_Collection
     */
    sort : function (reverse) {
        if (reverse == null || reverse == false) {
            reverse = false;
        } else {
            reverse = true;
        }
        if (reverse == false) {
            if (this.type == 'string') {
                this.array.sort();
            } else if (this.type == 'number') {
                this.array.sort(function(a, b){return a - b});
            } else if (typeof this.type == 'function') {
                var instance = new this.type();
                if (typeof instance.compareTo == 'function') {
                    this.array.sort(function(a, b){return a.compareTo(b);});
                } else {
                	throw "instance not implement compareTo";
                }
            }
        } else {
            if (this.type == 'string') {
                this.array.sort();
                this.array.reverse();
            } else if (this.type == 'number') {
                this.array.sort(function(a, b){return b - a});
            } else if (typeof this.type == 'function') {
                var instance = new this.type();
                if (typeof instance.compareTo == 'function') {
                    this.array.sort(function(a, b){return b.compareTo(a);});
                } else {
                	throw "instance not implement compareTo";
                }
            }
        }
    },
    
    /** 
     * check object is collection or not
     * 
     * @memberof com_yung_util_Collection
     * @param  {object} obj - object to check
     * @return {boolean} is collection
     */
    isCollection : function (obj) {
    	return obj instanceof com.yung.util.CollectionInterface;
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_Collection
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
    	return _validType(this.type, element);
    },
    
    getFirstElement :  function () {
        if (this.size() == 0) {
            return null;
        }
        return this.array[0];
    },
    
    binarySearchArray : function(arr, x, start, end, reverse, exact) {
    	return binarySearchArray(arr, x, start, end, reverse, exact);
	}
});

/**
 * Basic set, only allow type string ,number and boolean
 * 
 * @property {object} classProp - class property
 * @property {string} classProp.name - class name
 * @augments com_yung_util_Collection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicSet = com_yung_util_Collection.extend({
    
	classProp : { 
		name : "com.yung.util.BasicSet"
	},
	
	/**
     * element array
     * @member {string | numbe | boolean}
     * @instance
     * @memberof com_yung_util_Collection
     */
	array : [],
	obj : {},
	tsoArray : [],
	tsoIdx : 0,
	
	/**
	 * constructor
     * @memberof com_yung_util_BasicSet
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.BasicSet type can not be null!";
        }
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type != 'string' && type != 'number' && type != 'boolean') {
            throw "com.yung.util.BasicSet type can only be string, number or boolean!";
        }
        this.type = type;
        this.array = [];
        this.obj = {};
        this.tsoArray = [];
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        if (!this.contains(element)) {
            this.obj[element + ""] = this.tsoIdx;
            this.array.push(element);
            this.tsoArray.push(this.tsoIdx);
            this.tsoIdx++;
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection if contains all elements in another collection 
     * 
     * @instance
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to check
     * @return {boolean} exist or not
     */
    containsAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (this.isCollection(collection) == false) {
            return false;
        }
        if (this.size() != collection.size()) {
            return false;
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            if (this.contains(array[i]) == false) {
                return false;
            }
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
        	return false;
        }
        if (this.obj[element + ""] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.tsoArray, this.obj[element + ""], 0, this.tsoArray.length - 1, false, true);
        if (index < 0) {
            return false;
        } else {
        	this.array.splice(index, 1);
        	this.tsoArray.splice(index, 1);
        	delete this.obj[element + ""];
            return true;
        }
    },
    
    /**
     * Removes the all elements of collection to this set 
     * 
     * @memberof com_yung_util_BasicSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to remove
     * @instance
     * @return {boolean} success or not
     */
    removeAll : function (collection) {
        if (collection == null) {
            return false;
        }
        if (this.isCollection(collection) == false) {
        	throw "argument is not com.yung.util.Collection";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.remove(array[i]);
        }
        return true;
    },
    
    clone : function () {
        var ret = new com.yung.util.BasicSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
    
});

/**
 * raw object set, not good in performance
 *
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_RawObjectSet = com_yung_util_BasicSet.extend({
    
	classProp : { 
		name : "com.yung.util.RawObjectSet"
	},
	
    /**
     * element array
     * @member {Array}
     * @instance
     * @memberof com_yung_util_RawObjectSet
     */
    array : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_RawObjectSet
     */
    init : function(){
        this.type = 'object';
        this.array = [];
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type must be object";
        }
        if (jQuery.inArray(element, this.array) < 0) {
        	this.array.push(element);
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_RawObjectSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        if (jQuery.inArray(element, this.array) < 0) {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_RawObjectSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var index = jQuery.inArray(element, this.array);
        if (index < 0) {
            return false;
        } else {
            this.array.splice(index, 1);
            return true;                
        }
    },
    
    clone : function () {
        var ret = new com.yung.util.RawObjectSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
    
});

/**
 * Raw object set, only allow type object
 * object element will use JSON.stringify to check exist 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ValueHashSet = com_yung_util_BasicSet.extend({
    
	classProp : { 
	    name : "com.yung.util.ValueHashSet"
    },
    
    /**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_ValueHashSet
     */
    array : null,
    obj : null,
    tsoArray : [],
    tsoIdx : 0,
    valueHash : true,
    
    /**
	 * constructor
     * @memberof com_yung_util_ValueHashSet
     */
    init : function(){
        this.array = [];
        this.obj = {};
        this.type = 'object';
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        var json = JSON.stringify(element);
        var hash = MD5Util.calc(json);
        if (this.obj[hash] == null) {
            this.obj[hash] = this.tsoIdx;
            this.array.push(element);
            this.tsoArray.push(this.tsoIdx);
            this.tsoIdx++;
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_ValueHashSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var json = JSON.stringify(element);
        var hashCode = MD5Util.calc(json);
        if (this.obj[hashCode] == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_ValueHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (typeof element != this.type) {
            return false;
        }
        var json = JSON.stringify(element);
        var hashCode = MD5Util.calc(json);
        if (this.obj[hashCode] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.tsoArray, this.obj[hashCode], 0, this.tsoArray.length - 1, false, true);
        if (index < 0) {
            return false;
        } else {
            this.array.splice(index, 1);
            this.tsoArray.splice(index, 1);
            delete this.obj[hashCode];
            return true;                
        }
    },
    
    clone : function () {
        var ret = new com.yung.util.ValueHashSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
    
});

/**
 * sorted set, only allow type string and number
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SortedSet = com_yung_util_BasicSet.extend({
	
    classProp : { 
    	name : "com.yung.util.SortedSet"
    },
    
    /**
     * reverse falg
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedSet
     */
    reverse : null,
    
    /**
     * element array
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_SortedSet
     */
    array : null,
    obj : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_SortedSet
     * @param  {string} type - element type
     * @param  {boolean} reverse - reverse flag
     */
    init : function(type, reverse){
        if (type == null) {
            throw "com.yung.util.SortedSet type can not be null!";
        }
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.SortedSet type can only be string or number!";
        }
        if (reverse === true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.type = type;
        this.array = [];
        this.obj = {};
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_SortedSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        if (this.obj[element + ""] == null) {
        	var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse);
        	if (this.reverse) {
        		if (element < this.array[index]) {
        			index = index + 1;
        		}
        	} else {
        		if (element > this.array[index]) {
        			index = index + 1;
        		}
        	}
        	this.obj[element + ""] = this.tsoIdx;
        	this.array.splice(index, 0, element);
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_SortedSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
        	return false;
        }
        if (this.obj[element + ""] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse, true);
        if (index < 0) {
            return false;
        } else {
        	this.array.splice(index, 1);
        	delete this.obj[element + ""];
            return true;
        }
    },
    
    clone : function () {
        var ret = new com.yung.util.SortedSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
});

/**
 * object hash set, only allow element which implements hashCode function
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ObjectHashSet = com_yung_util_BasicSet.extend({
    
	classProp : { 
		name : "com.yung.util.ObjectHashSet"
	},
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     */
	typeName : null,
    
	/**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     */
	array : null,
	obj : null,
	tsoArray : [],
	tsoIdx : 0,
	
	/**
	 * constructor
     * @memberof com_yung_util_ObjectHashSet
     * @param  {function} type - element function
     */
	init : function(type){
        if (type == null) {
            throw "com.yung.util.ObjectHashSet type can not be null!";
        }
        if (typeof type != 'function') {
            throw "com.yung.util.ObjectHashSet type can only be function!";
        }
        if ($Class.checkFunction(type, 'hashCode') == false) {
            throw "Please implement hashCode function in " + $Class.getClassName(type);
        }
        this.type = type;
        this.typeName = $Class.getClassName(type);
        this.array = [];
        this.obj = {};
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.typeName;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
            this.obj[hashCode] = this.tsoIdx;
            this.array.push(element);
            this.tsoArray.push(this.tsoIdx);
            this.tsoIdx++;
            return true;
        }
        return false;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_ObjectHashSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_ObjectHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.tsoArray, this.obj[hashCode], 0, this.tsoArray.length - 1, false, true);
        if (index < 0) {
            return false;
        } else {
            this.array.splice(index, 1);
            this.tsoArray.splice(index, 1);
            delete this.obj[hashCode];
            return true;
        }
    },
    
    clone : function () {
        var ret = new com.yung.util.ObjectHashSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
    
});

/**
 * tree object hash set, only allow element which implements hashCode and compareTo function
 * element will be sorted
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_ObjectHashSet
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SortedHashSet = com_yung_util_ObjectHashSet.extend({
    
	classProp : { 
		name : "com.yung.util.SortedHashSet"
	},
    
	/**
     * reverse falg
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	reverse : null,
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	typeName : null,
    
	/**
     * element array
     * @member {object}
     * @instance
     * @memberof com_yung_util_SortedHashSet
     */
	array : null,
	obj : null,
	
	/**
	 * constructor
     * @memberof com_yung_util_SortedHashSet
     * @param  {function} type - element function
     * @param  {boolean} reverse - reverse flag
     */
	init : function(type, reverse){
        if (type == null) {
            throw "com.yung.util.SortedHashSet type can not be null!";
        }
        if (typeof type != 'function') {
            throw "com.yung.util.ObjectHashSet type can only be function!";
        }
        var typeName = $Class.getClassName(type);
        if ($Class.checkFunction(type, 'hashCode') == false) {
            throw "Please implement hashCode function in " + typeName;
        }
        if ($Class.checkFunction(type, 'compareTo') == false) {
            throw "Please implement compareTo function in " + typeName;
        }
        this.type = type;
        this.typeName = typeName;
        if (reverse === true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.array = [];
        this.obj = {};
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_SortedHashSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.typeName;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
        	var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse);
        	if (this.reverse) {
        		if (this.array[index] != null && element.compareTo(this.array[index]) < 0) {
        			index = index + 1;
        		}
        	} else {
        		if (this.array[index] != null && element.compareTo(this.array[index]) > 0) {
        			index = index + 1;
        		}
        	}
        	this.array.splice(index, 0, element);
        	this.obj[hashCode] = this.tsoIdx;
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_SortedHashSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
        	return false;
        }
        var hashCode = element.hashCode();
        if (this.obj[hashCode] == null) {
        	return false;
        }
        var index = this.binarySearchArray(this.array, element, 0, this.array.length - 1, this.reverse, true);
        if (index < 0) {
            return false;
        } else {
        	this.array.splice(index, 1);
        	delete this.obj[hashCode];
            return true;
        }
    },
    
    clone : function () {
        var ret = new com.yung.util.SortedHashSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
});

/**
 * Basic list, only allow type string and number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_Collection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AbstractQueue = com_yung_util_Collection.extend({
    
    classProp : { 
        name : "com.yung.util.AbstractQueue",
        unimplemented : ['add', 'remove']
    },
    
    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @memberof com_yung_util_AbstractQueue
     * @instance
     * @return {element} retrieve element
     */
    peek : function () {
        if (this.array.length == 0) {
            return null;
        } else {
            return this.array[0];
        }
    },
    
    /**
     * Retrieves and removes the head of this queue,
     * or returns null if this queue is empty.
     * 
     * @memberof com_yung_util_AbstractQueue
     * @instance
     * @return {element} retrieve element
     */
    poll : function () {
        if (this.array.length == 0) {
            return null;
        } else {
            return this.array.shift();
        }
    }
    
});

/**
 * Basic list, only allow type string and number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_AbstractQueue
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicList = com_yung_util_AbstractQueue.extend({
    
	classProp : { 
		name : "com.yung.util.BasicList"
    },
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicList
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.List type can not be null!";
        }
        if (typeof type == "string") {
            type = type.toLowerCase();
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.List type can only be string or number!";
        }
        this.type = type;
        this.array = [];
        return this;
    },
    
    /**
     * Adds the specified element to this list by index 
     * index is optional
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element to add
     * @param  {number} index - index to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element, index) {
    	if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
    	var ret = null;
        if (index == null) {
            ret = this.addElement(element);
        } else {
            ret = this.addByIndex(element, index);
        }
        return ret;
    },
    
    /**
     * Adds the specified element to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    addElement : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        this.array.push(element);
        return true;
    },
    
    /**
     * Adds the specified element to this list by index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element to add
     * @param  {number} index - index to add
     * @instance
     * @return {boolean} success or not
     */
    addByIndex : function (element, index) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        this.array.splice(index, 0, element);
        return true;
    },
    
    /**
     * Adds the elements of collection to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @param  {number} index - index to add, if null then add to list tail
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection, index) {
    	var ret = null;
        if (index == null) {
            ret = this.addAllElements(collection);
        } else {
            ret = this.addAllByIndex(collection, index);
        }
        return ret;
    },
    
    /**
     * Adds the elements of collection to this list tail 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAllElements : function (collection) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.addElement(array[i]);
        }
        return true;
    },
    
    /**
     * Adds the elements of collection to this list 
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @param  {number} index - index to add, if null then add to list tail
     * @instance
     * @return {boolean} success or not
     */
    addAllByIndex : function (collection, index) {
    	if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        var idx = index;
        for (var i = 0; i < array.length; i++) {
        	this.addByIndex(array[i], idx);
            idx++;
        }
        return true;
    },
    
    /**
     * get element by index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index of element
     * @instance
     * @return {element} element
     */
    get : function (index) {
        return this.array[index];
    },
    
    /**
     * Returns the index of the first occurrence of the specified element in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element
     * @instance
     * @return {number} index
     */
    indexOf : function (element) {
        if (element == null) {
            return -1;
        }
        if (this.validType(element) == false) {
            return -1;
        }
        for (var i = 0; i < this.array.length; i++) {
            if (element === this.array[i]) {
                return i;
            }
        }
        return -1;
    },
    
    /**
     * Returns the index of the last occurrence of the specified element in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element
     * @instance
     * @return {number} index
     */
    lastIndexOf : function (element) {
        if (element == null) {
            return -1;
        }
        if (this.validType(element) == false) {
            return -1;
        }
        for (var i = this.array.length - 1; i >= 0; i--) {
            if (element === this.array[i]) {
                return i;
            }
        }
        return -1;
    },
    
    /**
     * Removes the element at the specified position in this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index
     * @instance
     * @return {validType} removed element
     */
    removeByIndex : function (index) {
        if (index < 0 || index >= this.array.length) {
            return null;
        }
        var ret = this.array[index];
        this.array.splice(index, 1);
        return ret;
    },
    
    /**
     * Removes the first occurrence of the specified element from this list
     * 
     * @memberof com_yung_util_BasicList
     * @param  {validType} element - element
     * @instance
     * @return {boolean} success or not
     */
    removeElement : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var index = jQuery.inArray(element, this.array);
        if (index < 0) {
            return false;
        } else {
            if (index !== -1) {
                this.array.splice(index, 1);
                return true;
            } else {
                return false;
            }
        }
    },
    remove : function (element) {
        return this.removeElement(element);
    },
    
    /**
     * Removes from this list all of its elements that are contained in the specified collection
     * 
     * @memberof com_yung_util_BasicList
     * @param  {com_yung_util_CollectionInterface} collection - collection to remove
     * @instance
     * @return {boolean} success or not
     */
    removeAll : function (collection) {
        if (collection == null) {
            return false;
        }
        if (this.isCollection(collection) == false) {
            return false;
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.removeElement(array[i]);
        }
        return true;
    },
    
    /**
     * set element by specified index
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} index - index to set
     * @param  {validType} element - element to set
     * @instance
     * @return {validType} element to set
     */
    set : function (index, element) {
    	if (this.validType(element) == false) {
    		throw "argument type is not " + this.type;
        }
    	this.array[index] = element;
        return element;
    },
    
    /**
     * Returns a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive.
     * 
     * @memberof com_yung_util_BasicList
     * @param  {number} fromIndex - from index
     * @param  {number} toIndex - to index
     * @instance
     * @return {com_yung_util_BasicList} result list
     */
    subList : function (fromIndex, toIndex) {
        var ret = new com.yung.util.List(this.type);
        for (var i = 0; i < this.array.length; i++) {
            if (i >= fromIndex && i <= toIndex) {
                ret.add(this.array[i]);
            }
        }
        return ret;
    },
    
    clone : function () {
        var ret = new com.yung.util.BasicList(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
});

/**
 * raw object list, only allow type object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicList
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_RawObjectList = com_yung_util_BasicList.extend({
    
	classProp : { 
		name : "com.yung.util.RawObjectList"
    },
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicList
     */
    init : function(){
        this.type = 'object';
        this.array = [];
        return this;
    },
    
    clone : function () {
        var ret = new com.yung.util.RawObjectList(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
});

/**
 * function object list, only allow type function object
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicList
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_ObjectList = com_yung_util_BasicList.extend({
    
	classProp : { 
		name : "com.yung.util.ObjectList"
	},
    
	/**
     * element type name
     * @member {string}
     * @instance
     * @memberof com_yung_util_ObjectList
     */
	typeName : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_BasicList
     * @param  {function} type - element function
     */
	init : function(type){
        if (type == null) {
            throw "com.yung.util.ObjectList type can not be null!";
        }
        if (typeof type != 'function') {
            throw "com.yung.util.ObjectList type can only be function!";
        }
        this.type = type;
        this.typeName = $Class.getClassName(type);
        this.array = [];
        return this;
    },
    
    clone : function () {
        var ret = new com.yung.util.ObjectList(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
});

/**
 * Link node
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_LinkNode = $Class.extend({
    
    classProp : { 
        name : "com.yung.util.LinkNode"
    },
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    type : null,
    
    /**
     * before link node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    before : null,
    
    /**
     * after link node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    after : null,
    
    /**
     * element
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkNode
     */
    value : null,
    
    /**
     * constructor
     * @memberof com_yung_util_LinkNode
     * @param  {all} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.LinkNode type can not be null!";
        }
        this.type = type;
        return this;
    },
    
    /**
     * set before link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} beforeNode - before link node
     */
    setBefore : function (beforeNode) {
        this.before = beforeNode;
    },
    
    /**
     * set after link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} afterNode - after link node
     */
    setAfter : function (afterNode) {
        this.after = afterNode;
    },
    
    /**
     * next link node
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @return {com_yung_util_LinkNode} next link node
     */
    next : function () {
        return this.after;
    },
    
    /**
     * set link node value
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  {com_yung_util_LinkNode} value - link node value
     */
    setValue : function (value) {
        if (this.validType(value) == false) {
            throw value + " is not " + this.type;
        }
        this.value = value;
    },
    
    /**
     * get link node value
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @return {all} link node value
     */
    getValue : function () {
        return this.value;
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_LinkNode
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
        return _validType(this.type, element);
    }
    
});

/**
 * Abstract Class for link collection
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_LinkCollection = com_yung_util_CollectionInterface.extend({
    
    classProp : { 
        name : "com.yung.util.LinkCollection",
        unimplemented : ['add', 'remove']
    },
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    type : null,
    
    /**
     * object to store link node
     * @member {object}
     * @private
     * @memberof com_yung_util_LinkCollection
     */
    obj : null,
    
    /**
     * collection length
     * @member {number}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    length : 0,
    
    /**
     * first linked node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    first : null,
    
    /**
     * last linked node
     * @member {com_yung_util_LinkNode}
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    last : null,
    
    /**
     * constructor
     * @memberof com_yung_util_LinkCollection
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.LinkCollection type can not be null!";
        }
        this.type = type;
        this.obj = {};
        return this;
    },
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     */
    clear : function () {
        this.obj = {};
        this.length = 0;
    },
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        if (this.type == 'string' || this.type == 'number') {
            if (this.obj[element + ""] == null) {
                return false;
            } else {
                return true;
            }
        } else if (typeof this.type == 'function') {
            var hash = null;
            if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            } else {
                if (typeof element["hashCode"] == 'function') {
                    hash = element["hashCode"]();
                } 
            }
            if (hash != null) {
                if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (this.type == 'object') {
            var hash = null;
            if (this.valueHash == true) {
                var json = JSON.stringify(element);
                hash = MD5Util.calc(json);
            }
            if (hash != null) {
                if (this.obj[hash] == null) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        throw "unknow condition";
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {number} collection size
     */
    size : function () {
        return this.length;
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {Array} element array
     */
    toArray : function () {
        var ret = [];
        ret.push(this.first.getValue());
        var linkNode = this.first;
        while (linkNode.next() != null) {
            linkNode = linkNode.next();
            ret.push(linkNode.getValue());
        }
        return ret;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @return {string} message string
     */
    toString : function () {
        var array = this.toArray();
        return this.classProp.name + JSON.stringify(array);
    },
    
    /** 
     * check object is link collection or not
     * 
     * @memberof com_yung_util_LinkCollection
     * @param  {object} obj - object to check
     * @return {boolean} is link collection
     */
    isCollection : function (obj) {
    	var ret = obj instanceof com.yung.util.CollectionInterface;
        return ret;
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_LinkCollection
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    getFirstElement :  function () {
        if (this.size() == 0) {
            return null;
        }
        return this.first.getValue();
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
        return _validType(this.type, element);
    }
});

/**
 * basic linked set, only allow type string and number.
 * better for large scale, ex more than 100000
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_LinkCollection
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_LinkedSet = com_yung_util_LinkCollection.extend({
    
    classProp : { 
        name : "com.yung.util.LinkedSet"
    },
    
    /**
     * constructor
     * @memberof com_yung_util_LinkCollection
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "com.yung.util.LinkedSet type can not be null!";
        }
        if (typeof type != 'string') {
            throw "com.yung.util.LinkedSet type can only be string or number";
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.LinkedSet type can only be string or number";
        }
        this.type = type;
        this.obj = {};
        return this;
    },
    
    /**
     * add element
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {all} element - element to add
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        var linkNode = new com_yung_util_LinkNode(this.type);
        linkNode.setValue(element);
        if (this.length == 0) {
            this.first = linkNode;
        } else {
            if (this.obj[element + ""] != null) {
                return false;
            }
            if (this.last == null) {
                linkNode.setBefore(this.first);
                this.first.setAfter(linkNode);
                this.last = linkNode;
            } else {
                linkNode.setBefore(this.last);
                this.last.setAfter(linkNode);
                this.last = linkNode;
            }
        }
        this.length++;
        this.obj[element + ""] = linkNode;
    },
    
    /**
     * remove element
     * 
     * @instance
     * @memberof com_yung_util_LinkCollection
     * @param  {all} element - element to remove
     * @return {all} removed element
     */
    remove : function (element) {
        if (element == null) {
            return null;
        }
        if (this.validType(element) == false) {
            return null;
        }
        var linkNode = this.obj[element + ""];
        if (linkNode == null) {
            return null;
        }
        if (linkNode == this.first) {
            var newFirst = this.first.after;
            this.first = newFirst;
        } else if (linkNode == this.last) {
            var newLast = this.last.before;
            this.last = newLast;
        } else {
            var prev = linkNode.before;
            var next = linkNode.after;
            prev.after = next;
            next.before = prev;
        }
        delete this.obj[element + ""];
        this.length--;
        return element;
    },
    
    clone : function () {
        var ret = new com.yung.util.LinkedSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
    
});

/**
 * JS utility Set by restrict type, best performance
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var system_util_Set = com_yung_util_CollectionInterface.extend({
    
    classProp : { 
        name : "system.util.Set"
    },
    
    /**
     * element type
     * @member {all}
     * @instance
     * @memberof system_util_Set
     */
    type : null,
    
    /**
     * JS set to store value
     * @member {Set}
     * @private
     * @memberof system_util_Set
     */
    obj : null,
    
    /**
     * constructor
     * @memberof system_util_Set
     * @param  {string} type - element type
     */
    init : function(type){
        if (type == null) {
            throw "system.util.Set type can not be null!";
        }
        this.type = type;
        this.obj = new Set();
        return this;
    },
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof system_util_Set
     */
    clear : function () {
        this.obj.clear();
    },
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        if (this.validType(element) == false) {
            return false;
        }
        return this.obj.has(element);
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof system_util_Set
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.obj.size == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof system_util_Set
     * @return {number} collection size
     */
    size : function () {
        return this.obj.size;
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof system_util_Set
     * @return {Array} element array
     */
    toArray : function () {
        var ret = [];
        this.obj.forEach(function (index, value, set) {
            ret.push(value);
        });
        return ret;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof system_util_Set
     * @return {string} message string
     */
    toString : function () {
        var array = this.toArray();
        return this.classProp.name + JSON.stringify(array);
    },
    
    /** 
     * check element type is valid
     * 
     * @instance
     * @memberof system_util_Set
     * @param  element - element to check
     * @return {boolean} element type valid or not
     */
    validType : function (element) {
        return _validType(this.type, element);
    },
    
    /**
     * add element
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {all} element - element to add
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        return this.obj.add(element);
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof system_util_Set
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /**
     * remove element
     * 
     * @instance
     * @memberof system_util_Set
     * @param  {all} element - element to remove
     * @return {all} removed element
     */
    remove : function (element) {
        if (element == null) {
            return null;
        }
        if (this.validType(element) == false) {
            return null;
        }
        return this.obj["delete"](element);
    },
    
    isCollection : function (obj) {
    	var ret = obj instanceof com.yung.util.CollectionInterface;
        return ret;
    },
    
    getFirstElement :  function () {
        if (this.size() == 0) {
            return null;
        }
        var ret = null;
        try {
            this.obj.forEach(function (index, value, set) {
                ret = value;
                throw "done";
            });
        } catch (e) {
            if ("done" != (e + "")) {
                throw e;
            }
        }
        return ret;
    },
    
    clone : function () {
        var ret = new system.util.Set(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
    
});

/**
 * Tree set collection, use TreeMap as base
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_CollectionInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeSet = com_yung_util_CollectionInterface.extend({
    
    classProp : { 
        name : "com.yung.util.TreeSet"
    },

    /**
     * element type
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeSet
     */
    type : null,
    reverse : null,
    map : null,
    validType : function (element) {
        return _validType(this.type, element);
    },
    
    /**
     * constructor
     * @memberof com_yung_util_TreeSet
     * @param  {function} type - element function
     * @param  {boolean} reverse - reverse flag
     */
    init : function(type, reverse){
        if (type == null) {
            throw "com.yung.util.TreeSet type can not be null!";
        }
        if (type != 'string' && type != 'number') {
            throw "com.yung.util.TreeSet type can only be string or number!";
        }
        this.type = type;
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.map = new com.yung.util.TreeMap(type, type, reverse);
        return this;
    },
    
    /**
     * Adds the specified element to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param {validType} element - element to add
     * @instance
     * @return {boolean} success or not
     */
    add : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            throw "argument type is not " + this.type;
        }
        var val = null;
        if (this.type == 'string') {
            val = "";
        } else if (this.type == 'number') {
            val = 0;
        }
        return this.map.put(element, val);
    },
    
    /**
     * Adds the all elements of collection to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param  {com_yung_util_CollectionInterface} collection - collection to add
     * @instance
     * @return {boolean} success or not
     */
    addAll : function (collection) {
        if (collection == null) {
            throw "argument can not be empty";
        }
        if (!collection instanceof com_yung_util_CollectionInterface) {
            throw "collection is not com.yung.util.CollectionInterface";
        }
        var array = collection.toArray();
        for (var i = 0; i < array.length; i++) {
            this.add(array[i]);
        }
        return true;
    },
    
    /**
     * Removes the specified element to this set 
     * 
     * @memberof com_yung_util_TreeSet
     * @param {validType} element - element to remove
     * @instance
     * @return {boolean} success or not
     */
    remove : function (element) {
        if (element == null) {
            return false;
        }
        if (this.validType(element) == false) {
            return false;
        }
        return this.map.remove(element);
    },
    
    /** 
     * clear all elements
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     */
    clear : function () {
        this.map.clear();
    },
    
    /**
     * add elements by array
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @param  {Array} elementArray - element array
     * @return {boolean} success or not
     */
    addByArray : function (elementArray) {
        if (elementArray != null) {
            if (jQuery.isArray(elementArray)) {
                for (var i = 0; i < elementArray.length; i++) {
                    this.add(elementArray[i]);
                }
                return true;
            } else {
                throw "argument type is not array";
            }
        }
        return true;
    },
    
    /** 
     * check collection contains element 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @param  {validType} element - element to check if exist
     * @return {boolean} exist or not
     */
    contains : function (element) {
        return this.map.containsKey(element);
    },
    
    /** 
     * check collection is empty 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {boolean} empty or not
     */
    isEmpty : function () {
        if (this.map.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * get collection size 
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {number} collection size
     */
    size : function () {
        return this.map.size();
    },
    
    /** 
     * return element array
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {Array} element array
     */
    toArray : function () {
        return this.map.getKeyArray();
    },
    
    getFirstElement : function () {
        return this.map.firstKey();
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_TreeSet
     * @return {string} message string
     */
    toString : function () {
        var ret = "";
        var array = this.toArray();
        if (array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "[";
        }
        for (var i = 0; i < array.length; i++) {
            if (this.type == 'string') {
                ret = ret + "'" + array[i] + "', ";
            } else if (this.type == 'number') {
                ret = ret + array[i] + ", ";
            } else {
                if (i == 0) ret = ret + "\n";
                if (jQuery.isArray(array[i])) {
                    ret = ret + "    " + JSON.stringify(array[i]) + ",\n"; 
                } else if (typeof array[i].toString == "function") {
                    ret = ret + "    " + array[i].toString() + ",\n"; 
                } else {
                    ret = ret + "    " + JSON.stringify(array[i]) + ",\n"; 
                }
            }
        }
        if (ret.length > 0) {
            ret = ret.substring(0, ret.length - 2);
        }
        if (array.length > 0 && (this.type == 'string' || this.type == 'number')) {
            ret = ret + "]";
        }
        return this.classProp.name + ret;
    },
    
    clone : function () {
        var ret = new com.yung.util.TreeSet(this.type);
        var array = this.toArray();
        for (var i = 0; i < array.length; i++) {
            ret.add(array[i]);
        }
        return ret;
    }
    
});

var com_yung_util_Set = function (type, valueHash) {
    if (typeof type == "string") {
        type = type.toLowerCase();
    }
    if (type == 'string' || type == 'number') {
        if (typeof Set != 'undefined') {
            return new system_util_Set(type);
        }
        return new com.yung.util.BasicSet(type);
    }
    if (typeof type == 'function') {
        if (valueHash == true) {
            return new com.yung.util.ValueHashSet(type);
        } else {
            if ($Class.checkFunction(type, 'hashCode') == true) {
                return new com.yung.util.ObjectHashSet(type);
            }
        }
    }
    if (type == 'object') {
        if (valueHash == true) {
            return new com.yung.util.ValueHashSet(type);
        } else {
            if (typeof Set != 'undefined') {
                return new system_util_Set(type);
            }
            return new com.yung.util.RawObjectSet(type);
        }
    }
    throw "argument type not match any type";
}
$Y.register("com.yung.util.Set", com_yung_util_Set);

var com_yung_util_List = function (type) {
    if (typeof type == "string") {
        type = type.toLowerCase();
    }
    if (type == 'string' || type == 'number') {
        return new com.yung.util.BasicList(type);
    }
    if (type == 'object') {
        return new com.yung.util.RawObjectList(type);
    }
    if (typeof type == 'function') {
        return new com.yung.util.ObjectList(type);
    } else {
        throw "argument type not match any type";
    }
}
$Y.register("com.yung.util.List", com_yung_util_List);

jQuery( document ).ready(function() {
    if (typeof com_yung_util_BasicMap == 'undefined') {
        alert("yung-Collection.js requires yung-Map.js!");
    }
});