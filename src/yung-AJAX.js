/**
 * AJAX tool, only support data type json and post action
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {string}  classProp.type - AJAX action type
 * @property {string}  classProp.dataType - AJAX data type
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AjaxHttp = $Class.extend({
	
    classProp : { 
    	name : "com.yung.util.AjaxHttp",
    	type : 'POST',
    	dataType : 'json',
        loadGif : "data:image/png;base64,R0lGODlhSwBLAPcAAERERP7+/v39/fz8/Pn5+f////Dw8Pf39+bm5uvr6/b29tjY2H9/f5aWloKCgvv7+6CgoOrq6vHx8Y6Ojnx8fIWFheDg4Ofn59TU1OPj47GxsfPz85mZmaSkpLe3t9vb26ysrLW1tampqaGhoc3Nzby8vLq6urOzs6amptHR0YGBgX5+fpCQkI2NjcvLy5OTk3t7e+Hh4aenp9/f38XFxa6urpycnMLCwvr6+p6enoSEhIiIiO3t7d3d3cfHx4eHh/j4+MPDw/X19ZiYmOjo6M/Pz4qKiouLi7CwsMrKyra2tuLi4tbW1pSUlNnZ2eTk5MTExNra2u7u7sHBwb6+vu/v79PT05ubm97e3vT09Li4uMnJyfLy8uzs7NDQ0NLS0oaGhoyMjMzMzK+vr6KioqioqMbGxqqqqpGRkdzc3LS0tLm5uaOjo8jIyLu7u729vdfX13l5eZeXl3h4eJqamp+fn7Kysr+/v+Xl5c7OzpKSktXV1aWlpZ2dnenp6YODg62trYCAgI+Pj4mJiaurq319fcDAwJWVlXd3d3p6enZ2dkVFRUZGRkhISEtLS05OTk1NTXR0dHNzc29vb0pKSkdHR0xMTG5ubnJycnFxcXV1dUlJSVZWVlBQUFNTU1RUVGxsbFVVVWFhYWRkZHBwcG1tbWhoaGVlZWtra1JSUl9fX2BgYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDIxIDc5LjE1NDkxMSwgMjAxMy8xMC8yOS0xMTo0NzoxNiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmODFlMTVmMC1jY2FlLTRkNjEtODhjMy1jMGE1MGRjZmE1YWMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MTYzOThDRkVBODRGMTFFMzkyMzhFOEZGRjdFQ0E0OUUiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MTYzOThDRkRBODRGMTFFMzkyMzhFOEZGRjdFQ0E0OUUiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6ZjgxZTE1ZjAtY2NhZS00ZDYxLTg4YzMtYzBhNTBkY2ZhNWFjIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOmY4MWUxNWYwLWNjYWUtNGQ2MS04OGMzLWMwYTUwZGNmYTVhYyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAUEAEUAIf4jUmVzaXplZCBvbiBodHRwczovL2V6Z2lmLmNvbS9yZXNpemUALAAAAABLAEsAAAjYAIsIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qFGWcm66aRDG4RVPADqhKWmEQgWGZDYBAGCKYY6tYJuKDAN2yMJUYAHMWcgpLYBHIyuARaQQhtsXCiG43VpI5BGwSRV2ShtnIdS0cEfuiCOIoQ2tAEY5djt15gkbHBy+ePR2x9EiMD6LHk26tOnTqFOrXs26tevXsGPLnk27tu3buHPr3s27d5GAACH5BAUEAAEALBEAIQAqAAkAAAiCAAMIHEiwoMGDCAO0MFjjCoiEBD2gAQOR4JBOASCFGdgEgMcdFfs0EiiqYoArBXUEUANgIIAaEDEOxFQxVEFHAeq0FAhATsJLBVlARGnwkoadAQAAMjlQUkWZA3Eq9AigQsUmIwOoMtmg4EKBJ4aMYVqGThOmCiEFcKQCrVuIQAcGBAAh+QQFBAANACwQACEAKwAJAAAIjAAbCGxgiMnAgwgTKlTYJCGdVAAssVh4UEuTQRQRPmzQSdDAPgAGAsBIMcfAUxkF9kFI0hPCRhgopjqoKCUnhJYayAl5EICbhTUP6snIR6EiLJAQUkoh86CmlDMP5tTJswEANBnpDFyVssEQhB4FsnAEoBHWlBquyOkqUNCjBpB0IBQzZQ/buwiDCgwIACH5BAkEAA8ALBAAIQArAAkAAAiFAB8IJBSHj8CDCBMqXJjwisI5AB4AMOWD4UE3QwRZTNjnk0A9B+MkHLWxwyaBpjYKrJPwyIMxERECIGPRE8JEKhU+erBiYcqFDHIiHAOUUMyDACAIfYBzo02EOx9ISihqY52TD0guPQjyASBSEQGIuqNSzQMbSw8JfGQkoQ1TNLfKdYAwIAAh+QQJBABMACwQACEAKwAJAAAIjwCZCGwD5Y7AgwgTKlyYUI7CMJsAbGLAEGGJBmEqJrziCUAnNAcnABg58odGMhEBnNIosA/JkUeYJElJctOUiqleJmKp6iWAR0yU+Bx5giEMnyAryhgKAMYWmiM3vcH5EhHLji+BMgHj849GDilHsWQiB+nBQBE3ORhrxwaHsQLldPppEmESEzfh6kVIAWFAACH5BAkEABgALBAAIQArAAkAAAiMADEIxIBloMGDCBMqbIBwwiUAowQpNDiGg56JB6+kAtBpwkBBmwCIBHAF44uQAEoZwoghx0iRRwROeglAFcZOL1uw5EQTEoYMNEWWUAiF5g6MKIICmIMBFU1ON3Oy3PjSJ4ZBNJtgnIAS1BuWDWiyGKhnVM1DLDHYsUEnrcAmjwBAqoCwjdu7CBMZDAgAIfkECQQAEAAsEQAhACoACQAACJQAIQgcSLAgQR9FDCoceKQgmziiVMhYOPCMKAAA/jChOJBDJwCQGkLg8xFjIw0U7TTCiDERRwhyWGKsAIGCTABzKBq5CQAQx1A3KUEwdTMURaI3R1DswxPAJBg3FVEcxFMEx5IsLUFAshJjJUIUAUV9eehmC4FnVoRCVOPlGVUYYZB4CWGCJQCUAtGlSCPJ3oGlCAYEACH5BAkEAAcALBEAIQAqAAkAAAibAA8IHEiwoEA3LRTpYGGwoUA9BgM5iGPCocAzjgBoBADK4kAOqQA8mjDw0sZRFRu28bRx4xGPNloC2HHgh8w/DnPI1OgIjkVOMh8dANoSYkMdOzVycIgiqSQHMh04HJKUEhOLIVtaOuDD1EZRWhySyChTh8dDMhkeMKSoyR81HslsapmJhMcDhywBsGT2bkMtglAheuF3oCKCAQEAIfkECQQACAAsEQAgACoACwAACJgAEQgcSLAgQSYGEypcuFBQKQCQVNRgSLCJQTaHclAMBKBjx04dKHJIBaATi4E7PMJYeMWjS1AMW7o0goCPSwAhE/656TFnQk43ISHIcZNmQjA8O85RKCKpIjY32ShckRTAiIWebloSqMNjnIWHkppiOORmi4F0WgyhiOgmJRkUWTyCqIOiwiuqADSKhMSuQER+GY75EjhhQAAh+QQJBAAKACwRACEAKgAJAAAImAAVCBxIsKBAJSpGaWJhsKFADgbf7FjjcCCHTQAyArjkpqJAG54ApGIocAjGTRAdQqGkUWMgj31aAgijYArGjJsAOZwgEyeUipxkPlKgR6Ymh6V6ZlTikJDSQlRuAth0xmELpZu0VAwlVCCdk00qlpCqEYzHKzJfDNQCo4ZHBUbInnrz9lAnAI92vHV4hgEnU39+7lWwgmBAACH5BAkEABYALBEAIQAqAAkAAAiOAC0IHEiwoEBDFY7AgGCwoUAjDWE4JGhDFYCLABJNHKinE4BHLAZ2sBjKzkZTGDEO2fgiJYAWAldh9DRxiMuLozZ6TLlpSyCXQRw2uXkRgUM9RG9YsHgxVU2iqnS63KQ0BycAntRsPHXz0EYWLv8QPLFRIIurGB2UtfACEoBNYNY2XDNHEAOGci2o2TIwIAAh+QQJBAAHACwRACEAKwAJAAAIjwAPCBxIsODAOoIEaTDIUMMOTRP4GEzA0KApABgZoak4UBDGj00GQjHFyNRCjhQ+ZhzCUY3KjxAElvooiuOZlxhVcDyCE4COA1koqexRsUbPUxwT9RwlkybHED0ZcATT04HAIBdFaeF4QBHOIxyN4iRDMAPXgao+Mjp0VsdLI2c5NlnBwkbcA2wGmapAZ2BAACH5BAkEAA0ALBIAIAAqAAsAAAieABsIHEiwoMGDBsXYkDOkDcKHA93A6ANxYB9LADJuklPxYKBGGUWNgQgho8mMegRSIIWmIouTADrVQCgGEsyMjbSsMBkGYqebFBCOuGnyhSKTox5eIcoJYR2iGQsVMnnE580dCHNABWCjwQ5ULCqCgQnpDEIvjog2GtlxoCKQADiVgUiHaIW2Bc+gGtSWDlwAjcDgHXzwhhwHTewMDAgAIfkECQQACQAsEgAgACoACwAACJQAEwgcSLCgwYMGbagQRCEHwocE1TiEKJABgIsXM1E8yIHTxTkmIAbCSPKSwCuqGlAc0Yikqoc5SMrs0wfjC4ikZAKogxCTTow7RGEMBNGjzCsIG/y8WMgGRhYQC+nsgzDn0goJ+qg6RLFOS4yoHtZcqnLjQA6pLoKaAlHRT1FmDSrRYlaSTFE04uo12EFFoB9IBQYEACH5BAkEABkALBIAIAAqAAsAAAiWADMIHEiwoMGDBqG0+NFADcKHBC9AJDhhE4CLm8BMPDgi0yZOGiEKukjyogqBdOawmCiDU0kKD31YLIlxxJWZfSAaoQkgBEI0PEmiCUQSFcQjPOkgZBH04ik6M69AHMRTCcKRTSdkGKIpJ0QynUrCeHhnJk8+GwmeQQXgUciHLYL+SWuQCV0jZgFoosv34BgjoPScGRgQACH5BAkEADsALBIAIAAqAAsAAAiSAHcIHEiwoMGDB89k6oCwoUOHbE4BmKhqwkOELS7paPKww6aJIDcNEcghDouLo0ACOORQosqJn3bY+Aigj8MGL1M1JPNSJRoHIC85hNGzBUKiPSfKmDnRhsMmLzs1PJN04p8dQxTZfFhKZR2HopJ2InGR4JsXpyhweCiHpsorZeMehMAJZKe1cvMWbKJnUJ6BAQEAIfkECQQAMQAsEgAgACoACwAACJUAYwgcSLCgwYMH7dzgg7ChQ4dnCm0CACCTjIcIr6iCUeMhIVUUQ3IiI5BDnCYYI4WkBMghg5AwTcW4MhEABIcQYAIA1VBLTZ0ANAQKeckhKp2PuCCkATSkCJoUbTisozNOQyVNKYaIMURRDoyFQm4S4RBRU00YDeoJ86PMQw2cdFLSkLbuQSSTQqI6Y7evQQ1jlBAMCAAh+QQJBAAJACwSACAAKgALAAAIjgATCBxIsKDBgwjhIFzIsGGCH5wAcKLgEOGZPocqggLAkeOqgXIQvXCooxFHU3cYDurIskUCDiYB2GA4BRLLHwxVsexoKQGDjqAYeti5YuGTnSzJwORIh2GRmBz1MIyItGeCQ5GuOFwKYJQJhj+QAjBSseAIDhNIOBy1c5SHsnAN/ugEANKRNXHzFhxTMCAAIfkECQQADAAsEgAhACoACQAACIAAGQgcSLCgwYMG+awYBYMOwocDXRwqA3GgAwAYMUoaSCdRg4oM+nDC+KeijYwo9YTchLFPRVQom0C8iBLjHAYqMqKCaKbmzoemamLkxMAGSwAuIcLM+AJiTqGKBHJQVAekHU8kK9YRCiAMSINQjJD5mqhmoK9oH9JZwSlRi4EBAQAh+QQJBABGACwSACEAKQAJAAAIgQCNCBxIsKDBgwaR/JGzIgfChwNPeIA4cMgmABg3CRLIIU4TikZQnMIIg6KdixgzqrGBsg/FUikBjID4I2ZKFYFSXoLYxeYgiIpsYhx0BeUViqZi2oBYQSiAFUYOzVlKkY0qjJgoykCZchMgkAZBnAE7iOsml2DTHoQw6IiKMQIDAgAh+QQJBAAbACwSACEAKQAJAAAIgwA3CBxIsKDBgwaZ9NEzxAXChwNP2IE4kI4oABhVoRA4ZM4Eihv4jMI4B+QpjCg5TbmyCaMNiqZQAuAA8YVMlEcCoSwFkcfNChCH3MSYiE5LAFdgyhwCUdBQAA02DNH0kuIIVRgxUWSCVeanOyANgjgTNkxXAJ48hF2LMAkaB0amCAwIACH5BAkEAAsALBIAIQApAAkAAAh7ABcIHEiwoMGDBzWoIISwIUE1IRwShAGgIqY7AukkOiRxgYxTFSl0pFCxpKkFEDZVrCMRVUkAVxyeeVnShoOSkxxKobnDYQuaFU3ZUAmATsuXNhyeAApAzgIOiEZ0zLGqoqKOc2ie6ngQ0BmuCySVHNUGrNmDHS7FFBgQACH5BAkEADUALBIAIQApAAkAAAh9AGsIHEiwoMGDCEO0QciQ4Ak1DQnqeQRgk46BHOK8iFgDxSkAAGBw3AGyZKEadTaBvBIRVUkANiJSfLnJB4OSlxp2eQngR8MKPEFqyaESAJ2Ipl5yiNiJ56Y7NYYoysGRjSqQkTi24AmG48ExIrzWGKRy0w6xaBFOMeRhYEAAIfkECQQAHwAsEgAhACkACQAACHkAPwgcSLCgwYMIiSBcWPDECYYED00CgOrQwCFzXkD8wGcUAAAUNjbZ9PEjmg9XSAKoA9FUSQAQIGZ6CUDUh0AlSzFMQPMHwyw0P/qgo9JGy5dXIE58qUqgHE0xIfZR9RHTRhYqPxrdaBCECK4fjrhUlRSsWYR7CAYEACH5BAUoADcALBIAIQApAAkAAAh4AG8IHEiwoMGDCBMqJHhCzcKBHSalijNiIIc4eh6iOAUAAAyNmzoC2HTlBoeQAGwsRCUSQJ2FmFoCkHQjkMhLCiXI/LGQk0wABq6gLKnQVMscMGViEjhEUZ+HbFR1jPSQDsqRDR4eBHFG6w0UiDZFGuK1bEIhBAMCADs="
    },
    
    /**
     * action url
     * @member {string}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    actionUrl : null,
    
    /**
     * form id for sending parameter
     * @member {string}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    formId : null,
    
    /**
     * Authorization header
     * @member {string}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    authorization : null,
    
    /**
     * Callback for AJAX reponse
     *
     * @callback AJAXCallback
     * @param {object} response - response data object
     * @param {com_yung_util_AjaxHttp} self - this com_yung_util_AjaxHttp
     * @param {object} caller - caller which pass by send method
     */
    /**
     * AJAX callback function
     * @member {AJAXCallback}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    callback : null,
    
    /**
     * show loading image, default true
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    loading : true,
    
    /**
     * show error message when AJAX error occurs, default true
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    showError : true,
    
    /**
     * show AJAX loading function
     * @member {function}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    ajaxLoadingShow : null,
    
    /**
     * hide AJAX loading function
     * @member {function}
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    ajaxLoadingHide : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_AjaxHttp
     * @param  {string} actionUrl - action url
     * @param  {string} formId - from id
     * @param  {AJAXCallback} callback - ajax callback
     * @param  {function} ajaxLoadingShow - function to show loading image
     * @param  {function} ajaxLoadingHide - function to hide loading image
     */
    init: function(actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide){
        this.actionUrl = actionUrl;
        if (formId) this.formId = formId;
        if (callback) this.callback = callback;
        this.loading = true;
        this.showError = true;
        if (typeof ajaxLoadingShow == 'function' && typeof ajaxLoadingHide == 'function') {
            // customized loading and function
            this.ajaxLoadingShow = ajaxLoadingShow;
            this.ajaxLoadingHide = ajaxLoadingHide;
        } else {
            // use standard loading
            com_yung_util_AjaxHttp.createLoadingDiv();
            var browser = com_yung_util_getbrowser();
            if (browser == 'msie') {
                com_yung_util_AjaxHttp.setImage(this.classProp.loadGif, this);
            }
            this.ajaxLoadingShow = function () {
            	var browser = com_yung_util_getbrowser();
            	var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
                if (windowHeight < jQuery(document).height()) {
                    // use maximum height
                    windowHeight = jQuery(document).height();
                }
                if (browser == 'msie') {
            		windowHeight = windowHeight - 3;
            	}
                var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
                if (windowWidth < jQuery(document).width()) {
                    // use maximum width
                    if (browser != 'msie') {
                        windowWidth = jQuery(document).width();
                    }
                }
                if (browser == 'msie') {
                	windowWidth = windowWidth - 3;
            	}
                if (jQuery("#loadingCSS-imgDiv")[0] != null) {
                    jQuery("#loadingCSS").css("width", windowWidth + "px");
                    jQuery("#loadingCSS").css("height", windowHeight + "px");
                    jQuery("#loadingCSS").css("left", "0px");
                } else {
                    jQuery("#yung-backgroundDiv").css("width", windowWidth + "px");
                    jQuery("#yung-backgroundDiv").css("height", windowHeight + "px");
                }
                jQuery("#loadingCSS").css("display", "block");
            };
            this.ajaxLoadingHide = function () {
                jQuery("#loadingCSS").css("display", "none");
                var blinkTxt = jQuery("#text-blink")[0];
                if (blinkTxt != null) {
                    clearInterval(yung_global_var["_global_text_blink"]);
                }
            };
        }
        return this;
    },
    
    /** 
     * before send
     *
     * @private
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    beforeSend : function() {
        if (this.loading) {
            if (yung_global_var["_global_ajaxTimeout"] == null) {
                yung_global_var["_global_ajaxTimeout"] = setTimeout(this.ajaxLoadingShow, 500);
            } else {
                clearTimeout(yung_global_var["_global_ajaxTimeout"]);
                delete yung_global_var["_global_ajaxTimeout"];
                yung_global_var["_global_ajaxTimeout"] = setTimeout(this.ajaxLoadingShow, 500);
            }
        }
    },
    
    /** 
     * after send
     *
     * @private
     * @instance
     * @memberof com_yung_util_AjaxHttp
     */
    sendComplete : function () {
        clearTimeout(yung_global_var["_global_ajaxTimeout"]);
        delete yung_global_var["_global_ajaxTimeout"];
        this.ajaxLoadingHide();
    },
    
    fixImageWhenScroll : function () {
        var imgDiv = jQuery("#loadingCSS-imgDiv");
        var imgWidth = imgDiv.width();
        var imgHeight = imgDiv.height();
        var position = new com.yung.util.Position(imgDiv);
        var pos = position.getCenterPosition(imgWidth, imgHeight);
        imgDiv.css("position", "absolute");
        imgDiv.css("top", pos.top + "px");
        imgDiv.css("left", pos.left + "px");
    },
    
    /** 
     * set show loading flag
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {boolean} loading - show loading flag
     */
    setLoading : function(loading){
        this.loading = loading;
    },
    
    /** 
     * set show error flag
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {boolean} loading - show error flag
     */
    setShowError : function(showErr){
        this.showError = showErr;
    },
    
    /** 
     * set authorization header
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {string} authorization - authorization header
     */
    setAuthorization : function(authorization){
        this.authorization = authorization;
    },
    
    /** 
     * send request
     *
     * @instance
     * @memberof com_yung_util_AjaxHttp
     * @param  {object} caller - caller for callback use
     */
    send : function(caller){
        if (this.actionUrl == null) {
            alert("please define actionUrl!");
            return;
        }
        // show load.gif if more than half second
        this.beforeSend();
        
        var params = "";
        // param serialize
        if (this.formId != null) {
            if (jQuery('#' + this.formId)[0] != null) {
                params = jQuery('#' + this.formId).serialize();
            } else if (jQuery('form[name="' + this.formId + '"]')[0] != null) {
                params = jQuery('form[name="' + this.formId + '"]').serialize();
            }
        }
        //alert("params ok");
        
        var callback = this.callback;
        var self = this;
        
        jQuery.ajax({
            type: self.classProp.type,
            beforeSend: function (xhr) {
                if (self.authorization != null && self.authorization != '') {
                    xhr.setRequestHeader('Authorization', self.authorization);
                }
            },
            dataType: self.classProp.dataType,
            error: function(x, status, error) {
                self.sendComplete();
                if (self.showError) alert("An ajax error occurred: " + status + ", Error: " + error + ", actionUrl: " + self.actionUrl);
            },
            url: this.actionUrl,
            data: params,
            jsonpCallback: callback,
            success:function(result){
                self.sendComplete();
                if (typeof result == 'string') {
                    result = JSON.parse(result);
                }
                this.jsonpCallback(result, self, caller);
            }
        });
    }
});
com_yung_util_AjaxHttp.createLoadingDiv = function () {
    var loading = jQuery("#loadingCSS");
    if (loading[0] == null) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight < jQuery(document).height()) {
            // use maximum height
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth < jQuery(document).width()) {
            // use maximum width
            windowWidth = jQuery(document).width();
        }
        var template = new com.yung.util.BasicTemplate();
        var data = {};
        data["windowHeight"] = windowHeight;
        data["windowWidth"] = windowWidth;
        template.add('<div id="loadingCSS" style="display:none" tabindex="1" >');
        template.add('    <div id="yung-backgroundDiv" style="width: {{windowWidth}}px; height: {{windowHeight}}px; position: absolute; text-align: left: 0px; top: 0px; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);" >');
        template.add('        <table class="loading_gif_center" >');
        template.add('            <tr><td>');
        template.add('                <div id="loadingCSS-content" class="loader">Loading...</div>');
        template.add('            </td></tr>');
        template.add('        </table>');
        template.add('    </div>');
        template.add('</div>');
        jQuery("body").append(template.toHtml(data));
    }
};
com_yung_util_AjaxHttp.setLoadingText = function (text, self) {
    if (jQuery("#loadingCSS-imgDiv")[0] == null) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        jQuery("#loadingCSS").attr("style", "width: " + windowWidth + "px; height: " + windowHeight + "px; display: none; position: absolute; text-align: left: 0px; top: 0px; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);");
        var template = new com.yung.util.BasicTemplate();
        var data = {};
        data["text"] = text;
        template.add('<table id="text-blink-table" style="position: absolute; width: 100%;" align="center" >');
        template.add('    <tr><td align="center" >');
        template.add('        <div id="loadingCSS-imgDiv" style="">');
        template.add('            <span id="text-blink">{{text}}</span>');
        template.add('        </div>');
        template.add('    </td></tr>');
        template.add('</table>');
        jQuery("#loadingCSS").html(template.toHtml(data));
        if (com_yung_util_getbrowser() == 'msie') {
            self.fixImageWhenScroll();
            jQuery(window).scrollEnd(self.fixImageWhenScroll, 150);
        }
    }
};
com_yung_util_AjaxHttp.setImage = function (imgSrc, self) {
    if (jQuery("#loadingCSS-imgDiv")[0] == null) {
        var windowHeight = (window.innerHeight || document.documentElement.scrollHeight || 0);
        if (windowHeight > jQuery(document).height()) {
            windowHeight = jQuery(document).height();
        }
        var windowWidth = (window.innerWidth || document.documentElement.scrollWidth || 0);
        if (windowWidth > jQuery(document).width()) {
            windowWidth = jQuery(document).width();
        }
        jQuery("#loadingCSS").attr("style", "width: " + windowWidth + "px; height: " + windowHeight + "px; display: none; position: absolute; text-align: left: 0px; top: 0px; vertical-align: middle; z-index: 15000; -moz-opacity: .85; opacity: .85; filter: alpha(opacity=85); background: none 0px 0px repeat scroll rgb(255, 255, 255);");
        var template = new com.yung.util.BasicTemplate();
        var data = {};
        data["imgSrc"] = imgSrc;
        template.add('<table class="loading_gif_center" style="width: 100%; height: 100%;" >');
        template.add('    <tr><td align="center" >');
        template.add('        <div id="loadingCSS-imgDiv" style="">');
        template.add('            <img id="pageLoading" src="{{imgSrc}}" />');
        template.add('        </div>');
        template.add('    </td></tr>');
        template.add('</table>');
        jQuery("#loadingCSS").html(template.toHtml(data));
        if (com_yung_util_getbrowser() == 'msie') {
            self.fixImageWhenScroll();
            jQuery(window).scrollEnd(self.fixImageWhenScroll, 150);
        }
    }
};

/**
 * AJAX upload tool
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_AjaxHttp
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_AjaxUpload = com_yung_util_AjaxHttp.extend({
    
	classProp : { 
		name : "com.yung.util.AjaxUpload"
	},
	
	/**
	 * constructor
     * @memberof com_yung_util_AjaxUpload
     * @param  {string} actionUrl - action url
     * @param  {string} formId - from id
     * @param  {AJAXCallback} callback - ajax callback
     * @param  {function} ajaxLoadingShow - function to show loading image
     * @param  {function} ajaxLoadingHide - function to hide loading image
     */
    init : function (actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide) {
        this._super(actionUrl, formId, callback, ajaxLoadingShow, ajaxLoadingHide);
        return this;
    },
    
    /** 
     * send request
     *
     * @instance
     * @memberof com_yung_util_AjaxUpload
     * @param  {object} caller - caller for callback use
     */
    send : function (caller) {
        if (this.actionUrl == null) {
            alert("please define action!");
            return;
        }
        // show load.gif if more than half second
        this.beforeSend();
        
        var formData = new FormData();
        jQuery("#" + this.formId + " :input").each(function(key, input){
            var name = jQuery(this).attr("name");
            var type = jQuery(this).attr("type");
            if ("file" == type) {
                var files = input.files;
                var i = 0;
                var file = files[i];
                while (file != null) {
                    formData.append(name, file);
                    i++;
                    file = files[i];
                }
            } else if ("button" != type && "submit" != type && name != null && name != '') {
                formData.append(name, input.value);
            }
        });
        
        var callback = this.callback;
        var self = this;
        
        jQuery.ajax({
            type:'POST',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            error: function(x, status, error) {
                self.sendComplete();
                if (self.showError) alert("An ajax error occurred: " + status + ", Error: " + error + ", actionUrl: " + self.actionUrl);
            },
            url: this.actionUrl,
            jsonpCallback: callback,
            success:function(result){
                self.sendComplete();
                if (typeof result == 'string') {
                    result = JSON.parse(result);
                }
                this.jsonpCallback(result, self, caller);
            }
        });
    }
});

jQuery( document ).ready(function() {
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-AJAX.js requires yung-BasicTemplate.js!");
    }
    if (typeof com_yung_util_Position == 'undefined') {
        alert("yung-AJAX.js requires yung-Position.js!");
    }
});    