/**
 * Convenient validate tool to check string
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Validator {
    
    classProp : { 
        name : "com.yung.util.Validator"
    }
    
    /** 
     * to check input string if it is valid email
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    static email (input: string): boolean
    
    /** 
     * to check input string if it is number
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    static digit (input: string): boolean
    
    /** 
     * to check input string if it is an account type
     * account type only allow digit[0-9] and letter[A-Z, a-z],
     * not allow special symbol, like '@','$'
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    static account (input: string): boolean
    
    /** 
     * to check input string if it is letter[A-Z, a-z]
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    static letter (input: string): boolean
    
    /** 
     * to check input string if it is letter[A-Z, a-z]
     * or a special symbol in an array
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @param {string} specialChars - special character array
     * @return {boolean} valid or not
     */
    static letterExtchar (input: string, specialChars: string): boolean
    
    /** 
     * to check input string if it is ascii code
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @return {boolean} valid or not
     */
    static ascii (input: string): boolean
    
    /** 
     * to check input string if it is account type[0-9, A-Z, a-z]
     * or a special symbol in an array
     * 
     * @memberof com_yung_util_Validator
     * @param {string} input - string to check
     * @param {string} specialChars - special character array
     * @return {boolean} valid or not
     */
    static accountExtchar (input: string, specialChars: string): boolean
}