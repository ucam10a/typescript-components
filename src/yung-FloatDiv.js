/**
 * Float div element, usually used for display message[simple HTML], like tooltip or menu. 
 * Note: float div will not create mask to block screen. 
 * if mask is required, use com_yung_util_Popup
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_FloatDiv = $Class.extend({

    classProp : { 
        name : "com.yung.util.FloatDiv" 
    },
    
    hashId : '',
    
    /**
     * float div id
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    divId : null,
    
    /**
     * div overflow
     * @member {string}
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    overflow : '',
    
    /**
     * constructor
     * @memberof com_yung_util_FloatDiv
     * @param  {string} divId - float div id
     * @param  {boolean} onblurClose - a flag to close float div when onblur
     * @param  {string} overflow - float div overflow
     */
    init : function (divId, onblurClose, overflow) {
        $Class.validate(this.classProp.name, divId);
        this.divId = divId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + divId);
        if (overflow != null) {
            this.overflow = overflow;
        }
        this.createFloatDiv();
        if (onblurClose == true) {
            this.onblurClose();
        }
        return this;
    },
    
    /** 
     * close float div when onblur
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    onblurClose : function () {
        var element = jQuery("body");
        var event = "click";
        var self = this;
        element.bind(event, function() {
            self.closeFloatDiv();
        });
        this.showCloseButton(false);
    },
    
    /** 
     * show close button in div
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {boolean} show - show or not
     */
    showCloseButton : function (show) {
        if (show == true) {
            jQuery("#yung-FloatDiv-closeBtn-" + this.divId).css('display', '');
        } else {
            jQuery("#yung-FloatDiv-closeBtn-" + this.divId).css('display', 'none');
        }
    },
    
    /** 
     * create div content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    createFloatDiv : function () {
        var floatDiv = jQuery("#" + this.divId)[0];
        if (floatDiv == null) {
            var obj = {};
            obj["divId"] = this.divId;
            obj["overflow"] = this.overflow;
            var template = new com.yung.util.BasicTemplate(obj);
            template.add('<div id="yung-FloatDiv-{{divId}}" class="float-border" style="position: absolute; left: 0px; top: 0px; margin: 0px; display: none; background: white; box-shadow: 5px 5px 5px #d7d7d7; width:300px; z-index: 5000; {{overflow}}">');
            template.add('    <div id="yung-FloatDiv-container-{{divId}}" style="display: block; width: 100%; height: 100%;">');
            template.add('        <div id="yung-FloatDiv-content-{{divId}}" style="position: absolute; margin: 0px; height: 100%; width: 100%;" ></div>');
            template.add('        <div id="yung-FloatDiv-closeBtn-{{divId}}" style="position: absolute; right: 2px; top: 0px; cursor: pointer;"><span style="color: red;" title="close" onclick="fireInstanceMethod(\'' + this.hashId + '\', \'showFloatDiv\', false);" style="cursor: pointer;" >&#10006;</span></div>');
            template.add('    </div>');
            template.add('</div>');
            var html = template.toHtml();
            jQuery("body").append(html);
        } else {
            throw "div id: " + this.divId + " element exist, please use another one.";
        }
    },
    
    /** 
     * set html content for float div
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} html - HTML code 
     */
    setFloatContent : function (html) {
        jQuery("#yung-FloatDiv-content-" + this.divId).html(html);
    },
    
    /** 
     * show float content
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     */
    showFloatContent : function () {
        jQuery("#yung-FloatDiv-container-" + this.divId).css("display", "block");
    },
    
    /** 
     * show float div
     * 
     * @private
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {boolean} show - show flag
     * @param  {com_yung_util_FloatDiv} self - com_yung_util_FloatDiv itself, usually for callback use
     */
    showFloatDiv : function (show, self) {
        var divId = this.divId;
        if (self != null) {
            divId = self.divId;
        }
        if (show == false) {
            var display = jQuery("#yung-FloatDiv-" + divId).css("display");
            if (display != 'none') {
                jQuery("#yung-FloatDiv-" + divId).css("display", "none");
            }
            return; 
        }
        var display = jQuery("#yung-FloatDiv-" + divId).css("display");
        if (display != 'block') {
            jQuery("#yung-FloatDiv-" + divId).css("display", "block");
        }
    },
    
    /**
     * Callback when close float div 
     *
     * @callback floatDivCloseCallback
     */
    /** 
     * close and hide div
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {floatDivCloseCallback} callback - callback when close float div 
     */
    closeFloatDiv : function (callback) {
        this.showFloatDiv(false);
        if (typeof callback === "function") {
            callback();
        }   
    },
    
    /** 
     * open float div, width and height are optional.
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @param  {string} html - HTML code for float div
     * @param  {object} pos - position object, include top and left number
     * @param  {number} width - float div width
     * @param  {number} height - float div height
     * @param  {function} callback - callback
     */
    openFloatDiv : function (html, pos, width, height, callback) {
        if (width == null) {
            width = 500;
        }
        if (height == null) {
            height = 300;
        }
        if (pos == null) {
            throw "please specify position!";
        }
        var windowWidth = jQuery(document).width();
        if (pos.left < 0) pos.left = 0; 
        if ((pos.left + width) > windowWidth) pos.left = windowWidth - width;
        var top = pos.top + "px";
        var left = pos.left + "px";
        jQuery("#yung-FloatDiv-" + this.divId).css("width", width);
        jQuery("#yung-FloatDiv-" + this.divId).css("height", height);
        jQuery("#yung-FloatDiv-" + this.divId).css("left", left);
        jQuery("#yung-FloatDiv-" + this.divId).css("top", top);
        jQuery("#yung-FloatDiv-content-" + this.divId).html(html);
        // use setTimeout to prevent body click event conflict
        var self = this;
        setTimeout(function(){ 
            self.showFloatDiv(true, self);
            if (typeof callback == 'function') {
                callback();
            }
        }, 300);
    },
    
    /**
     * return if div display
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @return {boolean} display or not
     */
    isDisplay : function () {
        var display = jQuery("#yung-FloatDiv-" + this.divId).css("display");
        if (display == 'none') {
            return false;
        } else {
            return true;
        }
    },
    
    /**
     * return float div element
     * 
     * @instance
     * @memberof com_yung_util_FloatDiv
     * @return {HTMLElement} float div element
     */
    getFloatDivElement : function () {
        return jQuery("#yung-FloatDiv-" + this.divId);
    }
});

/** 
 * get com_yung_util_FloatDiv global instance,
 * 
 * @param  {string} divId - div id
 * @param  {boolean} onblurClose - a flag to close float div when onblur
 * @param  {string} overflow - float div overflow
 * @return  {com_yung_util_FloatDiv} com_yung_util_FloatDiv instance
 */
com_yung_util_FloatDiv.instance = function(divId, onblurClose, overflow) {
    return $Class.getInstance("com.yung.util.FloatDiv", divId, onblurClose, overflow);
}

jQuery( document ).ready(function() {
    if (typeof com_yung_util_BasicTemplate == 'undefined') {
        alert("yung-FloatDiv.js requires yung-BasicTemplate.js!");
    }
});