import { $Class } from "./yung-JS-extend";

/**
 * Convenient debug tool to print message on browser console
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {string}  LINE_SEPARATOR - line separator
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_DebugPrinter extends $Class {
	
	classProp : { 
        name : "com.yung.util.DebugPrinter",
        unimplemented : [] 
    }
	
    /**
     * maximum indent threshold
     * @member {number}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	MAX_INDENT : 40
    
	LINE_SEPARATOR : "\n"
    
	/**
     * flag to show out of buffer
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	showOutOfBuffer : boolean
    
	/**
     * flag to display null value
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_DebugPrinter
     */
	showNull : boolean
    
	/**
	 * constructor
     * @memberof com_yung_util_DebugPrinter
     */
    constructor  ()
    
    /** 
     * to check input if it is allowable
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {Object} input - Object for check
     * @return {Boolean} allowable flag
     */
    allowType (input: object): boolean 
    
    /**
     * to check input if it is printable
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} input - object type
     * @param {Object} inputObj - Object for check
     * @return {Boolean} printable flag
     */
    printable (input: string, inputObj: object): boolean 
    
    /**
     * print object to buffer
     * 
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} bud - String buffer to hold message
     * @param {String} indent - String for indent
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    printObjectParam (bud: string, indent: string, objName: string, obj: object): string 
    
    /**
     * print Array to buffer
     *
     * @private
     * @memberof com_yung_util_DebugPrinter
     * @param {String} bud - String buffer to hold message
     * @param {String} indent - String for indent
     * @param {String} objName - array name
     * @param {Array} array - Array to print
     */
    printArray (bud: string, indent: string, objName: string, array: Array<object>): string 
    
    /**
     * print object to console
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    printObject (objName: string, obj: object): void 
    
    /**
     * print object to alert box
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} objName - object name
     * @param {Object} obj - Object to print
     */
    alertObject (objName: string, obj: object): void 
    
    /**
     * print message to console
     *
     * @instance
     * @memberof com_yung_util_DebugPrinter
     * @param {String} message - message to print
     */
    printMessage (message: string): void 
}