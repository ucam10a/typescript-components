/**
 * String utility, require {@link com_yung_util_Validator}
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_StringUtils = {
    
	classProp : { 
		name : "com.yung.util.StringUtils"
	},
	
	/** 
     * check string is empty or null
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @return {boolean} true or false
     */
    isBlank : function (str) {
        if (str === null || str === '') {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * Returns string, with leading and trailing whitespace omitted.
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - target string
     * @return {string} result string
     */
    trim : function (str) {
        return str.replace(/^\s*/, "").replace(/\s*$/, "");
    },
    
    /** 
     * check string if it start with pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    startsWith : function (str, pattern) {
        if (str.indexOf(pattern) === 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check string if it end with pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    endsWith : function (str, pattern) {
        var idx = str.indexOf(pattern);
        if (idx === (str.length - pattern.length)) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check string if it contains pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @param {string} pattern - string pattern
     * @return {boolean} true or false
     */
    contains : function (str, pattern) {
        if (str.indexOf(pattern) >= 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check string and replace string pattern to specific string
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} targetStr - target string
     * @param {string} strFind - string pattern
     * @param {strReplace} strFind - string to replace pattern
     * @return {string} result string
     */
    replaceAll : function (targetStr, strFind, strReplace) {
        return _replaceAll(targetStr, strFind, strReplace);
    },
    
    /** 
     * check string and remove string pattern
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} targetStr - target string
     * @param {string} pattern - string pattern
     * @return {string} result string
     */
    remove : function (str, pattern) {
        return this.replaceAll(str, pattern, "");
    },
    
    /** 
     * if string is null then return empty string
     * otherwise return itself
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - check string
     * @return {string} result string
     */
    defaultString : function (str) {
        if (str == null) {
            return "";
        } else {
            return str;
        }
    },
    
    /** 
     * convert string to number
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to convert
     * @return {number} result number
     */
    toNumber : function (str) {
        return str * 1.0;
    },
    
    /** 
     * check string if it can convert to number
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} str - string to check
     * @return {boolean} true or false
     */
    isNumeric : function (str) {
        return jQuery.isNumeric(str);
    },
    
    /** 
     * parse input url string, then convert to object map
     * , parameter key and parameter value, value will decodeURIComponent.
     * Note. if url is null then use window location
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} url - url string
     * @return {object} object map of parse result
     */
    parseURL : function (url) {
        var map = this.parseURINotDecode(url);
        for (var key in map) {
            var value = map[key];
            try {
                value = decodeURIComponent(value);
            } catch (e) {
                // ignore
            }
            map[key] = value;
        }
        return map;
    },
    
    /** 
     * parse input url string, then convert to object map
     * , parameter key and parameter value, value will not decodeURIComponent.
     * Note. if url is null then use window location
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} url - url string
     * @return {object} object map of parse result
     */
    parseURINotDecode : function (url) {
        var map = {};
        if (url == null) {
            url = window.location.search.substring(1);
        }
        var idx = url.indexOf("?");
        if (idx > 0) {
            url = url.substring(idx + 1, url.length);
        }
        var sURLVariables = url.split('&');
        var sParameterName;
        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[1] === undefined) {
                continue;
            } else {
                map[sParameterName[0]] = sParameterName[1];
            }
        }
        return map;
    },
    
    /** 
     * parse object map, then revert to string url, like '&a=1&b=2'
     * 
     * @memberof com_yung_util_StringUtils
     * @param {object} map - object map
     * @return {string} url string
     */
    mapToURL : function (map) {
        if (map == null) {
            return "";
        }
        var url = "";
        for (key in map) {
            var value = map[key];
            if (value != null) {
                if (url == '') {
                    url = key + "=" + value;
                } else {
                    url = url + "&" + key + "=" + value;
                }
            }
        }
        return url;
    },
    
    /** 
     * calculate string length, for those special string character(non ascii)
     * will be consider as 3 length(default), or specific length
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} input - target sting
     * @param {number} nonASCIILength - the length of special string character(non ascii)
     * @return {number} total length
     */
    calcLength : function (input, nonASCIILength) {
        if (nonASCIILength == null) {
            nonASCIILength = 3;
        } else {
            nonASCIILength = parseInt(nonASCIILength);
        }
        if (input != null && input != '') {
            var cnt = 0;
            for (var i = 0; i < input.length; i++) {
                var ch = input.charAt(i) + '';
                if (com.yung.util.Validator.ascii(ch)) {
                    cnt = cnt + 1;
                } else {
                    cnt = cnt + nonASCIILength;
                }
            }
            return cnt;
        } else {
            return -1;
        }
    },
    
    /** 
     * substring, for those special string character(non ascii)
     * will be consider as 3 length(default), or specific length
     * 
     * @memberof com_yung_util_StringUtils
     * @param {string} input - target sting
     * @param {number} length - final total length
     * @param {number} nonASCIILength - the length of special string character(non ascii)
     * @return {number} total length
     */
    cutLength : function (input, length, nonASCIILength) {
        if (input == null || input == '') {
            return input;
        }
        if (nonASCIILength == null) {
            nonASCIILength = 3;
        } else {
            nonASCIILength = parseInt(nonASCIILength);
        }
        var count = 0;
        var sb = '';
        for (var i = 0; i < input.length; i++) {
            var c = input.charAt(i) + "";
            if (com.yung.util.Validator.ascii(c)) {
                count = count + 1;
            } else {
                count = count + nonASCIILength;
            }
            if (count > length) {
                break;
            }
            sb = sb + c;
        }
        return sb;
    }
};
$Y.reg(com_yung_util_StringUtils);
jQuery( document ).ready(function() {
    if (typeof com_yung_util_Validator == 'undefined') {
        alert("yung-StringUtils.js requires yung-Validator.js!");
    }
});