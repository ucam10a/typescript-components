import { $Class } from "./yung-JS-extend";

/**
 * An easy tool to get position of element
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
export declare class com_yung_util_Position extends $Class {
	
    classProp : { 
        name : "com.yung.util.Position",
        unimplemented : []
    }
    
    /**
     * HTML element to get position
     * @member {HTMLElement}
     * @instance
     * @memberof com_yung_util_Position
     */
    element : JQuery 
    
    /**
	 * constructor
     * @memberof com_yung_util_Position
     * @param  {HTMLElement} element - HTML element to get position
     */
    constructor (element: any )
    
    /** 
     * check current window if in iframe
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {boolean} in iframe or not
     */
    inIframe (): boolean
    
    getParentBaseTop (): number

    getParentBaseLeft () : number
    
    /** 
     * get document element top position, use by scroll bar occurs
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {number} document element top position
     */
    getBaseTop (): number
    
    /** 
     * get document element left position, use by scroll bar occurs
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {number} document element left position
     */
    getBaseLeft (): number
    
    /** 
     * get bounding client rect
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {JQuery} base - base element [optional]
     * @return  {object} bounding client rect
     */
    getBoundingClientRect (base?: JQuery ): object

    /** 
     * get specified element bottom left position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {JQuery } base - base HTMLElement [optional]
     * @return  {object} specified element bottom left position
     */
    getBotLeftPosition (offsetX?: number, offsetY?: number, base?: JQuery ): object
    
    /** 
     * get specified element top left position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {JQuery} base - base HTMLElement [optional]
     * @return  {object} specified element top left position
     */
    getTopLeftPosition (offsetX?: number, offsetY?: number, base?: JQuery): object
    
    /** 
     * get specified element top right position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {JQuery} base - base HTMLElement [optional]
     * @return  {object} specified element top right position
     */
    getTopRightPosition (offsetX: number, offsetY: number, base?: JQuery): object
    
    /** 
     * get specified element bottom right position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} offsetX - offset left [default 0]
     * @param  {number} offsetY - offset top [default 0]
     * @param  {JQuery} base - base HTMLElement [optional]
     * @return  {object} specified element bottom right position
     */
    getBotRightPosition (offsetX?: number, offsetY?: number, base?: JQuery): object
    
    /** 
     * get center position of specified width and height
     * 
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @return  {object} specified element bottom left position
     */
    getCenterPosition (width?: number, height?: number): object
    
    /** 
     * get center position of specified width, height, windowWidth and windowHeight
     * 
     * @private
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @param  {number} windowWidth - window width
     * @param  {number} windowHeight - window height
     * @return  {object} specified element bottom left position
     */
    getCenterPositionWithWinSize (width: number, height: number, windowWidth: number, windowHeight: number): object
    
    /** 
     * get window screen size
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @return  {object} specified element width and height
     */
    getScrennSize (): object
    
    /** 
     * get window screen center position
     * 
     * @instance
     * @memberof com_yung_util_Position
     * @param  {number} width - width [default 0]
     * @param  {number} height - height [default 0]
     * @return  {object} specified element bottom left position
     */
    getScreenCenterPosition (width: number, height: number): object
    
    /** 
     * Returns browser scroll bar width
     * 
     * @memberof com_yung_util_Position
     * @return {number} scroll bar width
     */
    static getScrollbarWidth (): number

    /** 
     * Returns browser scroll bar height
     * 
     * @memberof com_yung_util_Position
     * @return {number} scroll bar height
     */
    static getScrollbarHeight (): number

}
