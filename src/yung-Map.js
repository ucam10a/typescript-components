/**
 * Map interface. Define implement methods.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @property {Array}  classProp.unimplemented - unimplemented method
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_MapInterface = $Class.extend({

    classProp : { 
		name : "com.yung.util.MapInterface",
		unimplemented : ['entryArray', 'size', 'getKeyArray', 'get', 'put', 'remove', 'containsKey']
    }
    
});

/**
 * Map entry
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_Entry = $Class.extend({
    
	classProp : { 
		name : "com.yung.util.Entry"
	},
    
	/**
     * key
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_Entry
     */
	key : null,
    
	/**
     * value
     * @member {string | number | object}
     * @instance
     * @memberof com_yung_util_Entry
     */
	value : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_Entry
     * @param  {string | number} key - key
     * @param  {string | number | boolean | object} value - value
     */
	init : function(key, value) {
        this.key = key;
        this.value = value;
    },
    
    /** 
     * get key
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number} key
     */
    getKey : function () {
        return this.key;
    },
    
    /** 
     * get value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @return  {string | number | bollean | object} value
     */
    getValue : function () {
        return this.value;
    },
    
    /** 
     * set value
     * 
     * @instance
     * @memberof com_yung_util_Entry
     * @param  {string | number | bollean | object} value - value
     */
    setValue : function (value) {
        this.value = value;
    }
});    

/**
 * Basic Map, only allow key type string, number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_BasicMap = com_yung_util_MapInterface.extend({
	
    classProp : { 
    	name : "com.yung.util.BasicMap"
    },
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    keyType : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueTypeName : null,
    
    /**
     * set to store all keys 
     * @member {com_yung_util_BasicSet}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    keyCollection : null,
    
    /**
     * obj to store all key value pair 
     * @member {object}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    obj : null,
    
    /**
	 * constructor
     * @memberof com_yung_util_BasicMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     */
    init : function(keyType, valueType){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.BasicMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "com.yung.util.BasicMap key type can only be string or number!";
        }
        this.keyType = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.obj = {};
        this.keyCollection = new com_yung_util_Set(keyType);
        return this;
    },
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    clear : function () {
        this.obj = {};
        this.keyCollection.clear();
    },
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey : function (key) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            return false;
        }
        if (this.obj[key + ""] == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue : function (value) {
        if (value == null) {
            return false;
        }
        if (this.validValueType(value) == false) {
            return false;
        }
        for (var key in this.obj) {
            if (this.obj[key + ""] === value) {
                return true;
            }
        }
        return false;
    },
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        return this.obj[key + ""];
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
    	if (this.validKeyType(key) == false) {
            throw "key type is not " + this.keyType;
        }
        if (this.validValueType(value) == false) {
            if (typeof this.valueType == "string") {
                throw "value type is not string";
            } else if (typeof this.valueType == "number") {
                throw "value type is not number";
            } else if (typeof this.valueType == "boolean") {
                throw "value type is not boolean";
            } else if (typeof this.valueType == "object") {
                throw "value type is not object";
            } else {
                throw "value type is not " + this.valueTypeName;
            }
        }
        if (this.obj[key + ""] == null) {
            this.keyCollection.add(key);
            this.obj[key + ""] = value;
        } else {
            this.obj[key + ""] = value;
        }
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll : function (map) {
        if (map == null) {
            return;
        } else {
        	if (!map instanceof com_yung_util_MapInterface) {
        		throw "map is not com_yung_util_MapInterface";
        	}
        	if (map.size() == 0) {
        		return;
        	}
            var firstKey = map.getKeyArray()[0];
        	if (this.validKeyType(firstKey) == false) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(map.get(firstKey)) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "boolean") {
                    throw "value type is not boolean";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else {
                    throw "value type is not " + this.valueTypeName;
                }
            }
        	var entryArray = map.entryArray()
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                this.put(entry.getKey(), entry.getValue());
            }
        }
    },
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        if (this.obj[key + ""] == null) {
        	return null;
        }
        var value = this.obj[key + ""]
        this.keyCollection.remove(key);
        delete this.obj[key + ""];
        return value;
    },
    
    /** 
     * map size
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {number} map size
     */
    size : function () {
        return this.keyCollection.size();
    },
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {boolean} is empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} map entry array
     */
    entryArray : function () {
        var array = [];
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var entry = new com.yung.util.Entry(keyArray[i], this.obj[keyArray[i]]);
            array.push(entry);
        }
        return array;
    },
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} map key array
     */
    getKeyArray : function () {
        return this.keyCollection.toArray();
    },
    
    /** 
     * return a copy of key array
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {Array} a copy of key array
     */
    cloneKeyArray : function () {
        var array = [];
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            array.push(keyArray[i]);
        }
        return array;
    },
    
    /** 
     * return a copy of key set
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_Set} a copy of key set
     */
    keySet : function () {
        return this.keyCollection;
    },
    
    /** 
     * return a copy of value list
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_CollectionInterface} a copy of value list
     */
    values : function () {
        var list = new com.yung.util.List(this.valueType);
        var entryArray = this.entryArray();
        for (var i = 0; i < entryArray.length; i++) {
            var entry = entryArray[i];
            list.add(entry.getValue());
        }
        return list;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {string} string
     */
    toString : function () {
        var ret = this.classProp.name + "\n";
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var val = this.obj[keyArray[i]];
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(this.obj[keyArray[i]])
            }
            if (this.keyType == 'string') {
                ret = ret + "    '" + keyArray[i] + "': " + valStr + " \n";
            } else if (this.keyType == 'number') {
                ret = ret + "    " + keyArray[i] + ": " + valStr + " \n";
            }
        }
        return ret;
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {com_yung_util_BasicMap} a copy of map
     */
    clone : function () {
        var ret = new com.yung.util.BasicMap(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {all} value
     */
    validValueType : function (value) {
        return _validType(this.valueType, value);
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_BasicMap
     * @return {all} key
     */
    validKeyType : function (key) {
    	return _validType(this.keyType, key);
    }
    
});

/**
 * Sorted Map
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_BasicMap
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_SortedMap = com_yung_util_BasicMap.extend({
    
	classProp : { 
		name : "com.yung.util.SortedMap"
	},
    
	
	/**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    keyType : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_BasicMap
     */
    valueTypeName : null,
    
    /**
     * set to store all keys 
     * @member {com_yung_util_SortedSet}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    keyCollection : null,
    
    /**
     * obj to store all key value pair 
     * @member {object}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
    obj : null,
    
    /**
     * flag to sort keys
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_SortedMap
     */
	reverse : null,
    
	/**
	 * constructor
     * @memberof com_yung_util_SortedMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     * @param  {boolean} reverse - flag to sort keys
     */
	init : function(keyType, valueType, reverse){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.SortedMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "com.yung.util.SortedMap key type can only be string or number!";
        }
        if (reverse === true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.keyType = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.obj = {};
        this.keyCollection = new com_yung_util_SortedSet(keyType, this.reverse);
        return this;
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
    	if (this.validKeyType(key) == false) {
            throw "key type is not " + this.keyType;
        }
        if (this.validValueType(value) == false) {
            if (typeof this.valueType == "string") {
                throw "value type is not string";
            } else if (typeof this.valueType == "number") {
                throw "value type is not number";
            } else if (typeof this.valueType == "boolean") {
                throw "value type is not boolean";
            } else if (typeof this.valueType == "object") {
                throw "value type is not object";
            } else {
                throw "value type is not " + this.valueTypeName;
            }
        }
        if (this.obj[key + ""] == null) {
            this.keyCollection.add(key);
            this.obj[key + ""] = value;
        } else {
            this.obj[key + ""] = value;
        }
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @return {com_yung_util_SortedMap} a copy of map
     */
    clone : function () {
        var ret = new com.yung.util.SortedMap(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    },
    
    binarySearchArray : function(arr, x, start, end, reverse, exact) {
    	return binarySearchArray(arr, x, start, end, reverse, exact);
	},
	
	/** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_SortedMap
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        if (this.obj[key + ""] == null) {
        	return null;
        }
        var value = this.obj[key + ""]
        this.keyCollection.remove(key);
        delete this.obj[key + ""];
        return value;
    }
});


/**
 * JS Map, only allow key type string, number. best performance
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var system_util_Map = com_yung_util_MapInterface.extend({
    
    classProp : { 
        name : "system.util.Map"
    },
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof system_util_Map
     */
    keyType : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof system_util_Map
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof system_util_Map
     */
    valueTypeName : null,
    
    /**
     * set to store all keys 
     * @member {system_util_Set}
     * @instance
     * @memberof system_util_Map
     */
    keyCollection : null,
    
    /**
     * map to store all key value pair 
     * @member {Map}
     * @instance
     * @memberof system_util_Map
     */
    obj : null,
    
    /**
     * constructor
     * @memberof system_util_Map
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     */
    init : function(keyType, valueType){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.BasicMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "system.util.Map key type can only be string or number!";
        }
        this.keyType = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.obj = new Map();
        this.keyCollection = new system_util_Set(keyType);
        return this;
    },
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof system_util_Map
     */
    clear : function () {
        this.obj.clear();
        this.keyCollection.clear();
    },
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey : function (key) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            return false;
        }
        if (this.obj.get(key) == null) {
            return false;
        } else {
            return true;
        }
    },
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue : function (value) {
        if (value == null) {
            return false;
        }
        if (this.validValueType(value) == false) {
            return false;
        }
        try {
            this.obj.forEach(function (val, key, set) {
                if (val === value) {
                    throw "done";
                }
            });
            return false;
        } catch (e) {
            if ("done" != (e + "")) {
                throw e;
            }
            return true;
        }
    },
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        return this.obj.get(key);
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
        if (this.validKeyType(key) == false) {
            throw "key type is not " + this.keyType;
        }
        if (this.validValueType(value) == false) {
            if (typeof this.valueType == "string") {
                throw "value type is not string";
            } else if (typeof this.valueType == "number") {
                throw "value type is not number";
            } else if (typeof this.valueType == "boolean") {
                throw "value type is not boolean";
            } else if (typeof this.valueType == "object") {
                throw "value type is not object";
            } else {
                throw "value type is not " + this.valueTypeName;
            }
        }
        if (this.obj.has(key) == false) {
            this.keyCollection.add(key);
            this.obj.set(key, value);
        } else {
            this.obj.set(key, value);
        }
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll : function (map) {
        if (map == null) {
            return;
        } else {
            if (!map instanceof com_yung_util_MapInterface) {
                throw "map is not com_yung_util_MapInterface";
            }
            if (map.size() == 0) {
                return;
            }
            var firstKey = map.getKeyArray()[0];
            if (this.validKeyType(firstKey) == false) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(map.get(firstKey)) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "boolean") {
                    throw "value type is not boolean";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else {
                    throw "value type is not " + this.valueTypeName;
                }
            }
            var entryArray = map.entryArray()
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                this.put(entry.getKey(), entry.getValue());
            }
        }
    },
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof system_util_Map
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        if (this.obj.has(key) == false) {
            return null;
        }
        var value = this.obj.get(key);
        this.keyCollection.remove(key);
        this.obj["delete"](key);
        return value;
    },
    
    /** 
     * map size
     * 
     * @instance
     * @memberof system_util_Map
     * @return {number} map size
     */
    size : function () {
        return this.keyCollection.size();
    },
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof system_util_Map
     * @return {boolean} is empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof system_util_Map
     * @return {Array} map entry array
     */
    entryArray : function () {
        var array = [];
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var entry = new com.yung.util.Entry(keyArray[i], this.obj.get(keyArray[i]));
            array.push(entry);
        }
        return array;
    },
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof system_util_Map
     * @return {Array} map key array
     */
    getKeyArray : function () {
        return this.keyCollection.toArray();
    },
    
    /** 
     * return a copy of key set
     * 
     * @instance
     * @memberof system_util_Map
     * @return {com_yung_util_Set} a copy of key set
     */
    keySet : function () {
        return this.keyCollection;
    },
    
    /** 
     * return a copy of value list
     * 
     * @instance
     * @memberof system_util_Map
     * @return {com_yung_util_CollectionInterface} a copy of value list
     */
    values : function () {
        var list = new com.yung.util.List(this.valueType);
        this.obj.forEach(function (value, key, map) {
            list.add(value);
        });
        return list;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof system_util_Map
     * @return {string} string
     */
    toString : function () {
        var ret = this.classProp.name + "\n";
        var keyArray = this.keyCollection.toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var val = this.obj.get(keyArray[i]);
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(this.obj[keyArray[i]])
            }
            if (this.keyType == 'string') {
                ret = ret + "    '" + keyArray[i] + "': " + valStr + " \n";
            } else if (this.keyType == 'number') {
                ret = ret + "    " + keyArray[i] + ": " + valStr + " \n";
            }
        }
        return ret;
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof system_util_Map
     * @return {system_util_Map} a copy of map
     */
    clone : function () {
        var ret = new system.util.Map(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof system_util_Map
     * @return {all} value
     */
    validValueType : function (value) {
        return _validType(this.valueType, value);
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof system_util_Map
     * @return {all} key
     */
    validKeyType : function (key) {
        return _validType(this.keyType, key);
    }
    
});

/**
 * Tree node.
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeNode = $Class.extend({

    classProp : { 
        name : "com.yung.util.TreeNode"
    },
    
    /**
     * is leaf node
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    isLeaf : null,
    
    /**
     * collection to store all keys 
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    keys : [],
    keyNumber : 0,
    leftMostKey : null,
    keyType : null,
    
    /**
     * object reference
     * @member {Array}
     * @instance
     * @memberof com_yung_util_TreeNode
     */
    nodeRef : [],
    prevLeaf : null,
    mergeInternal : null,
    
    /**
     * constructor
     * @memberof com_yung_util_TreeNode
     * @param  {boolean} isLeaf - whether leaf node
     * @param  {boolean} reverse - reverse order
     * @param  {all} keyType - key type
     */
    init : function(isLeaf, reverse, keyType){
        if (_validType('boolean', isLeaf) == false) {
            throw "isLeaf type is not boolean";
        }
        if (isLeaf == true) {
            this.isLeaf = true;
        } else {
            this.isLeaf = false;
        }
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.keyType = keyType;
        return this;
    },
    
    /**
     * copy node
     * @memberof com_yung_util_TreeNode
     * @param  {com_yung_util_TreeNode} node - the node to copy from
     * @param  {number} from - where in n to start copying from
     * @param  {number} num - the number of keys/refs to copy
     */
    copy : function (node, from, num) {
        this.keyNumber = num;
        for (var i = 0; i < num; i++) { 
            this.keys[i] = node.keys[from + i];
            this.nodeRef[i] = node.nodeRef[from + i];
        }
        this.nodeRef[num] = node.nodeRef[from + num];
    },
    
    /**
     * Find the "<=" match position in this node.
     * @memberof com_yung_util_TreeNode
     * @param  {key} key - the key to be matched.
     * @return  {number} the position of match within node, where nKeys indicates no match
     */
    find : function (key) {
        // TODO: use binary search
        if (this.keyType == 'string' || this.keyType == 'number') {
            for (var i = 0; i < this.keyNumber; i++) {
                if (this.reverse == true) {
                    if (key >= this.keys[i]) return i;
                } else {
                    if (key <= this.keys[i]) return i;
                }
            }
        } else {
            for (var i = 0; i < this.keyNumber; i++) {
                if (this.reverse == true) {
                    if (key.compareTo(this.keys[i]) >= 0) return i;
                } else {
                    if (key.compareTo(this.keys[i]) <= 0) return i;
                }
            }
        }
        return this.keyNumber;
    }
    
});
com_yung_util_TreeNode["notFound"] = function (){};

/**
 * Tree map. Best performance for sorted key, but only allow key type string or number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeMap = com_yung_util_MapInterface.extend({
    
    classProp : { 
        name : "com.yung.util.TreeMap"
    },
    
    ORDER : 29,  // odd number only
    MAX : null,
    MID : null,
    MIN_LEAF : null,
    MIN_NODE : null,
    
    /**
     * reverse order
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    reverse : false,
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyType : null,
    
    /**
     * key type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyTypeName : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    valueTypeName : null,
    
    /**
     * root of tree
     * @member {com_yung_util_TreeNode}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    root : null,
    
    /**
     * first leaf of tree
     * @member {com_yung_util_TreeNode}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    firstLeaf : null,
    
    /**
     * key size
     * @member {number}
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    keyCount : 0,
    
    /**
     * constructor
     * @memberof com_yung_util_TreeMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     * @param  {boolean} reverse - reverse order
     */
    init : function(keyType, valueType, reverse){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.TreeMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "com.yung.util.TreeMap key type can only be string or number!";
        }
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.MAX = this.ORDER - 1;
        this.MID = (this.ORDER + 1) / 2;
        this.MIN_LEAF = Math.floor(this.ORDER / 2);
        this.MIN_NODE = Math.floor(this.MAX / 2);
        this.keyType = keyType;
        this.keyTypeName = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.root = new com.yung.util.TreeNode(true, this.reverse, this.keyType);
        this.firstLeaf = this.root;
        return this;
    },
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {Array} map entry array
     */
    entryArray : function () {
        var entryArray = [];
        var nextLeaf = this.firstLeaf;
        while (nextLeaf != null) {
            for (var i = 0; i < nextLeaf.keys.length; i++) {
                var en = new com.yung.util.Entry(nextLeaf.keys[i], nextLeaf.nodeRef[i]);
                entryArray.push(en);
            }
            nextLeaf = nextLeaf.nodeRef[nextLeaf.nodeRef.length - 1];
        }
        return entryArray;
    },
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {Array} map key array
     */
    getKeyArray : function () {
        var keyArray = [];
        var nextLeaf = this.firstLeaf;
        while (nextLeaf != null) {
            for (var i = 0; i < nextLeaf.keys.length; i++) {
                keyArray.push(nextLeaf.keys[i]);
            }
            nextLeaf = nextLeaf.nodeRef[nextLeaf.nodeRef.length - 1];
        }
        return keyArray;
    },
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        var ret = this.find(key, this.root);
        if (ret === com_yung_util_TreeNode["notFound"]) {
            return null;
        }
        return ret;
    },
    find : function (key, node) {
        var nd = node;
        while (nd.isLeaf == false) {
            var i = this.findNextNode(key, nd);
            nd = nd.nodeRef[i];
        }
        var ret = nd.find(key);
        if (ret < nd.keyNumber) {
            if (key == nd.keys[ret]) {
                return nd.nodeRef[ret];
            } else {
                return com_yung_util_TreeNode["notFound"];
            }
        } else {
            return com_yung_util_TreeNode["notFound"];
        }
    },
    findNextNode : function (key, node) {
        if (this.keyType == 'string' || this.keyType == 'number') {
            for (var i = 0; i < node.keyNumber; i++) {
                if (node.reverse == true) {
                    if (key > node.keys[i]) return i;
                } else {
                    if (key < node.keys[i]) return i;
                }
            }
        } else {
            for (var i = 0; i < node.keyNumber; i++) {
                if (node.reverse == true) {
                    if (key.compareTo(node.keys[i]) > 0) return i;
                } else {
                    if (key.compareTo(node.keys[i]) < 0) return i;
                }
            }
        }
        return node.keyNumber;
    },
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to get value
     * @return {boolean} removed or not
     */
    remove : function (key) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            return false;
        }
        var originKeyCount = this.keyCount;
        this.delKey(key, this.root);
        if (originKeyCount == this.keyCount) {
            return false;
        } else {
            return true;
        }
    },
    delKey : function (key, node) {
        var handleNode = null;                                                      // holder for current handling node
        if (node.isLeaf == true) {                                                  // handle leaf node level
            var index = node.find(key);
            var ret = this.splice(key, node, index, true);                          // splice (key, value) pair at find position
            if (ret != false) {
                this.keyCount--;
            }
            if (node.keyNumber < this.MIN_LEAF) {
                return node;                                                        // node too small then go to next node to merge
            } else if (index == 0) {
                return node;                                                        // node left most key change 
            }
        } else {                                                                    // handle internal node level
            var i = this.findNextNode(key, node);                                   // find next node position
            handleNode = this.delKey(key, node.nodeRef[i]);                         // recursive call to delete    
            if (handleNode != null) {
                handleNode = this.deleteNode(key, handleNode, node);
            }
        }
        return handleNode; 
    },
    splice : function (key, node, index, isLeft) {
        if (this.keyType == 'string' || this.keyType == 'number') {
            if (index >= node.keyNumber || key != node.keys[index]) {
                // BpTreeMap.delete: attempt to delete not exist key
                return false;
            }
        } else {
            if (index >= node.keyNumber || key.compareTo(node.keys[index]) != 0) {
                // BpTreeMap.delete: attempt to delete not exist key
                return false;
            }
        }
        for (var j = index; j < node.keyNumber; j++) {
            node.keys[j] = node.keys[j + 1];                                     // make room: shift keys right
            if (isLeft || j > index + 1) {                                       // ???
                node.nodeRef[j] = node.nodeRef[j + 1];                           // make room: shift values right
            }
        }
        node.keys.pop();                                                         // remove last not used element
        node.nodeRef.pop();                                                      // remove last not used element
        node.keyNumber--;                                                        // decrement number of keys
        return true;
    },
    deleteNode : function (deleteKey, handleNode, node) {
        var nextNode = null;
        var leftMostKeyChange = false;
        var index = this.seekNodeIndex(node, handleNode);
        if (handleNode.isLeaf == true) {
            if (handleNode.keyNumber < this.MIN_LEAF) {
                if (index == 0) {                                                // left most leaf
                    if (node.nodeRef[index + 1] != null) {                       // right leaf if not null
                        if (node.nodeRef[index + 1].keyNumber > this.MIN_LEAF) { // borrow key from right leaf    
                            var borrowLeaf = node.nodeRef[index + 1];
                            var borrowKey = borrowLeaf.keys[0];
                            var borrowVal = borrowLeaf.nodeRef[0];
                            this.splice(borrowKey, borrowLeaf, 0, true);
                            this.wedge(borrowKey, borrowVal, handleNode, handleNode.find(borrowKey), true);
                            node.keys[index] = borrowLeaf.keys[0];
                        } else {                                                 // merge with right leaf
                            var rightLeaf = node.nodeRef[index + 1];
                            this.merge(handleNode, rightLeaf, node);
                            if (node.keyNumber < this.MIN_NODE) {
                                if (node === this.root) {
                                    if (node.nodeRef.length == 1) {
                                        this.root = handleNode;
                                        return null;
                                    }
                                } else {
                                    node.mergeInternal = true;
                                }
                            }
                        }
                    } else {
                        node.mergeInternal = true;                               // can not borrow or merge then return node for next level merge 
                    }
                } else {                                                         // not left most leaf
                    if (node.nodeRef[index - 1].keyNumber > this.MIN_LEAF) {     // borrow key from left leaf
                        var borrowLeaf = node.nodeRef[index - 1];
                        var borrowKey = borrowLeaf.keys[borrowLeaf.keyNumber - 1];
                        var borrowVal = borrowLeaf.nodeRef[borrowLeaf.keyNumber - 1];
                        this.splice(borrowKey, borrowLeaf, borrowLeaf.keyNumber - 1, true);
                        this.wedge(borrowKey, borrowVal, handleNode, handleNode.find(borrowKey), true);
                        node.keys[index - 1] = borrowKey;
                    } else if (node.nodeRef[index + 1] != null && node.nodeRef[index + 1].keyNumber > this.MIN_LEAF) { // borrow key from right leaf
                        var borrowLeaf = node.nodeRef[index + 1];
                        var borrowKey = borrowLeaf.keys[0];
                        var borrowVal = borrowLeaf.nodeRef[0];
                        this.splice(borrowKey, borrowLeaf, 0, true);
                        this.wedge(borrowKey, borrowVal, handleNode, handleNode.find(borrowKey), true);
                        node.keys[index] = borrowLeaf.keys[0];
                    } else {                                                     // merge with left leaf
                        var leftLeaf = node.nodeRef[index - 1];
                        this.merge(leftLeaf, handleNode, node);
                        if (node.keyNumber < this.MIN_NODE) {
                            if (node === this.root) {
                                if (node.nodeRef.length == 1) {
                                    this.root = leftLeaf;
                                    return null;
                                }
                            } else {
                                node.mergeInternal = true;
                            }
                        }
                    }
                }
            }
        } else {
            if (handleNode.mergeInternal == true) {
                if (index == 0) {                                                // left most node
                    if (node.nodeRef[index + 1] != null) {                       // right node if not null
                        if (node.nodeRef[index + 1].keyNumber > this.MIN_NODE) { // borrow key from right node    
                            var borrowNode = node.nodeRef[index + 1];
                            var borrowKey = borrowNode.keys[0];
                            var borrowRef = borrowNode.nodeRef[0];
                            this.splice(borrowKey, borrowNode, 0, true);
                            if (borrowNode.nodeRef[0].isLeaf == true) {
                                if (borrowNode.leftMostKey != borrowNode.nodeRef[0].keys[0]) {
                                    borrowNode.leftMostKey = borrowNode.nodeRef[0].keys[0];
                                }
                            } else {
                                if (borrowNode.leftMostKey != borrowNode.nodeRef[0].leftMostKey) {
                                    borrowNode.leftMostKey = borrowNode.nodeRef[0].leftMostKey;
                                }
                            }
                            node.keys[index] = borrowNode.leftMostKey;
                            if (this.keyType == 'string' || this.keyType == 'number') {
                                if (this.reverse == true) {
                                    if (borrowRef.keys[0] > borrowKey) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                } else {
                                    if (borrowRef.keys[0] < borrowKey) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                }
                            } else {
                                if (this.reverse == true) {
                                    if (borrowRef.keys[0].compareTo(borrowKey) > 0) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                } else {
                                    if (borrowRef.keys[0].compareTo(borrowKey) < 0) {
                                        borrowKey = borrowRef.keys[0];
                                    }
                                }
                            }
                            var idx = handleNode.find(borrowKey);
                            this.wedge(borrowKey, borrowRef, handleNode, idx, false);
                            if (handleNode.nodeRef[0].isLeaf == true) {
                                handleNode.keys[idx] = handleNode.nodeRef[idx + 1].keys[0];
                                if (idx > 0) {
                                    handleNode.keys[idx - 1] = handleNode.nodeRef[idx].keys[0];
                                }
                            } else {
                                handleNode.keys[idx] = handleNode.nodeRef[idx + 1].leftMostKey;
                                if (idx > 0) {
                                    handleNode.keys[idx - 1] = handleNode.nodeRef[idx].leftMostKey;
                                }
                            }
                            handleNode.mergeInternal = null;
                        } else {                                                 // merge with right node
                            var rightNode = node.nodeRef[index + 1];
                            this.mergeInternal(handleNode, rightNode, node);
                            if (handleNode.nodeRef[0].isLeaf == true) {
                                handleNode.leftMostKey = handleNode.nodeRef[0].keys[0];
                            } else {
                                handleNode.leftMostKey = handleNode.nodeRef[0].leftMostKey;
                            }
                            handleNode.mergeInternal = null;
                            if (node.keyNumber < this.MIN_NODE) {
                                if (node === this.root) {
                                    if (node.nodeRef.length == 1) {
                                        this.root = handleNode;
                                        return null;
                                    }
                                } else {
                                    node.mergeInternal = true;
                                }
                            }
                        }
                    } else {
                        handleNode.mergeInternal = null;
                        node.mergeInternal = true;                               // go to next top node to handle
                    }
                } else {                                                         // not left most node                         
                    if (node.nodeRef[index - 1].keyNumber > this.MIN_NODE) {     // borrow key from left node
                        var borrowNode = node.nodeRef[index - 1];
                        var borrowKey = borrowNode.keys[borrowNode.keyNumber - 1];
                        var borrowRef = borrowNode.nodeRef[borrowNode.keyNumber];
                        this.splice(borrowKey, borrowNode, borrowNode.keyNumber - 1, false);
                        var idx = handleNode.find(borrowKey);
                        this.wedge(borrowKey, borrowRef, handleNode, idx, true);
                        handleNode.keys[idx] = handleNode.nodeRef[idx + 1].keys[0];
                        if (handleNode.nodeRef[0].isLeaf == true) {
                            handleNode.leftMostKey = handleNode.nodeRef[0].keys[0];
                        } else {
                            handleNode.leftMostKey = handleNode.nodeRef[0].leftMostKey;
                        }
                        handleNode.mergeInternal = null;
                    } else if (node.nodeRef[index + 1] != null && node.nodeRef[index + 1].keyNumber > this.MIN_LEAF) { // borrow key from right node
                        var borrowNode = node.nodeRef[index + 1];
                        var borrowKey = borrowNode.keys[0];
                        var borrowRef = borrowNode.nodeRef[0];
                        this.splice(borrowKey, borrowNode, 0, true);
                        if (borrowNode.nodeRef[0].isLeaf == true) {
                            if (borrowNode.leftMostKey != borrowNode.nodeRef[0].keys[0]) {
                                borrowNode.leftMostKey = borrowNode.nodeRef[0].keys[0];
                            }
                        } else {
                            if (borrowNode.leftMostKey != borrowNode.nodeRef[0].leftMostKey) {
                                borrowNode.leftMostKey = borrowNode.nodeRef[0].leftMostKey;
                            }
                        }
                        node.keys[index] = borrowNode.leftMostKey;
                        if (this.keyType == 'string' || this.keyType == 'number') {
                            if (this.reverse == true) {
                                if (borrowRef.keys[0] > borrowKey) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            } else {
                                if (borrowRef.keys[0] < borrowKey) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            }
                        } else {
                            if (this.reverse == true) {
                                if (borrowRef.keys[0].compareTo(borrowKey) > 0) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            } else {
                                if (borrowRef.keys[0].compareTo(borrowKey) < 0) {
                                    borrowKey = borrowRef.keys[0];
                                }
                            }
                        }
                        var idx = handleNode.find(borrowKey);
                        this.wedge(borrowKey, borrowRef, handleNode, idx, false);
                        if (handleNode.nodeRef[0].isLeaf == true) {
                            handleNode.keys[idx] = handleNode.nodeRef[idx + 1].keys[0];
                            if (idx > 0) {
                                handleNode.keys[idx - 1] = handleNode.nodeRef[idx].keys[0];
                            }
                        } else {
                            handleNode.keys[idx] = handleNode.nodeRef[idx + 1].leftMostKey;
                            if (idx > 0) {
                                handleNode.keys[idx - 1] = handleNode.nodeRef[idx].leftMostKey;
                            }
                        }
                        handleNode.mergeInternal = null;
                    } else {                                                     // merge with left node
                        var leftNode = node.nodeRef[index - 1];
                        this.mergeInternal(leftNode, handleNode, node);
                        handleNode.mergeInternal = null;
                        if (node.keyNumber < this.MIN_NODE) {
                            if (node === this.root) {
                                if (node.nodeRef.length == 1) {
                                    this.root = leftNode;
                                    return null;
                                }
                            } else {
                                node.mergeInternal = true;
                            }
                        }
                    }
                }
            }
        }
        var firstRef = node.nodeRef[0];
        if (firstRef.isLeaf == true) {
            if (node.leftMostKey != firstRef.keys[0]) {
                node.leftMostKey = firstRef.keys[0];
                leftMostKeyChange = true;
            }
            for (var i = 0; i < node.keyNumber; i++) {
                node.keys[i] = node.nodeRef[i + 1].keys[0];
            }
        } else {
            if (node.leftMostKey != firstRef.leftMostKey) {
                node.leftMostKey = firstRef.leftMostKey;
                leftMostKeyChange = true;
            }
            for (var i = 0; i < node.keyNumber; i++) {
                node.keys[i] = node.nodeRef[i + 1].leftMostKey;
            }
        }
        if (node.keyNumber != node.keys.length) {
            // FIXME temporary fix
            var length = node.keys.length;
            for (var i = node.keyNumber; i < length; i++) {
                node.keys.pop();
            }
        }
        if (leftMostKeyChange == true) {
            return node;
        }
        if (node.mergeInternal == true) {
            return node;
        }
        return null;
    },
    seekNodeIndex : function (parent, child) {
        if (parent == null || child == null) {
            throw "parent or child is null";
        }
        for (var i = 0; i < parent.nodeRef.length; i++) {
            if (parent.nodeRef[i] === child) {
                return i;
            }
        }
        throw "not found";
    },
    merge : function (leftNode, rightNode, topNode) {
        var leftNodeKeyLength = leftNode.keys.length;
        leftNode.keyNumber = leftNodeKeyLength;
        for (var i = 0; i < rightNode.keyNumber; i++) {                                       // copy right leaf to left leaf
            leftNode.keys[leftNodeKeyLength + i] = rightNode.keys[i];
            leftNode.nodeRef[leftNodeKeyLength + i] = rightNode.nodeRef[i];
            leftNode.keyNumber++;
        }
        leftNode.nodeRef[leftNode.keyNumber] = rightNode.nodeRef[rightNode.keyNumber];        // link left leaf to right leaf
        var index = this.seekNodeIndex(topNode, rightNode);
        topNode.keys.splice(index - 1, 1);                                                    // remove top node key
        topNode.nodeRef.splice(index, 1);                                                     // remove top node reference
        topNode.keyNumber--;
        if (topNode.nodeRef.length == 1) {                                                    // update top node with new key value
            topNode.keys[0] = topNode.nodeRef[0].keys[0];                                     // add temporary key for next merge
        } else {
            if ((index - 2) >= 0) {
                topNode.keys[index - 2] = topNode.nodeRef[index - 1].keys[0];
            }
        }
    },
    mergeInternal : function (leftNode, rightNode, topNode) {
        var leftNodeKeyLength = leftNode.keys.length;
        var leftNodeRefLength = leftNode.nodeRef.length;
        leftNode.keyNumber = leftNodeKeyLength;
        for (var i = 0; i < rightNode.nodeRef.length; i++) {                                  // copy right node to left node
            leftNode.nodeRef[leftNodeRefLength + i] = rightNode.nodeRef[i];                   // add left node with new reference
            leftNode.keys[leftNodeKeyLength + i] = rightNode.nodeRef[i].keys[0];              // add left node with new keys
            leftNode.keyNumber++;
        }
        for (var i = 0; i < leftNode.keyNumber; i++) {                                        // update key value
            var key = null;
            if (leftNode.nodeRef[0].isLeaf == true) {
                key = leftNode.nodeRef[i + 1].keys[0];
            } else {
                key = leftNode.nodeRef[i + 1].leftMostKey;
            }
            leftNode.keys[i] = key;
        }
        var index = this.seekNodeIndex(topNode, rightNode);
        topNode.keys.splice(index - 1, 1);                                                    // remove top node key
        topNode.nodeRef.splice(index, 1);                                                     // remove top node reference
        topNode.keyNumber--;
        if (topNode.nodeRef.length == 1) {                                                    // update top node with new key value
            topNode.keys[0] = topNode.nodeRef[0].keys[0];                                     // add temporary key for next merge
        } else {
            if ((index - 2) >= 0) {
                topNode.keys[index - 2] = topNode.nodeRef[index - 1].keys[0];
            }
        }
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
        if (key == null) {
            return false;
        }
        if (this.validKeyType(key) == false) {
            throw "key: '" + key + "' type is not " + this.keyTypeName;
        }
        if (this.validValueType(value) == false) {
            throw "value: '" + value + "' type is not " + this.valueTypeName;
        }
        this.insert(key, value, this.root);
    },
    insert : function (key, value, node) {
        var newNode = null;                                                         // holder for right sibling node
        if (node.isLeaf == true) {                                                  // handle leaf node level
            var ret = this.wedge(key, value, node, node.find(key), true);           // wedge (key, value) pair at find position
            if (ret != false) {
                this.keyCount++;
            }
            if (node.keyNumber > this.MAX) {                                        // current node is full
                newNode = this.split(node);                                         // split current node, return right sibling
                node.nodeRef[node.keyNumber] = newNode;                             // link left leaf n to right leaf r
                newNode.prevLeaf = node;                                            // link right leaf n to left leaf r
                if (node == this.root) {
                    this.root = this.makeRoot(node, newNode.keys[0], newNode);      // at root => make a new root
                }
            }
        } else {                                                                    // handle internal node level
            var i = this.findNextNode(key, node);                                   // find next node position
            newNode = this.insert(key, value, node.nodeRef[i]);                     // recursive call to insert    
            if (newNode != null) {
                newNode = this.insertNode(newNode.keys[0], newNode, node);
            }
        }
        return newNode;   
    },
    wedge : function (key, value, node, index, isLeft) {
        if (this.keyType == 'string' || this.keyType == 'number') {
            if (index < node.keyNumber && key == node.keys[index]) {
                // BpTreeMap.insert: attempt to insert duplicate key
                node.nodeRef[index] = value;
                return false;
            }
        } else {
            if (index < node.keyNumber && key.compareTo(node.keys[index]) == 0) {
                // BpTreeMap.insert: attempt to insert duplicate key
                node.nodeRef[index] = value;
                return false;
            }
        }
        node.nodeRef[node.keyNumber + 1] = node.nodeRef[node.keyNumber];         // preserving the last value, last value is next sibling
        for (var j = node.keyNumber; j > index; j--) {
            node.keys[j] = node.keys[j - 1];                                     // make room: shift keys right
            if (isLeft || j > index + 1) {                                       // ???
                node.nodeRef[j] = node.nodeRef[j - 1];                           // make room: shift values right
            }
        }
        node.keys[index] = key;                                                  // place new key
        if (isLeft == true) {
            node.nodeRef[index] = value;                                         // place new value
        } else {
            node.nodeRef[index + 1] = value;                                     // ???
        }
        node.keyNumber++;                                                        // increment number of keys
        if (node.isLeaf == false) {
            var val = node.nodeRef[0];
            if (val.isLeaf == true) {
                node.leftMostKey = val.keys[0];
            } else {
                node.leftMostKey = val.leftMostKey;
            }
        }
        return true;
    },
    insertNode : function (key, value, node) {
        var newNode = null;
        var index = node.find(key);
        if (this.keyType == 'string' || this.keyType == 'number') {
            if (this.reverse == true) {
                if (value.isLeaf == false && key < value.leftMostKey) {
                    key = value.leftMostKey;
                }
            } else {
                if (value.isLeaf == false && key > value.leftMostKey) {
                    key = value.leftMostKey;
                }    
            }
        } else {
            if (this.reverse == true) {
                if (value.isLeaf == false && key.compareTo(value.leftMostKey) < 0) {
                    key = value.leftMostKey;
                }
            } else {
                if (value.isLeaf == false && key.compareTo(value.leftMostKey) > 0) {
                    key = value.leftMostKey;
                }    
            }
        }
        this.wedge(key, value, node, index, false);
        if (node.keyNumber > this.MAX) {                                         // current node is full
            newNode = this.splitInternal(node);                                  // split current node, return right sibling
            if (node == this.root) {
                var newKey = newNode.keys.shift();
                newNode.nodeRef.shift();
                newNode.keyNumber--;
                var val = newNode.nodeRef[0];
                if (val.isLeaf == true) {
                    newNode.leftMostKey = val.keys[0];
                } else {
                    newNode.leftMostKey = val.leftMostKey;
                }
                if (this.keyType == 'string' || this.keyType == 'number') {
                    if (this.reverse == true) {
                        if (newKey < newNode.leftMostKey) {
                            newKey = newNode.leftMostKey;
                        }
                    } else {
                        if (newKey > newNode.leftMostKey) {
                            newKey = newNode.leftMostKey;
                        }
                    }
                } else {
                    if (this.reverse == true) {
                        if (newKey.compareTo(newNode.leftMostKey) < 0) {
                            newKey = newNode.leftMostKey;
                        }
                    } else {
                        if (newKey.compareTo(newNode.leftMostKey) > 0) {
                            newKey = newNode.leftMostKey;
                        }
                    }    
                }
                this.root = this.makeRoot(node, newKey, newNode);                // at root => make a new root
                this.root.leftMostKey = node.leftMostKey;
            } else {
                newNode.keys.shift();
                newNode.nodeRef.shift();
                newNode.keyNumber--;
                var val = newNode.nodeRef[0];
                if (val.isLeaf == true) {
                    newNode.leftMostKey = val.keys[0];
                } else {
                    newNode.leftMostKey = val.leftMostKey;
                }
            }
        }
        return newNode;
    },
    split : function (node) {
        var r = new com.yung.util.TreeNode(true, this.reverse, this.keyType);    // make a right sibling node (r)
        r.copy(node, this.MID, this.ORDER - this.MID);                           // copy second half to node r
        for (var i = this.MID; i < node.keyNumber; i++) {                        // delete node key/value
            node.keys.pop();
            node.nodeRef.pop();
        }
        node.nodeRef.pop();                                                      // delete last node ref
        node.keyNumber = this.MID;                                               // reset the number of keys in node n
        var originalR = r.nodeRef[r.keyNumber];
        if (originalR != null) {
            originalR.prevLeaf = r;                                              // link original right leaf to new split leaf
        }
        return r;                                                                // return right sibling
    },
    splitInternal : function (node) {
        var r = new com.yung.util.TreeNode(false, this.reverse, this.keyType);   // make a internal right sibling node (r)
        r.copy(node, this.MID - 1, this.ORDER - this.MID + 1);                   // copy second half to node r
        for (var i = this.MID - 1; i < node.keyNumber; i++) {                    // delete node key/value
            node.keys.pop();
            node.nodeRef.pop();
        }
        node.keyNumber = this.MID - 1;                                           // reset the number of keys in node n
        return r;                                                                // return right sibling
    },
    makeRoot : function (leftChildNode, key, rightChildNode) {
        var nr = new com.yung.util.TreeNode(false, this.reverse, this.keyType);     // make a node to become the new root
        nr.keyNumber = 1;                                                           // key number set to 1
        nr.nodeRef[0] = leftChildNode;                                              // reference to left node
        nr.keys[0] = key;                                                           // divider key - largest left
        nr.nodeRef[1] = rightChildNode;                                             // reference to right node
        nr.leftMostKey = leftChildNode.keys[0];
        return nr;
    },
    
    /** 
     * return the first key of map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {keyType}the first key of map
     */
    firstKey : function () {
        return this.firstLeaf.keys[0];
    },
    
    /** 
     * return the last key of map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {keyType} the last key of map
     */
    lastKey : function () {
        var node = this.root;
        while(node.isLeaf != true) {
            node = node.nodeRef[node.nodeRef.length - 1];
        }
        return node.keys[node.keys.length - 1];
    },
    
    /** 
     * map size
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {number} map size
     */
    size : function () {
        return this.keyCount;
    },
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     */
    clear : function () {
        this.root = new com.yung.util.TreeNode(true, this.reverse, this.keyType);
        this.firstLeaf = this.root;
    },
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        var ret = this.find(key, this.root);
        if (ret === com_yung_util_TreeNode["notFound"]) {
            return false;
        }
        return true;
    },
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue : function (value) {
        var nextLeaf = this.firstLeaf;
        while (nextLeaf != null) {
            for (var i = 0; i < nextLeaf.keys.length; i++) {
                var val = nextLeaf.nodeRef[i];
                if (val === value) {
                    return true;
                }
            }
            nextLeaf = nextLeaf.nodeRef[nextLeaf.nodeRef.length - 1];
        }
        return false;
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll : function (map) {
        if (map == null) {
            return;
        } else {
            if (!map instanceof com_yung_util_MapInterface) {
                throw "map is not com_yung_util_MapInterface";
            }
            if (map.size() == 0) {
                return;
            }
            var firstKey = map.getKeyArray()[0];
            if (this.validKeyType(firstKey) == false) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(map.get(firstKey)) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "boolean") {
                    throw "value type is not boolean";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else {
                    throw "value type is not " + this.valueTypeName;
                }
            }
            var entryArray = map.entryArray()
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                this.put(entry.getKey(), entry.getValue());
            }
        }
    },
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {boolean} is empty or not
     */
    isEmpty : function () {
        if (this.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {all} value
     */
    validValueType : function (value) {
        return _validType(this.valueType, value);
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_TreeMap
     * @return {all} key
     */
    validKeyType : function (key) {
        return _validType(this.keyType, key);
    },
    
    toString : function () {
        var ret = this.classProp.name + "\n";
        var entryArray = this.entryArray();
        for (var i = 0; i < entryArray.length; i++) {
            var key = entryArray[i].getKey();
            var val = entryArray[i].getValue();
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(val);
            }
            if (this.keyType == 'string') {
                ret = ret + "    '" + key + "': " + valStr + " \n";
            } else if (this.keyType == 'number') {
                ret = ret + "    " + key + ": " + valStr + " \n";
            }
        }
        return ret;
    },
    
    debugNode : function (node, level, bud) {
        if (node == null) bud = bud + "print: unexpected null node\n";

        if (node == this.root) bud = bud + "BpTreeMap Node\n";
        bud = bud + "-------------------------------------------\n";

        for (var j = 0; j < level; j++) bud = bud + "\t";
        bud = bud + "[ . ";
        for (var i = 0; i < node.keyNumber; i++) {
            bud = bud + node.keys[i] + " . ";
        }
        bud = bud + "]\n";
        if (node.isLeaf == false) {
            for (var i = 0; i <= node.keyNumber; i++) {
                bud = this.debugNode(node.nodeRef[i], level + 1, bud);
            }
        }
        if (node == this.root) {
            bud = bud + "-------------------------------------------\n";
        }
        return bud;
    },
    
    debugLeaf : function (leaf, bud) {
        if (leaf == null) bud = bud + "print: unexpected null node\n";
        bud = bud + "BpTreeMap Leaf\n";
        bud = bud + "-------------------------------------------\n";
        var printLeaf = leaf;
        while (printLeaf != null) {
            bud = bud + "[";
            for (var i = 0; i < printLeaf.nodeRef.length - 1; i++) {
                if (i == 0) {
                    bud = bud + printLeaf.nodeRef[i];
                } else {
                    bud = bud + ", " + printLeaf.nodeRef[i];
                }
            }
            bud = bud + "] ";
            printLeaf = printLeaf.nodeRef[printLeaf.nodeRef.length - 1];
        }
        bud = bud + "\n";
        bud = bud + "-------------------------------------------\n";
        return bud;
    }
    
});

/**
 * Tree hash map, for key type is function
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_TreeMap
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_TreeHashMap = com_yung_util_TreeMap.extend({
    
    classProp : { 
        name : "com.yung.util.TreeHashMap"
    },

    /**
     * constructor
     * @memberof com_yung_util_TreeHashMap
     * @param  {function} keyType - key type
     * @param  {all} valueType - value type
     * @param  {boolean} reverse - reverse order
     */
    init : function(keyType, valueType, reverse){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.TreeHashMap key type and value type can not be null!";
        }
        if (typeof keyType == "function") {
            if ($Class.checkFunction(keyType, 'compareTo') == false) {
                throw "key type: " + $Class.getClassName(keyType) + " should implement compareTo function!";
            }
        } else {
            throw "com.yung.util.TreeHashMap key type can only be function!";
        }
        if (reverse == true) {
            this.reverse = true;
        } else {
            this.reverse = false;
        }
        this.MAX = this.ORDER - 1;
        this.MID = (this.ORDER + 1) / 2;
        this.MIN_LEAF = Math.floor(this.ORDER / 2);
        this.MIN_NODE = Math.floor(this.MAX / 2);
        this.keyType = keyType;
        this.keyTypeName = $Class.getClassName(keyType);
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.root = new com.yung.util.TreeNode(true, this.reverse, this.keyType);
        this.firstLeaf = this.root;
        return this;
    },
    
    toString : function () {
        var ret = this.classProp.name + "\n";
        var entryArray = this.entryArray();
        for (var i = 0; i < entryArray.length; i++) {
            var key = entryArray[i].getKey();
            var val = entryArray[i].getValue();
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(val);
            }
            if (typeof key.toString == 'function') {
                ret = ret + "    " + key + " : " + valStr + " \n";
            } else {
                ret = ret + "    " + JSON.stringify(key) + " : " + valStr + " \n";
            }
        }
        return ret;
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_TreeHashMap
     * @return {com_yung_util_TreeHashMap} a copy of map
     */
    clone : function () {
        var ret = new com_yung_util_TreeMap(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    }
    
});

/**
 * Quick Sort Value Map, value type only support string and number
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments com_yung_util_MapInterface
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
var com_yung_util_QuickSortValueMap = com_yung_util_MapInterface.extend({
    
    classProp : { 
        name : "com.yung.util.QuickSortValueMap"
    },
    
    /**
     * key type
     * @member {string | number}
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     */
    keyType : null,
    
    /**
     * value type
     * @member {all}
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     */
    valueType : null,
    
    /**
     * value type name
     * @member {all}
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     */
    valueTypeName : null,
    
    /**
     * reverse order
     * @member {boolean}
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     */
    reverse : false,
    
    /**
     * base cache map
     * @member {com_yung_util_BasicMap}
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     */
    base : null,
    
    /**
     * base equal value cache map
     * @member {com_yung_util_TreeMap}
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     */
    equal : null,
    
    /**
     * constructor
     * @memberof com_yung_util_QuickSortValueMap
     * @param  {string} keyType - key type
     * @param  {string | function | object} valueType - value type
     * @param  {boolean} reverse - reverse order
     */
    init : function(keyType, valueType, reverse){
        if (keyType == null || valueType == null) {
            throw "com.yung.util.BasicMap key type and value type can not be null!";
        }
        if (typeof keyType == "string") {
            keyType = keyType.toLowerCase();
        }
        if (keyType != 'string' && keyType != 'number') {
            throw "system.util.Map key type can only be string or number!";
        }
        if (reverse == true) {
            this.reverse = true;
        }
        this.keyType = keyType;
        this.valueType = valueType;
        this.valueTypeName = $Class.getClassName(valueType);
        this.base = new com_yung_util_BasicMap(keyType, valueType);
        this.equal = new com_yung_util_TreeMap(valueType, com_yung_util_BasicList, this.reverse);
        return this;
    },
    
    /** 
     * clear all key value pairs
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     */
    clear : function () {
        this.base.clear();
        this.equal.clear();
    },
    
    /** 
     * check map if contains key 
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @param  {keyType} key - key to check if exist
     * @return {boolean} exist or not
     */
    containsKey : function (key) {
        return this.base.containsKey(key);
    },
    
    /** 
     * check map if contains value 
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @param  {valueType} value - value to check if exist
     * @return {boolean} exist or not
     */
    containsValue : function (value) {
        return this.base.containsValue(key);
    },
    
    /** 
     * get value by key
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @param  {keyType} key - key to get value
     * @return {valueType} value
     */
    get : function (key) {
        return this.base.get(key);
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @param  {keyType} value - key to put
     * @param  {valueType} value - value to put
     */
    put : function (key, value) {
        this.removeOldKey(key, value);
        if (this.equal.get(value) == null) {
            var keyList = new com_yung_util_BasicList(this.keyType);
            keyList.add(key);
            this.equal.put(value, keyList);
        } else {
            this.equal.get(value).add(key);
        }
        return this.base.put(key, value);
    },
    removeOldKey : function (key, value) {
        var oldValue = this.base.get(key);
        if (oldValue == null) {
            return;
        } else {
            var keyList = this.equal.get(oldValue);
            keyList.remove(key);
        }
    },
    
    /** 
     * put key value to map 
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @param  {com_yung_util_MapInterface} map - map to put
     */
    putAll : function (map) {
        if (map == null) {
            return;
        } else {
            if (!map instanceof com_yung_util_MapInterface) {
                throw "map is not com_yung_util_MapInterface";
            }
            if (map.size() == 0) {
                return;
            }
            var firstKey = map.getKeyArray()[0];
            if (this.validKeyType(firstKey) == false) {
                throw "key type is not " + this.keyType;
            }
            if (this.validValueType(map.get(firstKey)) == false) {
                if (typeof this.valueType == "string") {
                    throw "value type is not string";
                } else if (typeof this.valueType == "number") {
                    throw "value type is not number";
                } else if (typeof this.valueType == "boolean") {
                    throw "value type is not boolean";
                } else if (typeof this.valueType == "object") {
                    throw "value type is not object";
                } else {
                    throw "value type is not " + this.valueTypeName;
                }
            }
            var entryArray = map.entryArray()
            for (var i = 0; i < entryArray.length; i++) {
                var entry = entryArray[i];
                this.put(entry.getKey(), entry.getValue());
            }
        }
    },
    
    /** 
     * remove value by key
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @param  {keyType} key - key to get value
     * @return {valueType} removed value
     */
    remove : function (key) {
        if (key == null) {
            return null;
        }
        if (this.validKeyType(key) == false) {
            return null;
        }
        if (this.base.containsKey(key) == false) {
            return null;
        }
        var value = this.base.get(key);
        var keyList = this.equal.get(value);
        keyList.remove(key);
        return value;
    },
    
    /** 
     * map size
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {number} map size
     */
    size : function () {
        return this.base.size();
    },
    
    /** 
     * map is empty or not
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {boolean} is empty or not
     */
    isEmpty : function () {
        if (this.base.size() == 0) {
            return true;
        } else {
            return false;
        }
    },
    
    /** 
     * return map entry array
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {Array} map entry array
     */
    entryArray : function () {
        var array = [];
        var keyArray = this.getKeyArray();
        for (var i = 0; i < keyArray.length; i++) {
            var entry = new com.yung.util.Entry(keyArray[i], this.base.get(keyArray[i]));
            array.push(entry);
        }
        return array;
    },
    
    /** 
     * get key array
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {Array} map key array
     */
    getKeyArray : function () {
        return this.keySet().toArray();
    },
    
    /** 
     * return a copy of key set
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {com_yung_util_Set} a copy of key set
     */
    keySet : function () {
        var keySet = new com_yung_util_BasicSet(this.keyType);
        var entryArray = this.equal.entryArray()
        for (var i = 0; i < entryArray.length; i++) {
            var entry = entryArray[i];
            var keyList = entry.getValue();
            var keyArray = keyList.toArray();
            for (var j = 0; j < keyArray.length; j++) {
                keySet.add(keyArray[j]);
            }
        }
        return keySet;
    },
    
    /** 
     * return a copy of value list
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {com_yung_util_CollectionInterface} a copy of value list
     */
    values : function () {
        var list = new com.yung.util.List(this.valueType);
        this.obj.forEach(function (value, key, map) {
            list.add(value);
        });
        return list;
    },
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {string} string
     */
    toString : function () {
        var ret = this.classProp.name + "\n";
        var keyArray = this.getKeyArray().toArray();
        for (var i = 0; i < keyArray.length; i++) {
            var val = this.base.get(keyArray[i]);
            var valStr = "";
            if (typeof val.toString == 'function') {
                valStr = val.toString();
            } else {
                valStr = JSON.stringify(this.obj[keyArray[i]])
            }
            if (this.keyType == 'string') {
                ret = ret + "    '" + keyArray[i] + "': " + valStr + " \n";
            } else if (this.keyType == 'number') {
                ret = ret + "    " + keyArray[i] + ": " + valStr + " \n";
            }
        }
        return ret;
    },
    
    /** 
     * return a copy of map
     * 
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {com_yung_util_QuickSortValueMap} a copy of map
     */
    clone : function () {
        var ret = new com_yung_util_QuickSortValueMap(this.keyType, this.valueType);
        ret.putAll(this);
        return ret;
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {all} value
     */
    validValueType : function (value) {
        return _validType(this.valueType, value);
    },
    
    /** 
     * check key type
     * 
     * @private
     * @instance
     * @memberof com_yung_util_QuickSortValueMap
     * @return {all} key
     */
    validKeyType : function (key) {
        return _validType(this.keyType, key);
    }
    
});

var com_yung_util_Map = function (keyType, valueType) {
    if (typeof keyType == "string") {
        keyType = keyType.toLowerCase();
    }
    if (keyType == 'string' || keyType == 'number') {
        if (typeof Map != 'undefined') {
            return new system_util_Map(keyType, valueType);
        }
        return new com.yung.util.BasicMap(keyType, valueType);
    }
    throw "argument keyType not match any type";
}
$Y.register("com.yung.util.Map", com_yung_util_Map);

jQuery( document ).ready(function() {
    if (typeof com_yung_util_Collection == 'undefined') {
        alert("yung-Map.js requires yung-Collection.js!");
    }
});