import {com_yung_util_SortedSet, com_yung_util_BasicSet, com_yung_util_TreeSet } from "../src/yung-Collection";
import {com_yung_util_TableGridSchema, com_yung_util_ColumnUtil, com_yung_util_TableGridUtil } from "../src/yung-TableGridUtil";

// define import classes
var $Y = {
    SortedSet : com_yung_util_SortedSet,
    BasicSet : com_yung_util_BasicSet,
    TreeSet : com_yung_util_TreeSet,
    TableGridSchema : com_yung_util_TableGridSchema,
    ColumnUtil : com_yung_util_ColumnUtil,
    TableGridUtil : com_yung_util_TableGridUtil,
    def : function(pack: string) {}
}

$Y.def("com.yung.util.*");
var bud = "";

var set1 = new $Y.SortedSet<string>("string");
set1.add("1");
set1.add("a");
set1.add("b");
set1.add("c");
set1.add("2");
var arr = ["1", "2", "z"];
set1.addByArray(arr);
bud = bud + "set1: " + set1 + "\n";


var set2 = new $Y.BasicSet<String>("string");
set2.add("1");
set2.add("a");
set2.add("b");
set2.add("x");
set2.add("y");
bud = bud + "set2: " + set2 + "\n";


bud = bud + "set2.contains(1): " + set2.contains(1) + "\n";
bud = bud + 'set2.contains("1"): ' + set2.contains("1") + '\n';
bud = bud + "set2.containsAll(set1): " + set2.containsAll(set1) + "\n";

bud = bud + "\n";
bud = bud + "set2.remove('1') \n";
set2.remove("1");
bud = bud + "set2: " + set2 + "\n";
bud = bud + "set2.removeAll(set1) \n";
set2.removeAll(set1);
bud = bud + "set2: " + set2 + "\n";

bud = bud + "\n";
bud = bud + "\n";
var set3 = new $Y.SortedSet<number>("number");
set3.add(1);
set3.add(1.1);
set3.add(1.10);
set3.add(30);
set3.add(100);
set3.add(11.98);
bud = bud + "set3: " + set3 + "\n";


bud = bud + "\n";
bud = bud + "\n";
var set4 = new $Y.BasicSet<string>("string");
bud = bud + "set1: " + set1 + "\n";
bud = bud + "set4.addAll(set1) \n";
set4.addAll(set1);
bud = bud + "set4: " + set4 + "\n";
bud = bud + "set1 hashCode: " + set1.hashCode() + "\n";
bud = bud + "set4 hashCode: " + set4.hashCode() + "\n";
bud = bud + "set4.getFirstElement(): " + set4.getFirstElement() + "\n";

var treeSet = new $Y.TreeSet<string>("string");
