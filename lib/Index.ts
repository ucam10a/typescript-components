import {com_yung_util_SortedSet, com_yung_util_BasicSet, com_yung_util_TreeSet } from "../src/yung-Collection";
import {com_yung_util_TableGridSchema, com_yung_util_ColumnUtil, com_yung_util_TableGridUtil } from "../src/yung-TableGridUtil";

// define import classes
var $Y = {
    SortedSet : com_yung_util_SortedSet,
    BasicSet : com_yung_util_BasicSet,
    TreeSet : com_yung_util_TreeSet,
    TableGridSchema : com_yung_util_TableGridSchema,
    ColumnUtil : com_yung_util_ColumnUtil,
    TableGridUtil : com_yung_util_TableGridUtil,
    def : function(pack: string) {}
}

var bud = "";

var set1 = new $Y.SortedSet<string>("string");
set1.add("1");
set1.add("a");
set1.add("b");
set1.add("c");
set1.add("2");
var arr = ["1", "2", "z"];
set1.addByArray(arr);
bud = bud + "set1: " + set1 + "\n";

console.log(bud);