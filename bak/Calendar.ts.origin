/**
 * Calendar tool, like JAVA calendar
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
class com_yung_util_Calendar extends $Class {
    
	public classProp: {
        name : "com.yung.util.Calendar",
        unimplemented: []
    }
    
    /**
     * Date object
     * @member {jsDate}
     * @instance
     * @memberof com_yung_util_Calendar
     */
    d : Date
    
    /**
	 * constructor
     * @memberof com_yung_util_Calendar
     * @param  {number} arg1 - year
     * @param  {number} arg2 - month
     * @param  {number} arg3 - day of month
     * @param  {number} arg4 - hour(24hr)
     * @param  {number} arg5 - minute
     * @param  {number} arg6 - second
     * @param  {number} arg7 - millisecond
     */
    constructor (arg1: any, arg2?: number, arg3?: number, arg4?: number, arg5?: number, arg6?: number, arg7?: number) {
        super();
        if (arg1 instanceof Date) {
            this.d = arg1;
            return this;
        }
        if (arg1 == null) arg1 = 0;
        if (arg2 == null) arg2 = 0;
        if (arg3 == null) arg3 = 0;
        if (arg4 == null) arg4 = 0;
        if (arg5 == null) arg5 = 0;
        if (arg6 == null) arg6 = 0;
        if (arg7 == null) arg7 = 0;
        if (typeof arg1 != "number") {
            throw "Year should be integer";
        }
        if (typeof arg2 != "number") {
            throw "Month should be integer";
        }
        if (typeof arg3 != "number") {
            throw "Day should be integer";
        }
        if (typeof arg4 != "number") {
            throw "Hours should be integer";
        }
        if (typeof arg6 != "number") {
            throw "Minutes should be integer";
        }
        if (typeof arg7 != "number") {
            throw "Milliseconds should be integer";
        }
        var year = null;
        var month = null;
        var day = null;
        var hour = null;
        var min = null;
        var sec = null;
        var milli = null;
        year = parseInt(arg1 + "");
        month = parseInt(arg2 + "") - 1;
        if (month < 0) month = 0;
        day = parseInt(arg3 + "");
        hour = parseInt(arg4 + "");
        min = parseInt(arg5 + "");
        sec = parseInt(arg6 + "");
        milli = parseInt(arg7 + "");
        var d = new Date(year, month, day, hour, min, sec, milli);
        this.d = d;
        return this;
    }
    
    /** 
     * get year
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} year number
     */
    getYear (): number {
        return this.d.getFullYear();
    }
    
    /** 
     * get month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} month number
     */
    getMonth (): number {
        return this.d.getMonth();
    }
    
    /** 
     * get day of month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} day of month number
     */
    getDate (): number {
        return this.d.getDate();
    }
    
    /** 
     * get hour(24hr)
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} hour(24hr) number
     */
    getHours (): number {
        return this.d.getHours();
    }
    
    /** 
     * get minute
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} minute number
     */
    getMinutes (): number {
        return this.d.getMinutes();
    }
    
    /** 
     * get second
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} second number
     */
    getSeconds (): number {
        return this.d.getSeconds();
    }
    
    /** 
     * get millisecond
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} millisecond number
     */
    getMilliseconds (): number {
        return this.d.getMilliseconds();
    }
    
    /** 
     * get day of week
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} day of week number
     */
    getDay (): number {
        return this.d.getDay();
    }
    
    /** 
     * set year
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} year - year number
     */
    setYear (year: number): void {
        this.d.setFullYear(year);
    }
    
    /** 
     * set month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} month - month number
     */
    setMonth (month: number): void {
        this.d.setMonth(month);
    }
    
    /** 
     * set day of month
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} day - day of month number
     */
    setDate (day: number): void {
        this.d.setDate(day);
    }
    
    /** 
     * set hours
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} hours - hours number
     */
    setHours (hours: number): void {
        this.d.setHours(hours);
    }
    
    /** 
     * set minute
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} min - minute number
     */
    setMinutes (min: number): void {
        this.d.setMinutes(min);
    }
    
    /** 
     * set second
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} sec - second number
     */
    setSeconds (sec: number): void {
        this.d.setSeconds(sec);
    }
    
    /** 
     * set millisecond
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} milli - millisecond number
     */
    setMilliseconds (milli: number): void {
        this.d.setMilliseconds(milli);
    }

    /** 
     * add year to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} year - year number
     */
    addYear (year: number): void {
        this.d.setFullYear(year + this.getYear());
    }
    
    /** 
     * add month to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} month - month number
     */
    addMonth (month: number): void {
        this.d.setMonth(month + this.getMonth());
    }
    
    /** 
     * add day to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} day - day number
     */
    addDay (day: number): void {
        this.d.setDate(day + this.getDate());
    }
    
    /** 
     * add hour to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} hours - hours number
     */
    addHours (hours: number): void {
        this.d.setHours(hours + this.getHours());
    }
    
    /** 
     * add minute to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} min - minute number
     */
    addMinutes (min: number): void {
        this.d.setMinutes(min + this.getMinutes());
    }
    
    /** 
     * add second to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} sec - second number
     */
    addSeconds (sec: number): void {
        this.d.setSeconds(sec + this.getSeconds());
    }
    
    /** 
     * add millisecond to calendar
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} milli - millisecond number
     */
    addMilliseconds (milli: number): void {
        this.d.setMilliseconds(milli + this.getMilliseconds());
    }
    
    /** 
     * get unix time
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {number} unit time number
     */
    getTime (): number {
        return this.d.getTime();
    }
    
    /** 
     * set unix time
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @param {number} time - unit time number
     */
    setTime (time: number): void {
        if (typeof time != 'number') {
            throw "argument time should be integer";
        }
        time = parseInt(time + "");
        this.d = new Date(time);
    }
    
    /** 
     * convert to jsDate
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     * @return {Date} jsDate
     */
    toDate (): Date {
        return new Date(this.d.getTime());
    }
    
    /** 
     * to string
     * 
     * @instance
     * @memberof com_yung_util_Calendar
     */
    toString (): string {
        return this.d.toString();
    }
}

/**
 * Date format tool, like JAVA SimpleDateFormat
 * @property {object}  classProp - class property
 * @property {string}  classProp.year - year pattern
 * @property {string}  classProp.month - month pattern
 * @property {string}  classProp.date - day pattern
 * @property {string}  classProp.hour - hour pattern
 * @property {string}  classProp.min - minute pattern
 * @property {string}  classProp.sec - second pattern
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
class com_yung_util_SimpleDateFormat extends $Class {
    
	classProp : { 
        name : "com.yung.util.SimpleDateFormat",
        unimplemented: [],
    	year : "yyyy", 
    	month : "MM", 
    	date : "dd", 
    	hour : "HH", 
    	min : "mm", 
    	sec : "ss",
    	milli : "SSS"
    }
    
    /**
     * date pattern
     * @member {string}
     * @instance
     * @memberof com_yung_util_SimpleDateFormat
     */
    dateFormat : string
    
    /**
	 * constructor
	 * 
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} arg1 - date format pattern
     */
    constructor(arg1: string) {
        super();
        if (typeof arg1 != 'string') {
            throw "argument type is not string";
        }
        this.dateFormat = arg1;
    }
    
    /**
	 * use date pattern to parse string to get com_yung_util_Calendar instance
 	 * 
	 * @instance
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} dateString - date string
     * @return {com_yung_util_Calendar} calendar
     */
    parse (dateString: string): com_yung_util_Calendar {
        var year = 0;
        var month = 0;
        var date = 0;
        var hour = 0;
        var min = 0;
        var sec = 0;
        var milli = 0;
        for (var key in this.classProp) {
            var match = this.classProp[key];
            if (key == 'year') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var yearStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(yearStr) == false) {
                        throw dateString + " format error!";
                    }
                    year = parseInt(yearStr);
                }
            } else if (key == 'month') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var monthStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(monthStr) == false) {
                        throw dateString + " format error!";
                    }
                    month = parseInt(monthStr);
                }
            } else if (key == 'date') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var dateStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(dateStr) == false) {
                        throw dateString + " format error!";
                    }
                    date = parseInt(dateStr);
                }
            } else if (key == 'hour') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var hourStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(hourStr) == false) {
                        throw dateString + " format error!";
                    }
                    hour = parseInt(hourStr);
                }
            } else if (key == 'min') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var minStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(minStr) == false) {
                        throw dateString + " format error!";
                    }
                    min = parseInt(minStr);
                }
            } else if (key == 'sec') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var secStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(secStr) == false) {
                        throw dateString + " format error!";
                    }
                    sec = parseInt(secStr);
                }
            } else if (key == 'milli') {
                var start = this.dateFormat.indexOf(match);
                if (start >= 0) {
                    var end = start + match.length;
                    var milliStr = dateString.substring(start, end);
                    if (jQuery.isNumeric(milliStr) == false) {
                        throw dateString + " format error!";
                    }
                    milli = parseInt(milliStr);
                }
            }
        }
        var cal = new com_yung_util_Calendar(year, month, date, hour, min, sec, milli);
        return cal;
    }
    
    /**
	 * padding zero to string
	 * 
	 * @private
     * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} str - target string
     * @param  {number} total - total length of result string
     * @return {string} result string
     */
    paddingZero (str: string, total: number): string {
        var diff = total - str.length;
        if (diff > 0) {
            var ret = "";
            for (var i = 0; i < diff; i++) {
                ret = ret + "0";
            }
            ret = ret + str;
            return ret;
        } else {
            return str;
        }
    }
    
    /**
	 * use date pattern to format calendar to date string
	 * 
	 * @instance
	 * @memberof com_yung_util_SimpleDateFormat
     * @param  {com_yung_util_Calendar | jsDate} date - calendar
     * @return {string} date string
     */
    format (date: any): string {
        var cal = null;
        if (date instanceof Date) {
            cal = new com_yung_util_Calendar(date);
        } else if (date instanceof com_yung_util_Calendar) {
            cal = date;
        } else {
            throw "argument date is not a Date or com.yung.util.Calendar";
        }
        var ret = "" + this.dateFormat;
        for (var key in this.classProp) {
            if (key == 'year') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getYear() + "", 4));
            } else if (key == 'month') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero((cal.getMonth() + 1) + "", 2));
            } else if (key == 'date') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getDate() + "", 2));
            } else if (key == 'hour') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getHours() + "", 2));
            } else if (key == 'min') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getMinutes() + "", 2));
            } else if (key == 'sec') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getSeconds() + "", 2));
            } else if (key == 'milli') {
                ret = this.replaceAll(ret, this.classProp[key], this.paddingZero(cal.getMilliseconds() + "", 2));
            }
        }
        return ret;
    }
    
    /**
	 * use date pattern to check date string if valid
	 * 
	 * @instance
	 * @memberof com_yung_util_SimpleDateFormat
     * @param  {string} dateString - date string
     * @return {boolean} valid or not
     */
    validate (dateString: string): boolean {
        try {
            this.parse(dateString);
            return true;
        } catch (err) {
            return false;
        }
    }
}