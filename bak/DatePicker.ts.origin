/**
 * Date picker
 * 
 * @property {object}  classProp - class property
 * @property {string}  classProp.name - class name
 * @augments $Class
 * @class
 * @author Yung Long Li <ucam10a@gmail.com>
 */
class com_yung_util_DatePicker extends $Class {

    classProp : { 
        name : "com.yung.util.DatePicker",
        unimplemented: []
    }
    
    hashId : string
    
    /**
     * element id
     * @member {string}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    eleId : string
    
    /**
     * float div to place word list
     * @member {com_yung_util_FloatDiv}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    floatDiv : com_yung_util_FloatDiv
    displayHeight : number
    displayWidth : number
    blockFire : boolean
    delayFire : number
    adjustX : number
    adjustY : number
    format : string
    
    
    /**
     * select date
     * @member {com_yung_util_Calendar}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    selectDate : com_yung_util_Calendar
    
    /**
     * Callback after date choose value
     *
     * @callback AfterDateChoose
     * @param {string} eleId - element id
     * @param {string} inputVal - select date
     */
    /**
     * callback after date choose value
     * @member {AfterDateChoose}
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    afterChoose : Function
    
    
    /**
     * constructor
     * @memberof com_yung_util_DatePicker
     * @param  {string} eleId - element id
     * @param  {string} format - date format, ex: yyyy/MM/dd, See JAVA SimpleDateFormat
     */
    protected constructor (eleId: string, format: string) {
        super();
        $Class.validate(this.classProp.name, eleId);
        if (eleId == null || eleId == '') {
            throw "Please specify eleId";
        }
        this.eleId = eleId;
        this.hashId = MD5Util.calc(this.classProp.name + "-" + eleId);
        if (format == null) {
            throw "format is empty!";
        }
        if (_validType('string', format) == false) {
            throw "format is not string";
        }
        this.format = format;
        this.selectDate = new com_yung_util_Calendar(new Date());
        jQuery("#" + eleId).attr("autocomplete", "off");
        this.createPicker();
        return this;
    }
    
    /** 
     * set after choose callback
     * 
     * @instance
     * @memberof com_yung_util_DatePicker
     * @param {AfterDateChoose} afterChoose - after choose callback
     */
    setAfterChoose (afterChoose: Function): void {
        if (typeof afterChoose != 'function') {
            throw "afterChoose is not function";
        }
        this.afterChoose = afterChoose;
    }
    
    /** 
     * set input value
     * 
     * @instance
     * @memberof com_yung_util_DatePicker
     * @param {string} inputVal - date put into input
     */
    setInputValue (inputVal: string): void {
        jQuery("#" + this.eleId).val(inputVal);
        if (typeof this.afterChoose == 'function') {
            this.afterChoose(this.eleId, inputVal);
        }
        this.floatDiv.closeFloatDiv();
        var sdf = new com_yung_util_SimpleDateFormat(this.format);
        this.selectDate = sdf.parse(inputVal);
    }
    
    /** 
     * create date picker
     * 
     * @private
     * @instance
     * @memberof com_yung_util_DatePicker
     */
    createPicker (): void {
        this.floatDiv = $Class.getInstance("com.yung.util.FloatDiv", "datePicker-" + this.eleId);
        this.floatDiv.showCloseButton(false);
        this.bindFunction();
    }
    private bindFunction (): void {
        var bindId = this.eleId;
        var self = this;
        // show when onFoucus
        jQuery("#" + bindId).focus(function() {
            var display = self.floatDiv.isDisplay();
            if (display == false) {
                self.showPicker(bindId);
            }
        });
        // hide when blur
        jQuery("#" + bindId).blur(function() {
            setTimeout(function(){ 
                if (self.blockFire == true) {
                    self.blockFire = false;
                    jQuery("#" + bindId).focus();
                    return;
                }
                self.floatDiv.closeFloatDiv();
            }, self.delayFire);
        });
    }
    
    /** 
     * show date picker
     * 
     * @memberof com_yung_util_DatePicker
     * @param {string} bindId - binding element id
     */
    showPicker (bindId?: string): void {
        if (bindId == null) {
            bindId = this.eleId;
        }
        // display floatDiv
        var display = this.floatDiv.isDisplay();
        var val = jQuery("#" + bindId).val();
        if (display == false) {
            var sdf = new com_yung_util_SimpleDateFormat(this.format);
            if (sdf.validate(val) == true) {
                this.selectDate = sdf.parse(val);
            }
        }
        var html = this.createMonthTable(val);
        if (display == false) {
            var browser = com_yung_util_getbrowser();
            var ele = jQuery("#" + bindId);
            var position = new com_yung_util_Position(ele);
            var offsetY = 7;
            var offsetX = 0;
            var typeName = jQuery("#" + this.eleId).prop('nodeName');
            typeName = typeName.toUpperCase();
            if ("TEXTAREA" == typeName) {
            	offsetY = offsetY - 3;
            }
            if (browser == 'msie') {
                offsetY = offsetY - 4;
                offsetX = offsetX - 2;
            }
            if (browser == 'safari') {
                offsetX = offsetX - 1;
            }
            if (browser == 'firefox') {
                offsetY = offsetY - 2;
                offsetX = offsetX - 1;
            }
            if (browser == 'edge') {
                offsetY = offsetY - 2;
                offsetX = offsetX - 1;
            }
            offsetX = offsetX + this.adjustX;
            offsetY = offsetY + this.adjustY;
            var pos = position.getBotLeftPosition(offsetX, offsetY);
            var self = this;
            this.floatDiv.openFloatDiv(html, pos, this.displayWidth, this.displayHeight, function () {
                self.adjustSize();
            });
        } else {
            this.floatDiv.setFloatContent(html);
            this.adjustSize();
        }
    }
    private adjustSize (): void {
        var tableWidth = jQuery("#yung-datePicker-" + this.eleId).width() + 2;
        var tableHeight = jQuery("#yung-datePicker-" + this.eleId).height() + 1;
        var browser = com_yung_util_getbrowser();
        if (browser == 'msie') {
            tableHeight = tableHeight + 3;
        } else if (browser == 'safari') {
            tableHeight = tableHeight + 1;
        }
        jQuery("#yung-FloatDiv-datePicker-" + this.eleId).css("height", tableWidth + "px");
        jQuery("#yung-FloatDiv-datePicker-" + this.eleId).css("height", tableHeight + "px");
    }
    private createMonthTable (val?: string) {
        var sdf = new com_yung_util_SimpleDateFormat(this.format);
        var inputCal = null;
        var now = new com_yung_util_Calendar(new Date());
        if (val != null && val != '') {
            if (sdf.validate(val)) {
                inputCal = sdf.parse(val);
            }
        }
        var cal = null;
        if (this.selectDate != null) {
            cal = this.selectDate;
        } else {
            cal = new com_yung_util_Calendar(new Date());
        }
        var year = cal.getYear();
        var month = cal.getMonth();
        var totalDays = this.getMonthDays(year, month + 1);
        var weekIdx = 0;
        var weekDayIdx = 0;
        var weekTmpArray = [];
        
        var weekTmp = null;
        var weekDayArrayTmp = null;
        var weekDayArrayData = [];
        var thisMonthStartDay = new com_yung_util_Calendar(year, month + 1, 1);
        var wDay = thisMonthStartDay.getDay();
        for (var i = 0; i < totalDays; i++) {
            if (weekDayIdx == 0) {
                weekTmp = new com_yung_util_ComplexTemplate();
                weekTmp.add("<tr>");
                weekTmp.add("    {{weekDayArrayTmp}}");
                weekTmp.add("</tr>");
                weekDayArrayTmp = new com_yung_util_ArrayTemplate();
                weekDayArrayTmp.add("<td align='center' style='width: 30px; height: 22px; border: 1px solid; border-collapse: collapse; {{background}}'>");
                weekDayArrayTmp.add("    <a href='javascript:void(0);' style='{{style}}' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"setInputValue\", \"{{val}}\");' >{{day}}</a>");
                weekDayArrayTmp.add("</td>");
                weekDayArrayData = [];
                if (weekIdx == 0 && wDay > 0) {
                    for (var j = 0; j < wDay; j++) {
                        var data = {};
                        data["day"] = "";
                        data["val"] = "";
                        data["background"] = "";
                        weekDayIdx++;
                        weekDayArrayData.push(data);
                    }
                }
            }
            var data = {};
            data["day"] = "" + (i + 1);
            data["val"] = this.getDateString(year, month + 1, i + 1);
            if (inputCal != null && (i + 1) == inputCal.getDate() && month == inputCal.getMonth() && year == inputCal.getYear()) {
                data["background"] = "background: yellow;";
            } else if (inputCal == null && (i + 1) == now.getDate() && month == now.getMonth() && year == now.getYear()) {
                data["background"] = "background: yellow;";
            } else {
                data["background"] = "";
            }
            weekDayIdx++;
            weekDayArrayData.push(data);
            if (weekDayIdx == 7) {
                weekTmp.addTemplate("weekDayArrayTmp", weekDayArrayTmp, weekDayArrayData);
                weekTmpArray.push(weekTmp);
                weekDayIdx = 0;
                weekIdx++;
            }
        }
        if (weekDayArrayData.length > 0 && weekDayArrayData.length < 7) {
            if (weekDayArrayData.length < 7) {
                var diff = 7 - weekDayArrayData.length;
                for (var i = 0; i < diff; i++) {
                    var data = {};
                    data["day"] = "";
                    data["val"] = "";
                    data["background"] = "";
                    weekDayArrayData.push(data);
                }
            }
            weekTmp.addTemplate("weekDayArrayTmp", weekDayArrayTmp, weekDayArrayData);
            weekTmpArray.push(weekTmp);
        }
        var tableTmp = new com_yung_util_ComplexTemplate();
        tableTmp.add("<table id='yung-datePicker-" + this.eleId + "' align='center' style='background: white; table-layout:fixed; word-break:break-all; font-size: 13px; border: 2px solid black; border-collapse: collapse;' >");
        tableTmp.add("    <tr>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"prevYear\");'> &#10096; &#10096;</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"prevMonth\");'> &#10096; </td>");
        tableTmp.add("        <td align='center' colspan='3' style='border:1px solid; border-collapse:collapse;'>{{yearMonthTmp}}</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"nextMonth\");'> &#10097; </td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse; cursor: pointer; background: gray; color: white;' onclick='fireInstanceMethod(\"" + this.hashId + "\", \"nextYear\");'> &#10097;  &#10097; </td>");
        tableTmp.add("    </tr>");
        tableTmp.add("    <tr style='background: lightgray;'>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Sun</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Mon</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Tue</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Wed</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Thr</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Fri</td>");
        tableTmp.add("        <td align='center' style='width: 30px; border:1px solid; border-collapse:collapse;'>Sat</td>");
        tableTmp.add("    </tr>");
        for (var i = 0; i < weekTmpArray.length; i++) {
            tableTmp.add("    <tr>{{weekTmpArray" + i + "}}</tr>");
            tableTmp.addTemplate("weekTmpArray" + i, weekTmpArray[i], weekTmpArray[i].getData());
        }
        tableTmp.add("</table>");
        var monthStr = this.getMonthString(year, month + 1);
        var yearMonthTmp = new com_yung_util_SimpleTemplate("{{val}}", "val", monthStr);
        // @ts-ignore
        tableTmp.addTemplate("yearMonthTmp", yearMonthTmp, yearMonthTmp.getData());
        return tableTmp.toHtml();
    }
    
    getMonthDays (year: number, month: number): number {
        var thisMonthStartDay = new com_yung_util_Calendar(year, month, 1);
        var nextMonthStartDay = new com_yung_util_Calendar(year, month + 1, 1);
        var duration = nextMonthStartDay.getTime() - thisMonthStartDay.getTime();
        return (duration / 1000 / 3600 / 24);
    }
    
    getDateString (year: number, month: number, day: number): string {
        var cal = new com_yung_util_Calendar(year, month, day);
        var sdf = new com_yung_util_SimpleDateFormat(this.format);
        return sdf.format(cal);
    }
    
    getMonthString (year: number, month: number): string {
        var cal = new com_yung_util_Calendar(year, month, 1);
        var sdf = new com_yung_util_SimpleDateFormat("yyyy/MM");
        return sdf.format(cal);
    }
    
    prevYear (): void {
        this.blockFire = true;
        this.selectDate.addYear(-1);
        this.showPicker();
    }
    
    prevMonth (): void {
        this.blockFire = true;
        this.selectDate.addMonth(-1);
        this.showPicker();
    }
    
    nextYear (): void {
        this.blockFire = true;
        this.selectDate.addYear(1);
        this.showPicker();
    }
    
    nextMonth (): void {
        this.blockFire = true;
        this.selectDate.addMonth(1);
        this.showPicker();
    }

    /** 
     * get com_yung_util_DatePicker global instance
     * 
     * @param  {string} eleId - element id
     * @param  {string} format - date format, ex: yyyy/MM/dd, See JAVA SimpleDateFormat
     * @return  {com_yung_util_DatePicker} com_yung_util_DatePicker instance
     */
    static instance (eleId: string, format: string): com_yung_util_DatePicker {
        return $Class.getInstance("com.yung.util.DatePicker", eleId, format);
    }

    _super (eleId: string, format: string): any {
    }

}

// specific table grid date picker
class com_yung_util_GridDatePicker extends com_yung_util_DatePicker {
    
    // @ts-ignore
    classProp : { 
        name : "com.yung.util.GridDatePicker",
        unimplemented: []
    }
    
    grid : string
    delayFire : number
    
    constructor (eleId: string, format: string, grid: string) {
        super(eleId, format);
        this.grid = grid;
        return this._super(eleId, format);
    }
    
    private bindFunction (): void {
        var bindId = this.eleId;
        var self = this;
        // show when onFoucus
        jQuery("#" + bindId).focus(function() {
            var display = self.floatDiv.isDisplay();
            if (display == false) {
                self.showPicker(bindId);
            }
        });
        // hide when blur
        jQuery("#" + bindId).blur(function() {
            setTimeout(function(){ 
                if (self.blockFire == true) {
                    self.blockFire = false;
                    jQuery("#" + bindId).focus();
                    return;
                }
                self.floatDiv.closeFloatDiv();
                jQuery("#" + self.grid["gridDivId"] + "-datePicker").css("display", "none");
                if (typeof self.afterChoose == 'function') {
                    var inputVal = jQuery("#" + self.grid["gridDivId"] + "-datePicker").val();
                    self.afterChoose(bindId, inputVal);
                }
            }, self.delayFire);
        });
    }
    
    static instance (eleId: string, format: string, grid: string) : com_yung_util_GridDatePicker {
        return $Class.getInstance("com.yung.util.GridDatePicker", eleId, format, grid);
    }

}